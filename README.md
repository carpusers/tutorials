# Tutorials to teach usage of CARPentry modeling environment (CME) #

### What is this repository for? ###

* Quick summary
This repository contains a number of tutorials for teaching the basics of using the CME.
Tutorials are provided for most (but so far not all) CARPentry features. 
This examples are intended to transfer basic user knowhow in an efficient way. 
The scripts for building the tutorials are designed as mini-experiments 
which should also serve as a basic building block for building more complex experiments.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

The tutorials rely upon a proper installation of the entire CME including all relevant executables,
carputils and, for single cell tutorials, also limpetPyQtGUI. 
A detailed installation instruction is found online [here](https://carp.medunigraz.at/carputils/cme-installation.html).


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
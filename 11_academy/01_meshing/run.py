#!/usr/bin/env python

r"""
Overview
========

The objectives in this section are to learn the use of :ref:`mesher <mesher>` for:

* Generating of simple slab/wedge-like meshes
* Controling the assignment of fiber and sheet arrangements
* Defining regions by tagging elements

Details on mesher functions are given :ref:`here <mesher>`.

.. _task01-regular-slab:

**Task 01: Build a simple slab mesh**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Use mesher to be build a discrete slab model according to the following specifications:

   .. code-block:: bash

      length (x-direction) : 4.0 cm
      width  (y-direction) : 4.0 cm
      depth  (z-direction) : 0.025 cm
      resolution           : 250 :math:`\mu m`isotropic
   

   Label the mesh using the basename *thinwedge*.

..   Solution:
..     mesher -size "{4. 4. 0.025}" -resolution   "{250 250 250}" -mesh thinwedge
.. 

2. Use meshalyzer and meshtool to verify that the mesh follows the prescribed specifiations

   * Compute the size of the bounding box using :ref:`meshtool <meshtool>`
   * Visualize the fiber orientations with :ref:`meshalyzer <meshalyzer>`


   .. _fig-academy-meshing-task01-fibers:

   .. figure:: /images/11_ac_01_task01_verify.png 
      :width: 50%
      :align: center

      Verification of size and assigned fiber orientation. Shown is meshalyzer 
      with loaded fiber data.


**Task 02: Build a simple slab mesh immersed in a bath**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Use the same specs as in :ref:`Task 01 <task01-regular-slab>`, but immerse the slab in a bath.
   Use the following bath dimensions:

   .. code-block:: bash

      bath (x-direction) : 0.5 cm
      bath (y-direction) : 0.5 cm
      bath (z-direction) : 0.05 cm
      resolutio          : 250 :math:`\mu m`isotropic


   Label the mesh using the basename *thinwedge-bath*.


..  Solution:
..    mesher -size "{4. 4. 0.025}" -resolution   "{250 250 250}" -mesh thinwedge \
..           -bath "{0.5 0.5 0.05}"

  

2. Use meshalyzer and meshtool to verify that the mesh follows the prescribed specifiations

   * Compute the size of the bounding box using :ref:`meshtool <meshtool>`
   * Visualize the mesh using :ref:`meshalyzer <meshalyzer>`, assign different colors to bath and tisue


   # .. meshtool query bbox -msh=./thinwedge


   .. _fig-academy-meshing-task02_bath:

   .. figure:: /images/11_ac_01_task02_bath.png 
      :width: 100%
      :align: center

      Verification of added bath which is only added to :math:`+x, +y, +z`.
      Shown is meshalyzer with colored tags (Tag 0: bath in blue; Tag 1: tissue in red).
      Details on region tags are found in the :ref:`tagging tutorial <region-tagging-tutorial>`
      and the :ref:`region section <region-definition>` of the manual.


**Task 03: Build a simple slab mesh with a central circular region**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.  Use the same specs as in :ref:`Task 01 <task01-regular-slab>`, but add a 
    circular region of radius :math:`r = 1 cm` to the center of the tissue.



**Task 04: Build a simple wedge mesh with rotating fibers**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Use the same specs as in :ref:`Task 01 <task01-regular-slab>`, but increase the transmural depth :math:`z`
   to twice the spatial resolution of the mesh such that two layers of elements are built.

   .. note::

      Tensors are assigned on a per element basis. Thus, to allow for transmural fiber rotation
      at least two element layers are nessecary. 
"""

EXAMPLE_DESCRIPTIVE_NAME = 'Simple meshing'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'

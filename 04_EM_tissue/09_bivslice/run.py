#!/usr/bin/env python
r"""

.. _embivslice:

This example provides pure mechanics and electromechanics examples on a
slice biventricular geometry. The main use of this example is to test
coupling of biventricular setups with a circulatory system.

Problem Setup
=============

This problem generates a slice bi-ventricular mesh using the
:class:`carputils.mesh.BiVSlice` class. The bi-ventricular slice is tessellated
into tetrahedra as shown below:

.. _fig-tutorial-bivslice-mesh:

.. figure:: /images/bivslice_mesh.png
    :width: 45%
    :align: center

    Automatically generated (CGAL-based) mesh of a LV-RV bi-ventricular slice
    model.

In all experiment types in this example, the top and bottom surfaces of the
slice are constrained to lie in the same plane with Dirichlet boundary
conditions, and an additional three nodes on the bottom (:math:`z=0`) surface
are constrained such that free body rotation and translation is prevented.
Two nodes on the x axis are prevented from moving in the y direction, and one
node on the y axis is prevented from moving in the x direction:

.. _fig-tutorial-bivslice-rigid-bc:

.. figure:: /images/bivslice_rigid_bc.png
    :width: 45%
    :align: center

    Dirichlet boundary conditions applied to prevent rigid body motion.

Experiments
===========

Several experiments are defined:

* ``active-free`` - Run an active contraction simulation without constraints on
  cavity size or pressure
* ``active-iso`` - Run an active contraction simulation with an isovolumetric
  cavity constraint
* ``active-pv-loop`` - Run an active contraction stimulation with pressure/flux
  constraints imposed by Windkessel or circulatory models coupled to both
  RV and LV cavity. In this case both left and right ventricular cavities are
  coupled to a 3-element Windkessel model

.. _fig-tutorial-bivslice-wk-coupling:

.. figure:: /images/bivslice_wk_coupling.png
    :width: 70%
    :align: center

    Lumped representation of the PDE bi-ventricular slice model coupled to a
    simplified 0D model of the circulation.
    Prescribed pressure in the left and right atrium
    :math:`p_{\mathrm{LA}}, p_{\mathrm{RA}}` serve to steer *preload*
    and a 3-element Windkessel model serves as *afterload* model. The valves
    are modeled as simple resistors to allow pressure gradients to build up.

Other Arguments
===============

Another key argument is the stress model. The available active stress model is:

* ``TanhStress`` - A very simple active stress model based on activation times
  and constructed with exponential functions which also accounts for
  length-dependent development of active tension
  (see :ref:`TanhStress model <TanhStress-model>` for details).

This active stress model is based on

* ``[1]`` `Niederer et al. (2011). Cardiovascular research, 89(2), 336-343.`_

.. _Niederer et al. (2011). Cardiovascular research, 89(2),
  336-343.: http://doi.org/10.1093/cvr/cvq318

The stress model can be modified with the following arguments:

* ``s_peak`` - Peak stress in kPa (default: 50 kPa)
* ``tau_c`` - Time constant governing rate of rise in active stress model
  (default: 45 ms)
* ``ld_on`` - Turn on length dependence (default: off)

Usage
=====

To run a simple ``active-free``-experiment call

.. code-block:: bash

    ./run.py \
    --experiment active-free  `# experiment to run, possible choices: \
                               #     'active-free', 'active-iso' or 'active-pv-loop'` \
    --duration 180            `# duration of the experiment (default 180 ms)` \
    --s_peak 50               `# Peak stress in kPa (default 50 kPa)` \
    --tau_c 45                `# Time constant governing rate of rise \
                               #     in active stress model (default 45 ms)` \
    --np 10                   `# number of processes` \
    --visualize               `# visualize with meshalyzer`

or call

.. code-block:: bash

    ./run.py \
    --experiment active-pv-loop  `# possible experiments: \
                                  #     'active-free', 'active-iso' or 'active-pv-loop'` \
    --duration 180               `# duration of the experiment (default 180 ms)` \
    --s_peak 50                  `# Peak stress in kPa (default 50 kPa)` \
    --tau_c 45                   `# Time constant governing rate of rise \
                                  #    in active stress model (default 45 ms)` \
    --np 10                      `# number of processes` \
    --visualize                  `# visualize with meshalyzer`

for a simple ``active-pv-loop``-experiment.

Post-processing
===============

The ``active-pv-loop``-experiments output a cavity-information file
(usually called **cav.LV.csv**) which contains pressure information,
volume information, flow rates and many other additional informations.
This cavity-information file can be used for a post-processing analysis using
the following tools.

cavplot
-------

The ``cavplot``-tool is a simple tool for plotting the pressure volume relation.
Call

.. code-block:: bash

    cavplot cav.LV.csv cav.RV.csv --pressure

to plot the pressure-volume-relation, see figure :numref:`fig-ring-cavplot`.
To plot just a single trace use the ``--mode`` flag and one of the options
``combined``, ``pvloop``, ``volume``, ``pressure``, ``flux``, ``pressuredot``,
``fluxdot``, ``q_out``, ``q_in``.
If you want to add the loading phase to your plot use the ``--loading`` flag.

.. _fig-ring-cavplot:

.. figure:: /images/bivslice_cavplot.png
    :width: 500pt
    :align: center

    Pressure plot produced by cavplot. Lines show pressure over time in the
    left and right ventricle and the pressure in the attached outflow
    compartments, respectively.


cavinfo
-------

The ``cavinfo``-tool is an improved version of the cavplot tool.
In addition to plotting pressure-volume data ``cavinfo`` performs a detailed
analysis, computes various metrics and annotates the pressure-volume plots.
Call

.. code-block:: bash

    cavinfo --file cav.RV.csv     `# RV cavity information file` \
            --output cavity.info  `# output file name (default 'cavity.info')`

to plot the pressure-volume-relation and to determine many other quantities
as ESV, EDV, etc., see figure :numref:`fig-ring-cavinfo`.
All the information is stored in the output file and printed to the terminal,
see output below.

.. code-block:: bash

    cavity file         : 2018-09-13_active-pv-loop_fast_P1-P0_pt_np2/cav.RV.csv
    negative flow       : True
    time range          : 0.000 - 180.000 ms
    IVC begin           : 24.000 ms
    ejection begin      : 54.000
    IVR begin           : 135.000 ms
    V0                  : 5.534 ml
    EDV                 : 7.268 ml  ( at 54.000 ms )
    ESV                 : 3.503 ml  ( at 134.000 ms )
    SV                  : 3.765 ml
    EF                  : 51.802 %
    peak flow           : 0.083 ml/ms  ( 83.496 ml/s ) ( 0.083 L/s )  ( 5.010 L/min )
    peak flow time      : 85.000 ms
    open pressure       : 1.849 kPa  ( 13.870 mmHg )
    open pressure time  : 54.000 ms
    peak pressure       : 3.976 kPa  ( 29.826 mmHg )
    peak pressure time  : 92.000 ms
    mean pressure       : 3.543 kPa  ( during ejection )
    hm peak pressure    : 4.306 kPa  ( 32.296 mmHg )
    work                : 13.338 kPa/ml  ( 0.013 J )
    estimated work      : 11.977 kPa/ml  ( 0.012 J )
    external work       : 13.358 kPa/ml  ( 0.013 J )
    contraction time    : 68.000 ms  ( peak pressure time - IVC begin )
    Ea                  : 0.788 kPa/ml
    Ees(E)              : 0.236 kPa/ml

To get the total argument list run ``cavinfo --help``.

.. _fig-ring-cavinfo:

.. figure:: /images/bivslice_cavinfo.png
    :width: 85%
    :align: center

    Pressure-volume plot of the RV as produced by ``cavinfo``.
    Note the additional annotations compared to the plots produced with
    ``cavplot`` shown in :numref:`fig-ring-cavplot`.

.. hint::
    The tool is right now part of the ``pvprocess`` module but this will be
    changed!
"""

import os
from datetime import date

from carputils import settings
from carputils import tools
from carputils import ep
from carputils import model
from carputils.mesh import linear_fibre_rule
from carputils.mesh.bivslice import BiVSlice
from carputils.resources import petsc_options

EXAMPLE_DESCRIPTIVE_NAME = 'BiVSlice'
EXAMPLE_AUTHOR = ('Christoph Augustin <christoph.augustin@medunigraz.at>, '
                  'Gernot Plank <gernot.plank@medunigraz.at>')

# =============================================================================
#    Command-line PARSER
# =============================================================================
def parser_commands():
    """ Setup command line parser.
    """
    parser = tools.standard_parser()
    group = parser.add_argument_group('experiment specific options')

    group.add_argument('--experiment',
                       default='active-free',
                       choices=['active-free',
                                'active-iso',
                                'active-pv-loop'],
                       help='pick experiment type (default: "active-pv-loop")')

    group.add_argument('--duration',
                       default=180.0,
                       help='duration of experiment (default: 180 ms)')

    group.add_argument('--fast',
                       action='store_true',
                       help='Run for speed (smaller mesh, simple material)')

    group.add_argument('--s_peak',
                       type=float, default=50,
                       help='Peak stress in kPa (default: 50 kPa)')

    group.add_argument('--tau_c',
                       type=float, default=45,
                       help='Rate of rise in active stress model \
                             (default: 45 ms)')

    group.add_argument('--ld_on',
                       action='store_true',
                       help='Turn on length dependence in the active stress \
                             model if supported (default: off)')
    return parser

# =============================================================================
#    MAIN
# =============================================================================
def run(args, job):
    """ Main
    """
    # generate mesh
    mesh_name = generate_mesh(args.fast)

    # generate initial command arguments
    cmd = tools.carp_cmd('bivslice.par')

    # define simulation ID and mesh
    cmd += ['-simID', job.ID,
            '-meshname', mesh_name]

    # Setup timer variables
    cmd += setup_timers(args.duration)

    # Setup solver settings
    cmd += setup_solver(args)

    # Setup EP
    cmd += setup_ep(mesh_name)

    # Setup mechanics
    cmd += setup_mech(args, mesh_name)

    # Setup cardiovascular system
    cmd += setup_cardiovascular_system(args, mesh_name)

    # Setup visualization
    cmd += setup_visualization(args)

    # Run main CARP simulation
    job.carp(cmd)

    # Visualize with meshalyzer
    if args.visualize and not settings.platform.BATCH and not args.dry:
        visualize_meshalyzer(job, mesh_name)


# =============================================================================
#    General functions
# =============================================================================

# -----------------------------------------------------------------------------
def generate_mesh(fast):
    """ Generate mesh.
    Returns:
        mesh name
    """

    mesh_dir = 'mesh'
    mesh_name = f'{mesh_dir}/bivslice'
    if not os.path.exists(mesh_dir):
        os.mkdir(mesh_dir)

    # Generate a plus 60 - minus 60 fibre rule
    p60m60 = linear_fibre_rule(60, -60)

    if fast:
        mesh = BiVSlice(25, 30, 10, 5, 5,
                        div_rvtrans=1,
                        div_height=1, div_lvtrans=1,
                        fibre_rule=p60m60)
    else:
        mesh = BiVSlice(25, 30, 10, 5, 5,
                        div_rvtrans=3,
                        div_height=3, div_lvtrans=3,
                        fibre_rule=p60m60)

    mesh.generate_carp(mesh_name, faces=['bottom', 'top', 'lvendo', 'rvendo',
                                         'lvepi', 'rvepi', 'epi'])
    mesh.generate_carp_rigid_dbc(mesh_name)
    mesh.generate_vtk(f'{mesh_name}.vtk')

    return mesh_name

# -----------------------------------------------------------------------------
def setup_timers(duration):
    """Sets the timers for the electro-mechanical simulation.
    Args:
        duration: experiment duration
    Returns:
        List of timer options.
    """
    timer_opts = ['-dt', 10.,           # time step for electrics [us]
                  '-tend', duration,    # length of the simulation [ms]
                  '-timedt', 5.,        # time between temporal output [ms]
                  '-spacedt', 1.,       # time between spacial output [ms]
                  '-mechDT', 0.5]       # time step for mechanics [ms]

    return timer_opts

# -----------------------------------------------------------------------------
def setup_solver(args):
    """ Setup solver options for the simulation

    Returns:
        List of solver options
    """
    # Linear solver options
    lopts = ['-cg_tol_ellip', '1e-8',       # solver tol for elliptic problem
             '-cg_tol_parab', '1e-8',       # solver tol for parabolic problem
             '-krylov_tol_mech', '1e-8',    # solver tol for mechanics problem
             '-cg_norm_ellip', '0',         # 0: energy norm for elliptic solver
             '-cg_norm_parab', '0',         # 0: energy norm for parabolic solver
             '-krylov_norm_mech', '0',      # 0: energy norm for mechanic solver
             '-krylov_maxit_mech', '1000']  # max iterations for mechanics

    lopts += ['-mechanics_options_file', petsc_options.path('gamg_gmres_opts_agg')]

    # Newton options
    nopts = ['-newton_stagnation_detection', '1',  # activate stag detection
             '-newton_adaptive_tol_mech', '0',     # deactivate adaptive tol
             '-newton_tol_mech', '1e-6',           # tolerance
             '-newton_forcing_term', '2',          # forcing term
             '-newton_line_search', '0',           # deactivate line-search
             '-newton_maxit_mech', '30']           # max iterations

    if args.experiment in ['active-pv-loop', 'active-iso']:
        nopts += ['-loadStepping', 16]
    else:
        if args.mech_element != 'P1-P1-bubble':
            nopts += ['-newton_adaptive_tol_mech', '1'] # activate adaptive tol

    return lopts + nopts

# =============================================================================
#    EP FUNCTIONS
# =============================================================================

# -----------------------------------------------------------------------------
def setup_ep(mesh_name):
    """ Setup electrophysiology
    Args:
        mesh_name: mesh name
    Returns:
        List of EP options
    """
    # ionic model setup
    num_imp_regions = 1 # number of ionic model regions
    imp_settings = ['-num_imp_regions', num_imp_regions,
                    '-imp_region[0].name', 'BiV',
                    '-imp_region[0].im', 'TT2']

    # conductivity region setup: we use default settings for the whole domain
    g_settings = []

    # stimuli
    start_t_lv = 0.
    start_t_rv = 0.
    num_stims, stim_options = setup_stimuli(start_t_lv, start_t_rv, mesh_name)

    # propagation in R-E mode: we use default settings for eikonal setup
    ek_settings = []

    ek_stim, num_stims = ep.fake_ep_opts(num_stims, '', None)
    stim_options += ek_stim

    ep_options = imp_settings + g_settings + ek_settings \
               + ['-num_stim', num_stims] + stim_options

    return ep_options

# -----------------------------------------------------------------------------
def setup_stimuli(start_t_lv, start_t_rv, mesh_name):
    """ Setup stimulus.
    Args:
        start_t_lv: start of stimulus in left ventricle
        start_t_rv: start of stimulus in right ventricle
        mesh_name: mesh name
    Returns:
        List of stimulus options
    """

    # Generate electrical trigger options
    stim_opts = []
    num_stimuli = 0      # initialize number of stimuli
    stim_type = 0        # 0: transmembrane current
    strength_lv = 20.0   # stimulus strength in left ventricle in mV
    strength_rv = 20.0   # stimulus strength in right ventricle in mV
    stim_duration = 2.0  # stimulus duration in ms

    # define stimulus
    lv_endo = f'./{mesh_name}_lvendo'      # left ventricle surface
    rv_endo = f'./{mesh_name}_rvendo'      # right ventricle surface

    # set up LV electrode
    stim_opts = ['-stimulus[0].vtx_file', lv_endo,
                 '-stimulus[0].stimtype', stim_type,
                 '-stimulus[0].start', start_t_lv,
                 '-stimulus[0].strength', strength_lv,
                 '-stimulus[0].duration', stim_duration,
                 '-stimulus[1].vtx_file', rv_endo,
                 '-stimulus[1].stimtype', stim_type,
                 '-stimulus[1].start', start_t_rv,
                 '-stimulus[1].strength', strength_rv,
                 '-stimulus[1].duration', stim_duration]

    num_stimuli = 2

    return num_stimuli, stim_opts


# =============================================================================
#    Mechanics FUNCTIONS
# =============================================================================

# -----------------------------------------------------------------------------
def setup_mech(args, mesh_name):
    """ Setup generic mechanical settings and materials.
    Args:
        args: parser arguments
        mesh_name: mesh name
    Returns:
        List of mechanics options
    """
    # set bulk modulus kappa
    kappa = 500 if args.mech_element == 'P1-P0' else 1e10

    # Set passive material properties
    mat = model.mechanics.GuccioneMaterial([1], 'BiV', a=1.5, b_f=18.480,
                                           b_fs=1.627, b_t=3.580, kappa=kappa)

    pass_mat = model.optionlist([mat])

    # Set active material parameters
    active_stress_params = set_tanh_stress_params(args.s_peak, args.tau_c,
                                                  args.ld_on)

    act_mat = ['-imp_region[0].plugins', 'TanhStress',
               '-imp_region[0].plug_param', active_stress_params]

    # allow some contractile force acting in the sheet direction
    act_mat += ['-active_stress_percentage_s', '0.2',
                '-active_stress_percentage_n', '0.05']

    mech_opts = pass_mat + act_mat

    # length dependence
    if args.ld_on:
        mech_opts += ['-mech_lambda_upd', 2]

    # Dirichlet Boundary conditions
    mech_opts += setup_dbc(mesh_name)

    # Neumann boundary conditions
    mech_opts += setup_nbc(mesh_name)

    return mech_opts

# -----------------------------------------------------------------------------
def setup_dbc(mesh_name):
    """ setup Dirichlet boundary conditions

    Fix geometry or prescribe a given displacement on part of the boundary
    :math:`Gamma_D > 0`.
    Dirichlet BC are essential for the existence of a solution.

    Args:
        mesh_name: mesh name
    Returns:
        List of Dirichlet BC options
    """

    bc_tpl = mesh_name + '_{}'

    dbc_name_bottom = bc_tpl.format('bottom')
    dbc_name_top = bc_tpl.format('top')
    dbc_name_xaxis = bc_tpl.format('xaxis')
    dbc_name_yaxis = bc_tpl.format('yaxis')

    # Dirichlet Boundary Conditions
    num_dbc = 4

    dbc_opts = ['-num_mechanic_dbc', num_dbc]

    # Fix bottom to xy plane
    dbc_opts += ['-mechanic_dbc[0].name', 'bottom_plane',
                 '-mechanic_dbc[0].bctype', 0,
                 '-mechanic_dbc[0].vtx_file', dbc_name_bottom,
                 '-mechanic_dbc[0].apply_ux', 0,
                 '-mechanic_dbc[0].apply_uy', 0,
                 '-mechanic_dbc[0].apply_uz', 1,
                 '-mechanic_dbc[0].dump_nodes', 0]

    # Fix top to xy plane
    dbc_opts += ['-mechanic_dbc[1].name', 'top_plane',
                 '-mechanic_dbc[1].bctype', 0,
                 '-mechanic_dbc[1].vtx_file', dbc_name_top,
                 '-mechanic_dbc[1].apply_ux', 0,
                 '-mechanic_dbc[1].apply_uy', 0,
                 '-mechanic_dbc[1].apply_uz', 1,
                 '-mechanic_dbc[1].dump_nodes', 0]

    # Prevent free rotation and translation
    dbc_opts += ['-mechanic_dbc[2].name', 'xaxis_fix',
                 '-mechanic_dbc[2].bctype', 0,
                 '-mechanic_dbc[2].vtx_file', dbc_name_xaxis,
                 '-mechanic_dbc[2].apply_ux', 0,
                 '-mechanic_dbc[2].apply_uy', 1,
                 '-mechanic_dbc[2].apply_uz', 1,
                 '-mechanic_dbc[2].dump_nodes', 0]

    dbc_opts += ['-mechanic_dbc[3].name', 'yaxis_fix',
                 '-mechanic_dbc[3].bctype', 0,
                 '-mechanic_dbc[3].vtx_file', dbc_name_yaxis,
                 '-mechanic_dbc[3].apply_ux', 1,
                 '-mechanic_dbc[3].apply_uy', 0,
                 '-mechanic_dbc[3].apply_uz', 1,
                 '-mechanic_dbc[3].dump_nodes', 0]

    return dbc_opts

# -----------------------------------------------------------------------------
def setup_nbc(mesh_name):
    """ Setup Neumann boundary conditions.

    Prescribe pressure or traction to part of the boundary Gamma_N.

    Args:
        mesh_name: mesh name
    Returns:
        List of Neumann BC options
    """

    bc_tpl = mesh_name + '_{}'
    nbc_lv_name = bc_tpl.format('lvendo')
    nbc_rv_name = bc_tpl.format('rvendo')

    # Neumann Boundary Conditions
    num_nbc = 2
    nbc_opts = ['-num_mechanic_nbc', num_nbc,
                '-mechanic_nbc[0].name', 'lv_endo',
                '-mechanic_nbc[0].pressure', 0.0,
                '-mechanic_nbc[0].surf_file', nbc_lv_name,
                '-mechanic_nbc[1].name', 'rv_endo',
                '-mechanic_nbc[1].pressure', 0.0,
                '-mechanic_nbc[1].surf_file', nbc_rv_name]

    return nbc_opts

# =============================================================================
#    Active stress functions
# =============================================================================

# -----------------------------------------------------------------------------
def set_tanh_stress_params(s_peak, tau_c, length_dep):
    """ Set parameters for the Tanh active stress model.

    See Niederer et al 2011 Cardiovascular Research 89
    and the thesis of Andrew Crozier

    Length dependence is regulated with 'ld' and 'ld_on'
    larger ld means steeper dependence,
    depending on lambda more or less heterogeneity

    Args:
        s_peak: peak magnitude of stress transient
        tau_c: time constant contraction
        length_dep: length dependence
    Returns:
        parameter settings for the Tanh active stress model
    """

    # current settings  #   default values (see TanhStress.c in LIMPET)
    # ----------------- # ----------------------------------------------------
    t_emd = 15.0        #  15.0 ms  electro-mechanical delay (t_delay)
    s_peak0 = s_peak    # 100.0 kPa peak isometric tension
    tau_c0 = tau_c      #  40.0 ms  time constant contraction (t_r)
    tau_r = 25.0        # 110.0 ms  time constant relaxation (t_d)
    t_dur = 140.0       # 550.0 ms  duration of transient (t_max)
    lambda_0 = 0.6      #   0.7 -   sarcomere length ratio (a_7)
    len_dep = 25.0      #   5.0 -   degree of length dependence (a_6)
    ld_up = 500.0       # 500.0 ms  length dependence of upstroke time (a_4)
    ld_on = length_dep  #   0/1 -   turn on/off length dependence for this case
    vm_thresh = -60.0   # -60.0 mV  threshold Vm for deriving LAT

    tpl = 't_emd={},Tpeak={},tau_c0={},tau_r={},t_dur={},lambda_0={},' + \
          'ld={},ld_up={},ldOn={},VmThresh={}'
    return tpl.format(t_emd, s_peak0, tau_c0, tau_r, t_dur, lambda_0, len_dep,
                      ld_up, ld_on, vm_thresh)

# =============================================================================
#    Cardiovascular system functions
# =============================================================================

# -----------------------------------------------------------------------------
def setup_cardiovascular_system(args, mesh_name):
    """ Setup cardiovascular system.

    Args:
        args: parser arguments
        mesh_name: mesh name
    Returns:
        A list of cardiovascular system options cv_opts.
    """

    # setup cavity volumes
    num_cavs = 2
    cv_opts = setup_cavity_volumes(mesh_name, num_cavs)

    # in the active-free case we don't apply any pressure in the cavity
    # but we want to keep track of the cavity volume
    if args.experiment in ['active-free']:
        return cv_opts

    # link volume and pressure boundaries to define cavity
    cv_opts += cv_sys_parameters(args, num_cavs)

    return cv_opts

# -----------------------------------------------------------------------------
def setup_cavity_volumes(mesh_name, num_cavs):
    """ Setup computation of cavity volumes.

    Computation of volume using the surface enclosing the volume and
    vector field f(x,y,z)
    the openings of this geometry are in the x-y-plane, hence we can use
    f(x,y,z) = x

    Args:
        mesh_name: mesh name
        num_cavs: number of cavities

    Returns:
        A list of cavity volume options.
    """
    bc_tpl = mesh_name + '_{}'
    nbc_lv_name = bc_tpl.format('lvendo')
    nbc_rv_name = bc_tpl.format('rvendo')

    grid_id = model.mechanics.grid_id()  # mechanics grid

    lv_vol_opts = ['-surfVols[0].name', 'lv_endo',
                   '-surfVols[0].surf_file', nbc_lv_name,
                   '-surfVols[0].grid', grid_id,
                   '-surfVols[0].vecf_x', 1.0,
                   '-surfVols[0].vecf_y', 0.0,
                   '-surfVols[0].vecf_z', 0.0]

    rv_vol_opts = ['-surfVols[1].name', 'rv_endo',
                   '-surfVols[1].surf_file', nbc_rv_name,
                   '-surfVols[1].grid', grid_id,
                   '-surfVols[1].vecf_x', 1.0,
                   '-surfVols[1].vecf_y', 0.0,
                   '-surfVols[1].vecf_z', 0.0]

    biv_vol_opts = ['-numSurfVols', num_cavs] + rv_vol_opts + lv_vol_opts

    # element volumes
    vol_tracking = 1  # 1: track myocardial volume changes due to mech deform
    num_elem_vols = 1
    elem_vol_opts = ['-numElemVols', num_elem_vols,
                     '-elemVols[0].name', 'BiV',
                     '-elemVols[0].numtags', 0,
                     '-elemVols[0].grid', grid_id,
                     '-volumeTracking', vol_tracking]

    return biv_vol_opts + elem_vol_opts

# -----------------------------------------------------------------------------
def cv_sys_parameters(args, num_cavs):
    """ Set cardiovascular system parameters.
    Args:
        args: parser arguments
        num_cavs: number of cavities
    Returns:
        A list of the cardiovascular system parameters.
    """
    # set defaults for active-iso
    cav_state = 0  # 0: isovolumetric, valves closed
    cv_param = []  # default attached cvsys options

    if args.experiment == 'active-pv-loop':
        cav_state = -1   # -1: state of cavity managed automatically

        wk3_ao = ['-lv_wk3.R1', '0.168',
                  '-lv_wk3.R2', '4.13',
                  '-lv_wk3.C', '0.11',
                  '-lv_wk3.name', 'Aorta']

        wk3_pa = ['-rv_wk3.R1', '0.125',
                  '-rv_wk3.R2', '1.6',
                  '-rv_wk3.C', '0.33',
                  '-rv_wk3.name', 'PulmArtery']

        # set valve parameters
        wk3_ao += ['-aortic_valve.Rfwd', 0.01,
                   '-aortic_valve.type', 0]

        wk3_ao += ['-mitral_valve.Rfwd', 0.50,
                   '-mitral_valve.type', 0]

        wk3_pa += ['-pulmonic_valve.Rfwd', 0.01,
                   '-pulmonic_valve.type', 0]

        wk3_pa += ['-tricuspid_valve.Rfwd', 0.20,
                   '-tricuspid_valve.type', 0]

        cv_param += wk3_ao + wk3_pa
        cv_param += ['-CV_coupling', 0,         # coupling using volumes
                     '-CV_FE_coupling', 1,
                     '-CVS_mode', 0]            # standard CV system (Windkessel)

    lv_cav = ['-cavities[0].cav_type', 0,       # 0 LV 1 RV 2 LA 3 RA
              '-cavities[0].cavP', 0,           # link to nbc
              '-cavities[0].cavVol', 0,         # link to volume
              '-cavities[0].p0_cav', 10.0,      # define initial pressure
              '-cavities[0].p0_in', 10.0,
              '-cavities[0].p0_out', 80.0,
              '-cavities[0].state', cav_state]

    rv_cav = ['-cavities[1].cav_type', 1,       # 0 LV 1 RV 2 LA 3 RA
              '-cavities[1].cavP', 1,           # link to nbc
              '-cavities[1].cavVol', 1,         # link to volume
              '-cavities[1].p0_cav', 3.0,       # define initial pressure
              '-cavities[1].p0_in', 3.0,
              '-cavities[1].p0_out', 15.0,
              '-cavities[1].state', cav_state]

    biv_cavs = ['-num_cavities', num_cavs] + lv_cav + rv_cav

    return cv_param + biv_cavs

# =============================================================================
#    Visualization functions
# =============================================================================

# ----------------------------------------------------------------------------
def setup_visualization(visualize):
    """ Setup visualization options.
    Args:
        visualize: (true) include output of variables
    Returns:
        A list of visualization options.
    """
    vis_opts = []
    if visualize:
        vis_opts = ['-mech_output', 1+2,       # 1: igb, 2: vtk
                    '-vtk_output_mode', 1,     # 1: legacy vtk (ascii)
                    '-mech_output_domain', 1,  # 1: volumetric domain
                    '-gzip_data', 0,           # 0: do not zip data
                    '-gridout_i', 3]           # 3: full grid output

        # specify output variables
        vis_opts += ['-stress_value', 4+16,  # 4: 1st principal, 16: full tensor
                     '-strain_value', 1,     # 1: fiber strain on nodes
                     '-work_value', 1]       # 1: work on nodes
    return vis_opts

# -----------------------------------------------------------------------------
def visualize_meshalyzer(job, mesh_name):
    """ visualize with Meshalyzer

    Args:
        job: job definition
        mesh_name: mesh name
    """
    # split off mesh basename
    vis_mesh = f'{os.path.basename(mesh_name)}_i'
    geom = os.path.join(job.ID, vis_mesh)
    data = os.path.join(job.ID, 'firstPrincipalStress.igb')
    deform = os.path.join(job.ID, 'x.dynpt')
    job.gunzip(deform)
    job.meshalyzer(geom, data, deform)

# -----------------------------------------------------------------------------
def setup_job_id(args):
    """ Generate name of top level output directory.
    """
    today = date.today()
    subtest = args.experiment
    if args.fast:
        subtest += '_fast'

    tpl = '{}_{}_{}_{}_np{}'

    return tpl.format(today.isoformat(), subtest,
                      args.mech_element, args.flv, args.np)

# =============================================================================
if __name__ == '__main__':
    # tear down decorator function and assign it to constant function RUN
    RUN = tools.carpexample(parser_commands, setup_job_id)(run)
    RUN()

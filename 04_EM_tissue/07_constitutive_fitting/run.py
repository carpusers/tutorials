#!/usr/bin/env python
r"""

.. _constitutivefit:

Fitting of a passive mechanical material model to the Klotz end-diastolic pressure-volume relation (EDPVR).

**Klotz Relation** 
******************

As clinical data of the EDPVR is limited the computational method proposed by [Klotz2007]_ is used, which enables 
prediction of the EDPVR by a single measured volume-pressure pair. 

According to Klotz et al. the stress-free volume :math:`V_0`  of the left ventricle (LV) can be determined empirically by

.. math::
    :label: equ-const-v0
    
    {V}_{0} = {V}_{m}(0.6 - 0.006{P}_{m}),

where :math:`{V}_{m}` and :math:`{P}_{m}` is a measured volume-pressure pair at end diastole. 
Further, they showed that the EDPVR can be described by the following power law

.. math::
    :label: equ-const-powerlaw

    P = \alpha {V}^{\beta}, 

where :math:`P` is cavity pressure in mmHg, :math:`V` is cavity volume in ml, and  the constants :math:`\alpha` and 
:math:`\beta` can be defined by the relations

.. math::
    :label: equ-const-alpha
    
    \alpha = \frac{30}{V_{30}^\beta} \qquad \text{ and } \qquad \beta = \frac{\log\left(\tfrac{P_m}{30}\right)}{\log\left(\tfrac{V_m}{V_{30}}\right)}.

    
:math:`V_{30}` is the cavity volume at a pressure of 30 mmHg, given by

.. math::
    :label: equ-const-v30

    {V}_{30} = {V}_{0} + \frac{V - {V}_{0}}{\left(\tfrac{P_m}{A_{n}}\right)^{1/B_n}},

where :math:`{A}_{n}` and :math:`{B}_{n}` were determined empirically as 27.78 mmHg and 2.76 respectively.

To avoid a singularity in equation :eq:`equ-const-alpha` :math:`\alpha` and :math:`\beta` 
were reposed to 

.. math::
    :label: equ-const-alpha2
    
    \alpha = \frac{P_m}{V_m^\beta} \qquad \text{ and } \qquad \beta = \frac{\log\left(\tfrac{P_m}{15}\right)}
                 {\log\left(\tfrac{V_m}{V_{15}}\right)}

for :math:`{P}_{m}` greater than 22 mmHg. :math:`V_{15}` is determined analytically as

.. math::
    :label: equ-const-v15
    
    V_{15} = 0.8 (V_{30} - V_0) + V_0.

**Constitutive Model** 
**********************

As the myocardium is treated as nearly incompressible the strain energy function  :math:`\Psi`,
see equation :eq:`equ-const-strain`, is split into its decoupled form consisting of 
a volumetric :math:`U(J)` (volume changing) and isochoric part :math:`\Psi_{iso}` (volume preserving). 
:math:`U(J)` is composed of the bulk modulus :math:`\kappa` and a penalty term related to the Jacobian J,
see equation :eq:`equ-const-vol`. Thus, volume change can be penalized and compressibility can be controlled. 


.. math::
    :label: equ-const-strain

    \Psi = U(J) + \Psi_{iso}
    
.. math::
    :label: equ-const-vol

    U(J) = \kappa \Phi_{vol}

The isochoric strain-energy function is composed of the transversely isotropic constitutive model of Guccione et al. 
[Guccione1991]_ consisting of four material parameters, see equation :eq:`equ-const-gucci`.

.. math::
    :label: equ-const-gucci

    \Psi_{iso} & = \frac{1}{2}C_{1}(e^{Q} - 1) \\
    Q & = b_{f}E_{ff}^2 + 2b_{ft}(E_{fs}^2 + E_{fn}^2) + b_{t}(E_{ss}^2 + E_{nn}^2 + 2E_{sn}^2)

:math:`C_{1}`, :math:`b_{f}`, :math:`b_{ft}` and :math:`b_{t}` are material parameters accounting for the different 
mechanical behavior of the tissue along the fibres (f), across the transverse planes (t) and the fibre-transverse shear 
planes. :math:`E_{ff}`, :math:`E_{fs}`, :math:`E_{fn}`, :math:`E_{ss}`, :math:`E_{nn}` and :math:`E_{sn}` are 
projections of the Green-Lagrange strain tensor in fibre (f), sheet(s) and sheet normal (n) direction.


**Fitting** 
***********

Fitting is done by matching cavity volume of the LV in the stress free configuration, approximated during unloading, 
and EDPVR with those of the Klotz relation (see :numref:`fig-constfit-fit`). :math:`\kappa` is set to 650 and 
for the material parameters :math:`b_{f}`, :math:`b_{ft}` and :math:`b_{t}` the default values 1.627, 3.580 and 18.480 
found by [Guccione1991]_ are used respectively. The parameter C is then varied to achieve the best fit to the Klotz 
Relation.

.. _fig-constfit-fit:
    
.. figure:: /images/Constitutive_fitting.png
   :width: 650pt
   :align: center
   
   Klotz end-diastolic pressure-volume relation (EDPVR) (red dashed) and fitted EDPVR with varying paramter C (blue,
   yellow, green, red)
   
*Example for passive inflation and cavity plot*
===============================================

A simple ring model, assumed to be in stress free configuration, is passively inflated by running the following command 
in the directory tutorial/inflate:

.. code-block:: bash

   ./run.py --meshtype ring --pressure 2.0 --fast
   
where **meshtype** indicates the type of mesh chosen, **pressure** the maximum pressure to inflate to in kPa and **fast** sets
the resolution and material to fast-solving values. 

Switching to the generated file, containing the data files **endo.nbc_p.dat** and **endo.vol.dat** with obtained pressure
and volume traces, and calling 

.. code-block:: bash

   cavplot endo.nbc_p.dat endo.vol.dat --klotz --pvloop

generates a plot (see :numref:`fig-constfit-fit-ring`) containing the Klotz EDPVR (**klotz**) and the pressure-volume trace 
from the data files (**pvloop**).

.. _fig-constfit-fit-ring:
    
.. figure:: /images/Constitutive_fitting_ring.png
   :width: 650pt
   :align: center

   Klotz end-diastolic pressure-volume relation (EDPVR) (red dashed) and pressure-volume trace recorded during passive
   inflation (blue).


.. rubric:: References

.. [Klotz2007] *Klotz, S. and Dickstein, M.L. and Burkhoff, D.*,
                    **A computational method of prediction of the end-diastolic pressure-volume relationship by single beat (2007)**, 
                    Nature Protocols, Volume 2
.. [Guccione1991] *Guccione, J.L. and McCulloch, A.D. and Waldman, L.K.*,
                        **Passive material properties of intact ventricular myocardium determined from a cylindrical model (1991)**,
                        J Biomech Eng, Volume 113
                   
"""


EXAMPLE_DESCRIPTIVE_NAME = 'Constitutive Fitting'
EXAMPLE_AUTHOR = ('Laura Marx <laura.marx@medunigraz.at>')

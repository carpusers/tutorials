-84.9401            # Vm
1                   # Lambda
0                   # delLambda
0                   # Tension
-                   # Stiffness
-                   # K_e
-                   # Na_e
-                   # Ca_e
0.00336177          # Iion
0                   # dTdl
TT2
0.140619            # Ca_i
4.04628             # CaSR
0.000394139         # CaSS
0.904537            # R_
2.97407e-07         # O
9.05546             # Na_i
136.011             # K_i
0.00183982          # M
0.735036            # H
0.662625            # J
0.0176087           # Xr1
0.467752            # Xr2
0.0139858           # Xs
2.55516e-08         # R
0.999995            # S
3.52387e-05         # D
0.733413            # F
0.96064             # F2
0.99552             # FCaSS

TanhStress
-84.9401            # VmPrev
-1                  # trel
49500.9             # tact
50000               # tCur


#!/usr/bin/env python

r"""

.. _em-tissue-coupling-tutorial:

Electro-mechanical coupling at the tissue scale
===============================================

This tutorial elucidates the setting up of electro-mechanically coupled tisue simulations.
As an example we use a simpliefied model of a left ventricular wedge preparation.
For the sake of computational efficiency the apico-basal and circumferential dimension of the wedge
are chosen by default to be small and the resolution of the model is coarse.
As in the matching :ref:`single cell stretcher <single-cell-EM-tutorial>` experiment
the same EP models (TT2, GPB) and stress models
(:ref:`TanhStress <TanhStress-model>`,
:ref:`LandStress <LandStress-model>`,
:ref:`GPB-LandStress-model <GPB-LandStress-model>`)
are available which cover all three coupling modes
(:ref:`activation-time based ECC <activation-time-ecc>`,
:ref:`weak calcium-driven ECC <calcium-weak-ecc>`
and :ref:`strong calcium-driven ECC <calcium-strong-ecc>`).
The differences between coupling modes are illustrated in :numref:`fig-ECC-physiology`.

Experimental setup
------------------

.. figure:: /images/ECC_wedge_experiment.png
    :name: fig-ECC-wedge
    :align: center
    :width: 40%

    The setup uses a simple LV wedge preparation where the apicobasal and
    circumferential dimension can be chosen by setting the
    ``--length`` and ``--width`` input parameters.
    The wedge is mechanically fixated through homogeneous Dirichlet boundary
    conditions at the apical face of the wedge.
    At the anterior and posterior faces homogeneous Dirichlet boundary
    conditions are also applied, preventing any displacement in radial
    (z-axis) and circumferential (x-axis) direction,
    but allowing the wedge to slide along the apico-basal (y-axis) direction.
    Fiber rotation is set to standard values with -60 degrees at the
    epicardial face and +60 degrees at the endocardial face.
    The wedge is electrically stimulated epicardially at the center of the
    preparation.


Input parameters
----------------

To run the examples of this tutorial do

.. code-block:: bash

   cd ${TUTORIALS}/04_EM>_tissue/04_EM_coupling

To inquire the exposed experimental input parameters run

.. code-block:: bash

   ./run.py --help

which yields the following options:

.. code-block:: bash

   --EP {TT2,GPB}        pick human EP model (default is TT2)

   --Stress {LandStress,TanhStress}

                        pick stress model (default is TanhStress)

   --width WIDTH        choose circumferential width of wedge in mm

   --resolution RESOLUTION

                        choose mesh resolution in mm (default is 2.0 mm)
   --duration DURATION  duration of simulation (ms) (default: 500.0 ms)

   --mechanics-off      switch off mechanics to generate activation vectors



Expected results
----------------

Simulation results computed at a higher spatial resolution of
:math:`h=250 \mu m` are shown in the following for a full
contraction-relaxation cycle.
Specifically, the activation sequence is shown in terms of all relevant
signals involved in active force generation in :numref:`fig-ecc-wedge`.

.. figure:: /images/ECC_wedge.gif
    :name: fig-ecc-wedge
    :align: center

    Shown are signals involved in the
    :ref:`excitation-contraction coupling cascade <ecc>`.
    A stimulus is delivered at the center of the epicardial surface to
    initiate the propagation of action potentials.
    The leftmost panels shows transmebrane voltages
    :math:`V_{\rm m}(\mathbf{x},t)` (blue: -90 mV, red: +50 mV).
    Propagating action potentials initiate Calcium transients which drive the
    generation of active stress.
    The mid-left panel shows the intracellular Calcium transients
    :math:`[Ca_{\rm i}(\mathbf{x},t)`
    (blue: 0. :math:`\mu mol`, red: 1.2 :math:`\mu mol`).
    Due to fiber rotation a significant heterogeneity in fiber stretch,
    :math:`\lambda`, is wittnessed,
    which is shown in the mid-right panel
    (blue: 0.7, red: 1.3).
    The rightmost panel shows active tension
    :math:`\sigma_{\rm a}(\mathbf{x},t)`
    (black: 0.0 kPa, red: 40 kPa).


.. _exp-em-ti-ecc-01:

**Experiment exp01 (setting up activation-time based tension model)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In a first step we solve only the EP problem with the active stress plug-in
enabled to verify that activation sequence and force generation are working
properly.
Solving of the mechanics problems is turned off by adding the
``--mechanics-off`` flag,
thus fiber stretch remains constant at
:math:`\lambda=1.0` for the entire simulation.
Tension therefore is not modulated by length-dependence.
The ``--visualize`` option shows transmembrane voltage :math:`V_{\rm m}`,
active stress :math:`\sigma_{\mathrm a}` and fiber stretch :math:`\lambda`.

.. code-block:: bash

   ./run.py --EP TT2 --Stress TanhStress --ID exp-em-ti-01 --mechanics-off --visualize


.. _exp-em-ti-ecc-02:

**Experiment exp02 (contraction with activation-time based tension model)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Omitting the ``--mechanics-off`` flag turns on the computation of deformation.
Using the default settings for ``--length``, ``--width`` and ``--resolution``
leads to a fairly coarse discretization, but allows for sufficiently short
simulation cycles.
EP in this simulation is driven by a reaction-eikonal approach
(see Neic et al [#neic_re]_ for details)
which yields undistorted propagation patterns even on coarse meshes.
For the sake of saving compute time we use a resolution of 4 mm.

.. code-block:: bash

   ./run.py --EP TT2 --Stress TanhStress --resolution 4 --ID exp-em-ti-02 --visualize


.. _exp-em-ti-ecc-03:

**Experiment exp03 (setting up Calcium-driven tension model)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As before in experiment exp01, we solve only the EP problem with the active stress plug-in enabled
to verify that activation sequence and force generation are working as expected.
As we are using a Calcium-driven active stress model the ``--visualize``
option shows also the Calcium signals which serve as input for driving the
active stress model.

.. code-block:: bash

   ./run.py --EP TT2 --Stress LandStress --ID exp-em-ti-03 --mechanics-off --visualize


.. _exp-em-ti-ecc-04:

**Experiment exp04 (contraction with Calcium-driven tension model)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We repeat experiment exp03 with deformation enabled now.
Due to the weak coupling approach used the Calcium signals remain unaltered
as compared to experiment exp03 without deformation (i.e. :math:`\lambda=1.0`).

.. code-block:: bash

   ./run.py --EP TT2 --Stress LandStress --resolution 4 --ID exp-em-ti-04 --visualize


.. _exp-em-ti-ecc-05:

**Experiment exp05 (setting up strongly coupled Calcium-driven tension model)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This experiment repeats exp03, but with strong coupling
For the sake of saving compute time only 80 ms activity are simulated
to observe the onset of contraction.

.. code-block:: bash

   ./run.py --EP GPB --Stress LandStress --mechanics-off --ID exp-em-ti-05 --visualize


.. _exp-em-ti-ecc-06:

**Experiment exp06 (contraction with strongly coupled Calcium-driven tension model)**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

   ./run.py --EP GPB --Stress LandStress --resolution 4 --ID exp-em-ti-06 --visualize


Literature
==========

.. [#neic_re] Neic A, Campos FO, Prassl AJ, Niederer SA, Bishop MJ, Vigmond EJ, Plank G.
   **Efficient compuation of electrograms and ECGs in human whole heart
   simulations using a reaction-eikonal model.**
   J Comput Phys. 346:191-217, 2017.
   `[Online] <http://www.sciencedirect.com/science/article/pii/S0021999117304655>`__

"""

import os
import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import model
from carputils import ep
from carputils.resources import petsc_options

EXAMPLE_DESCRIPTIVE_NAME = "Electro-mechanical coupling"
EXAMPLE_AUTHOR = ("Gernot Plank <gernot.plank@medunigraz.at> and "
                  "Christoph Augustin <christoph.augustin@medunigraz.at")

def parser_cmd():
    """ setup command line parser """
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--ep-model',
                         default='monodomain',
                         choices=ep.MODEL_TYPES,
                         help='pick electrical model type')

    group.add_argument('--EP',  default='TT2',
                        choices=['TT2','GPB'],
                        help='pick human EP model')

    group.add_argument('--Stress', default='TanhStress',
                        choices=['LandStress','LandHumanStress','TanhStress'],
                        help='pick stress model')

    group.add_argument('--length',
                        type=float, default=60.,
                        help='choose apicobasal length of wedge in mm')

    group.add_argument('--width',
                        type=float, default=20.,
                        help='choose circumferential width of wedge in mm')

    group.add_argument('--resolution',
                        type=float, default=2.0,
                        help='choose mesh resolution in mm')

    group.add_argument('--duration',
                        type=float, default=450.,
                        help='duration of simulation (ms)')

    group.add_argument('--mechanics-off',
                        action='store_true',
                        help='switch off mechanics to generate activation vectors')

    return parser


def job_id(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return f'{today.isoformat()}_{args.EP}_{args.Stress}_ab_{args.length}_cw_{args.width}'


@tools.carpexample(parser_cmd, job_id)
def run(args, job):
    """ main run function """

    # --- start building command line-----------------------------------------
    cmd  = tools.carp_cmd('em_coupling.par')
    cmd +=['-simID', job.ID,
            '-tend', args.duration]

    # --- Generate mesh ------------------------------------------------------
    meshname, geom, wedge_tags = build_wedge(args)
    cmd += ['-meshname', meshname]

    # --- set up mechanics materials------------------------------------------
    wedge = setup_mech_mat(wedge_tags)

    # --- define stimuli -----------------------------------------------------
    cmd += setup_stimuli(geom)

    # --- define type of source model ---------------------------------------
    cmd += ep.model_type_opts(args.ep_model)

    # --- Define boundary conditions ----------------------------------------
    cmd += setup_dbc(geom)

    # --- solve mechanics ---------------------------------------------------
    cmd += ['-mechDT', 1.0 * (not args.mechanics_off)]

    # Add material options
    cmd += model.optionlist([wedge])

    # --- setup EP regions --------------------------------------------------
    imps, gregs, ekregs = setup_ep(wedge_tags)
    cmd += imps + gregs + ekregs


    # --- active stress setting ---------------------------------------------
    active_imp = 0  # define active imp region
    cmd += setup_active(args.Stress, args.EP, active_imp)

    # --- electromechanical coupling ----------------------------------------
    cmd += setup_em_coupling('weak')

    # --- configure solvers -------------------------------------------------
    cmd += configure_solvers()

    if args.visualize:
        cmd += ['-gridout_i',    3,
                '-stress_value', 4]

    # --- Run simulation -----------------------------------------------------
    job.carp(cmd)

    # --- Do meshalyzer visualization ----------------------------------------
    if args.visualize and not args.dry and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')

        if args.mechanics_off:
            # view transmembrane voltage
            view = 'wedge_vm.mshz'
            data = os.path.join(job.ID, 'vm.igb.gz')
            job.meshalyzer(geom, data, view)

            # view fiber stretch
            view = 'wedge_lambda.mshz'
            data = os.path.join(job.ID, 'Lambda.igb.gz')
            job.meshalyzer(geom, data, view)

            # if calcium-driven, show Cai transients
            if args.Stress != 'TanhStress':
                view = 'wedge_Cai.mshz'
                data = os.path.join(job.ID, 'Ca_i.igb.gz')
                job.meshalyzer(geom, data, view)

            # view tension
            view = 'wedge_tension.mshz'
            data = os.path.join(job.ID, 'Tension.igb.gz')
            job.meshalyzer(geom, data, view)

        else:
            # deformation data
            deform = os.path.join(job.ID, 'x.dynpt')
            job.gunzip(deform)

            # view lambda first
            view = 'wedge_lambda.mshz'
            data = os.path.join(job.ID, 'Lambda.igb.gz')
            job.meshalyzer(geom, data, deform, view)

            # if calcium-driven, show Cai transients
            if args.Stress != 'TanhStress':
                view = 'wedge_Cai.mshz'
                data = os.path.join(job.ID, 'Ca_i.igb.gz')
                job.meshalyzer(geom, data, deform, view)

            # view tension
            view = 'wedge_tension.mshz'
            data = os.path.join(job.ID, 'Tension.igb.gz')
            job.meshalyzer(geom, data, deform, view)



# ============================================================================
#    FUNCTIONS
# ============================================================================

def build_wedge(args):
    """ build geometry """

    # Units are mm
    ab_len = args.length  # apico-basal length
    cc_len = args.width   # circumferential width
    wall_t = 10.0         # transmural wall thickness

    geom = mesh.Block(size=(cc_len, ab_len, wall_t), resolution=args.resolution)

    # Set fibre angle to 0, sheet angle to 90
    geom.set_fibres(-60, 60, 90, 90)

    # Define regions
    # apicobasal coordinates
    ab_0 = - ab_len/2
    ab_1 = - ab_len/2 + 0.33*ab_len
    ab_2 = - ab_len/2 + 0.66*ab_len
    ab_3 = + ab_len/2

    # transmural coordinates
    tm_0 = -wall_t/2
    tm_1 = -wall_t/2 + 0.33*wall_t
    tm_2 = -wall_t/2 + 0.66*wall_t
    tm_3 =  wall_t/2

    # circumferential coordinates, no regions here
    cf_0 = -cc_len/2
    cf_1 =  cc_len/2

    # define epicardial layer
    epi_apex = mesh.BoxRegion((cf_0,ab_0,tm_0), (cf_1,ab_1,tm_1), tag=11)
    geom.add_region(epi_apex)
    epi_mid  = mesh.BoxRegion((cf_0,ab_1,tm_0), (cf_1,ab_2,tm_1), tag=12)
    geom.add_region(epi_mid)
    epi_base = mesh.BoxRegion((cf_0,ab_2,tm_0), (cf_1,ab_3,tm_1), tag=13)
    geom.add_region(epi_base)

    # define midmyocardial layer
    mid_apex = mesh.BoxRegion((cf_0,ab_0,tm_1), (cf_1,ab_1,tm_2), tag=21)
    geom.add_region(mid_apex)
    mid_mid  = mesh.BoxRegion((cf_0,ab_1,tm_1), (cf_1,ab_2,tm_2), tag=22)
    geom.add_region(mid_mid)
    mid_base = mesh.BoxRegion((cf_0,ab_2,tm_1), (cf_1,ab_3,tm_2), tag=23)
    geom.add_region(mid_base)

    # define endocardial layer
    endo_apex = mesh.BoxRegion((cf_0,ab_0,tm_2), (cf_1,ab_1,tm_3), tag=31)
    geom.add_region(endo_apex)
    endo_mid  = mesh.BoxRegion((cf_0,ab_1,tm_2), (cf_1,ab_2,tm_3), tag=32)
    geom.add_region(endo_mid)
    endo_base = mesh.BoxRegion((cf_0,ab_2,tm_2), (cf_1,ab_3,tm_3), tag=33)
    geom.add_region(endo_base)

    wedge_tags = [11, 12, 13, 21, 22, 23, 31, 32, 33]

    # Generate and return base name
    meshname = mesh.generate(geom)

    return meshname, geom, wedge_tags


# -----------------------------------------------------------------------------
def setup_mech_mat(wedge_tags):
    """ Material definitions """
    wedge = model.mechanics.GuccioneMaterial(wedge_tags, 'wedge',  kappa=1000., a=1.0 )
    return wedge

# -----------------------------------------------------------------------------
def setup_stimuli(geom):
    """ setup stimulus """

    _, up_bnd = geom.tissue_limits()
    res    = geom.resolution()
    radius = 0.25

    electrode = mesh.block_region(geom, 'stimulus', 0,
                                  [-radius, -radius, up_bnd[2]-res/2],
                                  [radius, radius, up_bnd[2]+res/2], False)


    strength = 150.0         # stimulus strength
    duration =   3.0         # stimulus duration

    num_stim = 2

    stm_opt = ['-floating_ground', 0,
               '-num_stim', num_stim,
               '-stimulus[0].stimtype', 0,
               '-stimulus[0].start', 5,
               '-stimulus[0].strength', strength,
               '-stimulus[0].duration', duration]
    stm_opt += electrode

    ekstim = ['-stimulus[1].stimtype', 8]
    stm_opt += ekstim

    return stm_opt

# -----------------------------------------------------------------------------
def setup_ep(wedge):
    """ set up EP """
    num_imp_regions = 1

    imps  = ['-num_imp_regions', num_imp_regions]

    imps += ['-imp_region[0].im', 'TT2']
    imps += ['-imp_region[0].num_IDs', len(wedge)]
    for ind, reg in enumerate(wedge):
        imps += [f'-imp_region[0].ID[{ind}]', reg]

    gregs  = ['-num_gregions', 1]
    gregs += ['-gregion[0].num_IDs', len(wedge)]
    for ind, reg in enumerate(wedge):
        gregs += [f'-gregion[0].ID[{ind}]', reg]

    eks  = ['-num_ekregions', 1]
    eks += ['-ekregion[0].ID', 0]
    eks += ['-ekregion[0].vel_f', 0.6,
            '-ekregion[0].vel_s', 0.4,
            '-ekregion[0].vel_n', 0.2]

    return imps, gregs, eks

# -----------------------------------------------------------------------------
def setup_dbc(geom):
    """ setup dirichlet boundary conditions """

    dbc = ['-num_mechanic_dbc', 2]
    # Dirichlet BC - fix 'apical face, no displacement in apico-basal direction'
    dbc += ['-mechanic_dbc[0].name', 'apex']
    dbc += mesh.block_boundary_condition(geom, 'mechanic_dbc', 0, 'y',
                                         lower=True)
    dbc += ['-mechanic_dbc[0].bctype',     0,
            '-mechanic_dbc[0].apply_ux',   1,
            '-mechanic_dbc[0].apply_uy',   1,
            '-mechanic_dbc[0].apply_uz',   1,
            '-mechanic_dbc[0].dump_nodes', 1]

    dbc += ['-mechanic_dbc[1].name', 'epi']
    dbc += mesh.block_boundary_condition(geom, 'mechanic_dbc', 1, 'z',
                                         lower=False)

    dbc += ['-mechanic_dbc[1].bctype',     0,
            '-mechanic_dbc[1].apply_ux',   0,
            '-mechanic_dbc[1].apply_uy',   0,
            '-mechanic_dbc[1].apply_uz',   1,
            '-mechanic_dbc[1].dump_nodes', 1]

    return dbc

# -----------------------------------------------------------------------------
def setup_active (stress_mdl, ep_mdl, active_imp):
    """ setup active stress setting """

    opts      = []
    im_pars   = []
    statefile = False
    veldep = 0

    if ep_mdl != 'GPB':
        # pick EP and Stress model
        im_pars += [f'-imp_region[{active_imp}].im', ep_mdl]
        im_pars += [f'-imp_region[{active_imp}].plugins', stress_mdl]

        # pick limit cycle state file
        sv_file = f'./states/{ep_mdl}_{stress_mdl}_bcl_500_ms.sv'
        statefile = True

    else:
        # pick EP and stress model
        im_pars += [f'-imp_region[{active_imp}].im', 'GPB_Land']

        gpb_pars  = 'T_ref=117.1,Ca_50ref=0.52,TRPN_50=0.37,n_TRPN=1.54'
        gpb_pars += ',k_TRPN=0.14,n_xb=3.38,k_xb=4.9e-3,lengthDep=1'

        im_pars  += [f'-imp_region[{active_imp}].im_param', gpb_pars]

        # pick limit cycle state file
        sv_file = f'./states/{ep_mdl}_{stress_mdl}_bcl_500_ms.sv'
        statefile = True
        veldep = 1

    # Calcium-driven active stress model outputs Ca_i
    if stress_mdl != 'TanhStress':
        veldep = 1
        im_pars += ['-num_gvecs', 1,
                    '-gvec[0].name',  "Ca_i",
                    '-gvec[0].ID[0]', "Ca_i",
                    '-gvec[0].bogus', -10.]

    # overrule default parameters of active stress model
    im_pars += setup_stress_params(ep_mdl, stress_mdl, active_imp)

    opts += ['-veldep', veldep]
    opts += im_pars

    # check existence of state file
    if statefile:
        if not os.path.isfile(sv_file):
            print(f'State variable initialization file {sv_file} not found!')
            sys.exit(-1)
        else:
            opts += [f'-imp_region[{active_imp}].im_sv_init', sv_file]

    return opts

# -----------------------------------------------------------------------------
def setup_stress_params(ep_mdl, stress_mdl, active_imp):
    """ set active stress parameters """

    param_str = []

    if ep_mdl != 'GPB':
        if stress_mdl == 'LandStress':
            # LandStress parameter string
            param = 'T_ref=40,n_TRPN=1.54'
        elif stress_mdl == 'LandHumanStress':
            # LandStress parameter string
            param = 'Tref=40,TRPN_n=2.0'
        else:
            # TanhStress
            param = 'Tpeak=40,t_dur=400,ldOn=0'

        param_str += [f'-imp_region[{active_imp}].plug_param', param]

    else:
        # GPB_Land stress parameters
        param  = 'T_ref=117.1,Ca_50ref=0.52,TRPN_50=0.37,n_TRPN=1.54'
        param += ',k_TRPN=0.14,n_xb=3.38,k_xb=4.9e-3,lengthDep=1'

        param_str += [f'-imp_region[{active_imp}].im_param', param]

    return param_str

# -----------------------------------------------------------------------------
def setup_em_coupling (em_coupling):
    """ setup electromechanical coupling """

    if em_coupling == 'weak':
        coupling = ['-mech_use_actStress', 1, '-mech_lambda_upd', 1, '-mech_deform_elec', 0]
    elif em_coupling == 'strong':
        coupling = ['-mech_use_actStress', 1, '-mech_lambda_upd', 2, '-mech_deform_elec', 1]
    elif em_coupling == 'none':
        coupling = ['-mech_use_actStress', 0, '-mech_lambda_upd', 0, '-mech_deform_elec', 0]

    return coupling

# -----------------------------------------------------------------------------
def configure_solvers():
    """ configure solver options """
    mech_opts = ['-dt',                 25.,
                 '-volumeTracking',     1,
                 '-krylov_tol_mech',    1e-8,
                 '-newton_tol_mech',    1e-6,
                 '-newton_maxit_mech',  10,
                 '-mech_tangent_comp',  1,
                 '-mech_output',        1]

    ep_opts = ['-cg_tol_ellip',       1e-8,
               '-cg_tol_parab',       1e-8]

    # Specify solver file for mechanics
    mech_opts += ['-mechanics_options_file', petsc_options.path('gamg_gmres_opts_agg')]

    return ep_opts + mech_opts

# -----------------------------------------------------------------------------
if __name__ == '__main__':
    run()

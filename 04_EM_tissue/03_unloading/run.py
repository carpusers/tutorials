#!/usr/bin/env python

r"""

.. _unloading:

Calculate the unloaded reference geometry from a known mesh configuration under loading.

**Introduction**
****************

Patient-specific models for numerical simulations of the cardiovascular system are
mainly based on medical imaging techniques such as X-ray computed tomography (CT)
or magnetic resonance imaging (MRI). But at the moment of the medical image acquisition,
a physiological pressure load is present. When using the in vivo obtained patient-specific
model, this model does not correspond to the unloaded configuration. Thus, we need a suitable
algorithm to determine the unloaded configuration from the in vivo model and from the present
physiological pressure.

*Method*
========

For our purpose, we use a backward displacement method described in [Bols2013]_ which is based
on a fixed-point iteration. Before we describe the method, we make some definitions. We denote
by :math:`\Omega(\mathbf{X}, \mathbf{0})` the stress free reference configuration which is yet
unknown. The first argument :math:`\mathbf{X}` denotes the material coordinates and the second argument
:math:`\mathbf{0}` corresponds to stress ( which is zero ) of the unloaded configuration. A simple
forward computation leads to the equilibrium configuration :math:`\Omega(\mathbf{x}, \mathbf{\sigma})`,
with the coordinates of the deformed geometry :math:`\mathbf{x}` and the second-prder stress tensor
:math:`\mathbf{\sigma}`. This configuration results from a pressure load :math:`p` applied at the inner
surface of the undeformed configuration, i.e.

.. math::

   p = - \tau \cdot \mathbf{n} = - ( \sigma \, \mathbf{n} ) \cdot \mathbf{n}

with the outer normal vector :math:`\mathbf{n}`, and :math:`\tau = 0` at the outer surface.
See figure :numref:`fig-unl-configurations`.

.. _fig-unl-configurations:

.. figure:: /images/unloading_configurations.png
   :width: 350pt
   :align: center

   Unloaded and loaded configuration.

We write

.. math::

   \Omega(\mathbf{x}, \mathbf{\sigma}) = \mathcal{S}\big(\Omega(\mathbf{X}, \mathbf{0}), p\big)

where :math:`\mathcal{S}` is an appropriate forward solver. The deformation is then defined
by the mapping :math:`\Phi : \mathbf{X} \mapsto \mathbf{x}`.

We denote the measured geometry and the measured pressure load by :math:`\mathbf{x}_m` and
:math:`p_m` respectively. Then the backward problem is as follows.

Find the in vivo configuration :math:`\Omega(\mathbf{x}_m, \mathbf{\sigma}^{\star})` which is
unknown since just :math:`\mathbf{x}_m` is known, and which is in equilibrium. Therefore, we
have to find the corresponding unloaded configuration such that

.. math::

   \Omega(\mathbf{x}_m, \mathbf{\sigma}^{\star}) = \mathcal{S}\big(\Omega(\mathbf{X}^{\star}, \mathbf{0}), p_m\big)

where :math:`\mathbf{X}^{\star}` denotes the unknown unloaded reference geometry which can
be written as :math:`\mathbf{X}^{\star} = \Phi^{-1} (\mathbf{x}_m)`.

*Algorithm*
===========

The backward displacement method then reads as follows.

.. math::

   \intertext{\textbf{Input:} Measured state $\Omega_m^r = \Omega(\mathbf{x}_m, \mathbf{0})$ and pressure $p_m$} \\[-1.2cm]
   & \line(1,0){330} \\[-0.5cm]
   \intertext{Set initial stress free configuration $\Omega_1^r = \Omega(\mathbf{X}_1, \mathbf{0})$ with $\mathbf{X}_1 = \mathbf{x}_m$} \\[-1cm]
   & i = 0 \\
   & \textbf{while} \; (\text{error} > \varepsilon) \; \textbf{do}  \\
   & \qquad i = i + 1 \\
   & \qquad \Omega_i^t = \Omega(\mathbf{x}_i, \mathbf{\sigma}_i) =  \mathcal{S}\big(\Omega_i^r, p_m\big) \\
   & \qquad \mathbf{U}_i = \mathbf{x}_i - \mathbf{X}_i \\
   & \qquad \mathbf{X}_{i+1} = \mathbf{x}_m - \mathbf{U}_i \\
   & \qquad \Omega_{i+1}^r = \Omega(\mathbf{X}_{i+1}, \mathbf{0}) \\
   & \qquad \text{error} = error(\Omega_m^r, \Omega_i^t) \\
   & \textbf{end do} \\
   & \line(1,0){330} \\[-0.5cm]
   \intertext{\textbf{Output:} Zero pressure geometry $\Omega(\mathbf{X}^{\star}, \mathbf{0})$ with $\mathbf{X}^{\star} = \mathbf{X}_i$} \\[-1.2cm]
   \intertext{\phantom{\textbf{Output:}} In vivo stress tensor $\mathbf{\sigma}^{\star} = \mathbf{\sigma}_i$ } \\[-1.2cm]

*Usage*
=======

To run a simple ring example just run the command

.. code-block:: bash

   ./run.py --meshtype ring --pressure 4.0 --fast --visualize

where the parameter **meshtype** specifies the mesh and **pressure** is the
pressure to unload from. The **visualize** flag runs :ref:`meshalyzer <meshalyzer>` and shows
the result. For further information / help on the arguments and flags see below, to get the full
help run ``./run.py --help``.

.. code-block:: bash

     -h, --help                             show this help message and exit

     --pressure PRESSURE                    End diastolic pressure to unload from ( in kPa )
                                            (default: 1.5kPa for ring/ellipsoid, 5kPa for cube)

     --np NP                                number of processes

     --meshtype TYPE                        choose the mesh type,
                                            choices = {cube,ring,ellipsoid}

     --dimension DIMENSION                  choose the inner diameter of the mesh cavity, or the
                                            side length in the case of a cube ( in mm )

     --fast                                 set resolution and material (example specific) to
                                            fast-solving values

     --resolution RES                       set target mesh resolution ( in mm )

     --material TYPE                        choose the material model
                                            choices = {mooneyrivlin, linear, neohookean, holzapfelogden,
                                                       demiray, anisotropic, holzapfelarterial,
                                                       stvenantkirchhoff, guccione}

     --parameters PRM=VAL [PRM=VAL ...]     set material model parameters

   MATERIAL PARAMETERS:
        mooneyrivlin { kappa, c_1, c_2 }
        linear { E, lmbda, mu, nu }
        neohookean { kappa, c }
        holzapfelogden { kappa, a, a_f, a_fs, a_s, b, b_f, b_fs, b_s }
        demiray { kappa, a, b }
        anisotropic { kappa, a_f, b_f, c }
        holzapfelarterial { kappa, c, k_1, k_2 }
        stvenantkirchhoff { lmbda, mu }
        guccione { kappa, b_f, b_fs, b_t, a }

*Examples*
==========

In this section we want to present two examples to which the algorithm was applied.
The black wireframe shows the initial geometry.

.. figure:: /images/unloading_ringexample.gif
   :width: 250pt
   :align: center

   Three dimensional ring.

.. figure:: /images/unloading_ellexample.gif
   :width: 250pt
   :align: center

   Three dimensional ellipsoid.

.. rubric:: References

.. [Bols2013] *Bols, J. and Degroote, J. and Trachet, B. and Verhegghe, B. and Segers, P. and Vierendeels, J.*,
              **A computational method to assess the in vivo stresses and unloaded configuration of patient-specific blood vessels (2013)**,
              Journal of computational and Applied mathematics, Volume 246

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Unloading'
EXAMPLE_AUTHOR = EXAMPLE_AUTHOR = ('Matthias Gsell <matthias.gsell@medunigraz.at>')

import os
from datetime import date

from carputils import settings
from carputils import tools
from carputils import model

from devtests.mechanics import standardmesh

DEFAULT_MATERIAL = 'neohookean'

def parser():
    parser = tools.standard_parser()
    standardmesh.parser_add_meshopts(parser, cube=True)
    standardmesh.parser_add_materialopts(parser, default=DEFAULT_MATERIAL)
    parser.add_argument('--pressure',
                        type=float,
                        help='End diastolic pressurr to unload from, in kPa, '
                             '(default: 1.5kPa for ring/ellipsoid, 5kPa for '
                             'cube)')
    parser.add_argument('--validate',
                        action='store_true',
                        help='Do the unloading then an inflation to validate '
                             'the calculated reference state')
    parser.add_argument
    return parser

def pressure(args):
    if args.pressure is not None:
        return args.pressure
    if args.meshtype == 'cube':
        return 5
    else:
        return 1.5

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_{}_{}kPa_{}_np{}'.format(today.isoformat(), args.meshtype[:4],
                                        pressure(args), args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    meshdir  = 'mesh'
    meshinfo = standardmesh.mesh_from_parser(args, meshdir)

    # Additional solution settings
    configuration   = 1          # 0 Euler, 1 Lagrangian
    line_search     = 0          # use line-search method in Newton
    tangent_mode    = 0

    # Genertate shared command line
    cmd = tools.carp_cmd()
    cmd += ['-mech_configuration', configuration,
            '-mech_tangent_comp',  tangent_mode,
            '-volumeTracking',     1]

    cmd += meshinfo['dbc'] # Dirichlet boundary conditions

    if args.meshtype == 'cube':
        # Use point-based convergence metric, since there is no cavity
        cmd += ['-unload_conv', 1]
    else:
        # Use volume-based convergence metric
        cmd += ['-unload_conv', 0]
        cmd += meshinfo['vol']

    # Get material
    mat = standardmesh.material_from_parser(args)
    if isinstance(mat, model.mechanics.GuccioneMaterial):
        tangent_mode = 1
    cmd += model.optionlist([mat]) # Material definitions

    # Switch off EP
    cmd += ['-num_imp_regions',       1,
            '-imp_region[0].name',    'passive',
            '-imp_region[0].im',      'PASSIVE',
            '-imp_region[0].num_IDs', 100,
            '-num_stim',              0]

    # Visualisation options
    cmd += ['-gridout_i',                   3,
            '-mech_output',                 1,
            '-stress_value',                4]

    # Numerical options
    nopts = ['-parab_solve',        1,
             '-localize_pts',       1,
             '-cg_tol_ellip',       1e-8,
             '-cg_tol_parab',       1e-8,
             '-cg_norm_ellip',      0,
             '-cg_norm_parab',      0,
             '-krylov_maxit_mech',  1000,
             '-newton_line_search', line_search,
             '-newton_maxit_mech',  50]
    cmd += nopts

    # Add overrule options
    oopts = ['-mapping_mode',     1,
             '-redist_extracted', 0,
             '-localize_pts',     1,
             '-mass_lumping',     1]
    cmd += oopts

    #######################
    # Unload Run

    unload_simID = job.ID
    # Put in subdirectory if we're going to validate
    if args.validate:
        unload_simID = os.path.join(unload_simID, 'unload')

    extra_opts = ['-simID',        unload_simID,
                  '-meshname',     meshinfo['meshname'],
                  '-timedt',       1.0,
                  '-mechDT',       1.0,
                  '-spacedt',      1.0,
                  '-tend',         100.0,
                  '-loadStepping', 4,
                  '-experiment',   5]

    # Add trace and pressure to Neumann BC
    nbc = meshinfo['nbc'] + ['-mechanic_nbc[0].pressure', pressure(args)]

    unload_cmd = cmd + extra_opts + nbc

    # Execute unloading sim
    job.carp(unload_cmd)

    #######################
    # Validation Run

    if args.validate:

        # Set up unloaded mesh directory
        unload_meshdir  = os.path.join(job.ID, 'unloaded_mesh')
        unload_meshname = os.path.join(unload_meshdir,
                                       'unloaded_' + args.meshtype)

        if not args.dry:

            # Make directory
            job.mkdir(unload_meshdir)

            # Make links to mesh files
            unloaded_pts = os.path.join(unload_simID, 'reference.pts')

            job.link(os.path.abspath(unloaded_pts),
                     unload_meshname + '.pts')
            job.link(os.path.abspath(meshinfo['meshname'] + '.elem'),
                     unload_meshname + '.elem')
            job.link(os.path.abspath(meshinfo['meshname'] + '.lon'),
                     unload_meshname + '.lon')

        validate_simID = os.path.join(job.ID, 'validate')

        extra_opts = ['-simID',        validate_simID,
                      '-meshname',     unload_meshname,
                      '-timedt',       1.0,
                      '-mechDT',       1.0,
                      '-spacedt',      1.0,
                      '-tend',         10.0]
                      #'-loadStepping', 4]

        # Add trace and pressure to Neumann BC
        nbc = meshinfo['nbc'] + ['-mechanic_nbc[0].pressure', pressure(args),
                                 '-mechanic_nbc[0].trace',    'nbc_pressure10']

        validate_cmd = cmd + extra_opts + nbc

        # Execute unloading sim
        job.carp(validate_cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        if args.validate:
            simID = validate_simID
            meshname = unload_meshname
        else:
            simID = unload_simID
            meshname = meshinfo['meshname']

        geom   = os.path.join(simID, os.path.basename(meshname) + '_i')
        deform = os.path.join(simID, 'x.dynpt')

        job.gunzip(deform)

        if args.validate:
            data = os.path.join(simID, 'firstPrincipalStress.igb.gz')
            job.meshalyzer(geom, data, deform)
        else:
            job.meshalyzer(geom, deform)

if __name__ == '__main__':
    run()

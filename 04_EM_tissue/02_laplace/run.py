#!/usr/bin/env python
# -*- coding: utf-8 -*-

r"""

.. _lawoflaplace:

.. |r0| replace:: :math:`r(t=0)`
.. |R0| replace:: :math:`R(t=0)`
.. |h0| replace:: :math:`h(t=0)`
.. |press| replace:: :math:`p = p(t=T)`
.. |endt| replace:: :math:`T`
.. |kappa| replace:: :math:`\kappa`

.. |sigmaw| replace:: :math:`\sigma_W`
.. |sigmal| replace:: :math:`\sigma_L`
.. |msigmarr| replace:: :math:`\bar{\sigma}_{rr}`
.. |msigmatt| replace:: :math:`\bar{\sigma}_{\theta \theta}`
.. |msigmapp| replace:: :math:`\bar{\sigma}_{\varphi \varphi}`

.. |wext| replace:: :math:`W_{\text{ext}}`
.. |wint| replace:: :math:`W_{\text{int}}`
.. |wintw| replace:: :math:`W_{\text{int},W}`
.. |wintl| replace:: :math:`W_{\text{int},L}`
.. |epst| replace:: :math:`\varepsilon_t`

This example verifies the computed stress and the computed work using a simple spherical shell geometry
and time-varying Neumann boundary condition applied on the inner boundary. To verify the computed stress,
we use the law of Laplace, see [#Grossman1975]_, to estimate the wall stress and compare it against the mean
computed stress. For the work varification, we compute the external work analytically and compare it against
the computed internal work.

**Stress Verification**
***********************

To verify the stress, we use the law of Laplace to estimate the wall stress (wall tension).The
`law of Laplace <https://en.wikipedia.org/wiki/Young%E2%80%93Laplace_equation>`_ can be used to describe
the wall stress of a spherical shell in terms of the surface pressure applied on the inner boundary,
see Figure :numref:`fig-lol-setup`. To estimate the wall tension, we consider the geometry
at time :math:`t = T` with pressure :math:`p(t=T) = p` and radii :math:`r(t=T) = r, R(t=T) = R`.

.. _fig-lol-setup:

.. figure:: /images/lawoflaplace_setup.png
   :width: 500pt
   :align: center

   Deformation at time step :math:`t=0` and :math:`t=T`.

*Derivation Of Laplace's Law*
=============================

To derive Laplace's law we want to use the balance of forces. We assume that the center of the spherical shell
is the origin and we denote the inner radius of the deformed geometry by :math:`r` and the outer radius by
:math:`R` with :math:`R > r`, see figure :numref:`fig-lol-geometry`.

.. _fig-lol-geometry:

.. figure:: /images/lawoflaplace_geometry.png
   :width: 250pt
   :align: center

   Geometrical setting.

By :math:`h` we denote the wall thickness which is given by :math:`h = R - r`. Next, we clip the geometry using the x-y-plane
as a clipping plane and obtain two equally sized shells, see figure :numref:`fig-lol-forces`.

.. _fig-lol-forces:

.. figure:: /images/lawoflaplace_force.png
   :width: 250pt
   :align: center

   Acting forces.

The force vector acting on the inner surface is given in `spherical coordinates <https://en.wikipedia.org/wiki/Spherical_coordinate_system>`_ by

.. math::

   \mathbf{F}(\theta, \varphi) = p
   \left(
   \begin{array}{c}
      \sin(\theta) \, \cos(\varphi) \\
      \sin(\theta) \, \sin(\varphi) \\
      \cos(\theta)
   \end{array}
   \right)

with :math:`\theta \in [0, \pi]` and :math:`\varphi \in [0, 2 \pi]`. The components of the total force vector :math:`\mathbf{F}^{tot}` are

.. math::
   :label: equ-lol-totforcevec

   F_x^{tot} & = \int \limits_{\theta=0}^{\frac{\pi}{2}} \int \limits_{\varphi=0}^{2 \pi} \big( \mathbf{F} \cdot \mathbf{e}_x \big) \, r^2 \, \sin(\theta) \mathrm{d} \varphi \, \mathrm{d} \theta
   = p \, r^2 \int \limits_{\theta=0}^{\frac{\pi}{2}} \int \limits_{\varphi=0}^{2 \pi} \sin^2(\theta) \, \cos(\varphi) \mathrm{d} \varphi \, \mathrm{d} \theta = 0, \\
   F_y^{tot} & = \int \limits_{\theta=0}^{\frac{\pi}{2}} \int \limits_{\varphi=0}^{2 \pi} \big( \mathbf{F} \cdot \mathbf{e}_y \big) \, r^2 \, \sin(\theta) \mathrm{d} \varphi \, \mathrm{d} \theta
   = p \, r^2 \int \limits_{\theta=0}^{\frac{\pi}{2}} \int \limits_{\varphi=0}^{2 \pi} \sin^2(\theta) \, \sin(\varphi) \mathrm{d} \varphi \, \mathrm{d} \theta = 0, \\
   F_z^{tot} & = \int \limits_{\theta=0}^{\frac{\pi}{2}} \int \limits_{\varphi=0}^{2 \pi} \big( \mathbf{F} \cdot \mathbf{e}_z \big) \, r^2 \, \sin(\theta) \mathrm{d} \varphi \, \mathrm{d} \theta
   = p \, r^2 \int \limits_{\theta=0}^{\frac{\pi}{2}} \int \limits_{\varphi=0}^{2 \pi} \sin(\theta) \, \cos(\theta) \mathrm{d} \varphi \, \mathrm{d} \theta = p \, r^2 \, \pi \\

with :math:`\theta \in (0, \frac{\pi}{2})` since we only integrate over one half of the shell.

For the derivation of Laplace's law we approximate the wall stress :math:`\sigma` by its mean value :math:`\bar{\sigma}`,
see figure :numref:`fig-lol-forces`.  Since the geometry and the acting forces are symmetric, the tangential stress in any
direction must be the same and there will be zero shear stress. Due to the assumption, the mean wall stress at the cut face
just acts just in z-direction, thus we have :math:`\bar{\sigma} \cdot \mathbf{e}_x = 0` and :math:`\bar{\sigma} \cdot \mathbf{e}_y = 0`.
The components of the total wall stress vector :math:`\bar{\sigma}^{tot}` are

.. math::
   :label: equ-lol-totstensionvec

   \bar{\sigma}_x^{tot} & = \int \limits_{r=r}^{R} \int \limits_{\varphi=0}^{2 \pi} \big( \bar{\sigma} \cdot \mathbf{e}_x \big) \, r^2 \, \sin(\theta) \mathrm{d} \varphi \, \mathrm{d} r = 0, \\
   \bar{\sigma}_y^{tot} & = \int \limits_{r=r}^{R} \int \limits_{\varphi=0}^{2 \pi} \big( \bar{\sigma} \cdot \mathbf{e}_y \big) \, r^2 \, \sin(\theta) \mathrm{d} \varphi \, \mathrm{d} r = 0, \\
   \bar{\sigma}_z^{tot} & = \int \limits_{r=r}^{R} \int \limits_{\varphi=0}^{2 \pi} \big( \bar{\sigma} \cdot \mathbf{e}_z \big) \, r^2 \, \sin(\theta) \mathrm{d} \varphi \, \mathrm{d} r = \pi \, \big( R^2 - r^2 \big) \, \sigma_W

with :math:`\sigma_W := \bar{\sigma} \cdot \mathbf{e}_z`. From equations :eq:`equ-lol-totforcevec` and :eq:`equ-lol-totstensionvec`
we have

.. math::
   :label: equ-lol-equalcond

   \mathbf{F}^{tot} = \bar{\sigma}^{tot} \quad \Leftrightarrow \quad p \, r^2 = (R^2 - r^2) \, \sigma_W.

From :eq:`equ-lol-equalcond` we conclude that

.. math::
   :label: equ-lol-wallstress

   \sigma_W = \frac{p \, r^2}{R^2 - r^2} = \frac{p \, r}{2 \, h \left( 1 + \frac{h}{ 2 \, r } \right) }

.. \sigma_W = \frac{p \, r^2}{R^2 - r^2}  = \frac{p \, r^2}{(R - r) \, (R + r)} = \frac{p \, r^2}{h \, ( 2 \, r + h ) }
.. = \frac{p \, r^2}{2 \, h \, r \left( 1 + \frac{h}{ 2 \, r } \right) } = \frac{p \, r}{2 \, h \left( 1 + \frac{h}{ 2 \, r } \right) }

and for :math:`h \ll r` we obtain the **law of Laplace**

.. math::
   :label: equ-lol-lwallstress

   \sigma_W \approx \sigma_{L} = \frac{p \, r}{2 \, h}

which can be used to estimate the wall stress for thin walled spherical shells.

*Modeling Error*
================

The law of Laplace, see equation :eq:`equ-lol-lwallstress`, is just an approximation to the more accurate formula
:eq:`equ-lol-wallstress`. The approximation is justified if the wall thickness is mush smaller than the inner
radius. To figure out how strong the relative modeling error depends on :math:`R` and :math:`r`, we rewrite the
error as

.. math::
   :label: equ-lol-relerr

   err_{\text{rel}} := \left| \frac{\sigma_W - \sigma_L}{\sigma_W} \right| = \frac{R - r}{2 \, r} = \frac{h}{2 \, r}

.. err_{\text{rel}} := \left| \frac{\sigma_W - \sigma_L}{\sigma_W} \right| = \left| \frac{\frac{p \, r^2}{R^2 - r^2} - \frac{p \, r}{2 \, (R - r)}}{\frac{p \, r^2}{R^2 - r^2}} \right|
.. = \left| \frac{\frac{2 \, p \, r^2}{2 \, (R^2 - r^2)} - \frac{p \, r (R + r)}{2 \, (R^2 - r^2)}}{\frac{2 \, p \, r^2}{2 \, (R^2 - r^2)}} \right|
.. = \left| \frac{2 \, p \, r^2 - p \, r (R + r)}{2 \, p \, r^2} \right|
.. = \left| \frac{2 \,  r - (R + r)}{2 \, r} \right|
.. = \frac{R - r}{2 \, r} = \frac{h}{2 \, r}

which yields to the condition

.. math::

   \frac{h}{r} \stackrel{!}{<} 2 \, \varepsilon

if we want to ensure that the relative modeling error is less than :math:`\varepsilon \in \mathbb{R}_+`.

*Example*
---------

Assume, that we have a spherical shell with :math:`r = 30 mm` and we want to use the law of Laplace,
equation :eq:`equ-lol-lwallstress`, to estimate the surface tension. To ensure that the error is
less than one present, that is :math:`\varepsilon = 0.01`, we have to ensure that the wall thickness
satisfies :math:`h < 0.6 mm`. For :math:`\varepsilon = 0.1` we have :math:`h < 6 mm` and
for :math:`\varepsilon = 0.25` we have :math:`h < 15 mm`.

*Verification*
==============

To verify the stress, we first compute the stress tensor :math:`\sigma_C` elementwise and project the tensor from the
cartesian coordinate system to the spherical coordinate system, i.e.

.. math::
   :label: equ-lol-stresstensor

   \sigma_S = \left(
   \begin{array}{ccc}
      \sigma_{rr} & \sigma_{r \varphi} & \sigma_{r \theta } \\
      \sigma_{\varphi r} & \sigma_{\varphi \varphi} & \sigma_{\varphi \theta } \\
      \sigma_{\theta r} & \sigma_{\theta  \varphi} & \sigma_{\theta \theta}
   \end{array}
   \right) = \mathbf{P}^T \, \underbrace{ \left(
   \begin{array}{ccc}
      \sigma_{xx} & \sigma_{xy} & \sigma_{xz} \\
      \sigma_{yx} & \sigma_{yy} & \sigma_{yz} \\
      \sigma_{zx} & \sigma_{zy} & \sigma_{zz}
   \end{array}
   \right)}_{= \mathbb{\sigma}_C} \mathbf{P}

with the projection matrix :math:`\mathbf{P} = \big( \mathbf{e}_r \; \mathbf{e}_{\varphi} \; \mathbf{e}_{\theta} \big)`.
The mean stress tensor then is

.. math::

   \bar{\sigma}_S := \frac{1}{| \Omega(T) |} \int \limits_{\Omega(T)} \sigma_S \, \mathrm{d} \, \mathbf{x}

where the integration is applied componentwise.


.. _stress-experiments:

*Experiments*
=============

For the experiments we choose the following setups with two different geometries. The first geometry
is a thin walled spherical shell and the second geometry is a rather thick walled spherical shell.
The material model is the Demiray model with :math:`\kappa = 1000.0`, :math:`a=34.0` and :math:`b=10.0`.

+--------------+-----------+-----------+-----------+---------------+-------------+
|              | |r0| [mm] | |R0| [mm] | |h0| [mm] | |press| [kPa] | |endt| [ms] |
+==============+===========+===========+===========+===============+=============+
| Experiment 1 | 15.0      | 15.5      | 0.5       | 2.0           | 100.0       |
+--------------+-----------+-----------+-----------+---------------+-------------+
| Experiment 2 | 15.0      | 15.5      | 0.5       | 4.0           | 100.0       |
+--------------+-----------+-----------+-----------+---------------+-------------+
| Experiment 3 | 15.0      | 30.0      | 15.0      | 2.0           | 100.0       |
+--------------+-----------+-----------+-----------+---------------+-------------+
| Experiment 4 | 15.0      | 30.0      | 15.0      | 4.0           | 100.0       |
+--------------+-----------+-----------+-----------+---------------+-------------+

Using the method described in this section, we obtain the following results.


+--------------+---------+----------+------------+------------+------------+
|              | |sigmaw|| |sigmal| | |msigmarr| | |msigmatt| | |msigmapp| |
|              | [kPa]   | [kPa]    | [kPa]      | [kPa]      | [kPa]      |
+==============+=========+==========+============+============+============+
| Experiment 1 | 39.2682 | 38.7745  | -0.9303    | 38.8500    | 38.8516    |
+--------------+---------+----------+------------+------------+------------+
| Experiment 2 | 83.8585 | 84.8469  | -1.8753    | 83.7231    | 83.7220    |
+--------------+---------+----------+------------+------------+------------+
| Experiment 3 | 0.6937  | 1.0304   | -0.3601    | 0.6260     | 0.6271     |
+--------------+---------+----------+------------+------------+------------+
| Experiment 4 | 1.4428  | 2.1226   | -0.7198    | 1.2951     |  1.2973    |
+--------------+---------+----------+------------+------------+------------+

We see, that for the thin walled geometry the analytically computed wall stress |sigmaw| and
the Laplace approximation |sigmal| are almost equal to the computed mean tangential stresses
|msigmatt| and |msigmapp|. The radial stress |msigmarr| is compared to the tangential stresses
negligible. The relative modeling error for the first two experiments are :math:`err_{\text{rel}} = 0.0061`
and :math:`err_{\text{rel}} = 0.0056`, see equation :eq:`equ-lol-relerr`.
For the experiments with a thick walled geometry we have a rather high discrepancy in the analytical
and computed stresses. The relative modeling errors for the last two experiments are :math:`err_{\text{rel}} = 0.4853`
and :math:`err_{\text{rel}} = 0.4714`.

**Work Verification**
*********************

To verify the computed work, we compute the external work analytically and compare it with the computed
internal work. To compute the external work, we use the well known principle of
`pressure-volume work <https://en.wikipedia.org/wiki/Work_(thermodynamics)#Pressure-volume_work>`_
which is :math:`\delta W = p(V) \, \mathrm{d} V` where :math:`V` denotes the volume of the inner sphere
( :math:`V(t) = \frac{4 \, \pi \, r(t)^3}{3}` ).
The integral representation then reads

.. math::
   :label: equ-lol-extworkgen

   W_{\text{ext}} = \int \limits_{V = V(t=0)}^{V(t=T)} p(V) \, \mathrm{d} V.

Next, we assume that the pressure is a piecewise linear function of the radius, that is

.. math::
   :label: equ-lol-linpressure2

   p(V) = p(r) = \frac{p_{i}-p_{i-1}}{r_{i}-r_{i-1}} \, (r - r_{i-1}) + p_{i-1}

for :math:`r \in [r_{i-1}, r_{i}]` and :math:`i = 0, \ldots, n`. We get the pressure and radius values :math:`p_i, r_i` from the
simulation.

Using `spherical coordinates <https://en.wikipedia.org/wiki/Spherical_coordinate_system>`_ and equation :eq:`equ-lol-linpressure2`,
we can write equation :eq:`equ-lol-extworkgen` as

.. math::
   :label: equ-lol-extwork2

   W_{\text{ext}} & = \int \limits_{r = r(0)}^{r(T)} \, \int \limits_{\theta = 0}^{\pi} \, \int \limits_{\varphi = 0}^{2 \pi} r^2 \, \sin(\theta) \, p(r) \, \mathrm{d} \varphi \, \mathrm{d} \theta \, \mathrm{d} r
   = 4 \, \pi \, \sum \limits_{i=1}^{n} \int \limits_{r = r_{i-1}}^{r_i} r^2 \, p(r) \, \mathrm{d} r \\
   & = 4 \, \pi \, \sum \limits_{i=1}^{n} \, \left( \frac{p_{i} - p_{i-1}}{12\, (r_{i} - r_{i-1})} \big( 3 \, r_{i}^4 - 4 \, r_{i}^3\, r_{i-1} + r_{i-1}^4 \big) + \frac{p_{i-1}}{3} \big( r_{i}^3 - r_{i-1}^3 \big) \right)

which provides an analytical approximation of the external work in [kPa mm^3] = 10^(-6) [J].

To compute the internal work, we use the internal mechanical power :math:`\mathcal{P}_{\text{int}}` which is

.. math::
   :label: equ-lol-intpower

   \mathcal{P}_{\text{int}}(t) = \int \limits_{\Omega_{\text{ref}}} \mathbf{S} : \dot{\mathbf{E}} \, \mathrm{d} \mathbf{x},

see [#Holzapfel2000]_ (Section 4.4) for further details. The internal mechanical work :math:`W_{\text{int}}` is then given by

.. math::
   :label: equ-lol-intwork

   W_{\text{int}} = \int \limits_{t=0}^{T} \mathcal{P}_{\text{int}}(t) \, \mathrm{d} t.


.. COMMENT

   The formula in [#Fernandes2017]_ for the internal heart power and its modification yield

   .. math::
      :label: equ-lol-intheartpower

      \mathcal{P}_{\text{int},L}(t) = \sigma_L(t) \, V(t) \, \frac{\dot{r}(t)}{r(0)}

   where :math:`V(t)` denotes the volume of the geometry at time :math:`t`. If we assume that the radius depends
   linearly on time, we obtain

   .. math::
      :label: equ-lol-approxrad

      r(t) \approx \frac{r(T)-r(0)}{T} \, t + r(0) \qquad \text{ and } \qquad \dot{r}(t) \approx \frac{r(T)-r(0)}{T}

   and for :math:`t=T` we have

   .. math::

      \mathcal{P}_{\text{int},L}(T) = \frac{\sigma_L \, V}{T} \, \left( \frac{r(T)}{r(0)} - 1 \right)
      = \frac{\sigma_L \, V}{T} \, \varepsilon_t

   with :math:`\varepsilon_t = \frac{r(T)}{r(0)} - 1`.

   To get the internal work :math:`W_{\text{int},L}` we have to integrate :math:`\mathcal{P}_{\text{int},L}(t)` over
   time, which can be approximated by

   .. math::

      W_{\text{int},L} = \int \limits_{t=0}^{T} \mathcal{P}_{\text{int},L}(t) \mathrm{d} t
      \approx \frac{T}{2} \, \big( \mathcal{P}_{\text{int},L}(0) + \mathcal{P}_{\text{int},L}(T) \big)
      = \frac{T}{2} \, \mathcal{P}_{\text{int},L}(T) = \sigma_L \, V \, \frac{\varepsilon_t}{2}

   since :math:`\mathcal{P}_{\text{int},L}(0) = 0`.

   To obtain :math:`W_{\text{int},W}` we proceed in the same way by replacing :math:`\sigma_L` with :math:`\sigma_W`.

.. note::

   The formula in [#Fernandes2017]_ for the internal heart power and its modification yield

   .. math::
      :label: equ-lol-intheartpower

      \mathcal{P}_{\text{int},L}(t) = \sigma_L(t) \, V(t) \, 2 \, \dot{\bar{\varepsilon}}(t)

   where :math:`V(t)` denotes the volume of the geometry at time :math:`t` and :math:`\dot{\bar{\varepsilon}}(t)` denotes
   the mean strain rate with the mean strain
   :math:`\bar{\varepsilon}(t) = \frac{1}{2} \, \left( \frac{r(t)}{r(0)} + \frac{R(t)}{R(0)} \right)`.
   First, we assume that the stress, the volume and the radii depend piecewise linearly on time.

   For the mean strain  we obtain

   .. math::
      :label: equ-lol-approxmeanstrain

      \bar{\varepsilon}(t) \approx \frac{1}{2} \, \left( \frac{r_{i}-r_{i-1}}{r(0)} + \frac{R_{i}-R_{i-1}}{R(0)} \right) \, \frac{t-t_{i-1}}{t_{i}-t_{i-1}} +
      \frac{1}{2} \, \left( \frac{r_{i-1}}{r(0)} + \frac{R_{i-1}}{R(0)} \right) \qquad \text{ and } \qquad
      \dot{\bar{\varepsilon}}(t) \approx \frac{1}{2} \, \left( \frac{r_{i}-r_{i-1}}{r(0)} + \frac{R_{i}-R_{i-1}}{R(0)} \right) \, \frac{1}{t_{i}-t_{i-1}}

   with :math:`t \in (t_{i-1}, t_i)` and :math:`r_i := r(t_i), R_i := R(t_i)`. Thus, for :math:`t \in (t_{i-1}, t_i)`, we have

   .. math::

      \mathcal{P}_{\text{int},L}(t) \approx \left( \big( \sigma_{L,i}-\sigma_{L,i-1} \big) \, \frac{t-t_{i-1}}{t_i - t_{i-1}} + \sigma_{L,i-1} \right)
      \, \left( \big( V_i-V_{i-1} \big) \, \frac{t-t_{i-1}}{t_i - t_{i-1}} + V_{i-1} \right)
      \, \left( \frac{r_{i}-r_{i-1}}{r(0)} + \frac{R_{i}-R_{i-1}}{R(0)} \right) \, \frac{1}{t_{i}-t_{i-1}}

   with :math:`\sigma_{L,i} := \sigma_L(t_i)` and :math:`V_i := V(t_i)`.

   By integrating over :math:`t  \in (t_{i-1}, t_i)` we get

   .. math::

      \int \limits_{t=t_{i-1}}^{t_i} \mathcal{P}_{\text{int},L}(t) \, \mathrm{d} t \approx
      \frac{1}{3} \, \left( \frac{r_{i}-r_{i-1}}{r(0)} + \frac{R_{i}-R_{i-1}}{R(0)} \right) \,
      \left( \big( \sigma_{L,i} + \frac{1}{2} \, \sigma_{L,i-1} \big) \big( V_i + \frac{1}{2} \, V_{i-1} \big) + \frac{3}{4} \sigma_{L,{i-1}} V_{i-1} \right)

   and finally for the internal work :math:`W_{\text{int},L}` we have

   .. math::
      W_{\text{int},L} & = \int \limits_{t=0}^{T} \mathcal{P}_{\text{int},L}(t) \, \mathrm{d} t = \sum \limits_{i=1}^{n} \int \limits_{t=t_{i-1}}^{t_i} \mathcal{P}_{\text{int},L}(t) \, \mathrm{d} t \\
      & \approx \sum \limits_{i=1}^{n} \frac{1}{3} \, \left( \frac{r_{i}-r_{i-1}}{r(0)} + \frac{R_{i}-R_{i-1}}{R(0)} \right) \,
      \left( \big( \sigma_{L,i} + \frac{1}{2} \, \sigma_{L,i-1} \big) \big( V_i + \frac{1}{2} \, V_{i-1} \big) + \frac{3}{4} \sigma_{L,{i-1}} V_{i-1} \right).

   To obtain :math:`W_{\text{int},W}` we proceed in the same way by replacing :math:`\sigma_L` with :math:`\sigma_W`.

*Experiments*
=============

Using the same geometrical settings as in :ref:`stress-experiments` we obtain the following results.

+--------------+---------+---------+---------+---------+
|              | |wext|  | |wint|  | |wintl| | |wintw| |
|              | [J]     | [J]     | [J]     | [J]     |
+==============+=========+=========+=========+=========+
| Experiment 1 | 0.00407 | 0.00420 | 0.00434 | 0.00428 |
+--------------+---------+---------+---------+---------+
| Experiment 2 | 0.01072 | 0.01112 | 0.01181 | 0.01167 |
+--------------+---------+---------+---------+---------+
| Experiment 3 | 0.00074 | 0.00070 | 0.00097 | 0.00065 |
+--------------+---------+---------+---------+---------+
| Experiment 4 | 0.00303 | 0.00285 | 0.00396 | 0.00268 |
+--------------+---------+---------+---------+---------+


*Meshes*
--------

Download the following `Python-script <https://carp.medunigraz.at/~gsell/meshes/getmeshes.py>`_
and run it!


.. rubric:: References

.. [#Holzapfel2000] *Holzapfel, G.A.*,
                    **Nonlinear Solid Mechanics: A Continuum Approach for Engineering (2000)**,
                    West Sussex, England: John Wiley \& Sons, Ltd
.. [#Grossman1975] *Grossman, W. and Jones, D. and McLaurin, L.P.*,
                   **Wall stress and patterns of hypertrophy in the human left ventricle (1975)**,
                   American Society for Clinical Investigation
.. [#Fernandes2017] *Fernandes, J.F. and Goubergrits, L. and Brüning, J. and Hellmeier, F. and Nordmeyer, S. and da Silva, T.F. and Schubert, St. and Berger, F. and Kuehne, T. and Kelm, M. and others*,
                 **Beyond Pressure Gradients: The Effects of Intervention on Heart Power in Aortic Coarctation (2017)**,
                 Public Library of Science
"""

EXAMPLE_DESCRIPTIVE_NAME = 'Law of Laplace'
EXAMPLE_AUTHOR = ('Matthias Gsell <matthias.gsell@medunigraz.at>')

import os, sys
import math
import glob
import linecache
import numpy as np
from datetime import date
from matplotlib import pyplot

from carputils import settings
from carputils import tools
from carputils import plot
from carputils import model
from carputils import format
from carputils.carpio import igb
from carputils.settings import config

from devtests.mechanics import standardmesh

isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range

EQUALS_LINE = '=' * min(format.terminal_width(90), 90)
DASHED_LINE = '-' * min(format.terminal_width(90), 90)

MESH_PATH = config.MESH_DIR if "ALT_MESH_DIR" not in os.environ else os.environ["ALT_MESH_DIR"]

def get_mesh_basenames():
    """
    Generate an array of all valid mesh basenames
    """

    # templates for the mesh file names
    templates = ['{}.elem', '{}.pts', '{}.diam', '{}.tags', '{}.lon',
                 '{}.ibdry.neubc', '{}.ibdry.surf', '{}.ibdry.surf.vtx',
                 '{}.obdry.neubc', '{}.obdry.surf', '{}.obdry.surf.vtx',
                 '{}.x_axis.vtx', '{}.y_axis.vtx', '{}.z_axis.vtx',
                 '{}.rad.lon', '{}.sph.lon']

    # find all *.elem files in the mesh directory and remove the file extension
    elembases = map(lambda str: str[:-5], glob.glob(os.path.join(MESH_PATH, 'laplaceSphere', '*.elem')))
    meshbases = []
    for basename in elembases:
        # use the templates to create a list of all files which have to exist
        filenames = [template.format(basename) for template in templates]
        # check if all files exist
        filesok = all([os.path.isfile(filename) for filename in filenames])
        # if all file were found add basename to meshbases array
        if filesok:
            meshbases.append(os.path.basename(basename))
    return meshbases


def parser_add_meshopts(parser):
    meshop = parser.add_argument_group('mesh options')

    meshchoices = get_mesh_basenames()
    meshop.add_argument('--meshtype',
                        choices=meshchoices, default=meshchoices[0], type=str,
                        help='choose the mesh type')


def parser_add_materialopts(parser, default='linear'):
    matopt = parser.add_argument_group('material options')

    matopt.add_argument('--material',
                        choices=model.mechanics.keys(), default=default,
                        help='choose the material model')

    def assignment(string):
        k, v = string.split('=')
        return k, float(v)

    matopt.add_argument('--parameters', metavar='PARAM=VAL',
                        nargs='+', default=[], type=assignment,
                        help='set material model parameters')


def parser_add_extraopts(parser):
    extraop = parser.add_argument_group('extra options')

    extraop.add_argument('--novtk',
                         action='store_true', default=False,
                         help='no vtk output')


def parser():
    parser = tools.standard_parser()
    parser_add_meshopts(parser)
    parser_add_materialopts(parser)
    parser_add_extraopts(parser)

    parser.epilog = standardmesh.material_param_string()

    parser.add_argument('--pressure',
                        type=float, default=0.5,
                        help='Pressure to inflate to (kPa)')
    parser.add_argument('--plot',
                        action='store_true',
                        help='Plot the pressure-volume relation')
    return parser


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    mat = standardmesh.material_from_parser(args).slug()
    jobid = '{}_laplace_{}_{}_P{}_{}_np{}'.format(today.isoformat(), args.meshtype, mat, args.pressure, args.flv, args.np)
    return jobid.replace(',', '_')


def mesh_from_parser(args, meshdir, pressure, nbctracefile):
    info = {}

    # Generate and return base name
    meshname = os.path.join(meshdir, args.meshtype)
    orthoname = os.path.join(meshdir, args.meshtype)+'.sph'

    info['meshdir'] = os.path.dirname(meshname)
    info['meshname'] = meshname
    info['orthoname'] = orthoname

    # Fix dirichlet boundary vertices
    dbc = ['-num_mechanic_dbc', 3]
    dbc += ['-mechanic_dbc[0].name', 'x-axis',
            '-mechanic_dbc[0].vtx_file', '{}.x_axis'.format(meshname),
            '-mechanic_dbc[0].bctype', 0,
            '-mechanic_dbc[0].apply_ux', 0,
            '-mechanic_dbc[0].apply_uy', 1,
            '-mechanic_dbc[0].apply_uz', 1]
    dbc += ['-mechanic_dbc[1].name', 'y-axis',
            '-mechanic_dbc[1].vtx_file', '{}.y_axis'.format(meshname),
            '-mechanic_dbc[1].bctype', 0,
            '-mechanic_dbc[1].apply_ux', 1,
            '-mechanic_dbc[1].apply_uy', 0,
            '-mechanic_dbc[1].apply_uz', 1]
    dbc += ['-mechanic_dbc[2].name', 'z-axis',
            '-mechanic_dbc[2].vtx_file', '{}.z_axis'.format(meshname),
            '-mechanic_dbc[2].bctype', 0,
            '-mechanic_dbc[2].apply_ux', 1,
            '-mechanic_dbc[2].apply_uy', 1,
            '-mechanic_dbc[2].apply_uz', 0]

    # Apply pressure neumann boundary conditions on the inner surface
    nbc_name = 'nbc_in'
    nbc = ['-num_mechanic_nbc', 1,
           '-mechanic_nbc[0].name', nbc_name,
           '-mechanic_nbc[0].pressure', pressure,
           '-mechanic_nbc[0].surf_file', '{}.ibdry'.format(meshname),
           '-mechanic_nbc[0].trace', nbctracefile]

    # Store info
    info['dbc']      = dbc
    info['nbc']      = nbc
    info['nbc_name'] = nbc_name

    return info


def print_file(filename):
    """
    Print a text file to the standard output
    :param filename: name of file to read
    """
    if os.path.isfile(filename):
        with open(filename, 'r') as fp:
            print(fp.read())
    else:
        print('Failed to open`{}`'.format(filename))


def read_lines_from_file(filename, linenums, headerlines=0):
    """
    Read certain lines from a text file
    :param filename: name of file to read
    :param linenums: array like object containing the line numbers to read
    :param headerlines: number of header lines to skip
    :return: array of strings or empty array if file does not exist
    """
    lines = []
    if os.path.isfile(filename):
        for num in linenums:
            lines.append(linecache.getline(filename, 1+num+headerlines).strip())
    else:
        print('Failed to open`{}`'.format(filename))
    return lines


def read_dat_file(filename, split=False):
    """
    Read data from a *.dat file
    :param filename: name of file to read
    :param split: split data if several columns are given or not
    :return: array of strings if split is false, array of array
             of strings if split is true or empty array if file
             does not exist
    """
    lines = []
    if os.path.isfile(filename):
        with open(filename, 'r') as fp:
            line = fp.readline().strip()
            while len(line) > 0:
                if not line.startswith('#'):
                    lines.append(line.split() if split else line)
                line = fp.readline()
    else:
        print('Failed to open`{}`'.format(filename))
    return lines


def read_dynpt_file(filename):
   dyndata = {'traces': 0, 'tsteps': 0, 'data': None}
   if os.path.isfile(filename):
      igbobj = igb.IGBFile(filename)
      header = igbobj.header()
      num_traces = header.get('x')
      num_tsteps = header.get('t')
      inc_t = header.get('inc_t')
      dim_t = header.get('dim_t')
      dtype = header.get('type')
      assert (dtype == 'vec3f')
      data = igbobj.data().reshape(num_tsteps, num_traces, 3)
      igbobj.close()
      dyndata['traces'] = num_traces
      dyndata['tsteps'] = num_tsteps
      dyndata['data'] = data
      #dyndata = np.array(data[num_traces*3*(num_tsteps-1):]).reshape(num_traces,3)
   else:
      print('Failed to open`{}`'.format(filename))
   return dyndata


def write(str, fp=None):
    print(str)
    if fp is not None:
        fp.write('{}\n'.format(str))

def mesh_info(meshdir, meshtype, outfp=None):
    diamname = os.path.join(meshdir, meshtype)+'.diam'
    pntsname = os.path.join(meshdir, meshtype)+'.pts'
    with open(diamname, 'r') as fp:
        diampntidx = fp.read()
    diampntidx = map(int, diampntidx.strip().splitlines())
    pnts = map(lambda txt: np.array(map(float, txt.split())), read_lines_from_file(pntsname, diampntidx, 1))
    orad = np.linalg.norm(pnts[1] - pnts[0]) * 0.5
    irad = np.linalg.norm(pnts[3] - pnts[2]) * 0.5
    volm = 4.0 / 3.0 * math.pi * (orad ** 3 - irad ** 3)
    wthk = orad - irad
    write('\n', outfp)
    write(EQUALS_LINE, outfp)
    write(' MESH INFORMATION ({})'.format(meshtype), outfp)
    write(EQUALS_LINE, outfp)
    write('inner radius     (r)  :  {:12.6f} [µm], {:12.6f} [mm], {:12.6f} [cm]'.format(irad, irad*1e-3, irad*1e-4), outfp)
    write('outer radius     (R)  :  {:12.6f} [µm], {:12.6f} [mm], {:12.6f} [cm]'.format(orad, orad*1e-3, orad*1e-4), outfp)
    write('wall thickness   (h)  :  {:12.6f} [µm], {:12.6f} [mm], {:12.6f} [cm]'.format(wthk, wthk*1e-3, wthk*1e-4), outfp)
    write('                         {:12.6f} % of R'.format(100.0 * wthk / orad), outfp)
    write('                         {:12.6f} % of r'.format(100.0 * wthk / irad), outfp)
    write('volume           (v)  :  {:12.6f} [ml]'.format(volm*1e-12), outfp)
    write(EQUALS_LINE, outfp)
    return {'r':irad*1e-3, 'R':orad*1e-3, 'h':wthk*1e-3, 'v':volm*1e-12}


def determine_pressure_radius_relation(meshdir, meshtype, jobID, tend, tstep, pressure):
   assert ((tend / tstep).is_integer())
   numtsteps = int(tend / tstep)+1
   diamname = os.path.join(meshdir, meshtype) + '.diam'
   with open(diamname, 'r') as fp:
      diampntidx = fp.read()
   diampntidx = map(int, diampntidx.strip().splitlines())
   pnts = [None] * len(list(diampntidx))
   dyndata = read_dynpt_file(os.path.join(jobID, 'x.dynpt'))
   assert (numtsteps == dyndata['tsteps'])
   rel = []
   for i in xrange(0, numtsteps):
      dynpt = dyndata['data'][i]
      for cnt in xrange(0, len(list(diampntidx))):
         pnts[cnt] = dynpt[diampntidx[cnt]]
      orad = np.linalg.norm(pnts[1] - pnts[0]) * 0.5  # [µm]
      irad = np.linalg.norm(pnts[3] - pnts[2]) * 0.5  # [µm]
      rel.append([i, irad*1e-3, orad*1e-3, pressure*i*tstep/tend])
   return rel


def laplace_stress_info(meshdir, meshtype, jobID, pressure, material, outfp=None):
    diamname = os.path.join(meshdir, meshtype)+'.diam'
    #pntsname = os.path.join(meshdir, meshtype)+'.pts'
    with open(diamname, 'r') as fp:
        diampntidx = fp.read()
    diampntidx = map(int, diampntidx.strip().splitlines())
    #pnts = map(lambda txt: np.array(map(float, txt.split())), read_lines_from_file(pntsname, diampntidx, 1))
    pnts = [None]*len(diampntidx)
    dyndata = read_dynpt_file(os.path.join(jobID, 'x.dynpt'))
    dynpt = dyndata['data'][-1]
    for cnt in xrange(0, len(diampntidx)):
        pnts[cnt] = dynpt[diampntidx[cnt]]
    orad = np.linalg.norm(pnts[1] - pnts[0]) * 0.5 # [µm]
    irad = np.linalg.norm(pnts[3] - pnts[2]) * 0.5 # [µm]
    wthk = orad - irad  # [µm]
    volm = 4.0 / 3.0 * math.pi * (orad ** 3 - irad ** 3)  # [µm^3]
    sigma_lpl1 = 0.5 * pressure * irad / wthk
    sigma_lpl2 = 0.5 * pressure * irad / (wthk * (1.0+wthk*0.5/irad))
    write('\n', outfp)
    write(EQUALS_LINE, outfp)
    write(' LAW OF LAPLACE', outfp)
    write(EQUALS_LINE, outfp)
    write('inner radius     (r)    :  {:12.6f} [µm], {:12.6f} [mm], {:12.6f} [cm]'.format(irad, irad*1e-3, irad*1e-4), outfp)
    write('outer radius     (R)    :  {:12.6f} [µm], {:12.6f} [mm], {:12.6f} [cm]'.format(orad, orad*1e-3, orad*1e-4), outfp)
    write('wall thickness   (h)    :  {:12.6f} [µm], {:12.6f} [mm], {:12.6f} [cm]'.format(wthk, wthk*1e-3, wthk*1e-4), outfp)
    write('                           {:12.6f} % of R'.format(100.0 * wthk / orad), outfp)
    write('                           {:12.6f} % of r'.format(100.0 * wthk / irad), outfp)
    write('volume           (v)    :  {:12.6f} [ml]'.format(volm*1e-12), outfp)
    write('applied pressure (p)    :  {:12.6f} [kPa]'.format(pressure), outfp)
    write('sigma_lpl        (sl1)  :  {:12.6f} [kPa]'.format(sigma_lpl1), outfp)
    write('sigma_lpl        (sl2)  :  {:12.6f} [kPa]'.format(sigma_lpl2), outfp)
    write(DASHED_LINE, outfp)
    write('       p * r                          p * r           ', outfp)
    write('sl1 = -------        sl2 = ---------------------------', outfp)
    write('       2 * h               2 * h * ( 1 + h / (2 * r) )', outfp)
    if material in ['linear', 'stvenantkirchhoff']:
        write('\n', outfp)
        write(DASHED_LINE, outfp)
        write('--->  !!ATTENTION!! law of laplace is not valid for {} material models  <---'.format(material), outfp)
        write(DASHED_LINE, outfp)
    write(EQUALS_LINE, outfp)
    return {'r': irad*1e-3, 'R': orad*1e-3, 'h': wthk*1e-3, 'v': volm*1e-12, 's1': sigma_lpl1, 's2':sigma_lpl2}


def simulated_stress_info(filename, outfp=None):
    lines = read_dat_file(filename, True)
    sigma_crad = float(lines[0][1])
    sigma_cphi = float(lines[8][1])
    sigma_ctht = float(lines[4][1])
    write('\n', outfp)
    write(EQUALS_LINE, outfp)
    write(' SIMULATED STRESS', outfp)
    write(EQUALS_LINE, outfp)
    write('sigma_radial (scr)  :  {:12.6f} [kPa]'.format(sigma_crad), outfp)
    write('sigma_phi    (scp)  :  {:12.6f} [kPa]'.format(sigma_cphi), outfp)
    write('sigma_theta  (sct)  :  {:12.6f} [kPa]'.format(sigma_ctht), outfp)
    write(EQUALS_LINE, outfp)
    return {'sr': sigma_crad, 'sp': sigma_cphi, 'st': sigma_ctht}


@tools.carpexample(parser, jobID)
def run(args, job):

    dt              = 100.0 # [µs]
    tend            = 100.0 # [ms]
    tstep           = 1.0   # [ms]
    assert ((tend / tstep).is_integer())
    assert ((tstep / (dt * 1.0e-3)).is_integer())
    lstep           = int(tend / tstep)
    mstep           = tstep
    ostep           = tstep
    nb_trace_file   = 'nbc_pressure100'
    # Additional solution settings
    configuration   = 1     # 0 Euler, 1 Lagrangian
    line_search     = 0     # use line-search method in Newton
    tangent_mode    = 0
    log_level       = -1    # -1 no output, 0,1 partial output, 2 full output

    # Generate mesh
    meshdir  = os.path.join(MESH_PATH, 'laplaceSphere')
    meshinfo = mesh_from_parser(args, meshdir, args.pressure, nb_trace_file)

    # Switch off EP
    ep = ['-num_imp_regions',       1,
          '-imp_region[0].name',    'passive',
          '-imp_region[0].im',      'PASSIVE',
          '-imp_region[0].num_IDs', 100,
          '-num_stim',              0]

    # Get material
    mat = standardmesh.material_from_parser(args)
    if isinstance(mat, model.mechanics.GuccioneMaterial):
        tangent_mode = 1

    # Add all the non-general arguments
    cmd  = tools.carp_cmd()

    cmd += ['-meshname',                meshinfo['meshname'],
            '-orthoname',               meshinfo['orthoname'],
            '-dt',                      dt,
            '-simID',                   job.ID,
            '-timedt',                  tstep,
            '-mechDT',                  mstep,
            '-spacedt',                 ostep,
            '-tend',                    tend,
            '-newton_adaptive_tol_mech', 0,
            '-mech_configuration',      configuration,
            '-mech_finite_element',     0, # 0 for P1-P0
            '-mech_tangent_comp',       tangent_mode,
            '-localize_pts',            1,
            '-volumeTracking',          1,
            '-mech_output',             1,   # 1 igb, 4 ascii vtk
            '-mech_output_domain',      1,   # 1 domain, 2 surface
            '-work_value',              2,   # 2 compute work element wise
            '-stress_value',            20]  # 4 .. Compute first principal stress, 16 .. Compute stress tensor

    if args.visualize:
        cmd += ['-gridout_i',           3]

    # Add boundary conditions, materials and ep options
    cmd += ep
    cmd += model.optionlist([mat])
    cmd += meshinfo['nbc']
    cmd += meshinfo['dbc']

    # Set some solver options
    cmd += ['-parab_solve',        1,
            '-localize_pts',       1,
            '-krylov_tol_mech',    1e-8,
            '-krylov_maxit_mech',  10000,
            '-newton_tol_mech',    1e-6,
            '-newton_line_search', line_search]

    # Run simulation
    job.carp(cmd)

    prrel = determine_pressure_radius_relation(meshdir, args.meshtype, job.ID, tend, tstep, args.pressure)
    volume, lplstress, extlplstress = [], [], []

    with open(os.path.join(job.ID, 'pres_rad.dat'), 'w') as fp:
       fp.write('# step \t irad \t orad \t wall \t pressure \t laplace \t ext-laplace \n')
       for rel in prrel:
          irad = rel[1]
          orad = rel[2]
          wall = rel[2]-rel[1]
          volume.append(4.0/3.0*math.pi*(orad**3 - irad**3))
          pressure = rel[3]
          lplstress.append(0.5 * pressure * irad / wall)
          extlplstress.append(pressure * irad / (2.0 * wall + wall ** 2 / irad))
          fp.write('{} \t {} \t {} \t {} \t {} \t {} \t {}\n'.format(rel[0], irad, orad, wall, pressure, lplstress[-1], extlplstress[-1]))

    # Compute Mean Stress Tensor in Cartesian coordinate system
    cmd = ['GlIGBProcess']
    cmd += ['-m', meshinfo['meshname'],
            '-s', 1e-3,        # mesh is in µm -> mm
            '-i', os.path.join(job.ID, 'stressTensor.igb'),
            '-u', os.path.join(job.ID, 'x.dynpt'),
            '-T', 'amax',
            '-S', 'avint',
            '-v', log_level if log_level in [0, 1, 2] else 0,
            '-o', os.path.join(job.ID, 'stress'),
            '--spdata']
    job.bash(cmd)
    if log_level in [0,1]:
        print_file(os.path.join(job.ID, 'stress.dat'))

    # Compute Mean Stress Tensor in Spherical coordinate system
    cmd = ['GlIGBProcess']
    cmd += ['-m', meshinfo['meshname'],
            '-s', 1e-3,        # mesh is in µm -> mm
            '-i', os.path.join(job.ID, 'stressTensor.igb'),
            '-l', meshinfo['orthoname']+'.lon',
            '-u', os.path.join(job.ID, 'x.dynpt'),
            '-T', 'last',
            '-S', 'avint',
            '-v', log_level if log_level in [0, 1, 2] else 0,
            '-o', os.path.join(job.ID, 'projstress'),
            '--dumpigb',
            '--spdata']
    job.bash(cmd)
    if log_level in [0, 1]:
        print_file(os.path.join(job.ID, 'projstress.dat'))

    # Compute Total Work
    cmd = ['GlIGBProcess']
    cmd += ['-m', meshinfo['meshname'],
            '-s', 1e-3,        # mesh is in µm -> mm
            '-t', tstep*1e-3,  # data time steps in `tstep` ms -> s
            '-i', os.path.join(job.ID, 'elemWork.igb'),
            '-T', 'int',
            '-S', 'int',
            '-v', log_level if log_level in [0, 1, 2] else 0,
            '-o', os.path.join(job.ID, 'work')]
    job.bash(cmd)
    if log_level in [0, 1]:
        print_file(os.path.join(job.ID, 'work.dat'))

    if not args.novtk:
        # Create VTK Files
        job.mkdir(os.path.join(job.ID, 'vtk'))
        cmd = ['GlVTKConvert']
        cmd += ['-m', meshinfo['meshname'],
                '-e', os.path.join(job.ID, 'stressTensor.igb'),
                '-e', os.path.join(job.ID, 'stressTensor.dump.igb'),
                '-e', os.path.join(job.ID, 'elemWork.igb'),
                '-u', os.path.join(job.ID, 'x.dynpt'),
                '-l', meshinfo['orthoname']+'.lon',
                '-s', 0.001,  # scale from um to mm
                '-o', os.path.join(job.ID, 'vtk', 'stress')]
        job.bash(cmd)

    if not args.dry:
        info = {}
        outfp = open(os.path.join(job.ID, 'laplace.out'), 'w')
        info['mesh'] = mesh_info(meshdir, args.meshtype, outfp)
        info['lpls'] = laplace_stress_info(meshdir, args.meshtype, job.ID, args.pressure, args.material, outfp)
        info['sims'] = simulated_stress_info(os.path.join(job.ID, 'projstress.dat'), outfp)
        irad0 = info['mesh']['r']
        irad1 = info['lpls']['r']
        extwork = args.pressure*4.0/3.0*math.pi*(irad1**3-irad0**3)  # [kPa] * [mm^3]
        extwork *= (irad0**2+2*irad0*irad1+3*irad1**2)/(4*irad1**2+4*irad0*irad1+4*irad0**2)
        extwork *= 1.0e-6
        eps = irad1/irad0 - 1.0

        extwork2 = 0.0
        numprrel = len(prrel)
        for i in range(1, numprrel):
           p0, p1 = prrel[i-1][3], prrel[i][3]
           r0, r1 = prrel[i-1][1], prrel[i][1]
           extwork2 += (p1-p0)/(12.0*(r1-r0))*(3.0*(r1**4)-4.0*(r1**3)*r0+r0**4)+p0/3.0*(r1**3-r0**3)
        extwork2 *= 4.0 * math.pi * 1.0e-6

        intwork = float(read_dat_file(os.path.join(job.ID, 'work.dat'), True)[0][1])
        intworkj = info['lpls']['v'] * info['lpls']['s1'] * 0.5 * 1.0e-3
        #intworkl = info['lpls']['v'] * info['lpls']['s1'] * eps * 0.5 * 1.0e-3
        #intworkw = info['lpls']['v'] * info['lpls']['s2'] * eps * 0.5 * 1.0e-3

        #write(' WORK (OLD)', outfp)
        #write('int-work1 (iw1)  :  {:12.6f} [J]'.format(intworkl), outfp)
        #write('int-work2 (iw2)  :  {:12.6f} [J]'.format(intworkw), outfp)

        intworkl, intworkw = 0, 0

        for i in range(1, numprrel):
           #r0, r1 = 0.5*(prrel[i-1][1]+prrel[i-1][2]), 0.5*(prrel[i][1]+prrel[i][2])
           ir0, ir1 = prrel[i-1][1], prrel[i][1]
           or0, or1 = prrel[i-1][2], prrel[i][2]
           v0, v1 = volume[i-1], volume[i]
           s0, s1 = lplstress[i-1], lplstress[i]
           #intworkl += 2.0/(3.0*0.5*(prrel[0][1]+prrel[0][2]))*(r1-r0)*((s1+0.5*s0)*(v1+0.5*v0)+0.75*s0*v0)
           intworkl += 1.0/3.0*((ir1-ir0)/prrel[0][1]+(or1-or0)/prrel[0][2])*((s1+0.5*s0)*(v1+0.5*v0)+0.75*s0*v0)
           s0, s1 = extlplstress[i-1], extlplstress[i]
           #intworkw += 2.0/(3.0*0.5*(prrel[0][1]+prrel[0][2]))*(r1-r0)*((s1+0.5*s0)*(v1+0.5*v0)+0.75*s0*v0)
           intworkw += 1.0/3.0*((ir1-ir0)/prrel[0][1]+(or1-or0)/prrel[0][2])*((s1+0.5*s0)*(v1+0.5*v0)+0.75*s0*v0)

        write('\n', outfp)
        write(EQUALS_LINE, outfp)
        write(' WORK', outfp)
        write(EQUALS_LINE, outfp)
        #write('ext-work  (ew)   :  {:12.6f} [J] (linear radius-pressure-relation)'.format(extwork), outfp)
        write('ext-work2 (ew)   :  {:12.6f} [J] (piecewise linear radius-pressure-relation)'.format(extwork2), outfp)
        write('int-work  (iw)   :  {:12.6f} [J] (FEM)'.format(intwork), outfp)
        write('int-work0 (iw0)  :  {:12.6f} [J] (Joao`s formula)'.format(intworkj), outfp)
        write('int-work1 (iw1)  :  {:12.6f} [J] (extended Joao`s formula, simple Laplace)'.format(intworkl*1.0e-6), outfp)
        write('int-work2 (iw2)  :  {:12.6f} [J] (extended Joao`s formula, extended Laplace)'.format(intworkw*1.0e-6), outfp)
        '''
        write('abs-error (aerr) :  {:12.6f}'.format(abs(extwork - intwork)), outfp)
        write('rel-error (rerr) :  {:12.6f}'.format(abs(extwork-intwork)/abs(extwork)), outfp)
        write(DASHED_LINE, outfp)
        write('        v                                  v                      ', outfp)
        write('iw1 = ------ * sl1 * 10^3 * eps   iw2 =  ------ * sl2 * 10^3 * eps', outfp)
        write('       10^6                               10^6                    ', outfp)
        write('with eps = ( r(T) - r(0) ) / r(0) = {:12.6f}.'.format(eps), outfp)
        write(DASHED_LINE, outfp)
        write('                                           aerr ', outfp)
        write('aerr =  | ew - iw |               rerr =  ------', outfp)
        write('                                          | ew |', outfp)
        '''
        write(EQUALS_LINE, outfp)
        sigma_sim = (info['sims']['sr'], info['sims']['sp'], info['sims']['st'])
        sigma_lpl = (info['lpls']['s1'], info['lpls']['s2'])
        write('\n', outfp)
        write(EQUALS_LINE, outfp)
        write(' RELATIVE ERROR', outfp)
        write(EQUALS_LINE, outfp)
        write('rel-error (err1) :  {:12.6f}'.format(math.fabs(sigma_sim[1] - sigma_lpl[0]) / math.fabs(sigma_lpl[0])), outfp)
        write('rel-error (err2) :  {:12.6f}'.format(math.fabs(sigma_sim[1] - sigma_lpl[1]) / math.fabs(sigma_lpl[1])), outfp)
        write(DASHED_LINE, outfp)
        write('        | scp - sl1 |             | scp - sl2 |', outfp)
        write('err1 =  -------------     err2 =  -------------', outfp)
        write('           | sl1 |                   | sl2 |   ', outfp)
        write(EQUALS_LINE, outfp)
        outfp.close()

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        mesh_i = os.path.basename(meshinfo['meshname']) + '_i'

        geom   = os.path.join(job.ID, mesh_i)
        data   = os.path.join(job.ID, 'firstPrincipalStress.igb.gz')
        deform = os.path.join(job.ID, 'x.dynpt')

        job.gunzip(deform)
        job.meshalyzer(geom, data, deform)

    if args.plot and not settings.platform.BATCH and not args.dry:
        fig = pyplot.figure()
        fig.suptitle("Volume-Pressure", fontsize=11)
        ax = fig.gca()
        ax.set_xlabel("Volume [ml]")
        ax.set_ylabel("Pressure [kPa]")

        ax.plot(1.0e-3*4.0/3.0*math.pi*np.array(prrel).T[1]**3, np.array(prrel).T[3])
        pyplot.show()


if __name__ == '__main__':
    run()

#!/usr/bin/env python
"""

.. _mech-bc-tutorial:

This tutorial demonstrates the time-dependent application of different boundary condition types, i.e., Dirichlet and Neumann boundary conditions, on a cuboidal geometry. For more details on the definition of mechanical boundary conditions in :ref:`carpentry <carpentry>` see manual section :ref:`mechanical boundary conditions <mech-boundary-conditions>`.

Geometry and Material Definition
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A simple tetrahedral finite element mesh of a cube shown in :numref:`fig-schematic` with side length :math:`a = 1.0` mm and resolution :math:`r = 0.1` mm as default is generated. The cube is assigned a neo-Hookean material model with shear modulus :math:`\mu = 10` kPa and penalty parameter :math:`K = 1000` kPa.

.. _fig-schematic:

.. figure:: /images/MechanicalBoundaryConditions_schematics.png
   :width: 600pt
   :align: center

   Schematic representation of the cuboidal geometry including Dirichlet (left panel) and Neumann (right panel) boundary conditions subsequently introduced in Experiment I and Experiment II, respectively.

Boundary Condition Application
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For the experiments I & II, movement of nodes located at :math:`x = -0.5 a` is restricted in all directions, i.e., homogeneous Dirichlet boundary conditions are applied (see left and right panels in :numref:`fig-schematic`).

**Experiment I - Dirichlet Boundary Conditions**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Experiment I provides guidance on the consistent application of inhomogeneous Dirichlet boundary conditions (see left panel in :numref:`fig-schematic`). Nodes located at :math:`x = 0.5 a` are translated under application of displacement :math:`\mathbf{u} = [u_{1},0,0]^{\mathrm{T}}` utilizing the command line

.. code-block:: bash

   ./run.py --experiment dirichlet --sidelength 1.00 --resolution 0.10 --magnitude 0.10 --visualize

where the parameter **experiment** selects the correct boundary condition type, parameters **sidelength** and **resolution** provide an optional input for the mesh generation and **magnitude** defines the displacement :math:`u_{1}^{\mathrm{s}}` in mm. The application of inhomogeneous Dirichlet boundary conditions is time-dependent and necessitates existence of a specific trace file (.trc), in which the time course for the selected displacement is provided calculated as :math:`\mathbf{u} = \phi \mathbf{u}^{\mathrm{s}} = \phi [u_{1}^{\mathrm{s}},u_{2}^{\mathrm{s}},u_{3}^{\mathrm{s}}]^{\mathrm{T}}`, where :math:`\phi` represents the scale factor (see :numref:`fig-traces`).

.. note:: Node selection for application of Dirichlet boundary conditions is currently based on internal functions, but can alternatively be performed utilizing a vertex file (.vtx).

**Experiment II - Neumann Boundary Conditions**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Experiment II provides guidance on the consistent application of Neumann boundary conditions (see right panel in :numref:`fig-schematic`). Nodes located at :math:`x = 0.5 a` are translated under application of pressure :math:`p` on the corresponding surface triangles utilizing the command line

.. code-block:: bash

   ./run.py --experiment neumann --sidelength 1.00 --resolution 0.10 --magnitude 5.00 --visualize

where the parameter **experiment** selects the correct boundary condition type, parameters **sidelength** and **resolution** provide an optional input for the mesh generation and **magnitude** defines the applied pressure :math:`p` in kPa. Again, the application of Neumann boundary conditions is time-dependent and necessitates existence of a specific trace file (.trc), in which the time course for the selected pressure is provided calculated as :math:`p = \phi p`, where :math:`\phi` represents the scale factor (see :numref:`fig-traces`).

.. note:: Element selection for application of Neumann boundary conditions is currently based on internal fuctions, but can alternatively be performed utilizing a surface element file (.neubc).

.. _fig-traces:

.. figure:: /images/MechanicalBoundaryConditions_traces.png
   :width: 400pt
   :align: center

   Dirichlet (blue) and Neumann (red) boundary condition application traces indicating the time-dependent application using a scale factor :math:`\phi` during simulation specified in corresponding trace files (.trc).

**Experimental Results**
^^^^^^^^^^^^^^^^^^^^^^^^

Upon application of the command lines introduced above, results provided in :numref:`fig-results` will be obtained. While in Experiment I inhomogeneous Dirichlet boundary conditions are consistently stretching the cube, Neumann boundary conditions in Experiment II administer both, compression and tension on the cube (compare boundary condition application traces in :numref:`fig-traces`).

.. _fig-results:

.. figure:: /images/MechanicalBoundaryConditions_results.gif
   :width: 600pt
   :align: center


   Results obtained upon application of command lines introduced in Experiment I using Dirichlet boundary conditions (left panel) and Experiment II using Neumann boundary conditions (right panel) with the color-code representing displacement in :math:`1`-direction (top row) and :math:`1^{\mathrm{st}}` principal stress (bottom row).


For the experiments III & IV, the plane at :math:`x = -0.5 a` is attached to a spring acting in all directions, i.e., homogeneous Robin boundary conditions are applied.

**Experiment III - Dirichlet Boundary Conditions**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Experiment III provides guidance on the consistent application of inhomogeneous Dirichlet boundary conditions (see left panel in :numref:`fig-schematic`). Nodes located at :math:`x = 0.5 a` are translated under application of displacement :math:`\mathbf{u} = [u_{1},0,0]^{\mathrm{T}}` utilizing the command line

.. code-block:: bash

   ./run.py --experiment dirichlet --fixation weak --sidelength 1.00 --resolution 0.10 --magnitude 0.10 --visualize

where the parameter **experiment** selects the correct boundary condition type, parameters **sidelength** and **resolution** provide an optional input for the mesh generation and **magnitude** defines the displacement :math:`u_{1}^{\mathrm{s}}` in mm. The application of inhomogeneous Dirichlet boundary conditions is time-dependent and necessitates existence of a specific trace file (.trc), in which the time course for the selected displacement is provided calculated as :math:`\mathbf{u} = \phi \mathbf{u}^{\mathrm{s}} = \phi [u_{1}^{\mathrm{s}},u_{2}^{\mathrm{s}},u_{3}^{\mathrm{s}}]^{\mathrm{T}}`, where :math:`\phi` represents the scale factor (see :numref:`fig-traces`).

.. note:: Node selection for application of Dirichlet boundary conditions is currently based on internal functions, but can alternatively be performed utilizing a vertex file (.vtx).

**Experiment IV - Neumann Boundary Conditions**
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Experiment IV provides guidance on the consistent application of Neumann boundary conditions (see right panel in :numref:`fig-schematic`). Nodes located at :math:`x = 0.5 a` are translated under application of pressure :math:`p` on the corresponding surface triangles utilizing the command line

.. code-block:: bash

   ./run.py --experiment neumann --fixation weak --sidelength 1.00 --resolution 0.10 --magnitude 5.00 --visualize

where the parameter **experiment** selects the correct boundary condition type, parameters **sidelength** and **resolution** provide an optional input for the mesh generation and **magnitude** defines the applied pressure :math:`p` in kPa. Again, the application of Neumann boundary conditions is time-dependent and necessitates existence of a specific trace file (.trc), in which the time course for the selected pressure is provided calculated as :math:`p = \phi p`, where :math:`\phi` represents the scale factor (see :numref:`fig-traces`).

.. note:: Element selection for application of Neumann boundary conditions is currently based on internal fuctions, but can alternatively be performed utilizing a surface element file (.neubc).

**Experimental Results**
^^^^^^^^^^^^^^^^^^^^^^^^

Upon application of the command lines introduced above, results provided in :numref:`fig-results2` will be obtained. While in Experiment III inhomogeneous Dirichlet boundary conditions are consistently stretching the cube, Neumann boundary conditions in Experiment IV administer both, compression and tension on the cube (compare boundary condition application traces in :numref:`fig-traces`).

.. _fig-results2:

.. figure:: /images/MechanicalBoundaryConditions2_results.gif
   :width: 600pt
   :align: center


   Results obtained upon application of command lines introduced in Experiment III using Dirichlet boundary conditions (left panel) and Experiment IV using Neumann boundary conditions (right panel) with the color-code representing displacement in :math:`1`-direction (top row) and :math:`1^{\mathrm{st}}` principal stress (bottom row).

"""

EXAMPLE_DESCRIPTIVE_NAME = "Mechanical Boundary Conditions"
EXAMPLE_AUTHOR = ("Christoph Augustin <christoph.augustin@medunigraz.at> and"
                  "Matthias Gsell <matthias.gsell@medunigraz.at>")

import os
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import model

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--experiment',
                        default='dirichlet',
                        choices=['dirichlet', 'neumann'],
                        help='pick experiment type')
    group.add_argument('--fixation',
                        default='strong',
                        choices=['strong', 'weak'],
                        help='choose fixation of the bottom')
    group.add_argument('--sidelength',
                        type=float, default=1.0,
                        help='choose side length of the cube in [mm]')
    group.add_argument('--resolution',
                        type=float, default=0.1,
                        help='choose mesh resolution in [mm]')
    group.add_argument('--magnitude',
                        type=float,
                        help='magnitude of boundary conditions (dirichlet: [mm]; neumann: [kPa]')
    group.add_argument('--duration',
                        type=float, default=500.0,
                        help='duration of simulation [ms]')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    tpl = '{}_{}_{}_{}_GA_np{}' if args.mech_with_inertia else '{}_{}_{}_{}_np{}'
    return tpl.format(today.isoformat(), args.experiment,
                                     args.mech_element, args.flv, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

# --- Generate finite element mesh ---------------------------------------------

    sl = args.sidelength

    # define cube side length and resolution
    geom = mesh.Block(size=(sl,sl,sl), resolution=args.resolution)

    # define fiber angle (0) and sheet angle (90)
    geom.set_fibres(60, -60, 90, 90)

    # generate finite element mesh
    meshname = mesh.generate(geom)

# --- Specify material model ---------------------------------------------------

    # define penalty parameter
    if args.mech_element == 'P1-P0':
        kappa = 1000		# penalty formulation
    else:
        kappa = 10e100		# nearly incompressible

    # define material model
    material = model.mechanics.NeoHookeanMaterial([1], 'tissue', kappa=kappa, c=10.0)
    tangent_mode = 0        	# use exact (symbolic) tangent computation



    # dirichlet boundary conditions
    if args.experiment == 'dirichlet':

        displ = 250 if args.magnitude is None else args.magnitude*1000
        spring = 0.05

        # specify tolerances
        tol_mech = 1e-07
        tol_mech_type = 1

        if args.fixation == 'strong':
            # --- Specify boundary conditions ----------------------------------------------
            dbcx0 = mesh.block_boundary_condition(geom, 'mechanic_dbc', 0, 'x', True)
            dbcx1 = mesh.block_boundary_condition(geom, 'mechanic_dbc', 1, 'x', False)

            # number of Dirichlet BCs
            bc = ['-num_mechanic_dbc', 2]
            # homogeneous
            bc += dbc_homogeneous(dbcx0, 0, 'bottom')
            # inhomogeneous
            bc += dbc_inhomogeneous(dbcx1, 1, 'top', 'traces/dbc_cube', displ, 'x')

            tol_mech = 1e-07
            tol_mech_type = 1

        elif args.fixation == 'weak':
            # --- Specify boundary conditions ----------------------------------------------
            nbcx0 = mesh.block_boundary_condition(geom, 'mechanic_nbc', 0, 'x', True)
            dbcx1 = mesh.block_boundary_condition(geom, 'mechanic_dbc', 0, 'x', False)

            # define boundary spring
            bc = ['-num_mechanic_bs', 1]
            bc += bs(0, spring)

            # number of Neumann BCs
            bc += ['-num_mechanic_nbc', 1]
            # fix bottom weakly
            bc += nbc(nbcx0, 0, 'bottom', 'traces/nbc_cube', 0.0, 0)

            # number of Dirichlet BCs
            bc += ['-num_mechanic_dbc', 1]
            # Move top
            bc += dbc_inhomogeneous(dbcx1, 0, 'top', 'traces/dbc_cube', displ, 'x')

        else:
            raise Exception('invalid fixation')

    # neumann boundary conditions
    elif args.experiment == 'neumann':

        pressure = 10 if args.magnitude is None else args.magnitude
        spring = 0.05

        # specify tolerances
        tol_mech = 1e-08
        tol_mech_type = 1

        if args.fixation == 'strong':
            # --- Specify boundary conditions ----------------------------------------------
            dbcx0 = mesh.block_boundary_condition(geom, 'mechanic_dbc', 0, 'x', True)
            nbcx1 = mesh.block_boundary_condition(geom, 'mechanic_nbc', 0, 'x', False)

            # number of Dirichlet BCs
            bc = ['-num_mechanic_dbc', 1]
            # fix bottom strongly
            bc += dbc_homogeneous(dbcx0, 0, 'bottom')

            # number of Neumann BCs
            bc += ['-num_mechanic_nbc', 1]
            # Apply pressure to top
            bc += nbc(nbcx1, 0, 'top', 'traces/nbc_cube', pressure, -1)

        elif args.fixation == 'weak':
            # --- Specify boundary conditions ----------------------------------------------
            nbcx0 = mesh.block_boundary_condition(geom, 'mechanic_nbc', 0, 'x', True)
            nbcx1 = mesh.block_boundary_condition(geom, 'mechanic_nbc', 1, 'x', False)

            # define boundary spring
            bc = ['-num_mechanic_bs', 1]
            bc += bs(0, spring)

            # number of Neumann BCs
            bc += ['-num_mechanic_nbc', 2]
            # fix bottom weakly
            bc += nbc(nbcx0, 0, 'bottom', 'traces/nbc_cube', 0.0, 0)
            # Apply pressure to top
            bc += nbc(nbcx1, 1, 'top', 'traces/nbc_cube', pressure, -1)

        else:
            raise Exception('invalid fixation')

    else:
        raise Exception('invalid experiment')

# --- Mechanical simulation ----------------------------------------------------

    # switch off electrics
    ep = ['-num_imp_regions',       1,
          '-imp_region[0].name',    'passive',
          '-imp_region[0].im',      'PASSIVE',
          '-imp_region[0].num_IDs', 1,
          '-num_stim',              0]

# --- Build command line argument ----------------------------------------------

    # include basic command line (including solver options)
    cmd = tools.carp_cmd()

    # include timers and options
    tstep = min(5.0, args.duration)
    cmd += ['-simID',                    job.ID,
            '-meshname',                 meshname,
            '-timedt',                   tstep,
            '-mechDT',                   tstep,
            '-spacedt',                  tstep,
            '-tend',                     args.duration,
            '-mapping_mode',             1,
            '-redist_extracted',         0,
            '-mass_lumping',             1,
            '-display_meminfo',          0,
            '-mech_tangent_comp',        tangent_mode,
            '-mech_configuration',       1,
            '-newton_line_search',       0,
            '-newton_maxit_mech',        50,
            '-newton_tol_mech',          tol_mech,
            '-newton_adaptive_tol_mech', 1,
            '-newton_tol_mech_type',     tol_mech_type]


    if args.mech_with_inertia:
        cmd += ['-mech_activate_inertia', 1]

    # include solver settings
    cmd += ['-parab_solve',        1,
            '-localize_pts',       1,
            '-krylov_tol_mech',    1e-10]

    # include volume tracking
    cmd += ['-volumeTracking',     1,
            '-numElemVols',        1,
            '-elemVols[0].name',   'block']

    # include stabilization
    # 1: rank one stabilization
    # 2: use mass matrix as preconditioner
    # 4: volumetric stabilization
    cmd += ['-mech_stabilization', 0]

    # switch off electrics
    cmd += ep

    # include material model
    cmd += model.optionlist([material])

    # include boundary conditions
    cmd += bc

    # include output options
    cmd +=  ['-mech_output',        3,	   # 1 igb, 8 binary vtk
             '-mech_output_domain', 1,     # 1 domain, 2 surface
             '-stress_value',       4,     # nodal first principal stress
             '-strain_value',       0,
             '-work_value',         0]

    if args.visualize:
        cmd += ['-gridout_i',  3]

    # run carp
    job.carp(cmd)

    # run meshalyzer visualization
    if args.visualize and not settings.platform.BATCH:

        # prepare file paths
        view = 'em_boundary_first_stresses.mshz'
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'firstPrincipalStress.igb')
        deform = os.path.join(job.ID, 'x.dynpt')

        # call meshalyzer
        job.meshalyzer(geom, data, deform, view)

# --- Homogeneous Dirichlet boundary conditions --------------------------------

def dbc_homogeneous(face, idx, name):
    """
    Generate homogeneous Dirichlet boundary condition.
    """

    base = '-mechanic_dbc[{}].'.format(idx)

    dbc = [base + 'name',       name,
           base + 'bctype',     0,
           base + 'apply_ux',   1,
           base + 'apply_uy',   1,
           base + 'apply_uz',   1,
           base + 'dump_nodes', 1]

    dbc += face

    return dbc

# --- Inhomogeneous Dirichlet boundary condition -------------------------------

def dbc_inhomogeneous(face, idx, name, trace, magnitude, direction):
    """
    Generate inhomogeneous Dirichlet boundary condition.
    """

    assert direction in ['x', 'y', 'z']

    base = '-mechanic_dbc[{}].'.format(idx)

    dbc = [base + 'name',     name,
           base + 'bctype',   1,
           base + 'apply_ux', 1,
           base + 'apply_uy', 1,
           base + 'apply_uz', 1]

    for c in ['x', 'y', 'z']:
        dbc += [base + 'u' + c, magnitude if c == direction else 0]

    dbc += [base + 'u{}_file'.format(direction), trace,
            base + 'dump_nodes',                 1]

    dbc += face

    return dbc

# --- Neumann boundary condition -----------------------------------------------

def nbc(face, idx, name, trace, magnitude, spring_idx):
    """
    Generate Neumann boundary condition.
    """

    base = '-mechanic_nbc[{}].'.format(idx)

    nbc = [base + 'name',            name,
           base + 'pressure',        magnitude,
           base + 'trace',           trace,
           base + 'spring_idx',      spring_idx]
#           base + 'load_spring_idx', spring_idx]

    nbc += face

    return nbc

# --- Mechanical boundary spring -----------------------------------------------

def bs(idx, spring):
    """
    Generate boundary spring.
    """

    base = '-mechanic_bs[{}].'.format(idx)

    bs = [base + 'value',   spring,
          base + 'edidx',   -1]

    return bs

if __name__ == '__main__':
    run()

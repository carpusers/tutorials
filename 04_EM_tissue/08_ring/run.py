#!/usr/bin/env python
r"""

.. _emring:

This example provides pure mechanics and electromechanics examples on a simple
ring geometry.

Problem Setup
=============

This problem generates a simple ring mesh using the
:class:`carputils.mesh.Ring` class. The ring is tessellated into tetrahedra as
shown below:

.. _fig-tutorial-ring-mesh:

.. figure:: /images/ring_mesh.png
    :width: 45%
    :align: center

    Automatically generated (CGAL-based) mesh of a LV ring model.

In all experiment types in this example, the top and bottom surfaces of the
ring are constrained to lie in the same plane with Dirichlet boundary
conditions, and an additional three nodes on the bottom (:math:`z=0`) surface
are constrained such that free body rotation and translation is prevented.
Two nodes on the x axis are prevented from moving in the y direction,
and one node on the y axis is prevented from moving in the x direction:

.. _fig-tutorial-ring-rigid-bc:

.. figure:: /images/ring_rigid_bc.png
    :width: 45%
    :align: center

    Additional boundary conditions applied to prevent rigid body motion.


Experiments
===========

Three types of experiments are defined:

* ``active-free`` - Run an active contraction simulation without constraints on
  cavity size or pressure. This corresponds to a Langendorff-setup where the
  LV cavity is not pressurized.

* ``active-iso`` - Run an active contraction simulation with an isovolumetric
  cavity constraint. The LV cavity volume is kept constant, that is,
  both inflow and outflow valves are closed.

* ``active-pv-loop`` - Run an active contraction simulation with circulatory
  coupling. In this case the LV ring is coupled with a simple LA model
  (constant pressure) and a 3-element Windkessel model through inflow and
  outflow valves to regulate *preload* and *afterload*.
  The inflow valve closes when the LV pressure exceeds a prescribed pressure
  (approximately the pressure in the LA at end diastole).
  The outflow valve opens when the LV pressure exceeds the input pressure of
  the attached Windkessel model and closes when outflow turns negative.
  A lumped representation of this setup is given in
  :numref:`fig-tutorial-ring-pv-lumped`.


.. _fig-tutorial-ring-pv-lumped:

.. figure:: /images/ring_circulatory_coupling.png
    :width: 80%
    :align: center

    Lumped representation of the PDE ring model coupled to simplified model of
    the circulation. A prescribed pressure in the left atrium,
    :math:`p_{\mathrm{LA}}` serves to steer *preload*
    and a 3-element Windkessel model serves as *afterload* model. The valves
    are modeled as simple resistors to allow pressure gradients to build up.


Other Arguments
===============

Another key argument is the stress model. The available active stress model is:

* ``TanhStress`` - A simple stress model based on
  Niederer et al. (2011). Cardiovascular research, 89(2), 336-343.
  (see :ref:`TanhStress model <TanhStress-model>` for details).

The stress model can be modified with the following arguments:

* ``s_peak`` - Peak stress in kPa (default: 100 kPa)
* ``tau_c`` - Time constant governing rate of rise in active stress model
  (default: 10 ms)

Usage
=====

To run a simple ``active-free``-experiment call

.. code-block:: bash

    ./run.py \
    --experiment active-free  `# experiment to run, possible choices: \
                               #     'active-free', 'active-iso' or 'active-pv-loop'` \
    --duration 500            `# duration of the experiment (default 500 ms)` \
    --s_peak 100              `# Peak stress in kPa (default 100 kPa)` \
    --tau_c 10                `# Time constant governing rate of rise \
                               #     in active stress model (default 10 ms)` \
    --np 10                   `# number of processes` \
    --visualize               `# visualize with meshalyzer`

or call

.. code-block:: bash

    ./run.py \
    --experiment active-pv-loop  `# possible experiments: \
                                  #     'active-free', 'active-iso' or 'active-pv-loop'` \
    --duration 500               `# duration of the experiment (default 500 ms)` \
    --s_peak 100                 `# Peak stress in kPa (default 100 kPa)` \
    --tau_c 10                   `# Time constant governing rate of rise \
                                  #    in active stress model (default 10 ms)` \
    --np 10                      `# number of processes` \
    --visualize                  `# visualize with meshalyzer`

for a simple ``active-pv-loop``-experiment.

Expected results
----------------

Simulation results computed are shown in the following for a full
heartbeat.

.. figure:: /images/ring_deformation.gif
    :name: fig-ring-deformation
    :width: 70%
    :align: center

    Shown is the fiber projected strain on the left (blue: -0.15, red: 0.22)
    and active tension :math:`\sigma_{\rm a}(\mathbf{x},t)` on the right
    (blue: 0.0 kPa, red: 90 kPa).

Post-processing
===============

The ``active-pv-loop``-experiments output a cavity-information file
(usually called **cav.LV.csv**) which contains pressure information,
volume information, flow rates and many other additional informations.
This cavity-information file can be used for a post-processing analysis using
the following tools.

cavplot
-------

The ``cavplot``-tool is a simple tool for plotting the pressure volume relation.
Call

.. code-block:: bash

    cavplot cav.LV.csv

to plot the pressure-volume-relation, see figure :numref:`fig-ring-cavplot`.
To plot just a single trace use the ``--mode`` flag and one of the options
``combined``, ``pvloop``, ``volume``, ``pressure``, ``flux``, ``pressuredot``,
``fluxdot``, ``q_out``, ``q_in``.
If you want to add the loading phase to your plot use the ``--loading`` flag.

.. _fig-ring-cavplot:

.. figure:: /images/ring_cavplot.png
    :width: 500pt
    :align: center

    Pressure-volume plot produced by cavplot.


cavinfo
-------

The ``cavinfo``-tool is an improved version of the cavplot tool.
In addition to plotting pressure-volume data ``cavinfo`` performs a detailed
analysis, computes various metrics and annotates the pressure-volume plots.
Call

.. code-block:: bash

    cavinfo --file cav.LV.csv     `# cavity information file` \
            --output cavity.info  `# output file name (default 'cavity.info')`

to plot the pressure-volume-relation and to determine many other quantities
as ESV, EDV, etc., see figure :numref:`fig-ring-cavinfo`.
All the information is stored in the output file and printed to the terminal,
see output below.

.. code-block:: bash

    cavity file         : cav.LV.csv
    negative flow       : True
    time range          : 0.000 - 500.000 ms
    IVC begin           : 17.000 ms
    ejection begin      : 32.000
    IVR begin           : 101.000 ms
    V0                  : 9.792 ml
    EDV                 : 13.286 ml  ( at 32.000 ms )
    ESV                 : 6.080 ml  ( at 100.000 ms )
    SV                  : 7.206 ml
    EF                  : 54.238 %
    peak flow           : 0.268 ml/ms  ( 268.131 ml/s ) ( 0.268 L/s )  ( 16.088 L/min )
    peak flow time      : 41.000 ms
    open pressure       : 8.680 kPa  ( 65.109 mmHg )
    open pressure time  : 32.000 ms
    peak pressure       : 14.119 kPa  ( 105.902 mmHg )
    peak pressure time  : 56.000 ms
    mean pressure       : 13.022 kPa  ( during ejection )
    hm peak pressure    : 14.219 kPa  ( 106.651 mmHg )
    work                : 93.837 kPa/ml  ( 0.094 J )
    estimated work      : 81.394 kPa/ml  ( 0.081 J )
    external work       : 94.149 kPa/ml  ( 0.094 J )
    contraction time    : 39.000 ms  ( peak pressure time - IVC begin )
    Ea                  : 1.480 kPa/ml
    Ees(E)              : 0.164 kPa/ml

To get the total argument list run ``cavinfo --help``.

.. _fig-ring-cavinfo:

.. figure:: /images/ring_cavinfo.png
    :width: 85%
    :align: center

    Pressure-volume plot as produced by ``cavinfo``. Note the additional annotations
    compared to the plots produced with ``cavplot`` shown in :numref:`fig-ring-cavplot`.

.. hint::
    The tool is right now part of the ``pvprocess`` module but this will be changed!

"""

import os
from datetime import date

from carputils import settings
from carputils import tools
from carputils import ep
from carputils import model
from carputils.mesh import Ring, linear_fibre_rule
from carputils.resources import petsc_options

EXAMPLE_DESCRIPTIVE_NAME = 'Ring'
EXAMPLE_AUTHOR = ('Christoph Augustin <christoph.augustin@medunigraz.at> and '
                  'Matthias Gsell <matthias.gsell@medunigraz.at>')

# =============================================================================
#    Command-line PARSER
# =============================================================================
def parser_commands():
    """ Setup command line parser.
    """
    parser = tools.standard_parser()
    group = parser.add_argument_group('experiment specific options')

    group.add_argument('--experiment',
                       default='active-pv-loop',
                       choices=['active-free',
                                'active-iso',
                                'active-pv-loop'],
                       help='Pick experiment type (default: "active-pv-loop")')

    group.add_argument('--duration',
                       default=500.0,
                       help='duration of experiment (default: 500 ms)')

    group.add_argument('--fast',
                       action='store_true',
                       help='Run for speed (smaller mesh, simple material)')

    group.add_argument('--s_peak',
                       type=float, default=100.,
                       help='Peak stress in active stress model \
                             (default: 100 kPa)')

    group.add_argument('--tau_c',
                       type=float, default=10.,
                       help='Time constant governing rate of rise in active \
                             stress model (default: 10 ms)')

    return parser

# =============================================================================
#    MAIN
# =============================================================================
def run(args, job):
    """ MAIN
    """
    # generate mesh
    mesh_name = generate_mesh(args)

    # generate initial command arguments
    cmd = tools.carp_cmd('ring.par')

    # define simulation ID and mesh
    cmd += ['-simID', job.ID,
            '-meshname', mesh_name]

    # Setup timer variables
    cmd += setup_timers(args.duration)

    # Setup solver settings
    cmd += setup_solver()

    # Setup EP
    cmd += setup_ep(mesh_name)

    # Setup mechanics
    cmd += setup_mech(args, mesh_name)

    # Setup cardiovascular system
    cmd += setup_cardivascular_system(args, mesh_name)

    # Setup visualization
    cmd += setup_visualization(args.visualize)

    # Run main CARP simulation
    job.carp(cmd)

    # Visualize with meshalyzer
    if args.visualize and not settings.platform.BATCH and not args.dry:
        visualize_meshalyzer(job, 'ring')


# =============================================================================
#    General functions
# =============================================================================

# --- generate mesh -----------------------------------------------------------
def generate_mesh(args):
    """ Generate mesh

    Args:
        args: parser arguments
    Returns:
        mesh name
    """

    mesh_dir = 'mesh'
    mesh_name = f'{mesh_dir}/ring'
    if not os.path.exists(mesh_dir):
        os.mkdir(mesh_dir)

    if args.fast:
        p60m60 = linear_fibre_rule(60, -60)
        mesh = Ring(25.0, div_transmural=1, div_height=1,
                    div_circum=50, fibre_rule=p60m60)
    else:
        p60m60 = linear_fibre_rule(60, -60)
        mesh = Ring(25.0, div_transmural=3, div_height=2,
                    fibre_rule=p60m60)

    mesh.generate_carp(mesh_name, ['top', 'bottom', 'inside'])
    mesh.generate_carp_rigid_dbc(mesh_name)

    return mesh_name

# --- set timers --------------------------------------------------------------
def setup_timers(duration):
    """ Sets the timers for the electro-mechanical simulation

    Args:
        duration: experiment duration
    Returns:
        List of timer options.
    """
    timer_opts = ['-dt', 10.,           # time step for electrics [us]
                  '-tend', duration,    # length of the simulation [ms]
                  '-timedt', 5.,        # time between temporal output [ms]
                  '-spacedt', 1.,       # time between spacial output [ms]
                  '-mechDT', 0.5]       # time step for mechanics [ms]

    return timer_opts

# --- setup solver options ----------------------------------------------------
def setup_solver():
    """ Setup solver options for the simulation
    Returns:
        List of solver options
    """

    # Mechanics: linear solver options
    sopts = ['-krylov_tol_mech', '1e-10']   # solver tol for mechanics problem

    # Mechanics: Nonlinear solver options (Newton)
    sopts += ['-newton_stagnation_detection', '0',  # activate stag detection
              '-newton_adaptive_tol_mech', '0',     # no adaptive GMRES tolerance
              '-newton_tol_mech', '1e-8',           # set Newton tolerance
              '-newton_tol_mech_type', '0',         # set Newton tolerance type
              '-newton_maxit_mech', '20']           # Newton max iterations

    sopts += ['-mechanics_options_file', petsc_options.path('gamg_gmres_opts_agg')]

    return sopts

# =============================================================================
#    EP FUNCTIONS
# =============================================================================

# -----------------------------------------------------------------------------
def setup_ep(mesh_name):
    """ Setup electrophysiology
    Args:
        mesh_name: mesh name
    Returns:
        List of EP options
    """
    # Stimuli
    num_stims, stim_options = setup_stimuli(mesh_name)

    ek_stim, num_stims = ep.fake_ep_opts(num_stims, '', None)
    stim_options += ek_stim

    return ['-num_stim', num_stims] + stim_options

# -----------------------------------------------------------------------------
def setup_stimuli(mesh_name):
    """ Setup stimulus
    Args:
        mesh_name: mesh name
    Returns:
        List of stimulus options
    """
    num_stims = 1   # initialize number of stimuli

    bc_tpl = mesh_name + '_{}'
    lv_endo = bc_tpl.format('inside')

    stim_opts = ['-stimulus[0].vtx_file', lv_endo,
                 '-stimulus[0].stimtype', 0,
                 '-stimulus[0].start', 0,
                 '-stimulus[0].strength', 150.,     # stimulus strength in mV
                 '-stimulus[0].duration', 2.]       # stimulus duration in ms

    return num_stims, stim_opts

# =============================================================================
#    Mechanics FUNCTIONS
# =============================================================================

# -----------------------------------------------------------------------------
def setup_mech(args, mesh_name):
    """ Setup generic mechanical settings, materials
    Args:
        args: parser arguments
        mesh_name: mesh name
    Returns:
        List of mechanics options
    """
    # set bulk modulus kappa
    kappa = 500 if args.mech_element == 'P1-P0' else 1e10

    # Set passive material properties
    mat = model.mechanics.GuccioneMaterial([1], 'ring', a=3.0, b_f=18.480,
                                           b_fs=1.627, b_t=3.580, kappa=kappa)

    pass_mat = model.optionlist([mat])

    # set active material parameters
    active_stress_params = set_tanh_stress_params(args.s_peak, args.tau_c)

    act_mat = ['-imp_region[0].plugins', 'TanhStress',
               '-imp_region[0].plug_param', active_stress_params,
               '-active_stress_percentage_s', '0.2',
               '-active_stress_percentage_n', '0.05']

    mech_opts = pass_mat + act_mat

    # Dirichlet Boundary conditions
    mech_opts += setup_dbc(mesh_name)

    # Neumann boundary conditions
    mech_opts += setup_nbc(mesh_name)

    return mech_opts

# -----------------------------------------------------------------------------
def setup_dbc(mesh_name):
    """ Setup Dirichlet boundary conditions

    Fix geometry or prescribe a given displacement on part of the boundary
    Gamma_D > 0
    Dirichlet BC are essential for the existence of a solution.

    Args:
        mesh_name: mesh name

    Returns:
        List of Dirichlet BC options
    """

    bc_tpl = mesh_name + '_{}'

    dbc_name_bottom = bc_tpl.format('bottom')
    dbc_name_top = bc_tpl.format('top')
    dbc_name_xaxis = bc_tpl.format('xaxis')
    dbc_name_yaxis = bc_tpl.format('yaxis')

    # Dirichlet Boundary Conditions
    num_dbc = 4
    dbc_opts = ['-num_mechanic_dbc', num_dbc]

    # Fix bottom to xy plane
    dbc_opts += ['-mechanic_dbc[0].name', 'bottom_plane',
                 '-mechanic_dbc[0].bctype', 0,
                 '-mechanic_dbc[0].vtx_file', dbc_name_bottom,
                 '-mechanic_dbc[0].apply_ux', 0,
                 '-mechanic_dbc[0].apply_uy', 0,
                 '-mechanic_dbc[0].apply_uz', 1,
                 '-mechanic_dbc[0].dump_nodes', 0]

    # Fix top to xy plane
    dbc_opts += ['-mechanic_dbc[1].name', 'top_plane',
                 '-mechanic_dbc[1].bctype', 0,
                 '-mechanic_dbc[1].vtx_file', dbc_name_top,
                 '-mechanic_dbc[1].apply_ux', 0,
                 '-mechanic_dbc[1].apply_uy', 0,
                 '-mechanic_dbc[1].apply_uz', 1,
                 '-mechanic_dbc[1].dump_nodes', 0]

    # Prevent free rotation and translation
    dbc_opts += ['-mechanic_dbc[2].name', 'xaxis_fix',
                 '-mechanic_dbc[2].bctype', 0,
                 '-mechanic_dbc[2].vtx_file', dbc_name_xaxis,
                 '-mechanic_dbc[2].apply_ux', 0,
                 '-mechanic_dbc[2].apply_uy', 1,
                 '-mechanic_dbc[2].apply_uz', 1,
                 '-mechanic_dbc[2].dump_nodes', 0]

    dbc_opts += ['-mechanic_dbc[3].name', 'yaxis_fix',
                 '-mechanic_dbc[3].bctype', 0,
                 '-mechanic_dbc[3].vtx_file', dbc_name_yaxis,
                 '-mechanic_dbc[3].apply_ux', 1,
                 '-mechanic_dbc[3].apply_uy', 0,
                 '-mechanic_dbc[3].apply_uz', 1,
                 '-mechanic_dbc[3].dump_nodes', 0]

    return dbc_opts

# -----------------------------------------------------------------------------
def setup_nbc(mesh_name):
    """ Setup Neumann boundary conditions

    Prescribe pressure or traction to part of the boundary Gamma_N

    Args:
        mesh_name: mesh name

    Returns:
        List of Neumann BC options
    """

    bc_tpl = mesh_name + '_{}'
    nbc_name = bc_tpl.format('inside')

    # Neumann Boundary Conditions
    num_nbc = 1
    nbc_opts = ['-num_mechanic_nbc', num_nbc,
                '-mechanic_nbc[0].name', 'lv_endo',
                '-mechanic_nbc[0].surf_file', nbc_name]

    return nbc_opts

# =============================================================================
#    Active stress functions
# =============================================================================

# -----------------------------------------------------------------------------
def set_tanh_stress_params(s_peak, tau_c):
    """ Set parameters for the Tanh active stress model.

    See Niederer et al 2011 Cardiovascular Research 89
    and the thesis of Andrew Crozier

    Args:
        s_peak: peak magnitude of stress transient
        tau_c: time constant contraction
    Returns:
        parameter settings for the Tanh active stress model
    """

    # current settings    #   default values (see TanhStress.c in LIMPET)
    # ------------------- # ---------------------------------------------------
    t_emd = 15.0          #  15.0 ms  electro-mechanical delay (t_delay)
    s_peak0 = s_peak      # 100.0 kPa peak isometric tension
    tau_c0 = tau_c        #  40.0 ms  time constant contraction (t_r)
    tau_r =  70.0         # 110.0 ms  time constant relaxation (t_d)
    t_dur = 170.0         # 550.0 ms  duration of transient (t_max)
    lambda_0 = 0.6        #   0.7 -   sacomere length ratio (a_7)
    ld_a6 = 10.0          #   5.0 -   degree of length dependence (a_6)
    ld_up = 500.0         # 500.0 ms  length dependence of upstroke time (a_4)
    ld_on = 0             #   0/1 -   turn on/off length dependence
    vm_thresh = -60.0     # -60.0 mV  threshold Vm for deriving LAT

    tpl = 't_emd={},Tpeak={},tau_c0={},tau_r={},t_dur={},lambda_0={},' + \
          'ld={},ld_up={},ldOn={},VmThresh={}'
    return tpl.format(t_emd, s_peak0, tau_c0, tau_r, t_dur, lambda_0, ld_a6,
                      ld_up, ld_on, vm_thresh)

# =============================================================================
#    Cardiovascular system function
# =============================================================================

# -----------------------------------------------------------------------------
def setup_cardivascular_system(args, mesh_name):
    """ Setup cardiovascular system.
    Args:
        args: parser arguments
        mesh_name: mesh name
    Returns:
        A list of cardiovascular system options cv_opts.
    """

    # setup cavity volumes
    num_cavs = 1
    cv_opts = setup_cavity_volumes(mesh_name, num_cavs)

    # in the active-free case we don't apply any pressure in the cavity
    # but we want to keep track of the cavity volume
    if args.experiment in ['active-free']:
        return cv_opts

    # link volume and pressure boundaries to define cavity
    cv_opts += cv_sys_parameters(args, num_cavs)

    return cv_opts

# -----------------------------------------------------------------------------
def setup_cavity_volumes(mesh_name, num_cavs):
    """ Setup computation of cavity volumes

    Computation of volume using the surface enclosing the volume and
    vector field f(x,y,z)
    the openings of this geometry are in the x-y-plane, hence we can use
    f(x,y,z) = x

    Args:
        mesh_name: mesh name
        num_cavs: number of cavities

    Returns:
        A list of cavity volume options.
    """

    bc_tpl = mesh_name + '_{}'
    surf_file = bc_tpl.format('inside') # surface file enclosing the volume

    grid_id = model.mechanics.grid_id()  # mechanics grid

    cav_vol_opts = ['-numSurfVols', num_cavs,
                    '-surfVols[0].name', 'lv_endo',
                    '-surfVols[0].surf_file', surf_file,
                    '-surfVols[0].grid', grid_id,
                    '-surfVols[0].vecf_x', 1.0,
                    '-surfVols[0].vecf_y', 0.0,
                    '-surfVols[0].vecf_z', 0.0]

    # element volumes
    vol_tracking = 1    # 1: track myocardial volume changes due deformation
    num_elem_vols = 1   # number of different volumetric domains
    elem_vol_opts = ['-numElemVols', num_elem_vols,
                     '-elemVols[0].name', 'ring',
                     '-elemVols[0].numtags', 0,
                     '-elemVols[0].grid', grid_id,
                     '-volumeTracking', vol_tracking]

    return cav_vol_opts + elem_vol_opts

# -----------------------------------------------------------------------------
def cv_sys_parameters(args, num_cavs):
    """ Set cardiovascular system parameters
    Args:
        args: parser arguments
        num_cavs: number of cavities
    Returns:
        A list of the cardiovascular system parameters.
    """

    # set defaults for active-iso
    cav_state = 0     # 0: isovolumetric, valves closed
    cv_param = []     # default attached cvsys options
    p0_in = 5.        # initial inflow compartment pressure (in mmHg)
    p0_out = 0.       # initial outflow compartment pressure (in mmHg)

    if args.experiment == 'active-pv-loop':
        cav_state = -1   # -1: state of cavity managed automatically
        cv_coupling = 0  #  0: coupling using cavitary volumes
        p0_out = 80.0

        # set Windkessel parameters the LV
        wk3 = ['-lv_wk3.name', 'Aorta',
               '-lv_wk3.R1', '0.068',
               '-lv_wk3.R2', '1.13',
               '-lv_wk3.C', '0.11']

        # set valve parameters
        wk3 += ['-aortic_valve.Rfwd', 0.01,
                '-mitral_valve.Rfwd', 0.05]

        cv_param += wk3
        cv_param += ['-CV_coupling', cv_coupling,
                     '-CV_FE_coupling', 0,
                     '-CVS_mode', 0]

    p0_cav = p0_in
    cv_param += ['-num_cavities', num_cavs,         # number of cavities
                 '-cavities[0].cav_type', 0,        # 0 LV 1 RV 2 LA 3 RA
                 '-cavities[0].cavP', 0,            # link to nbc
                 '-cavities[0].cavVol', 0,          # link to volume
                 '-cavities[0].p0_cav', p0_cav,     # define initial pressure
                 '-cavities[0].p0_out', p0_out,     # init p in outflow tube
                 '-cavities[0].p0_in', p0_in,       # init p in inflow cavity
                 '-cavities[0].state', cav_state]   # state management

    cv_param += ['-loadStepping', 8]

    return cv_param

# =============================================================================
#    Visualization
# =============================================================================

# -----------------------------------------------------------------------------
def setup_visualization(visualize):
    """ Setup visualization options
    Args:
        visualize: (true) include output of variables
    Returns:
        A list of visualization options.
    """
    vis_opts = []
    if visualize:
        vis_opts = ['-mech_output', 1+2,       # 1: igb, 2: vtk
                    '-vtk_output_mode', 1,     # 1: legacy vtk (ascii)
                    '-mech_output_domain', 1,  # 1: volumetric domain
                    '-gzip_data', 0,           # 0: do not zip data
                    '-gridout_i', 3]           # 3: full grid output

        # specify output variables
        vis_opts += ['-stress_value', 4+16,  # 4: 1st principal, 16: full tensor
                     '-strain_value', 1,     # 1: fiber strain on nodes
                     '-work_value', 1]       # 1: work on nodes
    return vis_opts

# -----------------------------------------------------------------------------
def visualize_meshalyzer(job, mesh_name):
    """ Visualize with Meshalyzer
    Args:
        job: job definition
        mesh_name: mesh name
    """
    geom = os.path.join(job.ID, mesh_name + '_i')
    data = os.path.join(job.ID, 'firstPrincipalStress.igb')
    deform = os.path.join(job.ID, 'x.dynpt')
    job.gunzip(deform)
    job.meshalyzer(geom, data, deform)

# -----------------------------------------------------------------------------
def setup_job_id(args):
    """ Generate name of top level output directory.
    """
    today = date.today()
    subtest = args.experiment
    if args.fast:
        subtest += '_fast'
    return f'{today.isoformat()}_{subtest}_{args.mech_element}_{args.flv}_np{args.np}'

# =============================================================================
if __name__ == '__main__':
    # tear down decorator function and assign it to constant function RUN
    RUN = tools.carpexample(parser_commands, setup_job_id)(run)
    RUN()

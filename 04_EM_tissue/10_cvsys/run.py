#!/usr/bin/env python
r"""

.. _emcvsys:

**Introduction**
****************

This tutorial guides through using the 0D Cardiovascular system simulator.
The code is based on the `CircAdapt model <http://www.circadapt.org/>`_,
see [Arts2005]_, [Lumens2009]_, [Arts2012]_, and [Walmsley2015]_.

.. _fig-cvsys-circadapt:

.. figure:: /images/circadapt_ode_system.png
    :width: 90%
    :align: center

    Schematic of the 0D Closed-loop model CircAdapt

**Usage**
*********

(Requires the ``cvsys`` package)
====================================

**Basic Options**
=================

To see the full option list run

.. code-block:: bash

    cvstool --help

The following basic options are available:

.. code-block:: bash

  -h, --help              Display this information
  -s, --solver <solver>   Choose ODE solver
                          Choices:
                            forw-euler: forward Euler method
                            rfk45: Runge-Kutta-Fehlberg 45
                            cvode: Sundials CVode solver
  -d, --duration <float>  Duration of the simulation in (s)
                            default: 0.85 s
  -o, --output <dir>      Place the output into the directory <dir>
  --create-opts <file>    Create options file with default values
                            format choices are `json` and `prm`
                            default: `cvs_options.json`
  --opts <file>           Use input options in <file>


The following command computes the heartbeat of a healthy adult over 60 seconds:

.. code-block:: bash

    cvstool -d 60 \
	    -o healthy_adult


Parameters of the cardiovascular systems can be changed using specific option
files. First, generate options with

.. code-block:: bash

    cvstool --create-opts cvs_options.json

Second, modify the options file ``cvs_options.json``, e.g.,
set ``Aortic valve stenosis [pct.]`` to 70.

And third, simulate the heartbeat with a 70% aortic stenosis using the options
file with

.. code-block:: bash

    cvstool -d 60 \
	    -o aortic_stenosis \
            --opts cvs_options.json

**Post-processing**
=====================

The cvsys experiment output information file on the cardiovascular system in
the chosen output folder in csv format. Depending on the cardiovascular
component these contain pressure information, volume information,
flow rates and many other additional informations.
These files can be used for a post-processing analysis using the tool ``cavplot``.
Call

.. code-block:: bash

    cavplot --help

for a detailed description of the usage.

The default output containts pressure-volume loops as well as pressure and volume
traces

.. code-block:: bash

    cavplot ./healthy_adult/cav.LV.csv

To plot a time interval of the output use ``--time-interval`` or ``--time``
For example to plot the last two seconds of the simulation call

.. code-block:: bash

    cavplot ./healthy_adult/cav.LV.csv --time-interval 58 60

To change the unit to mmHg use the flag ``--mmHg``. To show PV loop and
pressure traces for the aortic stenosis case call

.. code-block:: bash

    cavplot ./aortic_stenosis/cav.LV.csv --time 58 --mmHg

This should give you a plot similar to

.. figure:: /images/cvsys_stenosis.png
    :width: 80%
    :align: center

    cavplot output for the 70% stenosis case.

The options ``l+=`` and ``l=`` allow to add labels to the output.
To compare the pvloops for the healthy case and the case with 70% aortic stenosis
call

.. code-block:: bash

    cavplot healthy_adult/cav.LV.csv:l="Healthy" \
            aortic_stenosis/cav.LV.csv:l="Aortic Stenosis" \
            --time 55 --mode pvloop

.. figure:: /images/cvsys_healthy_vs_stenosis.png
    :width: 60%
    :align: center

    Pressure-volume plot comparing the healthy and the stenotic case.

The option ``ls=`` in ``{dotted, dashdot, dashed, solid}`` allows to change the
linestyle. E.g., plot the pressure drop over the aortic valve:

.. code-block:: bash

    cavplot aortic_stenosis/cav.LV.csv \
            aortic_stenosis/tube.AO.csv:ls=dashed \
            --time 55 --mode pressure

.. _fig-cvsys-cavplot:

.. figure:: /images/cvsys_pressure_drop.png
    :width: 70%
    :align: center

    Pressure-drop over the aortic valve.

The options ``v*=``, ``p*=``, and ``f*=`` allows to scale the volume(v), pressure(p),
and flux(f), respectively. The same also works for ``\=``, and ``+=``, ``-=`` for a
translation.
E.g. plot the flux over the pulmonary outlet, the mitral valve, volume change of
the left atrium in one plot:

.. code-block:: bash

    cavplot aortic_stenosis/valve.PO.csv:f*=-1 \
            aortic_stenosis/valve.MV.csv \
            aortic_stenosis/cav.LA.csv:ls=dashed:l="LA Volume Change" \
            --mode flux --time 56.7 58.7

.. _fig-cvsys-cavplot:

.. figure:: /images/cvsys_flux.png
    :width: 70%
    :align: center

    Flux over pulmonary outlet and mitral valve and volume change of the left atrium.

**Advanced Options**
======================
The following more advanced options are available:

.. code-block:: bash

  --version <version>  Specify CircAdapt version to use (`2012`, `2015`)
                         default: `2015`
  --no-peri            Run simulation without pericardium
  --stiff-av-valves    Run simulation without updating leak area
                       of atrio-ventricular valves
  --mass-conservation  Enable mass conservation
  --allow-negative-p   Allow negative pressures in cavities

The different versions (2012, 2015) correspond to the publications
[Arts2012]_ and [Walmsley2015]_, see also
http://www.circadapt.org/downloads/files.

For example we can allow negative pressures in the cavities using

.. code-block:: bash

    cvstool -d 60 -o healthy_adult_np --allow-negative-p

We plot the difference in the left atrial PV loop using the plotting option
``c=`` that allows to change the color using RGB-HEX format

.. code-block:: bash

    cavplot healthy_adult/cav.LA.csv:l="standard":c=#000 \
            healthy_adult_np/cav.LA.csv:l="allow-negative-p":c=#ff0000 \
            --mode pvloop --time 55 --mmHg

.. _fig-cvsys-cavplot:

.. figure:: /images/cvsys_neg_p.png
    :width: 70%
    :align: center

    Left atrial pressure with the standard code in black and with the
    ``--allow-negative-p`` flag in red.

.. rubric:: References

.. [Arts2005] *T. Arts et al.*
              AJP Heart 288 (2005): H1943-H1954.
              `DOI: 10.1152/ajpheart.00444.2004
              <https://www.physiology.org/doi/10.1152/ajpheart.00444.2004>`_.
.. [Arts2012] *T. Arts et al.*
              PLoS Comput Biol 8 (2012): e1002369,
              `DOI: 10.1371/journal.pcbi.1002369
              <https://dx.plos.org/10.1371/journal.pcbi.1002369>`_.
.. [Lumens2009] *J. Lumens et al.*
             Ann Biomed Eng 37 (2009): 2234-2255,
             `DOI: 10.1007/s10439-009-9774-2
             <http://link.springer.com/10.1007/s10439-009-9774-2>`_.
.. [Walmsley2015] *J. Walmsley et al.*,
             PLoS Comput Biol 11 (2015): e1004284,
             `DOI: 10.1371/journal.pcbi.1004284
             <https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1004284>`_.

"""

EXAMPLE_DESCRIPTIVE_NAME = 'CvSys'
EXAMPLE_AUTHOR = ('Christoph Augustin <christoph.augustin@medunigraz.at>')

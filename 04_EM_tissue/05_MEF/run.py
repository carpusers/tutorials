#!/usr/bin/env python
"""
This example demonstrates mechano-electric feedback effects.
"""

import os
import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import model
from carputils import ep
from carputils.resources import petsc_options

EXAMPLE_DESCRIPTIVE_NAME = "Mechanics Strong Coupling"
EXAMPLE_AUTHOR = ("Gernot Plank <gernot.plank@medunigraz.at> and "
                  "Christoph Augustin <christoph.augustin@medunigraz.at")

def parser_cmd():
    """ setup command line parser """
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--ep-model',
                         default='monodomain',
                         choices=ep.MODEL_TYPES,
                         help='pick electrical model type')

    group.add_argument('--emf',
                        default='None',
                        choices=['None', 'electrotonic', 'Ca-TnC'],
                        help='electromechanical coupling approach')

    group.add_argument('--size',
                        default=10.,
                        help='choose side length of the cube in mm')

    group.add_argument('--resolution',
                        default=0.5,
                        help='choose mesh resolution in mm')

    group.add_argument('--mechanics-off',
                        action='store_true',
                        help='switch off mechanics to generate activation vectors')

    group.add_argument('--duration',
                        type=float, default=500.,
                        help='duration of simulation (ms)')
    return parser

def job_id(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return f'{today.isoformat()}_{args.flv}_np{args.np}'

@tools.carpexample(parser_cmd, job_id)
def run(args, job):
    """ main run function """

    # --- Configuration variables --------------------------------------------
    i_stim          = 150.0         # stimulus strength
    i_dur           = 3.0           # stimulus duration
    res             = args.resolution
    if res < 0.02:                  # for finer meshes we need a stronger stimulus
        i_stim      = 400.0
        i_dur       =  10.0
    active_imp      =   0           # identify active region

    # --- Material definitions ------------------------------------------------
    tissue = model.mechanics.AnisotropicMaterial([0], 'tissue', kappa=100, c=30)
    if res < 0.02:  # for finer meshes we take another material
        tissue = model.mechanics.GuccioneMaterial([0], 'tissue', kappa=500.,a=0.876)

    # --- Generate mesh ------------------------------------------------------
    # Units are mm
    s_l = args.size
    wid = 1.0
    geom = mesh.Block(size=(s_l,s_l,wid), resolution=res)

    # Set fibre angle to 0, sheet angle to 90
    geom.set_fibres(-60, 60, 90, 90)

    # Define regions
    # apicobasal coordinates
    ab_0 = -s_l/2
    ab_1 = -s_l/2 + 0.33*s_l
    ab_2 = -s_l/2 + 0.66*s_l
    ab_3 =  s_l/2

    # transmural coordinates
    tm_0 = -wid/2
    tm_1 = -wid/2 + 0.33*wid
    tm_2 = -wid/2 + 0.66*wid
    tm_3 =  wid/2

    # circumferential coordinates, no regions here
    cf_0 = -s_l/2
    cf_1 =  s_l/2

    # define epicardial layer
    epi_apex = mesh.BoxRegion((ab_0,cf_0,tm_0), (ab_1,cf_1,tm_1), tag=11)
    geom.add_region(epi_apex)
    epi_mid  = mesh.BoxRegion((ab_1,cf_0,tm_0), (ab_2,cf_1,tm_1), tag=12)
    geom.add_region(epi_mid)
    epi_base = mesh.BoxRegion((ab_2,cf_0,tm_0), (ab_3,cf_1,tm_1), tag=13)
    geom.add_region(epi_base)

    # define midmyocardial layer
    mid_apex = mesh.BoxRegion((ab_0,cf_0,tm_1), (ab_1,cf_1,tm_2), tag=21)
    geom.add_region(mid_apex)
    mid_mid  = mesh.BoxRegion((ab_1,cf_0,tm_1), (ab_2,cf_1,tm_2), tag=22)
    geom.add_region(mid_mid)
    mid_base = mesh.BoxRegion((ab_2,cf_0,tm_1), (ab_3,cf_1,tm_2), tag=23)
    geom.add_region(mid_base)

    # define endocardial layer
    endo_apex = mesh.BoxRegion((ab_0,cf_0,tm_2), (ab_1,cf_1,tm_3), tag=31)
    geom.add_region(endo_apex)
    endo_mid  = mesh.BoxRegion((ab_1,cf_0,tm_2), (ab_2,cf_1,tm_3), tag=32)
    geom.add_region(endo_mid)
    endo_base = mesh.BoxRegion((ab_2,cf_0,tm_2), (ab_3,cf_1,tm_3), tag=33)
    geom.add_region(endo_base)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # --- Define stimulus --------------------------------------------------
    stim0 = mesh.block_region(geom, 'stimulus', 0, [-0.15, -0.15, 0.40], [0.15, 0.15, 0.55], False)
    stim1 = mesh.block_region(geom, 'stimulus', 1, [-0.15, -0.15, 0.40], [0.15, 0.15, 0.55], False)

    # --- Define boundary conditions ---------------------------------------
    dbc = ['-num_mechanic_dbc', 2]

    # Fix 'base'
    dbc += ['-mechanic_dbc[0].name', 'base']
    dbc += mesh.block_boundary_condition(geom, 'mechanic_dbc', 0, 'y',
                                         lower=False)
    dbc += ['-mechanic_dbc[0].bctype',     0,
            '-mechanic_dbc[0].apply_ux',   1,
            '-mechanic_dbc[0].apply_uy',   1,
            '-mechanic_dbc[0].apply_uz',   1,
            '-mechanic_dbc[0].dump_nodes', 1]

    # Fix 'apex'
    dbc += ['-mechanic_dbc[1].name', 'apex']
    dbc += mesh.block_boundary_condition(geom, 'mechanic_dbc', 1, 'y',
                                         lower=True)
    dbc += ['-mechanic_dbc[1].bctype',     0,
            '-mechanic_dbc[1].apply_ux',   1,
            '-mechanic_dbc[1].apply_uy',   1,
            '-mechanic_dbc[1].apply_uz',   1,
            '-mechanic_dbc[1].dump_nodes', 1]

    # neumann ---------------------------------------------------------------
    nbc = ['-num_mechanic_nbc', 1]
    nbc +=['-mechanic_nbc[0].name', 'endo']
    nbc +=mesh.block_boundary_condition(geom, 'mechanic_nbc', 0, 'z',
                                        lower=True)
    nbc +=['-mechanic_nbc[0].trace',    'ramp']
    nbc +=['-mechanic_nbc[0].pressure', 2]

    # --- Build command line--------------------------------------------------
    # Get basic command line, including solver options
    cmd = tools.carp_cmd('slab.par')

    # Add options
    cmd += ['-simID',                job.ID,
            '-meshname',             meshname,
            '-mechDT',               1.0 * (not args.mechanics_off),
            '-spacedt',              1.0,
            '-dt',                   10.,
            '-tend',                 args.duration,
            '-cell_length',          args.resolution,
            '-volumeTracking',       1,
            '-newton_maxit_mech',    10,
            '-newton_tol_mech',      1e-6,
            '-mass_lumping',         1]
    cmd += ['-mechanics_options_file', petsc_options.path('gamg_gmres_opts_agg')]

    # Define type of source model
    cmd += ep.model_type_opts(args.ep_model)

    # Add boundary conditions
    cmd += dbc
    cmd += nbc

    # Add material options
    cmd += model.optionlist([tissue])

    # Specify solver settings
    cmd += ['-parab_solve',        1,
            '-cg_tol_ellip',       1e-8,
            '-cg_tol_parab',       1e-8,
            '-mech_output', 1]

    # Define stimuli
    cmd += setup_stimuli(stim0, stim1, args.duration, i_stim,i_dur)

    # Active stress setting
    cmd += setup_active('NoStress','GPB_Land', active_imp)

    # Electromechanical coupling
    cmd += setup_em_coupling('strong')

    if args.emf=='electrotonic':
        cmd += ['-emc_coupling', 4 ]

    cmd += ['-simID',    job.ID,
            '-meshname', meshname,
            '-tend',     args.duration]

    if args.visualize:
        cmd += ['-gridout_i',    3,
                '-stress_value', 4]

    # --- Run simulation -----------------------------------------------------
    job.carp(cmd)

    # --- Do meshalyzer visualization ----------------------------------------
    if args.visualize and not settings.platform.BATCH:

        # Prepare file paths
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        view = 'tension.mshz'
        data   = os.path.join(job.ID, 'firstPrincipalStress.igb.gz')
        deform = os.path.join(job.ID, 'x.dynpt')
        view   = 'stress.mshz'
        job.gunzip(deform)

        if args.mechanics_off:
            data = os.path.join(job.ID, 'vm.igb.gz')
            job.meshalyzer(geom, data, view)
        else:
            job.meshalyzer(geom, data, deform, view)

# ============================================================================
#    FUNCTIONS
# ============================================================================

def setup_stimuli(face0, face1, tend, strength, duration):
    """ set stimulus """
    second_stim = 275
    num_stim = 1
    if tend > (second_stim + duration):
        num_stim = 2

    stm_opt = ['-floating_ground', '0',
                '-num_stim', num_stim,
                '-stimulus[0].stimtype', '0',
                '-stimulus[0].start', '5',
                '-stimulus[0].strength', str(strength),
                '-stimulus[0].duration', str(duration)]
    stm_opt += face0

    if tend > (second_stim + duration):
        stm_opt += ['-stimulus[1].stimtype', '0',
                    '-stimulus[1].start', second_stim,
                    '-stimulus[1].strength', str(strength),
                    '-stimulus[1].duration', str(duration)]
        stm_opt += face1

    return stm_opt

def setup_active (stress, ep_mdl, active_imp):
    """ setup active stress setting """
    opts = []
    im_pars = []
    if ep_mdl == 'GPB_Land':
        stress = 'NoStress'   # turning off stress since GPB land is combined
        gpb_pars  = 'T_ref=117.1,Ca_50ref=0.52,TRPN_50=0.37,n_TRPN=1.54'
        gpb_pars += ',k_TRPN=0.14,n_xb=3.38,k_xb=4.9e-3,lengthDep=1'
        im_pars += [f'-imp_region[{active_imp}].im_param', gpb_pars]

        im_pars += ['-num_gvecs', 1,
                    '-gvec[0].name',  "Ca_i",
                    '-gvec[0].ID[0]', "Ca_i",
                    '-gvec[0].bogus', -10. ]

    else:
        im_pars += [f'-imp_region[{active_imp}].plugins', stress]

    opts += [f'-imp_region[{active_imp}].im', ep_mdl,
             '-veldep', 1]
    opts += im_pars

    statefile = False
    # overrule initial state?
    if (ep_mdl in ['TT2','ORd','UCLA_RAB']) and (stress in ['LandStress','NPStress','Rice']):
        sv_file = f'./states/{ep_mdl}_{stress}_bcl_800_ms.sv'
        statefile = True
    elif ep_mdl=='GPB_Land':
        sv_file = f'./states/{ep_mdl}_{stress}_cpl_strong_bcl_500_ms_dur_60000_ms_l_1_00.sv'
        statefile = True

    # check existence of state file
    if statefile:
        if not os.path.isfile(sv_file):
            print(f'State variable initialization file {sv_file} not found!')
            sys.exit(-1)
        else:
            opts += [f'-imp_region[{active_imp}].im_sv_init', sv_file]

    return opts

# -----------------------------------------------------------------------------
def setup_em_coupling(em_coupling):
    """ setup electromechanical coupling """
    if em_coupling == 'weak':
        coupling = ['-mech_use_actStress', 1, '-mech_lambda_upd', 1, '-mech_deform_elec', 0]
    elif em_coupling == 'strong':
        coupling = ['-mech_use_actStress', 1, '-mech_lambda_upd', 2, '-mech_deform_elec', 1]
    elif em_coupling == 'none':
        coupling = ['-mech_use_actStress', 0, '-mech_lambda_upd', 0, '-mech_deform_elec', 0]

    return coupling

# -----------------------------------------------------------------------------
if __name__ == '__main__':
    run()

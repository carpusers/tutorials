#!/usr/bin/env python

"""
.. _tutorial_phase-singularities:


Phase Singularities
===================

The most lethal types of cardiac arrhythmia are driven by reentrant wavefronts, which,
in many circumstances, have an organising centre.
In 2D, this is usually thought of as the tip of a rotating spiral wave (which reenters), whereas in 3D this becomes what is known as a filament which represents the centre-line of a scroll-wave.
Filaments can therefore be thought of as lines of wave break around which a scroll wave rotates. 
A line of wave break therefore defines a region in which surfaces of activation and recovery intersect one another. In 2D a point of wave break is thus defined by intersecting lines of activation and recovery.


Detection Algorithm
-------------------

There are many different algorithms which can be used to detected filaments, which are summarised nicely in Clayton et al
[#clayton]_.

Locating filaments and phase singularities therefore relies on finding the lines or points of intersection of activation and recovery. 
This may be done explicitly or else by first transforming activation into phase. 
The term phase singularity refers to the fact that such a point is surrounded by tissue exhibiting all different values of phase; thus, that point itself does not have a defined phase value.
In terms of activation and recovery intersection, you can think of a spiral wave core to be where the wavefront and waveback encroach on oneanother, which would be where activating tissue meets recovered (diastolic) tissue.

The method used in CARPentry relies on explicitly finding the intersection of activation and recovery.
The isosurface of activation is simply found by finding the surface corresponding to a certain membrane potential level, :math:`V_{\mathrm {thresh}}`. 
This is usually chosen to be around the point of activation of the sodium channel, so approximately -40mV.
Note that in some complex cases of VF or when it is used to analysis simulated optical mapping signals, 
the action potential amplitude may well be significantly lower than during sinus rhythm, and thus this value may want to be lowered.
The surface of recovery is usually defined similarly to be where :math:`dV_{m}/dt=0`, thus representing recovered and diastolic tissue.
If this method is used then it is important to also define the time-window over which this is evaluated over.
These two approaches are shown in the Figure below.
The detection algorithm is implemented in ``GlFilament`` which can be used :ref:`standalone <GlFilament>` or as a :ref:`post-processing option <carpentry-post-processing>`.

.. _fig-phase-singularity:

.. figure:: /images/phase-singularity.png
   :width: 50%
   :align: center

Representation of a spiral wave (a) with (b) showing isolines of voltage (blue) and recovery (red). Panel (c) shows a zoomed-in region close to the spiral core, demonstrating how the intersection of activation and recovery successfully highlight a phase singularity.
Taken from Clayton et al [#clayton]_


Experimental Parameters
-----------------------
Phase singularity detection can be performed in either 2D or 3D with CARPentry. However, it is restricted to meshes that are triangles or tetrahedra.

The performance of the algorithm is controlled by just two experimental parameters. the isofilament threshold and the iso-filament interval. The isofilament threshold defines the value of :math:`V_{m}` which correlates to activation. It therefore depends on the cell model.
The isofilament interval defines time window over which :math:`dV_{m}/dt=0` is evaluated. According to Clayton et al
[#clayton]_ for most cell models this would be 2 ms, however, longer values seem to work better for the implementation in CARPentry.


Output
------
The output of the filament detection is of the form of a series of Auxiliary Grid files which can be loaded into Meshalyzer for visualisation purposes.
These constitute ``.pts_t``, ``.elem_t`` files. Important to note is that these files are, by default, stored in the same location as the mesh itself.
In addition a series of files called ``filament_*.cnnx`` are also produced. 
These represent the connections (which define the filaments, or parts of filaments), at each time-slice. 
Connections simply define the two node numbers which therefore represent a line in space (i.e. a line of phase singularity in this case).
Note that the connections files are stored in the same place as the executable to compute the filaments was run.

Primarily, the output from the filament detection is for visualisation purposes.
However, an additional series of files are also produced which list the elements that a line of phase singularity is present within.
These are (currently) stored as ``filament_* martin.elem``.
These are stored in the same place as the connection files.
They are very useful for looking at the connectivity of filaments in 3D i.e. deciding how many separate unique filaments you have at any given time, and looking for filament birth/death/splitting/amalgamation.
More details of such further quantification of filament dynamics can be found in Bishop \& Plank (2012) [#bishopplank]_.


Visualisation
-------------
Once the data is loaded into Meshalyzer, to improve visibility, in the connection properties window the size of the connections should be increased, surfaces are turned off and data are visualized on vertices. 
Vertex opacity can be turned on using the threshold level used for filament detection as the threshold for opacity. 
If filaments were detected correctly, the connections should appear right at the isoline.


Experiments
-----------

The below defined experiment runs on re-computed voltage data due to the domain size and time required to simulate a sustained arrhythmia.
To run this experiment

::

	cd ${TUTORIALS}/02_EP_tissue/09_filaments

Run

.. code-block:: bash

	./run.py --help

to see all exposed experimental parameters.

::

  --iso_threshold       defines the value of :math:`V_{m}`  which approximately correlates to activation (default value is -20 mV).

  --iso_intv            defines time window over which :math:`dV_{m}/dt=0` is evaluated (default value is 8 ms).


.. _exp-filament-isothresh:

**Experiment exp01**
^^^^^^^^^^^^^^^^^^^^

This experimental simply detects the phase singularities present.

Run this experiment by

:: 

	./run.py --iso_threshold -20 --iso_intv 8 --visualize

Note that in this test example, all output data has been copied across to the specifiec output direction defined by the date and the values of ``iso_threshold`` and ``iso_intv``.

Verify that the algorithm is detecting phase singularities at the centre of the sprial wave.
Repeat this experiment, varying the values of the two isofilament parameters and see how it affects the detection of the phase singularities.


Literature
==========

.. [#clayton] Clayton, R, Zhuvhkova, E, Panfilov, A, **Phase singularities and filaments: Simplifying complexity in computational models of ventricular fibrillation**,
   Progress in Biophysics and Molecular Biology 90:378-398, 2006. 
   `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/16098568>`__
   `[Full] <http://www.sciencedirect.com/science/article/pii/S0079610705000520?via%3Dihub>`__


.. [#bishopplank] Bishop M.J and Plank, G. **The role of fine-scale anatomical structure in the dynamics of reentry in computational models of the rabbit ventricles**
   Journal Physiology 590(18):4515-4535, 2012.
   `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/22753546>`__
   `[PMC] <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3467803/>`__
   `[Full] <http://onlinelibrary.wiley.com/doi/10.1113/jphysiol.2012.229062/abstract;jsessionid=3690DC002FF21257346C64879D283FCC.f03t01>`__
"""

EXAMPLE_DESCRIPTIVE_NAME = 'Phase singularities'
EXAMPLE_AUTHOR = 'Martin Bishop <martin.bishop@kcl.ac.uk>'

import os
import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--iso_threshold',
                        type=float,
                        default=-20,
			help='defines the value of Vm  which approximately correlates to activation (mV)')
    group.add_argument('--iso_intv',
                        type=float,
                        default=2,
			help='defines time window over which dVm/dt=0 is evaluated (ms).')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_thresh_{}_int_{}'.format(today.isoformat(), args.iso_threshold, args.iso_intv)

@tools.carpexample(parser, jobID,  clean_pattern='^(\d{4}-\d{2}-\d{2})')
def run(args, job):
  
    # Defines mesh
    meshname = 'mesh/slab'

    # Defines Vm data
    vmFile = 'vmData/vm.igb.gz'

    # Creats command line
    cmd = [ settings.execs.GLFILAMENT, meshname, vmFile, args.iso_threshold, args.iso_intv ]

    # Run simulation with GlFilament
    job.bash(cmd)


    # Moves all relevant files to simID
    if not args.dry:
        cmdMake = 'mkdir %s'  % (job.ID)
        os.system(cmdMake)	
        cmdMv = 'mv filament* mesh/slab.*_t %s' % (job.ID)
        os.system(cmdMv)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        auxGrid = '%s/slab.pts_t' % (job.ID)
        geom    = meshname
        data    = vmFile 
        view    = 'visualisation.mshz'

        job.meshalyzer(geom, data, auxGrid, view)



if __name__ == '__main__':
    run()


#!/usr/bin/env python

"""
Show transmural dispersion of repolarization.
"""

EXAMPLE_DESCRIPTIVE_NAME = 'TDR'
EXAMPLE_AUTHOR = ('Gernot Plank <gernot.plank@medunigraz.at> '
                  'and Aurel Neic <aurel.neic@medunigraz.at>')

import os
import sys
from datetime import date
from math import log

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import ep
from carputils.carpio import dat2adj
from carputils import testing

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--sourceModel',
                        default='monodomain',
                        choices=ep.MODEL_TYPES,
                        help='pick type of electrical source model')
    group.add_argument('--propagation',
                        default='R-E+',
                        choices=ep.PROP_TYPES,
                        help='pick propagation driver, either R-D (reaction-diffusion), R-E (+) with (-) without diffusion'
                            +' or AP-E (prescribed AP without diffusion) shape with trigger instant')
    group.add_argument('--duration',
                        type=float, default=25.0,
                        help='duration of experiment, typically 25. ms for '
                            +'activation only, 400 ms to include repolarization')
    group.add_argument('--tag',
                        default='',
                        help='add tag to simulation ID')
    return parser


def setupItrStim(stimIdx,geom):

    numStims = 1
    Itr = ['-stimulus[%d].stimtype'%(stimIdx),    0,
           '-stimulus[%d].strength'%(stimIdx),  250.,
           '-stimulus[%d].duration'%(stimIdx),    2.,
           '-stimulus[%d].start'%(stimIdx),       2.  ]

    # define electrode locations
    endoStim = mesh.block_boundary_condition(geom, 'stimulus', stimIdx, 'x', lower=True,
                                             verbose=False)

    Itr += endoStim

    return Itr, numStims

def setupPropagation(propagation, numStims, stims):

    if propagation != 'AP-E':
        propOpts, numStims, stims = ep.setupPropagation(propagation, numStims, stims, '')
        if propagation == 'R-D':
            dt =  20.
        else:
            dt = 100.
    else:
        IDs   = [0, 1, 2]
        names = ['EPI', 'MID', 'ENDO' ]
        propOpts, numStims, stims = ep.setupPropagation(propagation, numStims, stims, '', None, IDs, names)
        dt = 1000.

    return propOpts, dt, numStims, stims


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    tpl = '{}_{}_{}_{}_np{}{}'
    return tpl.format(today.isoformat(), args.sourceModel, args.propagation,
                      args.flv, args.np, args.tag)


@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    geom = mesh.Block(size=(10, 0.1, 0.1))
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Define regions
    epi = mesh.BoxRegion((-6,-0.1,-0.1), (-4,0.1,0.1), tag=1)
    geom.add_region(epi)
    mid = mesh.BoxRegion((-4,-0.1,-0.1), (4,0.1,0.1), tag=5)
    geom.add_region(mid)
    endo = mesh.BoxRegion((4,-0.1,-0.1), (6,0.1,0.1), tag=3)
    geom.add_region(endo)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # Add all the non-general arguments
    cmd  = tools.carp_cmd('tdr.par')
    cmd += ['-simID', job.ID]

    # Determine model type
    cmd += ep.model_type_opts(args.sourceModel)

    # define transmembrane stimuli
    stims, numStims = setupItrStim(0,geom)

    # pick propagation mode
    propOpts, dt, numStims, stims = setupPropagation(args.propagation, numStims, stims)

    # final assembly of command line
    stimOpts = [ '-num_stim', numStims ] + stims + [ '-dt', dt ]
    cmd     += stimOpts + propOpts

    cmd += ['-meshname',     meshname,
            '-tend',         args.duration,
            '-dump_im',      1 ]

    # adjust output resolution
    spacedt_ms = 0.1
    dt_ms      = float(dt)/1000.
    if dt_ms > spacedt_ms:
        spacedt_ms = dt_ms

    cmd += ['-spacedt', spacedt_ms]

    if args.visualize:
        cmd += ['-gridout_i', 3]

    # Run simulations
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb')
        view = 'tdr.mshz'

        job.gunzip(data)
        job.meshalyzer(geom, data, view)


if __name__ == '__main__':
    run()

#!/usr/bin/env python

"""
.. _purkinje-tutorial:


Purkinje System
===============

.. sectionauthor:: |EVigmond|

The Purkinje System (PS) is the fast conducion system of the ventricles. 
It starts with the bundle of His, and then bifurcates into left and right bundle branches.
The left bundle branch (LBB) splits into three fascicles, while the Right Bundle Branch (RBB) has a branch which follows the moderator band.

Connections are only made at discrete end points, called Purkinje-Myocyte Junctions (PMJs) with anterograde conduction across the PMJ being much slower (3-12 ms) 
than retrograde conduction (1-5 ms). This asymmetry in conduction can be captured.

.. _fig-period-bcs:

.. figure:: /images/TBC_PS.png
   :align: center
   :width: 50%

   A rabbit ventricular mesh with Purkinje system 

Specification
-------------
The file describing the PS is separate from the finite element mesh. It has a specific format as described in 
:ref:`purkinje-file`.
Each cable in the system is allowed to have up to 2 cables which branch from it, the sons.
Simialrly, a cable can have up to 2 parent cables. 

Nodes in the PS are defined in the PS file so they do not have to be nodes of 
the ventricular finite element mesh.


Purkinje-Myocyte Coupling
-------------------------

PMJs are formed at the end of a cable with no sons. 
For propagation to occur from the PS to the myocardium, a liminal volume must be stimulated, and not just a single myocardial node.
All myocardial nodes less than ``PMJsize`` micrometers from the
terminal node in the PS cable are coupled to the 
Purkinje node.
If at least ``nPMJmin`` nodes are not contained within the PMJ volume,
or more than  ``nPMJmax`` nodes are contained, 
``PMJsize`` is automatically adjusted until the conditions are satisfied.


.. figure:: /images/PMJprop.png
   :align: center
   :width: 60%

   Asymmetry in propagation across a PMJ. The top trace shows activity entering from the PS into the myocardium, while the bottom shows activity from the myocardium entering the PS.


Anterograde propagation, from the PS to the myocardium, is described by

.. math::

    V_{myo}( t+\Delta t) += (V_{PS}-V_{myo})e^{-(\Delta t)/(C_m R_{\\text{PMJ}})}

with :math:`V_{PS}` and :math:`V_{myo}` being
the PS and myocardial transmembrane voltages respectively.
The PS is assumed to branch within the PMJ volume, below the level of discretization, so that the load seen is reduced by the factor ``PMJscale``.

.. math::

    i_{PS} = \\frac{1}{\\text{PMJscale}} \sum \\frac{V_{PS}-V_{myo}}{R_{\\text{PMJ}}}

By adjusting ``Rpmj`` and ``PMJscale``, the asymmetry of conduction across the PMJ can be tuned.

========= ======================================================
Option    Meaning
========= ======================================================
Rpmj      the resistance of PMJ juntion
PMJscale  divide PS load by this factor
nPMJ_min  minimum number of myocardial nodes
nPMJ-max  maximum number of myocardial nodes
PMJsize   distance from end point to search for myocardial nodes 
========= ======================================================

Propagation
-----------

Propagation velocity  within the PS is rapid, >4 m/s. 
The is controlled by setting the values of the sodium current in the ionic model,
and the values of the conductivity (``purk_cond``) and gap junction resistance
(``purk_resist``) in the PS.
Conductivity is homogenized, :math:`\sigma^*_i`, and that value used as defined below.

.. math::

    \\frac{L}{4\pi\sigma^*_i \\rho^2} = \\frac{L}{4\pi\sigma_i \\rho^2}+R_{GJ}

if ``purk_effcond`` is nonzero, the value of ``cell_length`` is used for the value of 
*L*, else, the actual internodal distance is used. If ``purkIgnoreGlobals``
is nonzero, then the values of resistance and conductivity are set on a cable
by cable basis as defined in the ``.pkje`` file, 
else they are set globally from the command line options below.

================= =============================================================
Option            Meaning
================= =============================================================
purk_effcond      effective conductivity is constant, else cable specific
purk_cond         intracellular conductivity of all PS cables, :math:`\sigma_i`
purk_resist       gap junction resistance between cable, :math:`R_{GJ}`
purkIgnoreGlobals ignore *purk_resist* and *purk_cond* if set
cell_length       nominal cell length
================= =============================================================

Experimental Setup
------------------

Experiments will be run in a slab with a relatively simple Purkinje network composed of a His cable 

Experiments
-----------

The below defined experiments demonstrate propagation from a single cable into a piece of tissue.
To run these experiments

::

    cd tutorials/02_EP_tissue/19_Pkje

Run

.. code-block:: bash

    ./run.py --help

to see all exposed experimental parameters

::

  --Rpmj RPMJ           resistance across PMJ (1000.0 kOhm)
  --cond COND           PS conductivity (0.6 S/m)
  --Rgj RGJ             resistance between PS cells (100.0 kOhm)
  --GNa GNA             scale conductance of sodium channel (1.0)
  --PMJscale PMJSCALE   scale PS current load (1000.0)
  --PS-act {His,retro}  type of Purkinje system activation (His)
  --nPMJ NPMJ NPMJ      number of min and max myocardial nodes/PMJ [10, 30]

**Experiment exp01**
^^^^^^^^^^^^^^^^^^^^

Observe transmission across the junction in the anterograde direction, that is, from the PS into the myocardium.

.. code-block:: bash

   ./run.py --visualize

How does it change if you change the value of Rpmj or PMJscale?

.. code-block:: bash

   ./run.py --visualize --Rpmj=10000

**Experiment exp02**
^^^^^^^^^^^^^^^^^^^^

We will look at retrograde conduction which occurs when activity from the myocardium enters into the PS
by crossing the PMJ.

.. code-block:: bash

   ./run.py --visualize --PS-act=retro --PMJscale 1000

How does it change if you change the value of the junctional parameters like Rpmj or PMJscale?


Literature
==========

.. [#clements2007] Clements and Vigmond, 
   Construction of a computer model to investigate sawtooth effects in the Purkinje system, 
   IEEE Trans Biomed Eng, 54(3):389-99, 2007.
   `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/17355050>`__
   `[Full] <https://ieeexplore.ieee.org/document/4100851>`__

.. [#boyle2010] Boyle, Deo, Plank, and Vigmond,
   Purkinje-mediated effects in the response of quiescent ventricles to defibrillation shocks,
   Ann Biomed Eng, 38(2):456-68, 2010.
   `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/19876737>`__
   `[Full] <https://link.springer.com/article/10.1007%2Fs10439-009-9829-4>`__

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Purkinje System'
EXAMPLE_AUTHOR = ' Edward Vigmond <edward.vigmond@u-bordeaux.fr> '

import os
import sys
import subprocess
from datetime import date

from carputils import settings
from carputils import tools
from carputils import testing
from carputils import mesh
from carputils import ep
from carputils import model

def parser():
    parser = tools.standard_parser()
    group = parser.add_argument_group( "experiment specific options" )
    group.add_argument('--Rpmj',
                        type=float,
                        default=1000.,
                        help='resistance across PMJ (%(default)s kOhm)' )
    group.add_argument('--cond',
                        type=float,
                        default = 0.6,
                        help = 'PS conductivity (%(default)s S/m)' )
    group.add_argument('--Rgj',
                        type=float,
                        default=100.,
                        help='resistance between PS cells (%(default)s kOhm)')
    group.add_argument('--GNa',
                        type=float,
                        default=1.,
                        help='scale conductance of sodium channel (%(default)s)')
    group.add_argument('--PMJscale',
                        type=float,
                        default=100.,
                        help='scale PS current load (%(default)s)')
    group.add_argument('--PS-act',
                        choices=['His','retro'],
                        default='His',
                        help='type of Purkinje system activation (%(default)s)' )
    group.add_argument('--nPMJ',
                        nargs=2,
                        default=[10, 30],
                        help = 'number of min and max myocardial nodes/PMJ %(default)s' )
    return parser


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_np_{}_{}'.format(today.isoformat(), 
                                             args.np,  args.PS_act)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh (units in mm)
    geom = mesh.Block(size=(6,6,1))

    # Generate and return base name
    meshname = mesh.generate(geom)

    # define S1 stimulus
    edge = 3000
    S1 = [model.Stimulus( 'S1',x0=-edge-1,y0=-edge-1,z0=-501,
        xd=edge*2+2, yd=202,zd=202,strength=60,duration=2,start=0)]

    # Get basic command line, including solver options
    cmd = tools.carp_cmd() + [ '-simID', job.ID ]

    His = args.PS_act=='His'
    vofile = 'vm_{}ro'.format('ante' if His else 'ret')

    # adds stimulus options
    if not His : cmd += model.optionlist(S1)
    
    cmd += ['-meshname',                meshname,
            '-tend',                     30,
            '-dt',                       10,
            '-spacedt',                 0.5,
            '-bidomain',                  0,
            '-vofile',                  vofile,
            '-His_n',                     1 if His else 0,
            '-PMJsize',                 300,
            '-Rpmj',                    args.Rpmj,
            '-PMJscale',                args.PMJscale,
            '-purk_cond',               args.cond/1000.,
            '-purkf',                  'testVe_X.ncf',
            '-nPMJ_min',                args.nPMJ[0],
            '-nPMJ_max',                args.nPMJ[1],
            '-PurkIon[0].im_param',    'G_Na*'+str(args.GNa),
            '-ellip_use_pt',              0, #from here below, override default solver
            '-parab_options_file',        ""]

    # Visualization
    if args.visualize  :
        # Prepare file paths
        geom  = os.path.join(job.ID, 'myopurk' )
        data  = os.path.join(job.ID, vofile+'.igb')
        view  = vofile[3:]+'.mshz'

    # Run simulation
    job.carp(cmd)

    if args.visualize and not settings.platform.BATCH:
        job.meshalyzer(geom, data, view, compsurf=True)
    
# Define some tests
# -----------------------------------------------------------------------------
# none
# -----------------------------------------------------------------------------

if __name__ == '__main__':
    run()

#!/usr/bin/env python


"""

Introduction
============


Experimental Setup
==================

The geometry and the electrodes are defined as follows:

.. _fig-laplace-solve-setup:

.. figure:: /images/02_EP_tissue_16_dogbone.png
    :width: 50 %
    :align: center
    
    A tetrahedral FE slab model with dimensions 30.0mm x 30.0mm x 1.0mm is generated within mesher. 
  
Experiment
==========

To navigate to this tutorial

::

    cd ${TUTORIALS}/02_EP_tissue/17_bidm_pacing

To initiate the tutorial by making tissue refractory, first run

::

    ./run.py --visualize 
    
Different pacing protocols can then be applied at 300 seconds into the simulation using both cathodal and anodal
stimulation using the``--pacing_type`` option:

Initiate an activation using a cathodal stimulus
::

    ./run.py --visualize --pacing_type cathode_make

Initiate an activation using a cathodal stimulus
::

    ./run.py --visualize --pacing_type anode_make

Initiate an activation using a cathodal stimulus
::

    ./run.py --visualize --pacing_type cathode_break

Initiate an activation using a cathodal stimulus
::
    
    ./run.py --visualize --pacing_type anode_break

.. Note::

    - The make_refractory pacing_type must be ran before the different pacing protocols can be applied. 

"""

#EXAMPLE_DESCRIPTIVE_NAME = 'Pacing Protocols'
#EXAMPLE_AUTHOR = 'Karli Gillette <karli.gillette@gmail.com>'

from datetime import date
from carputils import settings, tools, mesh, ep
import os, shutil

def parser():
    parser = tools.standard_parser()

    parser.add_argument('--pacing_type',
                        default='make_refractory',
                        choices=['make_refractory',
                                 'cathode_make',
                                 'anode_make',
                                 'cathode_break',
                                 'cathode_break_eq_aniso',
                                 'anode_break'])

    return parser

def jobID(args):
    """
    Generate name of top level output directory based on the pacing type.
    """

    return args.pacing_type

@tools.carpexample(parser, jobID)
def run(args, job):

    #Setup the simulation based on pacing type:
    cmd = tools.carp_cmd('stimulation.par')

    #Setup the block mesh:
    meshname, geom = construct_block(job)
    cmd += ['-meshname', meshname]

    #Setup stimulus of the block
    cmd += setup_stim(geom)

    cmd += eval(args.pacing_type)()
    cmd += ['-simID',job.ID]

    job.carp(cmd)
    if args.visualize: job.meshalyzer(meshname,
                                      os.path.join(job.ID,'vm.igb'),
                                      'view_rot.mshz')

    return

def make_refractory():

    print("Making Tissue Refratory")

    cmd = ['-bidomain', 0,
            '-tend', 310.01,
            '-ellip_options_file','amg_opts_new',
            '-write_statef','refractory',
            '-chkpt_start',280.,
            '-chkpt_stop', 310.,
            '-chkpt_intv',5.]

    #Setup stimulus:
    cmd += ['-stimulus[0].strength', 50,
            '-stimulus[0].duration',  5,
            '-stimulus[0].stimtype',   0]

    return cmd


def cathode_make():

    print("Running cathode make experiment")

    cmd = ['-tend', 12.00]

    cmd += ['-stimulus[0].strength', -1e6,
            '-stimulus[0].duration',  10]

    return cmd

def anode_make():

    print("Running anode make experiment")

    cmd = ['-tend', 12.00]

    cmd += ['-stimulus[0].strength', 10e6,
            '-stimulus[0].duration', 10,
            '-stimulus[1].strength', 0,
            '-stimulus[1].stimtype', 1,
            '-stimulus[3].strength', 0,
            '-stimulus[3].stimtype',  1]

    return cmd

def cathode_break():

    print("Running cathode break experiment")

    if not os.path.exists('make_refractory'):
        raise IOError('Please run the make_refractory experiment first')

    cmd = ['-tend', 360.00,
           '-start_statef', os.path.join('make_refractory','checkpoint.300.0')]

    #Setup stimulus:
    cmd += ['-stimulus[0].strength', -10e6,
            '-stimulus[0].duration',  20,
            '-stimulus[0].start',   300]

    return cmd

def cathode_break_eq_aniso():

    print("Running cathode break experiment with equal anisotropy ratios")

    if not os.path.exists('make_refractory'):
        raise IOError('Please run the make_refractory experiment first')

    cmd = ['-tend', 360.00,
           '-gregion[0].g_et', 0.0682,
           '-start_statef', os.path.join('make_refractory','checkpoint.300.0')]

    #Setup stimulus:
    cmd += ['-stimulus[0].strength', -10e6,
            '-stimulus[0].duration',  20,
            '-stimulus[0].start',   300]

    return cmd

def anode_break():

    print("Running anode break experiment")

    if not os.path.exists('make_refractory'):
        raise IOError('Please run the make_refractory experiment first')

    cmd = ['-tend', 360.00,
           '-start_statef', os.path.join('make_refractory','checkpoint.300.0')]

    #Setup stimulus:
    cmd += ['-stimulus[0].strength', 10e6,
            '-stimulus[0].duration',  20,
            '-stimulus[0].start',   300]

    return cmd

def construct_block(job):

    #Clean up the mesh directory
    if os.path.exists('meshes'):
        shutil.rmtree('meshes')

    #Generate the block without bath
    geom = mesh.Block(size=(3,3,0.1))
    meshname=mesh.generate(geom)

    #Extract the surface of the main block
    mcmd = ['extract', 'surface',
            '-msh=' + meshname,
            '-surf=' + meshname,
            '-op=1']
    job.meshtool(mcmd)

    return meshname,geom

def setup_stim(geom):

    cmd = ['-num_stim',5]

    cmd += mesh.block_boundary_condition(geom, 'stimulus', 1, 'x', lower=False)
    cmd += mesh.block_boundary_condition(geom, 'stimulus', 2, 'y', lower=False)
    cmd += mesh.block_boundary_condition(geom, 'stimulus', 3, 'x', lower=True)
    cmd += mesh.block_boundary_condition(geom, 'stimulus', 4, 'y', lower=False)

    return cmd

if __name__ == '__main__':
    run()
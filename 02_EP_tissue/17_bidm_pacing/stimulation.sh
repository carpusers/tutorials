#!/bin/bash



# generate mesh first

meshcmd="$MESHER -size[0] 3.   \
                 -size[1] 3.   \
                 -size[2] 0.01 \
                 -mesh sheet   \
        "         

echo "Generating sheet"
echo
echo $meshcmd
#$meshcmd

makeRefractory="\
$LAUNCH -n $NP \
$CARP +F stimulation.par               \
      -stimulus[0].strength  50        \
      -stimulus[0].duration   5        \
      -stimulus[0].stimtype   0        \
      -bidomain 0                      \
      -simID make_refractory           \
      -ellip_options_file amg_opts_new \
      -tend 310.01                     \
      -write_statef refractory         \
      -chkpt_start 280.                \
      -chkpt_stop  310.                \
      -chkpt_intv    5.                \
"
echo "Make tissue refractory"
echo
echo $makeRefractory
#$makeRefractory

cathMake="\
$LAUNCH -n $NP \
$CARP +F stimulation.par               \
      -stimulus[0].strength -1e6       \
      -stimulus[0].duration  10        \
      -simID cathode_make              \
      -ellip_options_file amg_opts_new \
      -tend 12                         \
" 

echo "Running cathode make experiment"
echo
echo $cathMake
#$cathMake

anodeMake="\
$LAUNCH -n $NP \
$CARP +F stimulation.par               \
      -stimulus[0].strength  10e6      \
      -stimulus[0].duration  10        \
      -stimulus[1].strength   0        \
      -stimulus[1].stimtype   1        \
      -stimulus[3].strength   0        \
      -stimulus[3].stimtype   1        \
      -simID anode_make                \
      -ellip_options_file amg_opts_new \
      -tend 12                         \
" 

echo "Running anode make experiment"
echo
echo $anodeMake
#$anodeMake


cathBreak="\
$LAUNCH -n $NP \
$CARP +F stimulation.par               \
      -stimulus[0].strength  -10e6     \
      -stimulus[0].duration   20       \
      -stimulus[0].start     300       \
      -simID cathode_break             \
      -ellip_options_file amg_opts_new \
      -tend 360                        \
      -start_statef ./make_refractory/checkpoint.300.0 \
" 

echo "Running cathode break experiment"
echo
echo $cathBreak
#$cathBreak

cathBreakEqAniso="\
$LAUNCH -n $NP \
$CARP +F stimulation.par               \
      -stimulus[0].strength  -10e6     \
      -stimulus[0].duration   20       \
      -stimulus[0].start     300       \
      -simID cathode_break_eqAniso     \
      -gregion[0].g_et      0.0682     \
      -ellip_options_file amg_opts_new \
      -tend 360                        \
      -start_statef ./make_refractory/checkpoint.300.0 \
" 

echo "Running cathode break experiment with equal anisotropy ratios"
echo
echo $cathBreakEqAniso
$cathBreakEqAniso


anodeBreak="\
$LAUNCH -n $NP \
$CARP +F stimulation.par               \
      -stimulus[0].strength 10e6       \
      -stimulus[0].duration  20        \
      -stimulus[0].start     300       \
      -simID anode_break               \
      -ellip_options_file amg_opts_new \
      -tend 360                        \
      -start_statef ./make_refractory/checkpoint.300.0 \      
" 

echo "Running anode break experiment"
echo
echo $anodeBreak
#$anodeBreak

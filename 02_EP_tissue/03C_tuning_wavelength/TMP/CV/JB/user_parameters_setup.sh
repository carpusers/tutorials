#!/bin/bash

#################################################
#####DIRECTORY SETUP
#################################################
MAINDIR=/home/jkilla/software/carp-examples/tutorials/02_EP_tissue/12_tuning_wavelength/CV/JB

#################################################
#####Cable INPUTS
#################################################
RES=200  #This is the resolution of an element in um
NEL=75 #This is the number of elements in the cable

#################################################
#####PACING Parameters
#################################################
BCL=1000 #BCL of pacing in ms
CV=0.22 #CV to match in meters/second

#################################################
#####CONDUCTIVITY Parameters
#################################################
GINIT=0.028 #Set above 0.1 or else will not propogate
GRES=0.001 #Resolution to increment G
GTOL=0.005 #Tolerance for conduction velocity

#################################################
#####IONIC model parameters
#################################################
EXTERNAL=0 #Set to 1 if external needed and change directory below
IMP_SO=/home/jkilla/Collaborations/Ruben_Ach/IMP/CRN_ACH.so
IMP=GTT2_fast
IMP_PAR="ENDO=0"

#################################################
#################################################

#################################################
#####TECHNICAL INPUTS
#################################################
#Carp simulation parameteres
CARPDIR=/home/jbayer/Software/dcse/CARP/carp.debug.petsc.pt
MPIRUN=/opt/petsc/linux-gnu/bin/mpirun
NP=1

#SOLVERS
NUMCOMP=/home/jbayer/Software/dcse/NumComp/solver_opts
SOLVERS="-parab_options_file $NUMCOMP/ilu_cg_opts -ellip_options_file $NUMCOMP/amg_cg_opts -purk_options_file $NUMCOMP/mumps_opts_nonsymmetric -ellip_use_pt 0 -parab_use_pt 0 -purk_use_pt 0 -mechanics_options_file $NUMCOMP/amg_cg_opts -mech_use_pt 0 -pstrat 1 -pstrat_i 1 -pstrat_backpermute 1"

#MESHALYZER directory
MESHALYZER=/home/jbayer/Software/meshalyzer/meshalyzer

#DIRECTORY OF SOURCE
PROGDIR=/home/jkilla/Software/JB_Tools/CVMESH

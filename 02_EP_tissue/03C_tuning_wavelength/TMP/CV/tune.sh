#!/bin/bash

DX=200.0
CV=0.22
TOL=0.0001
GIL=0.1
GIT=0.1
MODEL=GTT2_fast
SOURCEMODEL=monodomain
RES=200.0

/home/jkilla/software/carputils/bin/tuneCV --converge true --tol $TOL --velocity $CV --model $MODEL --sourceModel $SOURCEMODEL --resolution $RES

#!/bin/bash

#################################################
#####DIRECTORY SETUP
#################################################
MAINDIR=/home/jkilla/software/carp-examples/tutorials/02_EP_tissue/12_tuning_wavelength

#################################################
#####TECHNICAL INPUTS
#################################################
#Carp simulation parameteres
CARPDIR=/home/jbayer/Software/dcse/CARP/carp.debug.petsc.pt
MPIRUN=/opt/petsc/linux-gnu/bin/mpirun
NP=4

#SOLVERS
NUMCOMP=/home/jbayer/Software/dcse/NumComp/solver_opts
#SOLVERS="-parab_options_file $NUMCOMP/ilu_cg_opts -ellip_options_file $NUMCOMP/amg_cg_opts -purk_options_file $NUMCOMP/mumps_opts_nonsymmetric -ellip_use_pt 0 -parab_use_pt 0 -purk_use_pt 0 -mechanics_options_file $NUMCOMP/amg_cg_opts -mech_use_pt 0 -pstrat 1 -pstrat_i 1 -pstrat_backpermute 1"
SOLVERS=" "




#Run carp
$MPIRUN -np $NP ${CARPDIR} \
+F $MAINDIR/tuning_wavelength_state.par \
$SOLVERS >& $MAINDIR/tuning_wavelength_state.log

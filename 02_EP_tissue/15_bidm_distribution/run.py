#!/usr/bin/env python

r"""



Introduction
=============
The aim of this tutorial is to study the effect of changes in fiber orientation on the electrical behavior of cardiac tissue.
Generally, electrical properties of the myocardial tissue can be described by the bidomain model
accounting for the intracellular and extracellular domain. The domains are of anisotropic nature, the intracellular domain
even more so than the extracellular domain resulting in ''unequal anisotropy ratios'' affecting the heart's electrical behavior
causing two effects ''rotation'' and ''distribution''.  This tutorial focuses on the ''distribution'' effect, which refers
to the relative conductivity of the intracellular and extracellular domains. As current takes the path of least resistance
more current will flow in the domain where conductivity is the highest. The relative amount of current in the two spaces is defined
by their respective anisotropy ratios.  [#roth]_


Experimental Setup
==================
The setup is identical to the setup in the ''Fiber Angle Stimulation I - rotation'' tutorial, except a gradual change in fiber orientation along x is implemented.

A 2D strip of infinite length of width :math:`W=10` mm is given. The fiber angle varies from :math:`\alpha = 90^{\circ}` to :math:`\alpha = 0^{\circ}` over the length of the strip.
(marked as dotted gray lines in Fig. below). Conductivities are given as :math:`g_{iL} = 0.2` S/m, :math:`g_{iT} = 0.02` S/m,  :math:`g_{eL} = 0.2` S/m and  :math:`g_{eT} = 0.08` S/m
where L and T indicate directions along and transverse to the fiber orientation, respectively (Anisotropy ratios are unequal!). The
tissues membrane surface-to-volume ratio and the resistance are given as :math:`\beta = 0.14\mu m^{-1}`  and
:math:`R_{m} = 0.91 \Omega m^{2}`.


.. figure:: /images/02_EP_tissue_15_bidm_distribution_experimentalsetup.png
     :scale: 20 %

For numerical analysis we assume that the length L of the strip is not infinite, but
:math:`L\gg\lambda`. For the data given choosing L=60mm should suffice. The spatial resolution should be used as height of the strip (only one layer of 3D elements).
Thus, a hexahedral FE model with dimensions of 60mm x 10mm x 1mm is generated functioning as cardiac tissue strip.
An orthotropic fiber setup may be used  if the sheet angle differs from :math:`0^\circ`. As passive ionic model the modified Beeler-Router Druhard-Roberge model as implemented in LIMPET is used
with :math:`R_{m}` set to :math:`0.91 \Omega m^{2}` and :math:`\beta = 0.14\mu m^{-1}`. Current :math:`\mathbf{J}` is injected by Electrode A at x=-L/2 in x-direction and withdrawn by electrode B at x=+L/2.
Electrode C is located at x=0 and can be gounded or not.

Unequal anisotropy is assigned using the following values:

.. code-block:: bash

    gregion[0].g_il          = 0.2
    gregion[0].g_it          = 0.02
    gregion[0].g_in          = 0.02
    gregion[0].g_el          = 0.2
    gregion[0].g_et          = 0.08
    gregion[0].g_en          = 0.08

The simulation is solved using the parabolic formulation of the bidomain equations with the Crank-Nicolson method.


Input Parameters and Usage
==========================

Go to the following directory to run underlying experiment:

.. code-block:: bash

    cd ${TUTORIAL}/02_EP_tissue/14_bidm_rotation/

The following experiment specific options are available (default values are indicated):

.. code-block:: bash

    ./run.py --angle-epi            #pick left bound of fiber angle (0 is along the long edge of the preparation) (default:-60)
             --angle-endo           #pick right bound of fiber angle (default:+60)
             --sheet-angle          #pick sheet-angle and thus use orthotropic fiber setup (default:0, transversely isotropic fiber setup)
             --gil                  #pick intracellular conductivity longitudenally to fiber direction (S/m) (default: 0.2)
             --git                  #pick intracellular conductivity transversely to fiber direction (S/m)  (default: 0.02)
             --gel                  #pick extracellular conductivity longitudenally to fiber direction (S/m)  (default: 0.2)
             --get                  #pick extracellular conductivity transversely to fiber direction (S/m)  (default: 0.08)
             --duration             #pick duration of simulation (ms)
             --resolution           #choose mesh resolution in um (default: 250)
             --grounded {on, off}   #turn on/off ground (default:off)

.. Note::

    If a high mesh resolution is chosen it may take longer to run. You may use the ``--np`` option to increase number of cores.
    To visualize results with meshalyzer use ``--visualize``.

    ``--grounded`` - A ground can be used or not. In the latter case, no grounding electrode
    is used, that is, a pure Neumann-problem is solved. A rank-one stabilization is used
    in this case which essentially enforces the integral over the extracellular potential
    to be zero at each instant in time, but no specific point is clamped.


Results
=================

Numerical solutions are shown in terms of  the extracellular potential :math:`\phi_e` and transmembran voltage :math:`V_m` (frontal and rear view of strip).
The fiber angle is chosen to vary from :math:`90^{\circ}` to :math:`0^{\circ}` over the strip (transversely isotropic fiber setup with sheet angle of :math:`0^\circ`.


.. figure:: /images/02_EP_tissue_15_bidm_distribution_results_new.png
     :scale: 15 %



Interpretation
=================
On the left side of the tissue strip (see Fig. below, hyper and depolarisation effects at sealed boundaries are ignored) fibers are oriented in y-direction and current flows perpendicular to the fibers.
Extracellular conductivity transverse to fiber orientation :math:`g_{eT}=0.08` is much bigger
than the intracellular conductivity in this direction with :math:`g_{iT}=0.02`. As current takes the path of least resistance more current will flow
in the extracellular space. From x=-L/2 to x=+L/2 the fibers orientation changes gradually along x reaching a fiber angle of :math:`0^\circ` towards
the end of the strip. Intra- and extracellular conductivities are equal in fiber direction with :math:`g_{iL}=g_{eL}=0.20` resulting in similar
current densities :math:`J_i` and :math:`J_e`. In the middle of the strip, where the fiber angle varies, current must redistribute from the extracellular domain to
the intracellular domain causing hyperpolarization of the tissue. [#roth]_


.. figure:: /images/02_EP_tissue_15_bidm_distribution_analytical.png
     :scale: 20 %


Simulation results confirm theoretical expectations. It can be observed that :math:`\phi_e` is rotated towards the fiber direction over length of the strip.
Gradient :math:`\frac{\partial{V_m}}{\partial{y}}` hyperpolarizes and depolarizes edges of the tissue strip at
:math:`y=-W/2` and :math:`y=+W/2` but due to distribution effect based on changing fiber angle only around the center, where current must redistribute from the extracellular
to the intracellular domain.


Literature
==========

.. [#sepulveda] Sepulveda, Nestor G., Bradley J. Roth, and John P. Wikswo Jr., **Current injection into a two-dimensional anisotropic bidomain**,
 Biophysical journal 55.5 (1989): 987-999.
   `[Pubmed] <https://pubmed.ncbi.nlm.nih.gov/2720084/>`__
   `[Full] <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1330535/>`__
.. [#rothwood] Bradley J. Roth, and Marcella C. Woods, **The magnetic field associated with a plane wave front propagating through cardiac tissue**,
 IEEE transactions on biomedical engineering, Vol. 46, No.11 (1999).
   `[Pubmed] <https://pubmed.ncbi.nlm.nih.gov/10582413/>`__
.. [#roth] Bradley J. Roth, **How to explain why unequal anisotropy ratios is important usind pictures but no mathematics**,
 International Conference of the IEEE Engineering in Medicine and Biology Society (2006).
   `[Pubmed] <https://pubmed.ncbi.nlm.nih.gov/17946406/>`__

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Fiber Angle Stimulation II - Distribution'
EXAMPLE_AUTHOR = 'Laura Marx <laura.marx@medunigraz.at>'

import os
import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing
import numpy as np
import carputils.carpio.txt as cutxt
import subprocess
import matplotlib.pyplot as plt

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--angle-lb',
                        default=90,
                        help='pick fiber angle at left boundary (0 is along the long edge of the preparation)')
    group.add_argument('--angle-rb',
                        default=0,
                        help='pick fiber angle at right boundary (0 is along the long edge of the preparation)')
    group.add_argument('--sheet-angle',
                        default=0.0,
                        help='pick sheet angle (if sheet-angle=0.0 anisotropic fiber setup is used, otherwise orthotropic fiber setup is used)')
    group.add_argument('--gil',
                        default=0.2,
                        help='pick intracellular conductivity longitudenally to fiber direction (S/m) (default: 0.2)')
    group.add_argument('--git',
                        default=0.02,
                        help='pick intracellular conductivity transversely to fiber direction (S/m) (default: 0.02)')
    group.add_argument('--gel',
                        default=0.2,
                        help='pick extracellular conductivity longitudenally to fiber direction (S/m) (default: 0.2)')
    group.add_argument('--get',
                        default=0.08,
                        help='pick extracellular conductivity transversely to fiber direction (S/m) (default: 0.08)')
    group.add_argument('--duration',
                        type=float, default=10.,
                        help='Duration of simulation (ms)')
    group.add_argument('--resolution',
                        default=250,
                        help='pick resolution in um')
    group.add_argument('--grounded',
                        default='off',
                        choices=['on','off'],
                        help='turn on/off use of ground whereever possible')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_angle_{}'.format(today.isoformat(), args.angle_lb)



def changeFibers(meshname,args):
    # ----------------------------
    # create new .lon file

    # read points
    pnts_file = cutxt.TxtFile(meshname + '.pts', 'r')  # 'r' for read
    pnts_data = pnts_file.data()
    pnts = pnts_data[0]  # the acutal point data, N_nodes x3 numpy array

    # read elements
    elem_file = cutxt.TxtFile(meshname + '.elem', 'r')  # 'r' for read
    elem_data = elem_file.data()
    elem_nidx = elem_data[0]  # holds node indices for each element
    num_elems = elem_data[2]  # number of elements

    # find x,y,z coordinates for the four nodes for each element
    elem_x = np.take(pnts.T[0], elem_nidx)
    #elem_y = np.take(pnts.T[1], elem_nidx)
    #elem_z = np.take(pnts.T[2], elem_nidx)

    # compute coordinate of centriode
    centriod_x = (elem_x.T[0] + elem_x.T[1] + elem_x.T[2] + elem_x.T[3]) / 4
    #centriod_y = (elem_y.T[0] + elem_y.T[1] + elem_y.T[2] + elem_y.T[3]) / 4
    #centriod_z = (elem_z.T[0] + elem_z.T[1] + elem_z.T[2] + elem_z.T[3]) / 4

    #centriod = np.stack((centriod_x, centriod_y, centriod_z))

    # find out how many different x_coordinates exist for the centriodes
    centriod_x_diffval, centriod_x_diffcnt = np.unique(centriod_x, return_counts=True)
    centriod_x_ndiffval = len(centriod_x_diffval)

    # calculate fiber rotation vector for the different values of x
    fiber_rot = np.linspace(args.angle_lb, args.angle_rb, centriod_x_ndiffval)
    theta = np.radians(fiber_rot)

    start_a = np.array([1.0, 0.0])
    fiber_rot_vec = []
    for i in range(len(theta)):
        rotMatrix = np.array([[np.cos(theta[i]), -np.sin(theta[i])],
                              [np.sin(theta[i]), np.cos(theta[i])]])
        fiber_rot = np.dot(rotMatrix, start_a)
        fiber_rot_vec.append(fiber_rot)

    fibers_xy = []
    for idx, item in enumerate(centriod_x):
        idx_ = int(np.where(centriod_x_diffval == item)[0])
        fibers_xy.append(fiber_rot_vec[idx_])

    fibers_z = np.zeros(num_elems)


    if args.sheet_angle == 0.0:
        fibers=np.stack((np.array(fibers_xy).T[0], np.array(fibers_xy).T[1], fibers_z)).T
        # write lon file
        with open(meshname + '.lon', 'w') as fp:  # 'w' for write
            fp.write('1\n')
            np.savetxt(fp, fibers, fmt='%.8f')

    else:
        # get sheet vectors from old .lon file
        sheet_x, sheet_y, sheet_z = np.loadtxt(meshname + '.lon', skiprows=1, usecols=(3, 4, 5)).T

        # create array to fit structure of .lon file
        # f_x,f_y,f_z,s_x,s_y,s_z
        fibers = np.stack((np.array(fibers_xy).T[0], np.array(fibers_xy).T[1], fibers_z, sheet_x, sheet_y, sheet_z)).T

        # write lon file
        with open(meshname + '.lon', 'w') as fp:  # 'w' for write
            fp.write('2\n')
            np.savetxt(fp, fibers, fmt='%.8f')


#---------------------------------------------
# compute nodes indices at lying at center y=0, z=0 + save indices in .txt
# returns x_values of center nodes
# --------------------------------------------
def compute_center_nodes(meshname,args):
    pnts_file = cutxt.TxtFile(meshname + '.pts', 'r')  # 'r' for read
    pnts_data = pnts_file.data()
    pnts = pnts_data[0].T
    pnts_y = pnts[1]
    pnts_z = pnts[2]

    center_idx =[]
    for idx, item in enumerate(pnts_y):
        if item == 0 and pnts_z[idx] == 0:
            center_idx.append(idx)
        else:
            idx+=1

    center_node_valx = np.take(pnts[0], center_idx)

    with open(jobID(args) + '/center_nodes.txt', 'w') as f:
        for item in center_idx:
            f.write("%s\n" % item)
    f.close()


    fiber_rot = np.linspace(90,0, len(center_idx))

    def compute_gxx(gl, gt):  # args
        gxx_vec = []

        for i in range(len(fiber_rot)):
            gxx = gl * (np.cos(fiber_rot[i])) ** 2 + gt * (np.sin(fiber_rot[i])) ** 2
            gxx_vec.append(gxx)

        return np.array(gxx_vec).T

    gixx = compute_gxx(args.gil, args.git)
    gexx = compute_gxx(args.gel, args.get)
    return gixx, gexx, center_node_valx

#---------------------------------------------
# Extract values of specific nodes of igb file
# --------------------------------------------

def extract_igbfile(filename, filename_out, args):
    # compute phie at central nodes along x (y=0, z=0)
    cmd = ['igbextract']
    cmd +=[jobID(args) + '/' + filename]
    cmd +=['-i', jobID(args) +'/center_nodes.txt']
    cmd +=['-t', args.duration]
    cmd +=['-T', args.duration]
    cmd +=['-o', 'dat_t']
    cmd +=['-O', jobID(args) + '/' + filename_out]

    print(' '.join(map(str, cmd)))
    subprocess.check_call(map(str, cmd))


#------------------------------------------------
# compute phii
#------------------------------------------------
def compute_phii(filename_1, filename_2, args):
    vm_center = np.loadtxt(jobID(args) + '/' + filename_1, skiprows=2)
    phie_center = np.loadtxt(jobID(args) + '/' + filename_2, skiprows=2)
    return vm_center, phie_center, vm_center + phie_center

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    res = float(args.resolution)/1000. # convert um to mm
    geom = mesh.Block(size=(60, 10, 1),resolution=res)
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(45, 45, args.sheet_angle, args.sheet_angle)
    # Generate and return base name
    meshname = mesh.generate(geom)

    print('Creating new .lon file')
    fiber_rot = changeFibers(meshname,args)

    # define electrode locations
    lstm = mesh.block_boundary_condition(geom, 'stimulus', 0, 'x', lower=True,
                                        verbose=not args.silent)
    rstm = mesh.block_boundary_condition(geom, 'stimulus', 1, 'x', lower=False,
                                       verbose=not args.silent)
    astm = []
    mod  = []

    # define default view settings
    view = 'Phie_front_hyper.mshz'
    #view = 'Phie_back_depol.mshz'

    # make subtest specific settings
    # left electrode injects extracellular current
    # right electrode withdraws extracellular current
    # auxiliary electrode in the middle is grounded
    lstm += [ '-stimulus[0].stimtype', '1',
              '-stimulus[0].strength', '3.1e6']
    rstm += [ '-stimulus[1].stimtype', '1',
               '-stimulus[1].balance',  '0' ]
    if args.grounded == 'on':
        astm += [ '-stimulus[2].stimtype', '3' ]



    greg = ['-gregion[0].g_il', args.gil,
            '-gregion[0].g_it', args.git,
            '-gregion[0].g_el', args.gel,
            '-gregion[0].g_et', args.get]

    # Add all the non-general arguments
    cmd = tools.carp_cmd('distribute.par')

    cmd += ['-meshname',  meshname,
            '-gridout_i', 3,
            '-simID',     job.ID,
            '-tend', args.duration]

    cmd += greg

    # add PDE solver options
    cmd += lstm + rstm + astm

    if args.visualize:
        vis_cmd = ['-gridout_i', 3,
                   '-gridout_e', 3]
        cmd += vis_cmd

    # -------------------------------
    # Run simulations
    job.carp(cmd)

    print('Compute center nodes and xx components of conductivity tensors')
    gixx, gexx, x_coo_center = compute_center_nodes(meshname,args)

    print('Extract .igb data for nodes on centerline')
    extract_igbfile('phie.igb', 'phie_center', args)
    extract_igbfile('vm.igb', 'vm_center', args)

    print('Compute Jex and Jix along centerline')
    vm_center, phie_center, phii_center = compute_phii('phie_center.dat_t', 'vm_center.dat_t', args)
    Jex_center = (gexx * np.gradient(phie_center, x_coo_center)) * 10 ** 5
    Jix_center = (gixx * np.gradient(phii_center, x_coo_center)) * 10 ** 5

    # ------------------------------------------------
    # plot Jix und Jex at center of strip along x
    # ------------------------------------------------
    plt.figure()
    plt.title('$t=$' + str(args.duration) + 'ms')
    plt.ylabel('Current [$\mu$A/$\mathrm{cm^2}$]')
    plt.xlabel('Length of tissue strip [mm]')
    plt.plot(x_coo_center * 10 ** (-4), Jex_center, label='$J_\mathrm{ex}$')
    plt.plot(x_coo_center * 10 ** (-4), Jix_center, label='$J_\mathrm{ix}$')
    plt.tight_layout()
    plt.legend()
    plt.savefig(jobID(args) + '/currentalongx_center_8ms.pdf')
    plt.show()


    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # show phie first
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'phie.igb')

        job.gunzip(data)
        job.meshalyzer(geom, data, view)


        # show Vm, front view showing hyperpolarized edge
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb')

        view = 'Vm_front_hyper.mshz'
        #view = 'Vm_back_depol.mshz'

        job.gunzip(data)
        job.meshalyzer(geom, data, view)

# Define some tests
#__tests__ = []

if __name__ == '__main__':
    run()



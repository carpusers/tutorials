#!/usr/bin/env python

"""

Introduction
============

Unequal anisotropy ratios, as seen in cardiac tissue, can be responsible for the formation of unexpectedly complex
polarization patterns when applying stimuli through point sources. While an elliptical polarization pattern is normally expected
when applying a strong source potential, a dogbone pattern can be achieved under unequal conductive anisotropy.
The dogbone pattern is termed for the occurrence of two circular regions of opposite polarity (virtual electrodes) that
arise. The pattern can be achieved by applying anodal (-) or cathodal(+) simulation.

Experimental Setup
==================

The geometry and the electrodes are defined as follows:
    
**Model:** A hexahedral FE model with dimensions 20.0mm x 20.0mm x 10.0mm is submersed in a bath on both the upper and
lower ends with thickness 5.0 mm. Fibers are assigned in the model ranging from -60 to 60 degrees. The model is
automatically generated in mesher during the simulation.

.. figure:: /images/02_EP_tissue_16_dogbone_geometry.png
    :width: 75 %
    :align: center


**Stimulus:** A strong anodal or cathodal extracellular point stimulus (pt_stim) is applied just above the top surface of
the mesh in the bath. An extracellular ground electrode (GND) is assigned to the bottom of the bath.

**Assigning unequal anisotropy:** Conductivities are applied either anisotropically using the default carp values to
generate a dogbone pattern:

.. code-block:: bash

    gregion[0].g_il          = 0.174
    gregion[0].g_it          = 0.019
    gregion[0].g_in          = 0.019
    gregion[0].g_el          = 0.625
    gregion[0].g_et          = 0.236
    gregion[0].g_en          = 0.236


or to a single conductivity values of 0.625 to generate an elliptic pattern.

Usage
================================

The following optional arguments are available (default values are indicated):

.. code-block:: bash

    ./run.py --visualize         #Visualize results with meshalyzer
             --pt_stim           = anode (default), cathode #type of applied point source stimulus
             --no_anisotropy     #Turn off conductive anisotropy
             --res               # changes the resolution of the mesh [0.25,0.75] with default 0.5.


If the program is run with the ``--visualize`` option, meshalyzer will automatically
load the model with applied activation data showing the occurrence of the dog bone (8 seconds) in
the presence of anisotropy or an elliptical fir pattern without.

.. Note::

    This mesh is fairly high resolution and may take longer to run. Try increasing the number of cores using the
    ''--np'' option or changing the resolution of the mesh with the ``--res`` option.

Expected Results
================================

The expected outcomes of the four cases of applying a cathode or anode point stimulus with or without anisotropy are shown:

.. figure:: /images/02_EP_tissue_16_dogbone_results.png
    :width: 100 %
    :align: center


"""


EXAMPLE_DESCRIPTIVE_NAME = 'Dogbone Pattern Generation under Unequal Anisotropy'
EXAMPLE_AUTHOR = 'Karli Gillette <karli.gillette@gmail.com>'

import os, shutil

from datetime import date
from carputils import settings, tools, mesh, ep

def parser():
    parser = tools.standard_parser()

    parser.add_argument('--pt_stim',
                        default='anode',
                        choices=['anode','cathode'])
    parser.add_argument('--no_anisotropy',
                        action='store_true',
                        help='Turn on or off anistropy in the mesh')
    parser.add_argument('--res',
                        default=0.5,
                        type=float,
                        help='Change the resolution of the mesh. Please use a value in the range [0.25,0.75]. Default is 0.5.')

    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    # return '{}'.format(today.isoformat())
    ID='{}_dogbone_{}_res{}_aniso_{}'.format(today.isoformat(),
                                             args.pt_stim,
                                             str(args.res).replace('.','p'),
                                             ('on','off')[args.no_anisotropy])

    return ID



@tools.carpexample(parser, jobID)
def run(args, job):

    #Generate a block mesh:
    meshname=construct_block(job,args)

    # Setup command line and run simulation
    print('Setting up simulation....')

    cmd  = tools.carp_cmd()
    cmd += ep.model_type_opts('bidomain') #Using bidomain
    cmd += pt_shock(args) #Initialize Stimulus
    cmd += ['-meshname', meshname]

    #Setup up the Ionic Model Region:
    cmd += ['-num_imp_regions', 1]
    cmd += ['-imp_region[0].im', "PASSIVE"]
    cmd += ['-num_stim',2]

    if args.no_anisotropy:

        g_el=0.625

        cmd += ['-gregion[0].g_il',       g_el,
                 '-gregion[0].g_it',      g_el,
                '-gregion[0].g_in',       g_el,
                 '-gregion[0].g_el',      g_el,
                 '-gregion[0].g_et',      g_el,
                '-gregion[0].g_en',       g_el]

    #Stimulation Parameters:
    cmd += [ '-simID',                 job.ID,
             '-tend', 10,
             '-dt',100,
             '-spacedt', 1]

    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
            job.meshalyzer(meshname, job.ID + '/phie.igb','view_'+args.pt_stim+'.mshz')

    return

def construct_block(job,args):

    assert args.res >= 0.1 and args.res <= 0.7

    #Clean up the mesh directory
    if os.path.exists('meshes'):
        shutil.rmtree('meshes')

    #Generate the block with bath:
    geom = mesh.Block(size=(20,20,10), resolution=args.res, etype='hexa')
    geom.set_bath(thickness=(0,0,5), both_sides=True)
    

    geom.set_fibres(fibre_endo=-60.0,
                        fibre_epi=60.0,
                       sheet_endo=90,
                       sheet_epi=90)
        
    meshname=mesh.generate(geom)

    #Extract the surface of the main block
    mcmd = ['extract', 'surface',
            '-msh=' + meshname,
            '-surf=' + meshname,
            '-op=1']
    job.meshtool(mcmd)

    return meshname

def pt_shock(args):

    cmd = ['-stimulus[0].x0', -550]
    cmd +=['-stimulus[0].y0', -550]
    cmd +=['-stimulus[0].z0', 5050]
    cmd +=['-stimulus[0].xd', 1100]
    cmd +=['-stimulus[0].yd', 1100]
    cmd +=['-stimulus[0].zd', 1100]
    cmd +=['-stimulus[0].stimtype',  1]
    cmd +=['-stimulus[0].start',  0.0]
    cmd +=['-stimulus[0].duration',  8]

    if args.pt_stim == 'cathode':

        cmd +=['-stimulus[0].strength',  (1e8,1e7)[args.no_anisotropy]]

    elif args.pt_stim == 'anode':
        cmd +=['-stimulus[0].strength',  (-1e8,-1e7)[args.no_anisotropy]]

    cmd +=['-stimulus[1].x0', -15050 ]
    cmd +=['-stimulus[1].y0', -15050]
    cmd +=['-stimulus[1].z0', -10050]
    cmd +=['-stimulus[1].xd', 30100]
    cmd +=['-stimulus[1].yd', 30100]
    cmd +=['-stimulus[1].zd', 100]
    cmd +=['-stimulus[1].stimtype', 3]

    return cmd


if __name__ == '__main__':
    run()

#!/usr/bin/env python

r"""
.. _augmentation-tutorial:


Bath Loading
============

*In vivo*, but also under all common *in vitro* experimental conditions, 
cardiac tissue is surrounded by conducting fluid. 
*In vivo* its blood at the endocardial surfaces and pericardial fluid at the epicardial surface.
In experimental preparations tissue is perfused or superfused with Tyrode's solution. 
These surrounding fluid layers act to increase conduction velocity close to the tissue-bath interface
by reducing the resistance of the interstitial current pathways 
as local circuit currents are shunted through the bath domain where conductivity is higher. 
Thus depolarization wavefronts propagating in the vicinity of the tissue-bath interface
move faster than those in deeper layer of tissue,
thus inducing transmural wavefront curvature.
This effect is referred to as **bath-loading**. 
To account for bath loading in a computer model, in general, a full bidomain formulation is needed
which is computationally expensive.

Augmentation
============

Bidomain theory explicitly considers current flow in both intra- and extracellular
domains, and thus allows modeling of current flow and potential fields in the bath surrounding tissue.
In spite of the knowledge that bath loading modulates wavefront morphologies close to the tissue-bath interface, 
the vast majority of simulation studies use the monodomain approximation.
Monodomain models in a standard implementation are not able to account for bath loading effects,
but are roughly ~10x faster to compute than a bidomain model.
Thus in most computational model studies the monodomain model has been preferred.
However, using a technique referred to as **augmentation** bath loading effects can be taken into account
even when using a monodomain model. 
Briefly, the idea is to account for the lower interstitial resistance close to the tissue bath interface,
mediated by the lower resistance of the adjacent bath which shunts extracellular currents of local circuits
away from the interstitial current path, 
by using higher bath conducitivies within a thin layer along the tissue bath interface.
The width of this thin layer, referred to as **augmentation layer**, and the bath conductivy
introduce a bath loading effects very much comparable to those observed in full bidomain models. 
Details on **augmentation** are found in the literature 
where it has been demonstrated that augmented monodomain models faithfully replicate bath loading effects
with high fidelity, comparable to full bidomain simulation results. 
For details see the studies by Bishop et al. [#bishop_bathloading]_.

Pseudo-bidomain
===============

In addition to using **augmentation** as a method for improving the representation of electrical sources
the effect of **bath loading** upon extracellular electrograms can be studied by using a **pseudo-bidomain** approach.
The **pseudo-bidomain** model as opposed to a full **bidomain** model is attractive 
as it offers significant computational savings while providing a very close approximation 
(depending on how close the source distribution of the full bidomain model can be approximated by augmentation)
of a full bidomain solution with regard to extracellular potential fields.
The **pseudo-bidomain** in terms of computation is essentially a monodomain solve with infrequent elliptic solves
to recover the extracellular potential field :math:`\phi_{e}(\mathbf{x},t)` with appropriate adjustments of 
conductivity tensors. Details on the **pseudo-bidomain** approach are found again in Bishop et al [#bishop_bathloading]_.


This tutorial peruses to highlight all relevant carpentry parameters needed and
applied to a slab-like geometry.


Experimental Setup
------------------

.. _fig-pseudobdm-bathloading:

.. figure:: /images/bathloading_setup.png
   :align: center
   :width: 60%

   The setup uses a regular tetrahedral FE model, representing a 3-D slab
   of myocardial tissue with dimensions 1.50cm x 0.01cm x 1.00 cm (l,w,h) in the x-, y,
   and z-directions stimulated at the left front side.
   A conductive bath is attached along the +y and -y faces of the wedge,
   thus forming a tissue bath interface along which bath loading effects will occur.


Experiments
-----------

The below defined experiments demonstrate the wave front differences when choosing
different electrical source models. To run these experiments

::

    cd ${TUTORIALS}/02_EP_tissue/07A_augmentation

Run

.. code-block:: bash

    ./run.py --help

to see all exposed experimental parameters

::

  --sourceModel     Pick type of electrical source model {monodomain,bidomain,pseudo_bidomain}
  --tend TEND       Duration of simulation (ms)
  --depth DEPTH     Depth of augmentation layer (um)
  --conductivities  roberts82, roberts79 or clerc77


These parameters translate into CARPentry input parameters as follows:

.. code-block:: none

    bidomain          2              # source model, either monodomain, bidomain or pseudo-bidomain
    augment_depth   200.0            # depth of the augmentation layer in :math:`\mu m`

    num_gregions      1              
    gregion[0].name   roberts82      # tissue conductivities according to Roberts and Scher in 1982
    gregion[0].g_il   0.34
    gregion[0].g_it   0.06
    gregion[0].g_in   0.06
    gregion[0].g_el   0.12
    gregion[0].g_et   0.08
    gregion[0].g_en   0.08

    gregion[0].name   roberts79      # tissue conductivities according to Roberts and Scher in 1979
    gregion[0].g_il   0.28
    gregion[0].g_it   0.026
    gregion[0].g_in   0.026
    gregion[0].g_el   0.22
    gregion[0].g_et   0.13
    gregion[0].g_en   0.13

    gregion[0].name   clerc77        # tissue conductivities according to Clerc in 1976 [#clerc76]_
    gregion[0].g_il   0.174
    gregion[0].g_it   0.019
    gregion[0].g_in   0.019
    gregion[0].g_el   0.625
    gregion[0].g_et   0.236
    gregion[0].g_en   0.236

The conductivities used are based on the experimental studies of Clerc [#clerc76]_, 
Roberts, Hersh and Scher [#roberts79]_ and Roberts and Scher [#roberts82]_.

**Note that these early studies were not aware of the existence of sheets in cardiac tissue.
Thus rotational isotropy was assumed, that is, transverse conductivities within sheets
and sheet normal conductivities were assumed to be identical.**


**Experiment exp01**
^^^^^^^^^^^^^^^^^^^^

In this experiment, we stimulate a slab as shown in :numref:`fig-pseudobdm-bathloading` 
using a full bidomain formulation. Due to the stark difference in conductivity
between interstitial space and bath a pronounced :ref:`bath loading <fig-bathloading-bidomain>` effect is witnessed.

.. code-block:: bash

   ./run.py --sourceModel bidomain --depth 0 --tend 10  --visualize


.. _fig-bathloading-bidomain:

.. figure:: /images/02_EP_tissue_07A_bath_loading_bidomain.png
   :align: center                       
   :width: 100%

   Due to the pronounced differences in conductivities between interstitial fluid and bath
   a pronounced bath loading effect is observed.


**Experiment exp02**
^^^^^^^^^^^^^^^^^^^^

Now we repeat *exp01* using a monodomain formulation. 
In this case no bath loading effects are observed as bath loading is mediated by the potential field
in the extracellular medium which is not taken into account by the monodomain model. 
the electric field :ref:`loading effect <fig_bath_loading>` is witnessed.

.. code-block:: bash

   ./run.py --sourceModel monodomain --depth 0 --tend 10  --visualize

.. _fig-bathloading-monodomain:

.. figure:: /images/02_EP_tissue_07A_bath_loading_monodomain_Vm.png
   :align: center                       
   :width: 50%

   Since the extracellular potential field is ignored in a monodomain formulation
   no bath loading effects can be observed.


**Experiment exp03**
^^^^^^^^^^^^^^^^^^^^

The effect of bath loading can be mimicked by increaseing the conductivity of the intracellular space 
in a thin tissue layer adjacent to the tissue bath interface.
Typically, this is achieved by setting the interstitial conductivity equal to the extracellular bath conductivity.
The choice of the bath conductivity in combination with the depth of the augmentation layer 
modulate the bath loading effect.
Using this technique which has been published by Bishop et al in  [#bishop_bathloading]_ and is referred to as *augmentation*
bath loading can be taken into account without using the computationally much more expensive *bidomain* formulation.

In this experiment a monodomain formulation is used. A layer of 200 :math:`\mu m` depth will be used for augmentation,
that is, within this layer the interstitital conductivites are set to the bath conductivity.

.. code-block:: bash

   ./run.py --sourceModel monodomain --depth 200 --tend 10  --visualize

.. _fig-bathloading-monodomain-augment:

.. figure:: /images/02_EP_tissue_07A_bath_loading_monodomain_Vm_augmented.png
   :align: center                       
   :width: 50%

   Despite using a monodomain formulation a bath loading effect is clearly visible.
   This is achieved by augmentation of superficial tissue layers.


**Experiment exp04**
^^^^^^^^^^^^^^^^^^^^

Same setup as above, but choosing the bidomain approximation.

.. code-block:: bash

   ./run.py --sourceModel pseudo_bidomain --depth 200 --tend 10  --visualize

.. _fig-bathloading-pseudo-bidomain:

.. figure:: /images/02_EP_tissue_07A_bath_loading_pseudo_bidomain.png
   :align: center                       
   :width: 100%

   Similar to **exp04**, in the *pseudo-bidomain* the source is represented by an augmented monodomain model 
   to account for bath loading (A). Extracellular potentials shown in (B) are computed then infrequently 
   at the given temporal output granularity.

**Experiment exp05**
^^^^^^^^^^^^^^^^^^^^

As shown in [#bishop_bathloading]_, bath loading effects are mainly governed by the ratio
between interstitial conductivity along the direction of propagation of a depolarization wavefront
and the isotropic bath conductivity. 
This is easiy exmemplified by comparing the differences in bath loading effects 
when using the Clerc or Roberts conductivities.

To illustrate this effect repeat the simulation in exp04 using the conductivities measured by Clerc.
A comparison between these two cases is shown in :numref:`fig-bathloading-clerc76-versus-roberts82`.

.. code-block:: bash

   ./run.py --sourceModel pseudo_bidomain --depth 200 --tend 10 --conductivities clerc76 --visualize

.. _fig-bathloading-clerc76-versus-roberts82:

.. figure:: /images/02_EP_tissue_07A_bath_loading_pseudo_bidomain_clerc76_versus_roberts82.png
   :align: center                       
   :width: 100%

   Differences in bath loading when using either the conductivity values of Clerc [#clerc76]_
   or Roberts [#roberts82]_. Notice the much stronger bath loading effect, 
   as evidenced by the V-shape of the wavefront close to the tissue bath interface,
   in the Roberts case. Using the Clerc conductivities the interstitial longitudinal conductivity
   of 0.625 S/m is almost as high as the bath conductivity.

*VERIFY*
    - that the setup is inducing wavefront curvature in pseudo-bidomain (and bidomain) mode
    - the gain in time between the pseudo-bidomain and 'pure' bidomain approximation

[CHEAT MODE] :download:`pseudo-bidomain </images/bathloading_pseudobidomain.gif>`
:download:`bidomain </images/bathloading_bidomain.gif>`
:download:`monodomain </images/bathloading_monodomain.gif>`


.. rubric:: References

.. [#bishop_bathloading] Bishop MJ, Plank G.
   **Representing Cardiac Bidomain Bath-Loading Effects by an Augmented Monodomain Approach: Application to Complex Ventricular Models**,
   IEEE Trans BioMed Eng. 58(4):1066-75, 2011.
   `[PubMed] <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3378475/>`__

.. [#clerc76] Clerc L.
   **Directional differences of impulse spread in trabecular muscle from mammalian heart.**,
   J Physiol. 255(2):335-346, 1976.
   J Physiol. 255(2):335-346, 1976.
   `[PubMed] <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1309251/>`__

.. [#roberts79] Roberts DE, Hersh LT, Scher AM.
   **Influence of cardiac fiber orientation on wavefront voltage, conduction velocity, and tissue resistivity in the dog.**
   Circ Res. 44(5):701-12, 1979.
   `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/428066/>`__

.. [#roberts82] Roberts DE, Scher AM.
   **Effect of tissue anisotropy on extracellular potential fields in canine myocardium in situ.**,
   Circ Res. 50(3):342-51, 1982.
   `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/7060230/>`__

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Augmentation and Pseudo-bidomain'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>, Anton Prassl <anton.prassl@medunigraz.at>'

import os
import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import testing
from carputils import mesh
from carputils import ep

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--sourceModel',
                        default='pseudo_bidomain',
                        choices=ep.MODEL_TYPES,
                        help='pick type of electrical source model, default: pseudo_bidomain')
    group.add_argument('--propagation',
                        default='R-D',
                        choices=ep.PROP_TYPES,
                        help='pick propagation driver, either Reaction-Diffusion, Reaction-Eikonal'
                        +' or prescribe AP shape using Eikonal as a trigger, default: R-D')
    group.add_argument('--tend',
                        type=float,
                        default=30.,
                        help='Duration of simulation (ms), default: 30 ms')
    group.add_argument('--depth',
                        type=float,
                        default=200.,
                        help='Depth of augmentation layer (um), default: 200 um')

    group.add_argument('--conductivities',
                        default='roberts82',
                        choices={'roberts82', 'roberts79', 'clerc76'},
                        help='pick a set of tissue conductivities, default: roberts82')

    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_augmentation_{}_np_{}_{}_{}'.format(today.isoformat(), args.tend,
                                             args.np, args.sourceModel, args.propagation)

def tissue_conductivities(gs):

    if gs == 'roberts82':

        # roberts82
        g = [ '-gregion[0].name', gs,
              '-gregion[0].g_il', 0.34,
              '-gregion[0].g_it', 0.06,
              '-gregion[0].g_in', 0.06,
              '-gregion[0].g_el', 0.12,
              '-gregion[0].g_et', 0.08,
              '-gregion[0].g_en', 0.08 ]

    elif gs == 'roberts79':

        # roberts79 
        g = [ '-gregion[0].name', gs,
              '-gregion[0].g_il', 0.28,
              '-gregion[0].g_it', 0.026,
              '-gregion[0].g_in', 0.026,
              '-gregion[0].g_el', 0.22,
              '-gregion[0].g_et', 0.13,
              '-gregion[0].g_en', 0.13 ]

    else:

        # clerc76
        g = [ '-gregion[0].name', gs,
              '-gregion[0].g_il', 0.174,
              '-gregion[0].g_it', 0.019,
              '-gregion[0].g_in', 0.019,
              '-gregion[0].g_el', 0.625,
              '-gregion[0].g_et', 0.236,
              '-gregion[0].g_en', 0.236 ]
    return g

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh (units in mm)
    geom = mesh.Block(size=(15., 10., .1), etype='tetra', resolution=0.1)
    geom.set_fibres(0, 0, 90, 90) # set fibre angle to 0, sheet angle to 0
    geom.set_bath(thickness=(0,1,0), both_sides=True)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # Define the geometry of the stimulus at one end of the block
    stim = mesh.block_boundary_condition(geom, 'stimulus', 0, 'x')

    # Get basic command line, including solver options
    cmd = tools.carp_cmd('bathloading.par') + [ '-simID', job.ID ]

    # Determine model type
    cmd += ep.model_type_opts(args.sourceModel)
    if  args.sourceModel=="bidomain":
        # reset user input, otherwise carp will switch to pseudo-bidomain
        args.depth = 0.

    # pick conductivities
    cmd += tissue_conductivities(args.conductivities)

    # pick propagation mode
    numStims = 1    # stim defined in par file
    propOpts, numStims, stim = ep.setupPropagation(args.propagation, numStims, stim, '')

    # final assembly of command line
    stimOpts = [ '-num_stim', numStims ] + stim
    cmd += stimOpts + propOpts
    cmd += ['-meshname',            meshname,
            '-tend',                args.tend,
            '-dt',                  50,
            '-augment_depth',       args.depth,
            '-localize_pts',        1]


    if args.visualize:
        cmd += ['-gridout_i', 3,
                '-gridout_e', 3]

    # Run simulation
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
        # Prepare file paths
        #if args.sourceModel == "monodomain":
        # show Vm first
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb')
        view = 'view_vm.mshz'

        # Call meshalyzer
        job.meshalyzer(geom, data, view)

        # show extracellular potentials if computed
        if args.sourceModel != "monodomain":
            geom = os.path.join(job.ID, os.path.basename(meshname)+'_e')
            data = os.path.join(job.ID, 'phie.igb')
            view = 'view_phie.mshz'

            # Call meshalyzer
            job.meshalyzer(geom, data, view)

# Define some tests
# -----------------------------------------------------------------------------
# none
# -----------------------------------------------------------------------------

if __name__ == '__main__':
    run()

-85.7971            # Vm
-                   # Lambda
-                   # delLambda
-                   # Tension
-                   # K_e
-                   # Na_e
-                   # Ca_e
8.57806e-05         # Iion
-                   # tension_component
converted_TT2
2.49186             # CaSR
0.000198096         # CaSS
0.0843957           # Ca_i
3.14754e-05         # D
0.972439            # F
0.999287            # F2
0.999746            # FCaSS
3.98e-05            # GCaL
0.153               # Gkr
0.392               # Gks
0.294               # Gto
0.758806            # H
0.758412            # J
138.041             # K_i
0.00153687          # M
7.54005             # Na_i
2.21713e-08         # R
0.989039            # R_
0.999973            # S
0.000197027         # Xr1
0.476548            # Xr2
0.00316592          # Xs

INa_Bond_Markov
0.000153386         # C_Na1
0.0167148           # C_Na2
0.000128488         # I1_Na
0.00619437          # I2_Na
0.00590265          # IC_Na2
0.253364            # IC_Na3
5.43267e-05         # IF_Na
2.52522e-07         # O_Na


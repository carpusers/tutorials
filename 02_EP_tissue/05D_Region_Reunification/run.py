#!/usr/bin/env python

"""
This tutorial details how to output the values of state variables over time during a simulation using
the gvec[] interface. This tutorial is a stub to demonstrate the most basic functionality and should
be expanded when possible.

Problem Setup
================================

This example will run one simulation using a 2D sheet model (1 cm x 1 cm) that has been divided into
four regions, each of which uses a different ionic model. All four models have an implementation of
the fast sodium current (:math:`I_{Na}`), but in one of the models the state variable names are 
upper-case instead of lower-case (i.e., :math:`M/H/J` vs. :math:`m/h/j`). The gvec[] interface is
used to reunify these state variables from the four regions and output three IGB files: 
``I_Na_m.igb``, ``I_Na_h.igb``, and ``I_Na_j.igb``.

Usage
================================

No optional arguments are available.

If the program is run with the ``--visualize`` option, meshalyzer will automatically 
load the produced ``I_Na_m.igb`` file, which shows :math:`I_{Na}(t)` for the entire
simulation domain. 

What's Going On Under The Hood?
================================

The relevant part of the .par file for this example is shown below:

.. code-block:: bash

    #############################################################
    num_gvecs                = 3
    #############################################################
    gvec[0].name             = INa_m
    gvec[0].ID[0]            = m
    gvec[0].ID[1]            = m
    gvec[0].ID[2]            = M # <== Note upper-case letter
    gvec[0].ID[3]            = m
    
    gvec[1].name             = INa_h
    gvec[1].ID[0]            = h
    gvec[1].ID[1]            = h
    gvec[1].ID[2]            = H # <== Note upper-case letter
    gvec[1].ID[3]            = h
    
    gvec[2].name             = INa_j
    gvec[2].ID[0]            = j
    gvec[2].ID[1]            = j
    gvec[2].ID[2]            = J # <== Note upper-case letter
    gvec[2].ID[3]            = j

Each gvec[] structure contains information about one variable that is to be reunified and output.
Note that unlike gregion[] and imp_region[], there are no .num_ID variables here. That is because
the number of entries in each .ID array is actually determined by num_imp_regions. In this example,
the relevant imp_region details are as follows:

* ``imp_region[0].im = converted_LRDII_F``
* ``imp_region[0].im = converted_RNC``
* ``imp_region[0].im = converted_TT2``
* ``imp_region[0].im = converted_UCLA_RAB``

As noted above, state variables associated with the fast sodium current are :math:`M/H/J` in the TT2
implementation vs. :math:`m/h/j` in the other 3. This is why the gvec[].ID[2] entries, which are linked
to TT2 in imp_region[2], are upper-case instead of lower-case.

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Region Reunification (gvec)'
EXAMPLE_AUTHOR = 'Patrick Boyle <pmjboyle@gmail.com>'

from datetime import date

from carputils import settings
from carputils import tools

def parser():
    parser = tools.standard_parser()
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}'.format(today.isoformat())

@tools.carpexample(parser, jobID)
def run(args, job):
    # Setup command line and run simulation
    cmd = tools.carp_cmd('Region-Reunification.par')
    cmd += ['-simID', job.ID]
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
        job.meshalyzer('TestSlabMesher2D_i', job.ID + '/INa_m.igb', 'gv.mshz')

if __name__ == '__main__':
    run()

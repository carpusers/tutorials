-82.1828            # Vm
-                   # Lambda
-                   # delLambda
-                   # Tension
-                   # K_e
-                   # Na_e
-                   # Ca_e
0.0052404           # Iion
-                   # tension_component
converted_RNC
0.00360121          # CaCmdn
6.29344             # CaCsqn
1.41594             # CaRel
0.0139261           # CaTrpn
2.6023              # CaUp
0.206457            # Ca_i
29.26               # Cl_i
136.746             # K_i
13.4534             # Na_i
0.0748314           # Oa
0.997515            # Oi
-9.88127e-12        # QCa
0.0615684           # Ua
0.984906            # Ui
9.48462e-06         # d
0.851025            # f
0.441893            # fCa
0.971596            # h
0.981505            # j
0.00246983          # m
5.60519e-43         # u
0.999994            # v
0.999243            # w
1.42352e-06         # xr
0.0263678           # xs

INa_Bond_Markov
0.000233245         # C_Na1
0.0167716           # C_Na2
0.0690914           # I1_Na
0.0287768           # I2_Na
0.0131738           # IC_Na2
0.382125            # IC_Na3
0.000247764         # IF_Na
6.20895e-07         # O_Na


#!/usr/bin/env python

"""
Introduction
============

This experiment is designed to show how to switch
from the default Reaction-Diffusion propagation driver to a Reaction-Eikonal one
and to demonstrate the differences between the drivers w.r.t. the computed potentials.

A small tissue strand (dim 30 x 0.5 x 0.5 mm) is excited in its center
by a transmembrane current. Excitation then propagates towards both outer sides. The
propagation driver can be selected to be either the diffusion term of the Monodomain
/ Bidomain model, or one based on the solution of an associated eikonal model.

Usage
-----

The experiment-specific options are:

.. code-block:: bash

    --sourceModel {monodomain,bidomain,pseudo_bidomain}
                          pick type of electrical source model (default is
                          monodomain)
    --propagation {R-D,R-E+,R-E-,AP-E}
                          pick propagation driver, either R-D (reaction-
                          diffusion), R-E (+) with (-) without diffusion or AP-E
                          (prescribed AP without diffusion) shape with trigger
                          instant(default is R-E+)
    --duration DURATION   duration of experiment, typically 25. ms for
                          activation only, 400 ms to include repolarization
                          (default is 25.)
    --tag TAG             add tag to simulation ID


Using the ``--propagation`` argument, the user can select the propagation driver. The
choices are:

* R-D:  Ionic reaction term with the Mono-/Bidomain diffusion term.
* R-E-: Ionic reaction term with Eikonal-based propagation driver.
* R-E+: Ionic reaction term with the Mono-/Bidomain diffusion term and an \
additional Eikonal-based propagation driver to enforce conduction velocity.
* AP-E: Prescribed Action-Potential functions with Eikonal-based propagation driver.

Using the ``--dry`` option, one can observe which command-line options are added/removed
when switching, propagation drivers.

In a nutshell, R-E propagation is activated by adding the ``-stimulus[1].stimtype 8``
option.  The difference between R-E+ and R-E- is that R-E+ includes diffusion via the
``-diffusionOn 1`` option. As a consequence, in experiments featuring spatially
heterogeneous ionic models, R-E- shows significant artificial potential gradients
at the interfaces between regions with different models. The following picture shows Vm
at 7 and 112 ms for R-D, R-E+ and R-E-:

.. figure:: /images/06Eikonal_R-E.vs.R-D.png
    :scale: 50%
    :align: center

    Tissue strand after 7 and 112 milliseconds using different propagation drivers.

Example
=======

To run the example and visualize output, run:

.. code-block:: bash

    ./run.py --np NP --propagation PROP --visualize

Where NP denotes the desired number of processes and PROP the propagation mode.


"""

EXAMPLE_DESCRIPTIVE_NAME = 'Reaction-Eikonal model experiment'
EXAMPLE_AUTHOR = 'Aurel Neic <aurel.neic@medunigraz.at>'

import os
import sys
from datetime import date
from math import log

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import ep
from carputils.carpio import dat2adj
from carputils import testing

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--sourceModel',
                        default='monodomain',
                        choices=ep.MODEL_TYPES,
                        help='pick type of electrical source model')
    group.add_argument('--propagation',
                        default='R-E+',
                        choices=ep.PROP_TYPES,
                        help='pick propagation driver, either R-D (reaction-diffusion), R-E (+) with (-) without diffusion'
                            +' or AP-E (prescribed AP without diffusion) shape with trigger instant')
    group.add_argument('--duration',
                        type=float, default=25.0,
                        help='duration of experiment, typically 25. ms for '
                            +'activation only, 400 ms to include repolarization')
    group.add_argument('--tag',
                        default='',
                        help='add tag to simulation ID')
    return parser


def setupItrStim(stimIdx, l, d):

    ml = l * 1000
    md = d * 1000

    base = '-stimulus[%d].'%(stimIdx)
    Itr = [base+'stimtype',    0,
           base+'strength',  150.,
           base+'duration',    2.,
           base+'start',       0,
           base+'x0',       -ml*0.05,
           base+'y0',       -md,
           base+'z0',       -md,
           base+'xd',       ml*0.1,
           base+'yd',       2*md,
           base+'zd',       2*md,
           base+'npls',        1  ]

    return Itr, 1

def setupPropagation(propagation, numStims, stims):

    if propagation != 'AP-E':
        propOpts, numStims, stims = ep.setupPropagation(propagation, numStims, stims, '')
        if propagation == 'R-D':
            dt =  20.
        else:
            dt = 100.
    else:
        IDs   = [0, 1, 2]
        names = ['EPI', 'MID', 'ENDO' ]
        propOpts, numStims, stims = ep.setupPropagation(propagation, numStims, stims, '', None, IDs, names)
        dt = 1000.

    return propOpts, dt, numStims, stims


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    tpl = '{}_{}_{}_{}_np{}{}'
    return tpl.format(today.isoformat(), args.sourceModel, args.propagation,
                      args.flv, args.np, args.tag)


@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    l = 15
    d = 0.5
    res = 0.1
    if args.propagation != 'R-D':
        res = 0.25

    geom = mesh.Block(size=(2*l, d, d), resolution=res)
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Define regions
    epi = mesh.BoxRegion((-l*1.2,-d,-d), (-l*0.4, d, d), tag=1)
    geom.add_region(epi)
    mid = mesh.BoxRegion((-l*0.4,-d,-d), (l*0.4, d, d), tag=5)
    geom.add_region(mid)
    endo = mesh.BoxRegion((l*0.4,-d,-d), (l*1.2, d, d), tag=3)
    geom.add_region(endo)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # define default view settings
    view = 'eikonal.mshz'

    # Add all the non-general arguments
    cmd  = tools.carp_cmd('eikonal.par')
    cmd += ['-simID', job.ID]

    # Determine model type
    cmd += ep.model_type_opts(args.sourceModel)

    # define transmembrane stimuli
    stims, numStims = setupItrStim(0, l, d)

    # pick propagation mode
    propOpts, dt, numStims, stims = setupPropagation(args.propagation, numStims, stims)

    # final assembly of command line
    stimOpts = [ '-num_stim', numStims ] + stims + [ '-dt', dt ]
    cmd += stimOpts + propOpts

    cmd += ['-meshname',     meshname,
            '-tend',         args.duration,
            '-dump_im',      1 ]

    # adjust output resolution
    spacedt_ms = 0.1
    dt_ms   = float(dt)/1000.
    if dt_ms > spacedt_ms:
        spacedt_ms = dt_ms

    cmd += ['-spacedt', spacedt_ms,
            '-gridout_i', 3]

    # Run simulations
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb')

        job.gunzip(data)
        job.meshalyzer(geom, data, view)

# # test forced_foot mode
# test_re = testing.Test('re',
#                        run,
#                        ['--np',              4,
#                         '--propagation', 'R-E+',
#                         '--duration',      300],
#                        tags=[testing.tag.SHORT,
#                              testing.tag.PARALLEL,
#                              testing.tag.EIKONAL])
# test_forced_foot.add_filecmp_check('vm.igb.gz', testing.max_error, 0.001)
# test_forced_foot.add_filecmp_check('phie_recovery.igb', testing.max_error, 0.001)
# 
# __tests__ = [test_forced_foot]

if __name__ == '__main__':
    run()

#!/usr/bin/env python

"""
.. _region-tagging-tutorial:

In CARPentry regions are used to manage the assignment of heterogeneous tissue properties.
This tutorial explains the different approches of how regions can be defined.

-------
Regions
-------

Regions are a general concept in CARPentry for volumes which share common properties, be they ionic, mechanical or 
conductivity. For a general introduction of this concept see the :ref:`region <region-definition>` section of the manual.
In the mesh files, elements may be given a region tag as an optional field in the ``.elem`` file.

.. note::

    If no region tag is specified, the element is assigned a region tag of 0.

A region tag is a simply an integer associated with an element. CARPentry regions are then conglomerations of element region tags.
The following is an example of how to assign some region tags to IMP and conductivity regions::

    imp_region[1].num_IDS = 2
    imp_region[1].ID[0] = 1000 
    imp_region[1].ID[1] = 1100

    gregion[3].num_IDS = 5
    gregion[3].ID = 100 200 201 1000 2000 

Two slightly different syntaxes are shown for specifying the region tags.

.. note::

    If a region tag is not explicitly included in any region, it will be assigned to region 0.

-----------------
Dynamic Retagging 
-----------------

Each element in a mesh is designated as being in a region, and properties, be they electrical or mechanical, 
are assigned based on region.
It may be desired to change properties in a small volume which is not defined in the original mesh.
To avoid touching the mesh files, dynamic tagging was introduced.

Essentially, a geometrical volume is defined, and all elements wthin the volume are given a new tag.  
Overlaps are governed by a simple rule: **the highest region tag wins**. This also implies that your reassigned tags have to be higher than the original model tags.
The case where some but not all of an element's nodes are contained with the tagging volume must be handled. 
By default, elements with at least one node in the tagging volume are included, but ensuring that only elements with all nodes contained can be enabled by setting an option below.(*no_split_elem*)

Tag volumes can also be created by simply specifying a list of elements within the volume.


Region Definitions
__________________

The number of new tag regions is specified by ``numtagreg``.
For each region defined by ``tagreg[*]``, the table below lists the available fields to set. Bolded fields need to be specified
and depending on the value of *type*, different fields define the geometrical volume. Note that the units are micrometers.
See the figure below:

============= ==============================================================
Field         Meaning
============= ==============================================================
**tag**       numerical value of tag region
name          string to identify retagged region
**type**      how to define region (1=sphere,2=block,3=cylinder,4=elemfile)
no_elem_split all nodes of an element must be contained
p0            first point to define volume. *type=1|2|3* 
p1            second point to define volume. *type=2|3*
radius        radius for spherical/cylindrical volumes. *type=1|3*
elemfile      file with elements contained in tag region. *type=4* The
              file needs the extension ``.regele`` with the
              format being the number of elements followed by
              one element number per line.
============= ==============================================================

The tagging volumes are defined according to the following diagram:


.. figure:: /images/retag-vols.png
    :width: 50%
    :align: center

    Geometry definitions for dynamic retagging volumes sphere, block and cylinder.


--------------
Mesher Tagging
--------------

When using the :ref:`mesher <mesher>` program, tag volumes can be assigned to the blocks of tissue but the rules are slightly different. 

.. note::

    With mesher, bounds are defined in units of `cm`, not micrometers and the cylinder definition is different.

For each tag volume defined by ``regdef[*]``, the table below lists the available fields to set. Bolded fields need to be specified
and depending on the value of `type`, different fields define the geometrical volume. 

============= ==============================================================
Field         Meaning
============= ==============================================================
**tag**       numerical value of the tag 
**type**      how to define tag volume (0=block,1=sphere,2=cylinder)
*p0*          first point to define volume. *type=0|1|2* 
p1            UR point for block (*type=0*), or axis for cylinder which will be normalized (*type=2*)
rad           radius for spherical/cylindrical volumes. *type=1|2*
cyllen        cylinder length for cylindrical volumes. *type=2*
bath          set true for a bath region
============= ==============================================================

.. figure:: /images/regions-mesher-cyl.png
    :width: 25%
    :align: center

    Geometry definition for mesher cylinder. Spheres and blocks are defined as in dynamic retagging above.

If an element was originally created as bath, it cannot be turned into myocardium but myocardium can be turned into bath. 
To deal with overlapping regions, the rules are also different from dynamic tagging. 
For mesher, an element belongs to the first region which contains it.
Thus, the order of region definitions is important. An option is also available ``-first_reg``,
which if set false, scans the region definitions and assigns the last region which contains an element.


--------
Tutorial
--------

 
The following options are the pertinent ones for running the tutorial::

    run.py -h
        --mesher [{sphere,block,steps} [{sphere,block,steps} ...]]
                        regions implemented in mesher
        --dynamic [{cylinder,sphere,block} [{cylinder,sphere,block} ...]]
                        regions implemented dynamically
        --add-bath            mesher regions are bath
 
Multiple regions can be created in meshser or dynamically afterwards. Start with a single region implemented in mesher::

    run.py --mesher sphere

To see the regions better, click ``Image/Randomly color/surfaces`` from the menu ar at the top.
The colors are random, so if you do not like them, randomly color again (and again ...). You should see 3 regions: the bath, the original block of tissue, and the sherical region;
To see the points in the region, 

1. turn off clipping (open the clipping window under ``Data/Clipping``)
2. In the ``Region`` section in the control window, check *only* the region **10**
3. Check the ``Vertices`` and ``visible`` buttons
4. To control the size of the points, click on ``props`` beside the *Vertices* button.
5. Reduce the opacity of the surfaces (set it in ``Fill color`` of the ``Surfaces`` tab)

It should now resemble the figure below.

.. figure:: /images/regions_sphpts.png
   :width: 50%
   :align: center

Try adding another sphere, this time dynamically added afterwards at a different location::

    run.py --mesher sphere --dynamic sphere

Experiment with adding different combinations of regions. The order of multiple regions specified with the ``--mesher`` and ``--dynamic`` options matters. With this script, the first contained region is used for dynamic retagging.
To see the points contained in the regions, display vertices as above, and click ``Image/Randomly color/surfaces``
to colour them differently for each tag region.

For a more detailed introduction to the region concept as implemented in CARPentry see
the :ref:`Region Tagging Section <region-definition>` of the manual.


"""

EXAMPLE_DESCRIPTIVE_NAME = 'Region tagging'
EXAMPLE_AUTHOR = 'Edward Vigmond <edward.vigmond@u-bordeaux.fr>'

import os
import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--mesher',
                        default=[],
                        choices=[ 'sphere',
                                  'block',
                                  'steps'],
                        nargs='*',
                        help='regions implemented in mesher')

    group.add_argument('--dynamic',
                        default=[], 
                        choices=['cylinder',
                                 'sphere',
                                 'block' ], 
                        nargs='*',
                        help='regions implemented dynamically')

    group.add_argument('--add-bath',
                        action='store_true',
                        help='mesher regions are bath')    

    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    regspec = ''
    for r in args.mesher :
        if r is None :
            break
        regspec += r[0].upper()+r[1]
    if len(regspec) : regspec += '_'
    for r in args.dynamic :
        if r is None :
            break
        regspec += r[0]

    if args.add_bath  : regspec += '_Bth'

    #print (regspec)
        
    return 'regions_{}'.format(regspec) 


def tagregopt( reg, field, val ) :
    return ['-tagreg['+str(reg)+'].'+field, val ]


@tools.carpexample(parser, jobID)
def run(args, job):

    x = 5.
    y = 5.
    z = 5.

    # Block which is thin in z direction
    geom = mesh.Block(size=(x, y, z))
    geom.set_bath( (1,1,1), both_sides=True )

    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(-60, 60, 90, 90)

    # define a tag region
    for i,r in enumerate(args.mesher) :

        bath = 1 if args.add_bath else 0

        if r == 'block':
          geom.add_region(mesh.BoxRegion((-x/2-0.1,-y/2.-0.1,-z/2.-0.1), (0,y/2.+0.1,z/2.+0.1), tag=10*(i+1),bath=bath))
    
        elif r == 'steps':
            nstep=4
            dz = z/float(nstep)
            for s in range(nstep) :
                geom.add_region(mesh.BoxRegion((-x/2-0.1,-y/2.-0.1,-z/2.+dz*s), 
                                        (x/2+0.1,y/2.+0.1,-z/2.+dz*(s+1)), tag=10*(1+i)+s,bath=bath))
    
        elif r == 'sphere':
          reg = geom.add_region(mesh.SphereRegion(1., (0,0,0), bath=bath, tag=10*(i+1)))
    
        else:
           # None, don't add region 
           pass

    dyn_reg = [ '-numtagreg', len(args.dynamic) ]
    for reg,r in enumerate(args.dynamic) :

        if r == 'block':
          dyn_reg.extend( tagregopt(reg, 'type',    2) )
          dyn_reg.extend( tagregopt(reg, 'p0',  '{ -1000 -1000 -200 }') )
          dyn_reg.extend( tagregopt(reg, 'p1',  '{ 1000 1000 100 }') )
          dyn_reg.extend( tagregopt(reg, 'tag', 100*(reg+1)) )
    
        elif r == 'cylinder':
          dyn_reg.extend( tagregopt(reg, 'type',        3) )
          dyn_reg.extend( tagregopt(reg, 'p0',      '{ '+str(-x*1000/2.)+' 2000 1500 }') )
          dyn_reg.extend( tagregopt(reg, 'p1',      '{ -1000 2000 1500  }') )
          dyn_reg.extend( tagregopt(reg, 'radius',  250) )
          dyn_reg.extend( tagregopt(reg, 'tag',     100*(reg+1)) )
    
        elif r == 'sphere':
          dyn_reg.extend( tagregopt(reg, 'type',        1) )
          dyn_reg.extend( tagregopt(reg, 'p0',      '{ 0 700 0 }') )
          dyn_reg.extend( tagregopt(reg, 'radius',  500 ) )
          dyn_reg.extend( tagregopt(reg, 'tag',     100*(reg+1)) )
    
        else:
           # None, don't add region 
           pass


    # Generate and return base name
    meshname = mesh.generate(geom)

    # define default view settings
    view = 'tagged.mshz'
  
    # Add all the non-general arguments
    cmd = tools.carp_cmd() 

    cmd += ['-meshname',  meshname,
            '-gridout_i',  2, 
            '-gridout_e',  2, 
            '-experiment', 3,
            '-simID',     job.ID,
            '-retagfile', 'retagged.dat' ]  

    cmd += dyn_reg

    # Run simulations
    if args.dry :
        print (cmd)
    else :
        job.carp(cmd)

    # Do visualization
    view = 'tagging.mshz'
        
    geom = os.path.join(job.ID, os.path.basename(meshname)+'_e')

    job.meshalyzer(geom, view, compsurf=True)


if __name__ == '__main__':
    run()

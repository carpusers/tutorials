#!/usr/bin/env python

r"""



Introduction
=============
The aim of this tutorial is to study the effect of fiber orientation on the electrical behavior of cardiac tissue.
Generally, electrical properties of the myocardial tissue can be described by the bidomain model
accounting for the intracellular and extracellular domain. The domains are of anisotropic nature, the intracellular domain
even more so than the extracellular domain resulting in ''unequal anisotropy ratios'' affecting the heart's electrical behavior
causing two effects ''rotation'' and ''distribution''.  This tutorial focuses on the ''rotation'' effect, which occurs when
two spaces have different degrees of anisotropy. If considering an isotropic tissue the electrical field :math:`\mathbf{E}`
and current density :math:`\mathbf{J}` would point in the same direction. It follows that :math:`\mathbf{J}=g\mathbf{E}` with g being
the scalar conductivity. In anisotropic tissue this is not the case as the conductiviy tensor rotates :math:`\mathbf{J}` towards the fiber direction
relative to :math:`\mathbf{E}`. As anisotropy ratios are unequal in the intra- and extracellular space current densities :math:`\mathbf{J}_i` and
:math:`\mathbf{J}_e` are rotated by different amounts. [#roth]_


Experimental Setup
==================

**Analytical vs. Numerical Setup:**
Given is a 2D strip of infinite length of width W=10mm in which fibers are oriented with :math:`\alpha = 45^{\circ}` (shown as grey dotted lines in the figure below).
Conductivities are given as :math:`g_{iL} = 0.2` S/m, :math:`g_{iT} = 0.02` S/m,  :math:`g_{eL} = 0.2` S/m and  :math:`g_{eT} = 0.08` S/m
where L and T indicate directions along and transverse to the fiber orientation, respectively (Anisotropy ratios are unequal!). The
tissues membrane surface-to-volume ratio and the resistance are given as :math:`\beta = 0.14\mu m^{-1}`  and
:math:`R_{m} = 0.91 \Omega m^{2}` . For numerical analysis we assume that the length L of the strip is not infinite, but
:math:`L\gg\lambda`. For the data given choosing L=60mm should suffice. The spatial resolution should be used as height of the strip (only one layer of 3D elements).
The analytical solutions should be used to verify the numerical results in 3D.


.. figure:: /images/02_EP_tissue_14_bidm_rotation_experimentalsetup.png
     :scale: 20 %

**Model Settings:** A hexahedral FE model with dimensions of 60mm x 10mm x 1mm shall be generated functioning as cardiac tissue strip.
An orthotropic fiber setup is used with a sheet angle of :math:`90^{\circ}`.

**Electrical Settings:**
As passive ionic model the modified Beeler-Router Druhard-Roberge model as implemented in LIMPET is used with :math:`R_{m}` set to :math:`0.91 \Omega m^{2}` and :math:`\beta = 0.14\mu m^{-1}`.
Current :math:`\mathbf{J}` is injected by Electrode A at x=-L/2 and withdrawn by electrode B at x=+L/2. Electrode C is located at x=0 and can be gounded or not.


**Conductivity Settings:** Unequal anisotropy is assigned using the following values:

.. code-block:: bash

    gregion[0].g_il          = 0.2
    gregion[0].g_it          = 0.02
    gregion[0].g_in          = 0.02
    gregion[0].g_el          = 0.2
    gregion[0].g_et          = 0.08
    gregion[0].g_en          = 0.08

**Solution Method:** The simulation is solved using the parabolic formulation of the bidomain equations with the Crank-Nicolson method.


Input Parameters and Usage
==========================

Go to the following directory to run underlying experiment:

.. code-block:: bash

    cd ${TUTORIAL}/02_EP_tissue/14_bidm_rotation/

The following experiment specific options are available (default values are indicated):

.. code-block:: bash

    ./run.py --angle                #pick fiber angle (0 is along the long edge of the preparation) (default:45)
             --resolution           #choose mesh resolution in um (default: 250)
             --grounded {on, off}   #turn on/off ground (default:off)

.. Note::

    If a high mesh resolution is chosen it may take longer to run. You may use the ``--np`` option to increase number of cores.
    To visualize results with meshalyzer use ``--visualize``.

    ``--grounded`` - A ground can be used or not. In the latter case, no grounding electrode
    is used, that is, a pure Neumann-problem is solved. A rank-one stabilization is used
    in this case which essentially enforces the integral over the extracellular potential
    to be zero at each instant in time, but no specific point is clamped.

Theoretical Considerations
==========================

At :math:`x=-\infty` electrical current :math:`\mathbf{J}` is injected into the tissue strip and the same current :math:`\mathbf{J}` is withdrawn at :math:`x=-\infty`.
Initially, an electrical field :math:`\mathbf{E}` is setup at the center of the strip at y=0, which is oriented along x, but current density :math:`\mathbf{J}` is oriented towards the
fiber orientation (running against a sealed boundary under angle :math:`\alpha`, see Fig. A) with


.. math::

    \mathbf{E} =  \begin{bmatrix}
            E_x \\
            0
        \end{bmatrix}
        \quad \mathrm{and} \quad
    \mathbf{J} = \begin{bmatrix}
            J_x \\
            J_y
        \end{bmatrix}
        =
        \begin{bmatrix}
            g_{xx} &  g_{xy} \\
            g_{xy} &  g_{yy}
        \end{bmatrix}
        \begin{bmatrix}
            E_x \\
            0
        \end{bmatrix}.

Since :math:`J_y\neq0` current will be directed towards the sealed ends at :math:`y=\pm W/2`. Due to :math:`J_y` charge will accumulate at the sealed ends
which sets up an electrical field (see Fig. B). From

.. math::

    \begin{bmatrix}
        J_x \\
         J_y
    \end{bmatrix}
    =
    \begin{bmatrix}
        g_{xx} &  g_{xy} \\
        g_{xy} &  g_{yy}
    \end{bmatrix}
    \begin{bmatrix}
        E_x \\
        E_y
    \end{bmatrix}

and the fact that :math:`J_y` at :math:`y=\pm W/2` must be zero it can be concluded that :math:`\mathbf{E}` makes and angle with x given by

.. math::

    E_y = -\frac{g_{xy}}{g_{yy}}E_x

Due to :math:`\nabla \cdot \mathbf{J}=0` and :math:`\nabla \times \mathbf{E}=0` neither :math:`\mathbf{J}` nor :math:`\mathbf{E}` can depend on y.


.. figure:: /images/02_EP_tissue_14_bidm_rotation_analytical.png
     :scale: 20 %

Now we considering a bidomain with unequal anisotropy ratios. :math:`J_{iy}` and :math:`J_{ey}` must be zero individually at sealed boundaries.
Thus,

.. math::

    E_{iy} = -\frac{g_{ixy}}{g_{iyy}}E_{ix} \quad \mathrm{and} \quad E_{ey} = -\frac{g_{exy}}{g_{eyy}}E_{ex}.

As the tissue strip is infinitely long :math:`V_m = \Phi_i - \Phi_e` cannot be a function of x thus

.. math::

    - \frac{\partial{\Phi_{ix}}}{\partial{x}} = E_{ix} = E_{ex} = - \frac{\partial{\Phi_{ex}}}{\partial{x}}


and it follows that

.. math::

    E_{iy} = \frac{g_{ixy}g_{eyy}}{g_{iyy}g_{exy}}E_{ey}.


Thus,

.. math::

     E_{iy} =- \frac{\partial{\Phi_{iy}}}{\partial{y}} \neq E_{ey} = - \frac{\partial{\Phi_{ey}}}{\partial{y}} \Rightarrow \frac{\partial{V_m}}{\partial{y}} = \frac{\partial{\Phi_{iy}}}{\partial{y}} - \frac{\partial{\Phi_{ey}}}{\partial{y}} \neq 0.


It can be concluded that there must be a y-directed gradient of :math:`V_m` causing membrane polarisation near the boundary (see Fig. C).

Near the center of the strip at y=0 the net current :math:`\mathbf{J}` must be in direction of x with :math:`J_y=0`, but due to rotation intra- and
extracellular densities :math:`\mathbf{J}_i` and :math:`\mathbf{J}_e` make an angle with the x-axis. Intra- and extracellular field are equal (see Fig. D):

.. math::

     \mathbf{E}_{i} = \mathbf{E}_{e},

which implies that the :math:`\nabla V_m` vanishes near the center as

.. math::

     \nabla V_m = \nabla \Phi_i - \nabla \Phi_e = \mathbf{E}_{i} + \mathbf{E}_{e} = 0.

.. figure:: /images/02_EP_tissue_14_bidm_rotation_analytical_bidm.png
     :scale: 20 %


Results
=================
**Analytical Solution:**

First conductivity ratios are computed as

.. math::

    \frac{g_{iL}}{g_{iT}} = \frac{0.20}{0.02}=10 \quad \mathrm{and} \quad \frac{g_{iL}}{g_{iT}} =\frac{0.20}{0.08}= 2.5


followed by conductivity tensors in the fiber coordinate system

.. math::

     \tilde{g}_i = \begin{bmatrix}
                        g_{iL} & 0 \\
                        0 & g_{iT}
                    \end{bmatrix}
                 = \begin{bmatrix}
                        0.20 & 0.00 \\
                        0.00 & 0.02
                    \end{bmatrix}
     \quad
     \tilde{g}_e = \begin{bmatrix}
                        g_{eL} & 0 \\
                        0 & g_{eT}
                    \end{bmatrix}
                = \begin{bmatrix}
                        0.20 & 0.00 \\
                        0.00 & 0.08
                    \end{bmatrix}

and in the cartesian coordinate system assuming a fiber angle :math:`\alpha` of :math:`45^\circ`:

.. math::

     \begin{bmatrix}
            g_{ixx} & g_{ixy} \\
            g_{ixy} & g_{iyy}
     \end{bmatrix}
     = \begin{bmatrix}
            \cos \alpha & \sin \alpha \\
            -\sin \alpha & \cos \alpha
     \end{bmatrix}
     \begin{bmatrix}
            g_{iL} & 0 \\
            0 & g_{iT}
     \end{bmatrix}
     \begin{bmatrix}
            \cos \alpha & -\sin \alpha \\
            \sin \alpha & \cos \alpha
     \end{bmatrix}
     = \begin{bmatrix}
            g_{iL} \cos^2 \alpha + g_{iT} \sin^2 \alpha & -(g_{iL}-g_{iT}) \cos \alpha \sin \alpha \\
            -(g_{iL}-g_{iT}) \cos \alpha \sin \alpha & g_{iL} \sin^2 \alpha + g_{iT} \cos^2 \alpha
     \end{bmatrix}
     =  \begin{bmatrix}
            0.11 & -0.09\\
            -0.09 & 0.11
     \end{bmatrix}

and

.. math::

     \begin{bmatrix}
            g_{exx} & g_{exy} \\
            g_{exy} & g_{eyy}
     \end{bmatrix}
     = \begin{bmatrix}
            \cos \alpha & \sin \alpha \\
            -\sin \alpha & \cos \alpha
     \end{bmatrix}
     \begin{bmatrix}
            g_{eL} & 0 \\
            0 & g_{eT}
     \end{bmatrix}
     \begin{bmatrix}
            \cos \alpha & -\sin \alpha \\
            \sin \alpha & \cos \alpha
     \end{bmatrix}
     = \begin{bmatrix}
            g_{eL} \cos^2 \alpha + g_{eT} \sin^2 \alpha & -(g_{eL}-g_{eT}) \cos \alpha \sin \alpha \\
            -(g_{eL}-g_{eT}) \cos \alpha \sin \alpha & g_{eL} \sin^2 \alpha + g_{eT} \cos^2 \alpha
     \end{bmatrix}
     = \begin{bmatrix}
            0.14 & -0.06\\
            -0.06 & 0.14
     \end{bmatrix}.

At the sealed boundaries :math:`J_{iy}` and :math:`J_{ey}` must be zero individually at sealed boundaries thus,

.. math::

    E_{iy} = -\frac{g_{ixy}}{g_{iyy}}E_{ix} = -\frac{-0.09}{0.11}E_{ix} \sim 0.82E_{ix} \quad \mathrm{and} \quad E_{ey} = -\frac{g_{exy}}{g_{eyy}}E_{ex} -\frac{-0.06}{0.14}E_{ex} \sim 0.43E_{ex}

As the tissue strip is infinitely long :math:`V_m = \Phi_i - \Phi_e` cannot be a function of x thus

.. math::

    - \frac{\partial{\Phi_{ix}}}{\partial{x}} = E_{ix} = E_{ex} = - \frac{\partial{\Phi_{ex}}}{\partial{x}}


and it follows that

.. math::

    E_{iy} = \frac{g_{ixy}g_{eyy}}{g_{iyy}g_{exy}}E_{ey} = \frac{-0.09*0.14}{0.11*-0.06}E_{ey} \sim 1.91 E_{ey}.


Thus,

.. math::

     E_{iy} =- \frac{\partial{\Phi_{iy}}}{\partial{y}} \neq E_{ey} = - \frac{\partial{\Phi_{ey}}}{\partial{y}}

holds  with :math:`E_{iy} = 1.91 E_{ey} \neq E_{ey}` as well as

.. math::

    \frac{\partial{V_m}}{\partial{y}} = \frac{\partial{\Phi_{iy}}}{\partial{y}} - \frac{\partial{\Phi_{ey}}}{\partial{y}} \neq 0

with  :math:`\frac{\partial{V_m}}{\partial{y}} = E_{ey}-E_{iy}= E_{ey}- 1.91 E_{ey} = -0.91 E_{ey} \neq 0`.

It can be concluded that the y-directed gradient of :math:`V_m` causes membran polarisation near the boundaries (see results of numerical solution).

Near the center of the strip at y=0 the net current :math:`\mathbf{J}` must be in direction of x with :math:`J_y=0`, but due to rotation intra- and
extracellular densities :math:`\mathbf{J}_i` and :math:`\mathbf{J}_e` make an angle with the x-axis. Intra- and extracellular field are equal:

.. math::

     \mathbf{E}_{i} = \mathbf{E}_{e},

which implies that the :math:`\nabla V_m` vanishes near the center (see results of numerical solution) as

.. math::

     \nabla V_m = \nabla \Phi_i - \nabla \Phi_e = -\mathbf{E}_{i} + \mathbf{E}_{e} = 0.


**Numerical Solution:** Front and rear view of simulated tissue strip for :math:`\alpha = 45^{\circ}`:

.. figure:: /images/02_EP_tissue_14_bidm_rotation_numericalresults_new.png
     :scale: 20 %


Interpretation
=================

Simulation results confirm theoretical expectations. Gradient :math:`\frac{\partial{V_m}}{\partial{y}}` hyperpolarizes and depolarizes edges of the tissue strip at
:math:`y=-W/2` and :math:`y=+W/2` and vanishes at the center of the strip.

Literature
==========

.. [#sepulveda] Sepulveda, Nestor G., Bradley J. Roth, and John P. Wikswo Jr., **Current injection into a two-dimensional anisotropic bidomain**,
 Biophysical journal 55.5 (1989): 987-999.
   `[Pubmed] <https://pubmed.ncbi.nlm.nih.gov/2720084/>`__
   `[Full] <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1330535/>`__
.. [#rothwood] Bradley J. Roth, and Marcella C. Woods, **The magnetic field associated with a plane wave front propagating through cardiac tissue**,
 IEEE transactions on biomedical engineering, Vol. 46, No.11 (1999).
   `[Pubmed] <https://pubmed.ncbi.nlm.nih.gov/10582413/>`__
.. [#roth] Bradley J. Roth, **How to explain why unequal anisotropy ratios is important usind pictures but no mathematics**,
 International Conference of the IEEE Engineering in Medicine and Biology Society (2006).
   `[Pubmed] <https://pubmed.ncbi.nlm.nih.gov/17946406/>`__

"""


# EXAMPLE_* comment when tutorial is not supposed to be shown
EXAMPLE_DESCRIPTIVE_NAME = 'Fibre Angle Stimulation I - Rotation'
EXAMPLE_AUTHOR = 'Laura Marx <laura.marx@medunigraz.at>'

import os
import sys
from datetime import date

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--angle',
                        default=45,
                        help='pick fiber angle (0 is along the long edge of the preparation)')
    group.add_argument('--resolution',
                        default=250,
                        help='pick resolution in um')
    group.add_argument('--grounded',
                        default='on',
                        choices=['on','off'],
                        help='turn on/off use of ground whereever possible')
    return parser

def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    return '{}_angle_{}'.format(today.isoformat(), args.angle)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    res = float(args.resolution)/1000. # convert um to mm
    geom = mesh.Block(size=(60, 10, 1),resolution=res)
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(args.angle, args.angle, 90, 90)
    # Generate and return base name
    meshname = mesh.generate(geom)

    # define electrode locations
    lstm = mesh.block_boundary_condition(geom, 'stimulus', 0, 'x', lower=True,
                                        verbose=not args.silent)
    rstm = mesh.block_boundary_condition(geom, 'stimulus', 1, 'x', lower=False,
                                       verbose=not args.silent)
    astm = []
    mod  = []

    # define default view settings
    view = 'Phie_front_hyper.mshz'
    #view = 'Phie_back_depol.mshz'

    # make subtest specific settings
    # left electrode injects extracellular current
    # right electrode withdraws extracellular current
    # auxiliary electrode in the middle is grounded
    lstm += [ '-stimulus[0].stimtype', '1',
               '-stimulus[0].strength', '3.1e6']
    rstm += [ '-stimulus[1].stimtype', '1',
               '-stimulus[1].balance',  '0' ]
    if args.grounded == 'on':
        astm += [ '-stimulus[2].stimtype', '3' ]

    # Add all the non-general arguments
    cmd = tools.carp_cmd('rotate.par')

    cmd += ['-meshname',  meshname,
            '-gridout_i', 3,
            '-simID',     job.ID]

    # add PDE solver options
    cmd += lstm + rstm + astm

    if args.visualize:
        vis_cmd = ['-gridout_i', 3,
                   '-gridout_e', 3]
        cmd += vis_cmd

    # Run simulations
    job.carp(cmd)

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        # show phie first
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'phie.igb')

        job.gunzip(data)
        job.meshalyzer(geom, data, view)


        # show Vm, front view showing hyperpolarized edge
        geom = os.path.join(job.ID, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb')

        view = 'Vm_front_hyper.mshz'
        #view = 'Vm_back_depol.mshz'

        job.gunzip(data)
        job.meshalyzer(geom, data, view)

# Define some tests
#__tests__ = []

if __name__ == '__main__':
    run()

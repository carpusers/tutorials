#!/usr/bin/env python

"""

Introduction
============

The choice of discretization is of major significance for the accuracy of the numerical
approximation and thus for the validity of the computed simulation results. This example
allows to study the effects of changing spatial and temporal discretizations on the
reproduction of a known wedge activation.
The setup used here is entirely based on the N-version benchmark study due to Niederer et al [Niederer-Nversion]_.

Setup
-----

Given is a wedge of 20 x 7 x 3 mm. Starting at one corner, an activation wave propagates across
the wedge, arriving last at the opposite corner.
The time of activation T(x) of every space-point on the line between the
stimulation corner and the opposing corner is plotted. 
More on the specific details of this benchmark are found here [Niederer-Nversion]_
along with the benchmark results of the participating codes/groups.

The original figure as used in the publication explaining this setup is shown below in 
:numref:`fig-EP-NversionBenchmarkSetup`.

.. _fig-EP-NversionBenchmarkSetup: 

.. figure:: /images/EP_NversionBenchmarkSetup.jpg
   :align: center
   :width: 50%

   (a)Schematic showing the dimensions of the simulation domain. The stimulus was applied within the cube marked S.
   (b) Summary of points at which activation time was evaluated. 
   Activation times at points :math:`P_1 - P_9` were evaluated and were made available in the 
   `electronic supplementary material <http://rsta.royalsocietypublishing.org/content/roypta/suppl/2011/09/16/369.1954.4331.DC1/rsta20110139supp1.pdf>`_. 
   Plots of the activation time were evaluated along the line from :math:`P_1 - P_8`
   and plots of the activation along the plane shown are provided in two dimensions.


Usage
=====

To run the examples of this tutorial do

.. code-block:: bash

   cd ${TUTORIALS}/02_EP_tissue/11_discretization

To inquire the exposed experimental input parameters run

.. code-block:: bash

   ./run.py --help


which yields the following options:

.. code-block:: guess

    --sourceModel {monodomain,bidomain,pseudo_bidomain} pick type of source model
    --propagation {R-D,R-E+,R-E-,AP-E}
                          pick propagation mode, either full R-D, R-E+, R-E-, or
                          prescribe AP shape with trigger instant
    --duration DURATION   duration of experiment, typically 25. for activation
                          only, 400 to include repolarization
    --dx DX               resolution of the mesh in um to be used for benchmark
    --dt DT               temporal resolution in us
    --lump {0,1}          lump mass matrix
    --opSplit {0,1}       use operator splitting
    --graphPart {on,off}  turn graph partitioning on/off
    --plot {0,1}          set to 1 to generate a plot of activation time
                          alongthe wedge's diagonal
    --compare             run benchmark with --dx 100 200 and 500 and make
                          comparison plot

The user can either run single experiments, or do a comparison of the multiple
resolutions with the option ``--compare``.

Run example
===========

To test the effect of changing spatial resolutions when using the default
Reaction-Diffusion propagation model, run:

.. code-block:: bash

    ./run.py --compare --np 8 --propagation R-D


To test the effect of changing spatial resolutions when using the 
Reaction-Eikonal propagation model, run:

.. code-block:: bash

    ./run.py --compare --np 8 --propagation R-E-

Results plotted when providing the option ``--compare`` are shown in :numref:`fig-Nversion-R-E-minus`.

.. _fig-Nversion-R-E-minus:

.. figure:: /images/NversionBenchmark_R_E_minus.png
   :align: center

   Impact of resolution in the N-version Niederer benchmark 
   when using the R-E- model.

.. rubric:: References

.. [Niederer-Nversion] Niederer SA, Kerfoot E, Benson AP, Bernabeu MO, Bernus O, Bradley C, Cherry EM, Clayton R, 
                       Fenton FH, Garny A, Heidenreich E, Land S, Maleckar M, Pathmanathan P, 
                       Plank G, Rodriguez JF, Roy I, Sachse FB, Seemann G, Skavhaug O, Smith NP.
                       **Verification of cardiac tissue electrophysiology simulators using an N-version benchmark.**
                       *Philos Trans A Math Phys Eng Sci.* 369(1954):4331-51, 2011.
                       [`PubMed <https://www.ncbi.nlm.nih.gov/pubmed/21969679>`_] [
                       [`PMC <https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3263775/>`_] 
                       [`Full Text <http://rsta.royalsocietypublishing.org/content/369/1954/4331.long>`_]

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Electrophysiology N-version benchmark - Role of spatial discretization'
EXAMPLE_AUTHOR = 'Aurel Neic <aurel.neic@medunigraz.at>'


import os
import sys
from datetime import date
import numpy as np
from matplotlib import pyplot

from carputils import settings
from carputils import tools
from carputils import mesh
from carputils.carpio import txt
from carputils.carpio.txt import open as txt_open
from carputils import ep

isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range

def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--sourceModel',
                        default='monodomain',
                        choices=ep.MODEL_TYPES,
                        help='pick type of source model')
    group.add_argument('--propagation',
                        default='R-D',
                        choices=ep.PROP_TYPES,
                        help='pick propagation mode, either full R-D, R-E+, R-E-, '
                            +'or prescribe AP shape with trigger instant')
    group.add_argument('--duration',
                        type=float, default=50.0,
                        help='duration of experiment, typically 25. for '
                            +'activation only, 400 to include repolarization')
    group.add_argument('--dx',
                        type=float, default=100.0,
                        help='resolution of the mesh in um to be used for '
                            +'benchmark')
    group.add_argument('--dt',
                        type=float, default=20.0,
                        help = 'temporal resolution in us')
    group.add_argument('--lump',
                        type=int, default=0,
                        choices=[0, 1],
                        help='lump mass matrix')
    group.add_argument('--opSplit',
                        type=int, default=1,
                        choices=[0, 1],
                        help='use operator splitting')
    group.add_argument('--graphPart',
                        default='on',
                        choices=['on','off'],
                        help='turn graph partitioning on/off')
    group.add_argument('--plot',
                        type=int, default=1,
                        choices=[0, 1],
                        help='set to 1 to generate a plot of activation time along' +
                             'the wedge\'s diagonal')
    group.add_argument('--compare',
                        action='store_true',
                        help='run benchmark with --dx 100 200 and 500 and make'
                            +' comparison plot')

    return parser

def setupStimuli():

    numstim = 1
    stim = ['-stimulus[0].x0',      -10001,
            '-stimulus[0].y0',       -3501,
            '-stimulus[0].z0',       -1501,
            '-stimulus[0].xd',        1502,
            '-stimulus[0].yd',        1502,
            '-stimulus[0].zd',        1502,
            '-stimulus[0].strength', 35.71,
            '-stimulus[0].duration',  2.00,
            '-stimulus[0].start',     0.00]
    return stim, numstim

def setupAPshape():

  # reconfigure ionic models
  imps = ['-imp_region[0].im', 'IION_SRC',
          '-imp_region[0].im_param', '']

  tactAdj = ['-num_adjustments',        1,
             '-adjustment[0].file',     'IION_SRC.tact__',
             '-adjustment[0].dump',     1,
             '-adjustment[0].variable', 'IION_SRC.tact_']
  return imps + tactAdj


def setGraphPartFlags(graphPart, flv):

    if graphPart == 'on':
        part = 1
    else:
        if flv == 'PT':

            print ('\n\n--------------------------------------------')
            print ('Flavor pt not compatible with graphPart=off')
            print ('Setting graphPart=on')
            print ('--------------------------------------------\n\n')

            part = 1

        else:
            part = 0

    gpart = ['-pstrat',   part,
             '-pstrat_i', part ]

    return gpart


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    num_settings = ''
    if args.propagation != 'eikonal':
        tpl = '_dt_{}_us_lump_{}_op_{}_{}'
        num_settings = tpl.format(args.dt, args.lump, args.opSplit, args.flv)

    tpl = '{}_{}_{}_res_{}um{}_np{}'
    return tpl.format(today.isoformat(),
                      args.sourceModel, args.propagation, args.dx,
                      num_settings, args.np)

@tools.carpexample(parser, jobID)
def run(args, job):

    # Generate mesh
    # Block which is thin in z direction
    geom = mesh.Block(size=(20, 7, 3), resolution=args.dx/1000)
    # Set fibre angle to 0, sheet angle to 0
    geom.set_fibres(0, 0, 90, 90)

    # Generate and return base name
    meshname = mesh.generate(geom)

    # Add all the non-general arguments
    cmd = tools.carp_cmd('nversion.par')

    # define stimulus
    stim, numstim = setupStimuli()
    # set up propagation
    propOpts, numstim, stim = ep.setupPropagation(args.propagation, numstim, stim, '')
    cmd += ['-num_stim', numstim] + stim + propOpts

    # set source model
    cmd += ep.model_type_opts(args.sourceModel)

    ## toggle graph partitioning
    #cmd += setGraphPartFlags(args.graphPart, args.flv.upper())

    cmd += ['-meshname',           meshname,
            '-simID',              '%s/em'%(job.ID),
            '-dt',                 args.dt,
            '-mass_lumping',       args.lump,
            '-operator_splitting', args.opSplit,
            '-tend',               args.duration]

    if args.visualize:
        vis_cmd = ['-gridout_i', 3]
        cmd += vis_cmd

    # Run simulation
    job.carp(cmd)

    # analysis not executed if done in --dry mode
    if args.dry:
        sys.exit(0)

    # open activation time file
    act_fname = os.path.join(job.ID, 'em', 'init_acts_vm_act-thresh.dat')

    # Get activation times on diagonal
    # Get arrangement of points in mesh
    point_dims = geom.shape()
    divs = point_dims - 1

    # Work out points on diagonal
    diag_divs = 1
    for i in xrange(1, divs.min()):
        remainder = divs % i
        if (remainder == 0).all():
            diag_divs = i

    step = divs / diag_divs

    nodes = []
    for i_step in xrange(diag_divs + 1):
        dix, diy, diz = step * i_step
        nodes.append(  dix
                     + diy * point_dims[0]
                     + diz * point_dims[0] * point_dims[1])

    # Read activation data
    data      = txt.read(act_fname)
    act_times = data[nodes]

    if args.plot == 1:
        # Plot activation times
        fig = pyplot.figure()
        ax = fig.add_subplot(1,1,1)
        ax.plot(np.arange(diag_divs + 1), act_times, 'x-')

        if isinstance(args.plot, str):
            # Save to file
            pyplot.savefig(args.plot, bbox_inches='tight')
        else:
            pyplot.show()

    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        view = 'nversion.mshz'
        geom = os.path.join(job.ID, 'em', os.path.basename(meshname)+'_i')

        #job.gunzip(act_fname)
        job.meshalyzer(geom, act_fname, view)

    return act_times

# test nversion
#test_nversion = testing.Test('nversion',
#                            run, [],
#                            tags=[testing.tag.FAST, testing.tag.SERIAL])
#test_nversion.add_filecmp_check('init_acts_vm_act-thresh.dat', testing.max_error, 0.001)

#__tests__ = [test_eikonal]

if __name__ == '__main__':

    # Check if --compare has been set
    args = parser().parse_args()

    if args.compare:
        # Copy all other parameters expect dx
        argv = []
        for arg in sys.argv[1:]:
            if arg == '--compare':
                continue
            if arg in ['--dx', '--duration']:
                raise Exception('cannot set --dx or --duration with --compare')
            argv.append(arg)

        # Run sims and get times back
        times100 = run(argv + ['--dx', '100', '--duration', '50', '--plot', '0'])
        times200 = run(argv + ['--dx', '200', '--duration', '70', '--plot', '0'])
        times500 = run(argv + ['--dx', '500', '--duration', '150', '--plot', '0'])

        # Plot activation times
        fig = pyplot.figure()
        ax = fig.add_subplot(1,1,1)

        ax.plot(np.linspace(0, 21.4, len(times100)), times100, 'rx-',
                label='$\Delta x = 0.1\\mathrm{mm}$')
        ax.plot(np.linspace(0, 21.4, len(times200)), times200, 'gx-',
                label='$\Delta x = 0.2\\mathrm{mm}$')
        ax.plot(np.linspace(0, 21.4, len(times500)), times500, 'bx-',
                label='$\Delta x = 0.5\\mathrm{mm}$')

        ax.set_xlabel('Distance along diagonal (mm)')
        ax.set_ylabel('Activation time (ms)')
        pyplot.legend(loc='upper left')

        pyplot.show()

    else:
        # Just run the simulation
        run()

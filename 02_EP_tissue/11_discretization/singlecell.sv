-85.23             # Vm
-                  # Lambda
-                  # delLambda
-                  # Tension
-                  # K_e
-                  # Na_e
-                  # Ca_e
-0.0346182         # Iion
-                  # tension_component
TT2
0.126              # Ca_i
3.64               # CaSR
0.00036            # CaSS
0.9073             # R_
6.59708e-09        # O - not specified in benchmark, CARP specific
8.604              # Na_i
136.89             # K_i
0.00172            # M
0.7444             # H
0.7045             # J
0.00621            # Xr1
0.4712             # Xr2
0.0095             # Xs
2.42e-8            # R
0.999998           # S
3.373e-05          # D
0.7888             # F
0.9755             # F2
0.9953             # FCaSS


#!/usr/bin/env python

"""
.. _tutorial_laplace-dirichlet-fibers:

This tutorial demonstrates how to generate cardiac fiber and sheet orientation in biventricular models using the rule-based approach by [#Bayer2012]_.

Introduction
=========================
We developed a robust Laplace-Dirichlet Rule-Based algorithm for incorporating myocardial fiber orientation into computer models of biventricular geometries with speed, precision, and high usability. This tutorial demonstrates how to use the implementation of Bayer et al with CARP and its utilities. 

Problem setup
=========================

Ventricular fiber and sheet orientation rules
---------------------------------------------
In mammalian ventricles, the following rules for cardiac fiber orientation apply as derived from histological and diffusion-tensor imaging.

R1. The longitudinal fiber direction in the ventricular walls is parallel to the endocardial and epicardial surfaces.

R2. The longitudinal fiber direction rotates clockwise throughout the ventricular walls from the endocardium (+ :math:`\\alpha`) to the epicardium (- :math:`\\alpha`), where :math:`\\alpha` is the helical angle with respect to the counterclockwise circumferential direction in the heart when looking from the base
towards the apex.

R3. The longitudinal fiber direction in papillary muscles and trabeculae is parallel to the long axis of these structures.

R4. The transverse fiber direction is perpendicular to the longitudinal fiber direction and is defined by the angle :math:`\\beta`, where :math:`\\beta` is the angle with respect to the outward transmural axis of the heart. 

R5. The sheet normal is orthonormal to the longitudinal and transverse fiber directions.

R6. Fiber orientation in the septum is continuous with the ventricular walls.

Laplace-Dirichlet method
------------------------
To define the rules above in any finite element mesh of mammalian ventricles, we solve the solution to Laplace's equation with Dirichlet boundary conditions for the four scenarios shown in the lefthand side of the figure below. We then use these solutions to develop the local coordinate system shown in the righthand side of the figure below. Tutorials for how to solve Laplace's equation and gradients of scalar fields can be found at (TBA) and (TBA), respectively.

.. _fig-cv_calculation-2D:

.. figure:: /images/ld_rb_method.png
   :width: 100 %
   :align: center

Using the local coordinate system derived by taking the gradients of the Laplace solutions, we then generate a longitudinal fiber direction according to user inputs for :math:`\\alpha` on the cardiac surfaces, and the transverse and normal directions for sheet orientation according to the user inputs for :math:`\\beta` on the cardiac surfaces.

Canine biventricular model
--------------------------
For this example, we use a finite element mesh of an adult canine biventricular geometry. The model is composed of 149,734 tetrahedral elements with an average edge length of 1.75 mm. This particular mesh was chosen since it's lower resolution facilitates quick computation and visualization times. Note, all boundary conditions as shown in the figure above were computed beforehand using a customized tool. A tutorial for written in the near future.

Usage
=========================
The following optional arguments are available (default values are indicated):

.. code-block:: bash

  ./run.py --help 
    --mode              Options: {all,fibersonly}, Default: all
                        The option "all" computes the Laplace solutions
                        as well as the rule-based fiber orientation.
    --apex              Options: {apex_lv,apex_rvlv}, Default: apex_lv
                        The option sets the apical boundary condition either
                        at the LV apex or at the junction of the RV and LV.
    --alpha_endo        Default: 40  degrees
                        Helical angle on endocardial surface
    --alpha_epi         Default: -50  degrees
                        Helical angle on epicardial surface
    --beta_endo         Default: -65  degrees
                        Sheet angle on endocardial surface
    --beta_epi          Default: 25  degrees
                        Sheet angle on epicardial surface

After running run.py, the results for the fiber orientations can be found in the output subdirectory corresponding to the input parameters. The longitudinal fiber direction can be found in canine_long.vec, the transverse direction in canine_tran.vec, and the sheet normal direction in canine_norm.vec.

If the program is ran with the ``--visualize`` option, meshalyzer will automatically load the longitudinal fiber orientation. 

Tasks
=========================
1. Generate fiber and sheet orientation using the apex_lv boundary condition.

.. code-block:: bash

  ./run.py --apex apex_lv

2. Generate fiber and sheet orientation using the apex_rvlv boundary condition.

.. code-block:: bash

  ./run.py --apex apex_rvlv

3. Visualize a purely circumferential longitudinal fiber orientation.

.. code-block:: bash

  ./run.py --alpha_endo 0 --alpha_epi 0

4. Visualize a purely apicobasal longitudinal fiber orientation.

.. code-block:: bash

  ./run.py --alpha_endo 90 --alpha_epi 90


Literature
==========

.. [#Bayer2012] Bayer JD, Blake R, Plank G, Trayanova NA.
                **A Novel Rule-Based Algorithm for Assigning Myocardial Fiber Orientation to Computational Heart Models.**
                *Annals of Biomedical Engineering*, 40(10):2243-2254, 2012.
                `[Pubmed] <https://www.ncbi.nlm.nih.gov/pubmed/22648575>`_

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Laplace-Dirichlet Rule-Based fiber and sheet generation.'
EXAMPLE_AUTHOR = 'Jason Bayer <jason.bayer@ihu-liryc.fr>'

import os
import sys

from datetime import date
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    # Generate the standard command line parser
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    # Add arguments    
    group.add_argument('--mode',
                        default='all', 
                        choices=['all', 'fibersonly'],
                        help='Mode to run program, all does laplaces')
    group.add_argument('--apex',
                        default='apex_lv', 
                        choices=['apex_lv', 'apex_rvlv'],
                        help='Options to put apex node')
    group.add_argument('--alpha_endo',
                        type=float, 
                        default=40,
                        help='Longitudinal fiber angle endocardium')
    group.add_argument('--alpha_epi',
                        type=float, 
                        default=-50,
                        help='Longitudinal fiber angle epicardium')
    group.add_argument('--beta_endo',
                        type=float, 
                        default=-65,
                        help='Sheet angle endocardium')
    group.add_argument('--beta_epi',
                        type=float, 
                        default=25,
                        help='Sheet angle epicardium')

    return parser

def jobID(args):
    today = date.today()
    ID = '{}_alpha_endo-{}_alpha_epi-{}_beta_endo-{}_beta_epi'.format(today.isoformat(), args.alpha_endo, args.alpha_epi, args.beta_endo, args.beta_epi)
    return ID

@tools.carpexample(parser, jobID)
def run(args, job):

    meshdir = './Mesh/canine'
    surfdir = './Surfs/surf'
    parfdir = './Parfiles'
    if args.apex == 'apex_lv':
        apexdir = '_apex'
    if args.apex == 'apex_rvlv':
        apexdir = '_apex_rvlv'

    if args.mode == 'all':

        #####################################
        #Solver for the epi laplace soluton
        #####################################
        cmd = tools.carp_cmd(parfdir+'/lps_epi.par')
        simid = './L_EPI'
        cmd += [ '-simID', simid,
                 '-meshname', meshdir,
                 '-stimulus[0].vtx_file', surfdir+'_lendo',
                 '-stimulus[1].vtx_file', surfdir+'_rendo',
                 '-stimulus[2].vtx_file', surfdir+'_epi']
        
        #Run simulation
        job.carp(cmd)
        
        #Now make a data file
        cmd = [ settings.execs.igbextract, 
                '-o', 'ascii_1pL',
                '-O', './L_EPI/phie.dat',
                './L_EPI/phie.igb' ]
        
        # run igbextract
        job.bash(cmd)
        
        #####################################
        #Solver for the lv laplace soluton
        #####################################
        cmd = tools.carp_cmd(parfdir+'/lps_lv.par')
        simid = './L_LV'
        cmd += [ '-simID', simid,
                 '-meshname', meshdir,
                 '-stimulus[0].vtx_file', surfdir+'_epi',
                 '-stimulus[1].vtx_file', surfdir+'_rendo',
                 '-stimulus[2].vtx_file', surfdir+'_lendo']
        
        #Run simulation
        job.carp(cmd)
        
        #Now make a data file
        cmd = [ settings.execs.igbextract, 
                '-o', 'ascii_1pL',
                '-O', './L_LV/phie.dat',
                './L_LV/phie.igb' ]
        
        # run igbextract
        job.bash(cmd)
        
        #####################################
        #Solver for the rv laplace soluton
        #####################################
        cmd = tools.carp_cmd(parfdir+'/lps_rv.par')
        simid = './L_RV'
        cmd += [ '-simID', simid,
                 '-meshname', meshdir,
                 '-stimulus[0].vtx_file', surfdir+'_lendo',
                 '-stimulus[1].vtx_file', surfdir+'_epi',
                 '-stimulus[2].vtx_file', surfdir+'_rendo']
        
        #Run simulation
        job.carp(cmd)
        
        #Now make a data file
        cmd = [ settings.execs.igbextract, 
                '-o', 'ascii_1pL',
                '-O', './L_RV/phie.dat',
                './L_RV/phie.igb' ]
        
        # run igbextract
        job.bash(cmd)
        
        #####################################
        #Solver for the epi laplace soluton
        #####################################
        cmd = tools.carp_cmd(parfdir+'/lps_ab.par')
        simid = './L_AB'
        cmd += [ '-simID', simid,
                 '-meshname', meshdir,
                 '-stimulus[0].vtx_file', surfdir+apexdir,
                 '-stimulus[1].vtx_file', surfdir+'_base' ]
        
        #Run simulation
        job.carp(cmd)
        
        #Now make a data file
        cmd = [ settings.execs.igbextract, 
                '-o', 'ascii_1pL',
                '-O', './L_AB/phie.dat',
                './L_AB/phie.igb' ]
        
        # run igbextract
        job.bash(cmd)
        
    #Run the fiber generation
    ###########How to properly call GlRuleFibers?########
    cmd = [ settings.execs.GlRuleFibers, 
            '-m', meshdir,
            '-a', './L_AB/phie.dat',
            '-e', './L_EPI/phie.dat',
            '-l', './L_LV/phie.dat',
            '-r', './L_RV/phie.dat',
            '--alpha_endo='+str(args.alpha_endo),
            '--alpha_epi='+str(args.alpha_epi),
            '--beta_endo='+str(args.beta_endo),
            '--beta_epi='+str(args.beta_epi),
            '-o', meshdir+'_rb.lon' ]
    
    # run igbextract
    job.bash(cmd)        

    #Load the data file and write the data files
    lonfile = meshdir+'_rb.lon'
    filel = open(lonfile, 'r')
    fibfile = meshdir+'_long.vec'
    filef = open(fibfile, 'w')
    tranfile = meshdir+'_tran.vec'
    filet = open(tranfile, 'w')
    sheetfile = meshdir+'_norm.vec'
    files = open(sheetfile, 'w')

    lines=filel.readlines()
    cnt=0    
    for line in lines:
        if cnt > 0:
            p = line.split( )
        
            fx = float(p[0])
            fy = float(p[1])
            fz = float(p[2])
            tx = float(p[3])
            ty = float(p[4])
            tz = float(p[5])

            #Take the cross product to get the sheet
            sx = (fy*tz - fz*ty)
            sy = (fz*tx - fx*tz)
            sz = (fx*ty - fy*tx)

            #write the vectors
            filef.writelines(str(fx)+' '+str(fy)+' '+str(fz)+'\n')
            filet.writelines(str(tx)+' '+str(ty)+' '+str(tz)+'\n')
            files.writelines(str(sx)+' '+str(sy)+' '+str(sz)+'\n')
        cnt += 1

    filel.close()
    filef.close()
    filet.close()
    files.close()

    cpcmd = 'cp ' + meshdir + '_elcpt.pts ' + meshdir + '_long.vpts'
    os.system(cpcmd)
    cpcmd = 'cp ' + meshdir + '_elcpt.pts ' + meshdir + '_tran.vpts'
    os.system(cpcmd)
    cpcmd = 'cp ' + meshdir + '_elcpt.pts ' + meshdir + '_norm.vpts'
    os.system(cpcmd)

    #Now move into the job directory
    cpcmd = 'mv ' + meshdir + '_long.* ' + job.ID
    os.system(cpcmd)
    cpcmd = 'mv ' + meshdir + '_tran.* ' + job.ID
    os.system(cpcmd)
    cpcmd = 'mv ' + meshdir + '_norm.* ' + job.ID
    os.system(cpcmd)

    #Visualize with meshalyzer
    if args.visualize:
        job.meshalyzer('./Mesh/canine_long.vpts', 'fiber_vis.mshz')     

if __name__ == '__main__':
    run()

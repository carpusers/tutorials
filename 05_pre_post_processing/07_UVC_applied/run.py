#!/usr/bin/env python

"""


.. _UVCs-applied:

=========================
Applying UVCs
=========================

This tutorial demonstrates how to apply UVCs.

Introduction
============

UVCs allow one to easily define structures on a mesh or transfwer data between two meshes. 
We can view UVC space as the reference frame and as such, transferring from Cartesian to UVC space is 
a pull operation, and going the other way is a push.

If transferring scalar data between meshes, it suffices to have the UVCs of where the data is on the source mesh, and interpolate based on UVC positions on the destination mesh.

For transferring gradients and directions, the quantities are vectors which are first *pulled* to UVC and *pushed* to Cartesian making use of the deformation gradient.
In these cases, the deformation gradient is calculated by taking the gradient of the UVCs with respect to the Cartesian coordiantes.
Care must be taken performing the calculation for the :math:`\\phi` coordinate because of the branch cut at :math:`\\pi`.
Even though  the value :math:`\\phi`  decreases when going clockwise, crossing from :math:`\\pi` to :math:`-\\pi`, 
the gradient is still positive because we need to ignore the branch cut and continue to increase  :math:`\\phi` above
:math:`\\pi`.

The program *closest_hc_wf.py* is capable of these mappings. To see how to use the program, detailed help is available::

    closest_hc_wf.py -h


Applications
=========================

The generation of UVCs for a particular mesh should have been previously described. This tutorial 
will describe how to use UVCs to define location generically and how to transfer data.

Looking at the experiment specific options::

  --line Z0 R0 PHI0 Z1 R1 PHI1 V
                        define a line by start and end points (default: None)
  --n-line-pts N_LINE_PTS
                        number of points in each line [50]
  --box-region Z0 R0 PHI0 Z1 R1 PHI1 V LABEL
                        define a region by bounding box and give a label to it
                        (default: None)
  --expr-region EXPRESSION REGION_VALUE
                        include points (Z,R,P,V) satisfying specified python
                        expression (default: None)
  --transfer UVCs DATA  UVC points on to which data is to be transferred
                        (default: None)
  --mesh MESH           mesh on which to define objects [canine]
  --UVC UVC             UVC file for mesh [dog_vtx]
  --scale SCALE SCALE   scaling for UVC search (default: [3, 0.3333])
  --IDWpower IDWPOWER   inverse distance weighting power (default: 2)
  --numf NUMF           number of closest points to find (4)

Lines
-----

Lines can be defined in UVCs by giving their starting and ending coordinates (except :math:`\\nu` which is specified at the end).
The option can be given multiple times to define multiple lines. The parameters are linearly interpolated between the specified end points.
Set :math:`\\nu` to *-1* for the LV and *1* for the RV.
Example: ``0 0 -3.141 .5 0 0 -1`` for an endocardial line in the lower half of the LV. 

.. figure:: /images/UVCline.png
   :align: center
   :width: 60%

   A line defined by *run.py --line 1 1 1.57  0.12 1 1.57  -1 --numf 25 --scale 2 .7 --IDW 1*.
   :math:`\\phi` data is displayed on the surfaces.

Bounding Box Regions
--------------------

Like lines, the *region* option can be given multiple times to 
define multiple regions by bounding boxes in UVCs.
Any point in the bounding box is tagged as within the region. Again, it is assumed that the region lies only in one
ventricle so one :math:`\\nu` value is specified after the other coordinates. A region label is also expected which is simply
an identifying integer.

Expression Regions
------------------

Regions may be defined by expressions as well. Expressions are written as Python functions of 
*Z*, *R*, *P*, and *V* which refer to the UVC coordinates :math:`\\mathfrak{z}`, :math:`\\rho`, :math:`\\phi`, and
:math:`\\nu` respectively.  The function is evaluated at each coordinate and those coordinates evaluating
to a true result are added to the region.
A value for the region is also required. Examples: 

* The lower half of the RV: *Z<0.5 and V>=0*
* The LV freewall endocardium: *R<0.1 and V<=0 and (-3.1415926<P<-1.57 or 3.1415926>P>1.57)*

Controlling Interpolation
-------------------------

Interpolating on to the new points from the known is performed using Shepard's formula, that is, inverse distance weighting to an exponent. The exponent on the distance can be controlled with ``--IDWpower``. The number of the nearest points to use for interpolation is controlled by ``--numf``.
The distance between points *A* and *B* in UVC space is determined by

.. math::

    d(A,B) = \\sqrt{ S_{\\mathfrak{z}}(\\mathfrak{z}_A-\\mathfrak{z}_B)^2 + S_{\\rho} (\\rho_A-\\rho_B)^2 + (\\phi_A-\\phi_B)^2}

where :math:`S_{\\mathfrak{z}}` and :math:`S_{\\rho}` can be set with ``--scale``. Usually *3 0.3* is reasonable.


Exercises
=========

.. note::

    The dog mesh is low resolution which accounts for some of the jaggedness of results.

#. Define the LAD as a line
#. Define the circumflex artery. You may have to use more than 1 line and cross ventricles.
#. Define a full thickness, RV free wall scar region
   covering the middle third in the apical basal direction.
#. Try using an expression to define a circular region of radius r centered at (B,*,A,1) with something like::
      
      run.py --expr-region '((P-A)**2 + (Z-B)**2 < r**2) and V>=0' 2 --vis

   Replace *A*, *B* and *r* with apropriate values. You may need to scale in a direction to get it more circular. 
  
#. Transfer the dog mesh *Z* coordinates to the rabbit mesh. First extract the *Z* coordinates from 
   the UVC file of the dog::
   
      awk '{print $1;}' dog_vtx.pts | tail -n +2 > dog_z.dat

#. Define a region on the dog and transfer it (*region.dat*) to the rabbit

Literature
==========

.. [#bayer2018] Bayer et al., 
   Universal ventricular coordinates: A generic framework for describing position within
   the heart and transferring data,
   Med Image Anal, 45: 83-93, 2018.
   `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/29414438>`__
   `[Full] <https://www.medicalimageanalysisjournal.com/article/S1361-8415(18)30020-3/fulltext>`__


"""

EXAMPLE_DESCRIPTIVE_NAME = 'UVC Applications'
EXAMPLE_AUTHOR = 'Edward Vigmond <edward.vigmond@ihu-liryc.fr>'

import os
import sys
import numpy as np
from subprocess import call

from datetime import date
from carputils import settings
from carputils import tools
from carputils import mesh
from carputils import testing

def parser():
    # Generate the standard command line parser
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    # Add arguments    
    group.add_argument('--line',
                        action='append',
                        type=float,
                        nargs=7,
                        metavar=('Z0','R0','PHI0','Z1','R1','PHI1','V'),
                        default=None,
                        help='define a line by start and end points')
    group.add_argument('--n-line-pts',
                        type=int,
                        default=50,
                        help='number of points in each line [%(default)s]' )
    group.add_argument('--box-region',
                        action='append',
                        nargs=8,
                        metavar=('Z0','R0','PHI0','Z1','R1','PHI1','V', 'LABEL'),
                        type=float,
                        default=None,
                        help='define a region by bounding box and give a label to it')
    group.add_argument('--expr-region',
                        nargs=2,
                        metavar=('EXPRESSION','REGION_VALUE'),
                        action='append',
                        default=None,
                        help='include points (Z,R,P,V) satisfying specified python expression' )
    group.add_argument('--transfer',
                       default=None,
                       metavar=('UVCs','DATA'),
                       nargs=2,
                       help='UVC points on to which data is to be transferred' )
    group.add_argument('--mesh',
                       default='canine',
                       help='mesh on which to define objects [%(default)s]' )
    group.add_argument('--UVC',
                       default='dog_vtx',
                       help='UVC file for mesh [%(default)s]' )
    group.add_argument('--scale',
                        type=float, 
                        nargs=2,
                        default=[ 3, .3333 ],
                        help='scaling for UVC search')
    group.add_argument('--IDWpower',
                       type=float,
                       default=2,
                       help='inverse distance weighting power')
    group.add_argument('--numf',
                       type=int,
                       default=4,
                       help='number of closest points to find (%(default)s)' )
    return parser


def jobID(args):
    today = date.today()
    ID = '{}'.format(today.isoformat())
    return ID

def read_UVCs( model ) :
    '''
    read UVC point file
    '''
    with open( model, 'r' ) as modin :
        npts = int(modin.readline().split()[0])
        pts = []
        for i in range(npts):
            pts.append([ float(x) for x in modin.readline().split()[0:4] ])

        return pts

def make_fcn( s ) : 
    return eval('lambda Z,R,P,V:'+ s)

def make_regions( args, pts, regid ):
    '''
    define a data file which gives a region for every point
    '''
    for reg in args.box_region :
        
        z0,   z1   = reg[0], reg[3]
        r0,   r1   = reg[1], reg[4]
        phi0, phi1 = reg[2], reg[5]
        v0         = reg[6]
        val        = reg[7]

        for j,(z,r,p,v) in enumerate(pts) :
            if z0<=z<=z1 and r0<=r<=r1 and phi0<=p<=phi1 and np.sign(v)==np.sign(v0) :
                regid[j]=val

def make_expr_regions( args, pts, regid ) :
    '''
    define regions based on arbitrary expressions
    '''
    for reg in args.expr_region :
        inRegion = make_fcn(reg[0])
        for i,p in enumerate(pts) :
            if inRegion( *p ):
                regid[i] = reg[1]


def make_lines( args ) :
    '''
    make lines defined in UVCs as an auxilliary grid
    '''
    tmp_line_UVCs = '.lines_UVC'

    #linearly interpolate from start to end point and store in file
    with open( tmp_line_UVCs+'.pts', 'w' ) as po:
        po.write( '{}\n'.format( len(args.line)*args.n_line_pts) );
        for i, l in enumerate(args.line) :
            zs = np.linspace( l[0], l[3], num=args.n_line_pts )
            rs = np.linspace( l[1], l[4], num=args.n_line_pts )
            ps = np.linspace( l[2], l[5], num=args.n_line_pts )
            for p in zip( zs, rs, ps ) :
                po.write( '{} {} {} {v}\n'.format( *p, v=l[6] ) )

    # convert UVC to XYZ
    cmdstr  = './closest_hc_wf.py --Hscale {0.scale[0]} {0.scale[1]} --numfind={0.numf} --transformed=.lines_XYZ '
    cmdstr += '--IDW-power {0.IDWpower} --Cartesian={0.mesh} pts-from-H dog_vtx {1}'
    cmd = cmdstr.format(args,tmp_line_UVCs).split()
    if not args.silent:
        print(cmd)
    call( cmd )
    with open( '.lines_XYZ.pts', 'r' ) as ptin :
        pts = ptin.readlines()[1:]

    #make an auxilliary grid 
    with open('lines.dat_t','w') as do, open('lines.pts_t','w') as po, open('lines.elem_t','w') as eo:
        do.write('1\n{}\n'.format(len(args.line)*args.n_line_pts) )
        po.write('1\n{}\n'.format(len(args.line)*args.n_line_pts) )
        eo.write('1\n{}\n'.format(len(args.line)*(args.n_line_pts-1)) )
        n = 0
        for i, l in enumerate(args.line) :
            for j in range(args.n_line_pts) :
                do.write( str(i)+'\n' )
                po.write( pts[n] )
                if j>0 :
                    eo.write('Ln {} {}\n'.format( n-1, n ) );
                n+=1

def transfer_data( args ) :
    ''' 
    transfer scalar data from one mesh to another
    '''
    cmdstr  = './closest_hc_wf.py --Hscale {0.scale[0]} {0.scale[1]} --numfind={0.numf} --transformed=transfer '
    cmdstr += '--data={0.transfer[1]} data-to-H {0.UVC} {0.transfer[0]}'
    cmd     = cmdstr.format(args).split()
    if not args.silent:
        print(cmd)
    call( cmd )


@tools.carpexample(parser, jobID)
def run(args, job):

    if args.transfer : transfer_data(args)

    if args.line   : make_lines( args )

    regid = None
    if args.box_region or args.expr_region :

        pts = read_UVCs( args.UVC+'.pts' )
        regid = [0]*len(pts)

        if args.box_region : 
            make_regions( args, pts, regid )
        if args.expr_region : 
            make_expr_regions( args, pts, regid )

        with open( 'regions.dat', 'w' ) as regout :
            for r in regid :
                regout.write( str(r)+'\n' )


    #Visualize with meshalyzer
    if args.visualize:
        arglist = ['./canine.pts',]
        if regid     : arglist += [ 'regions.dat' ]
        if args.line : arglist += [ 'lines.pts_t' ]
        arglist += [ 'lines.mshz' ]
        job.meshalyzer( *arglist )     

if __name__ == '__main__':
    run()

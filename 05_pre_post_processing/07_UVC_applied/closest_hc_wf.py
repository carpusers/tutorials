#!/usr/bin/python
#
class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   ITALIC = '\033[3m'
   STRIKE = '\033[9m'
   REVERSE = '\033[7m'
   BLINK = '\033[5m'
   END = '\033[0m'

description='''
This program maps different data types between Cartesian and UVCs using inverse distance weighting.
 
  {0.BOLD}heart_coords{0.END} is a points file of heart coordinates, in the same order as the model.pts file 
  {0.BOLD}coords{0.END} is a points file in Cartesian or heart coordinates, at which data is mapped to or from.
 
  One of the following operations must be specified:

      {0.BOLD}pts-to-H{0.END}: {0.UNDERLINE}coords is a set of Cartesian points{0.END} which will be
                converted to heart coordinates. A Cartesian points file, which corresponds 
                line by line to the heart_coords file must be specified
     
      {0.BOLD}pts-from-H{0.END} : {0.UNDERLINE}coords is a set of heart coordinate points{0.END} which will be
                   converted to Cartesian coordinates in the given model. 
                   A Cartesian points file corresponding to heart_coords must be specified
      
      {0.BOLD}data-to-H{0.END} : {0.UNDERLINE}coords is a set of heart coordinate points{0.END} at which data is to be transferred. 
                  data is specified with the data option and has a value for each point in the
                  source model. If F is specified, data are assumed to be gradient data and the 
                  deformation gradient is used to express the gradient in heart coordinates.
     
      {0.BOLD}data-from-H{0.END} : This is used for gradient data. {0.UNDERLINE}coords is a set of points in{0.END} 
                    {0.UNDERLINE}heart coordinates{0.END} on which data are specified. The data are transferred to 
                    the Cartesian reference frame using the deformation gradient, F.
     
      {0.BOLD}vec-to-H{0.END} :  {0.UNDERLINE}coords is a set of heart coordinate points{0.END} onto which vector data is to be 
                  transferred. data is specified with the data option and has a value for each
                  point in the source model. If F is specified, vectors are output
                  referenced to the local heart coordinate basis, otherwise they are
                  simply interpolated.
      
      {0.BOLD}vec-from-H{0.END} : {0.UNDERLINE}coords is a set of points in heart coordinates{0.END} on which data are specified. 
                   The data are transferred to the cartesian reference frame. If F is specified,
                   use the deformation gradient.
     
  {0.BOLD}F{0.END}: File of deformation gradients going to heart coordinates from Cartesian at each node. Each line is composed of
                    the entries of the nodal F (dz/dx dz/dy dz/dz drho/dx drho/dy drho/dz dphi/dx dphi/dy dphi/dz)
 
  Thus, scalar data is directly mapped between models, but vector and gradient data must be {0.BOLD}pulled to the heart 
  coordinate system{0.END} from model 1 and then {0.BOLD}pushed to Cartesian coordinates{0.END} of model 2
 
  Note that data is pulled to UVC and pushed to Cartesian coordinates
 
  {0.BOLD}Points files{0.END} (extension {0.ITALIC}.pts{0.END}) expect an intial line with the number of points followed by
  lines with the coordinates for each point.
  
  Assume, we have 2 models and the files A.pts, A_hc.pts, F_A.vec, B.pts, B_hc.pts, F_B.vec
 
  Ex 1) convert cartesian coordinates found in P.pts in model A space to heart coordinates:
         closest_hc.py pts-to-H --Cartesian=A A_hc  P
 
  Ex 2) transfer scalar data, S.dat, on nodes of model A to nodes of model B 
         closest_hc.py data-to-H --data=S.dat A_hc B_hc
 
  Ex 3) transfer gradient, G.vec, defined on nodes of model A to model B nodes
         closest_hc.py data-to-H   --data=G.vec           --F=F_A.vec --transformed=tmp_A_to_hc A_hc B_hc
         closest_hc.py data-from-H --data=tmp_A_to_hc.vec --F=F_B.vec --transformed=gradA       B_hc B_hc
'''.format(color)

import argparse
import math
import cmath
import numpy as np
import scipy.spatial
import collections
import numpy.matlib
import warnings
import sys
from multiprocessing import Process
import multiprocessing.sharedctypes as mpc
import ctypes

num_proc = 1
verbose  = False
idw_pow  = 2

class MyError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

#read pts/vertex file
def read_pts( modname, n=3, vtx=False ):
    if vtx :
        ptf = open( modname+'.vtx' )
    else :
        ptf = open( modname+'.pts' )
    if verbose : print("Started reading points file:"+modname)
    num = int(ptf.readline().split()[0])
    if vtx : ptf.readline()
    pt = list()
    for i in range(num) :
        pt.append( [float(i) for i in ptf.readline().split()[0:n]] )
    if verbose : print("Finished reading points file:"+modname)
    return pt


# read in a data file
#  fname=file
#  n=final data size
#  order=if sq_mat, construct matrix using these portions of each line
#  
def read_data( fname, n=1, order=None ) :
    inf = open( fname, 'r' ) 
    if verbose : print("Started reading "+fname)
    data = list()
    if n==1 :
        for l in inf :
            data.append( float(l.split()[0]) )
    elif not order:
        for l in inf :
            data.append( [ float(a) for a in l.split()[0:n] ] )
    else :
        for l in inf :
            entries = [ float(a) for a in l.split() ]
            m = list()
            for i in range(n) :
                m.append( entries[order[i]*n:(order[i]+1)*n] )
            data.append( np.matrix(m) )
    if verbose : print("Finished reading "+fname)
    return data


#listing can be a list or a list of lists
def output_list( listing, filename, ext, out_num=False ) :

    try :
        len( listing[0] )
    except TypeError :
        isListofLists = False
    else : 
        isListofLists = True
  
    if filename == '-' : 
        outf = sys.stdout
    else:
        outf = open( filename+ext, 'w' )

    if out_num : outf.write( str(len(listing))+'\n' )

    for i in listing :
        if isListofLists :
            for j in i:
                outf.write(str(j)+' ')
            outf.write('\n')
        else :
            outf.write( str(i)+'\n')
        
# divide up some objects between processors more or less equally
def divvy( num, sz ) :
    rng = list()
    cnt = int(0)
    inc = int((num+sz-1)/sz)
    while cnt+inc<num :
        rng.append( (cnt, cnt+inc) )
        cnt += inc
    rng.append( (cnt, num) )
    return rng


# limits for crossing the branch cut
def branch_limits( branch, tol ) :
    brsz = branch[1] - branch[0]
    return ( branch[0]+brsz*tol, branch[1]-brsz*tol, branch[0], brsz )


# add point to a ventricular list
# idx : node index
# vent : ventricular list
# mapping : map for ventricular list
# p : point to add
# branch : branch cut info
def add_to_ventricle( idx, vent, mapping, p_orig, scale, branch ) :
    pnew = map(lambda x,y:x*y, p_orig, scale )
    vent.append( pnew )
    mapping.append(idx)
    if pnew[2]< branch[0] or pnew[2] >branch[1]:
        bp = list(pnew)
        bp[2] += branch[3]*(1. if pnew[2]<branch[0] else -1.)  
        vent.append(bp)
        mapping.append(idx)


# split mesh into Left and right ventricles
# Use the LV-RV corordinate to determine the ventricle
# Also, if a node is near the branch cut, duplicate it with a rotational coordinate 
#   just on the other side of the cut
# RLidx = coordinate to determine for RV and LV, RLval = maximum value for RV point
def split_mesh( bivent, branch, scale, RLidx, RLval, nsearch=4, eps=0.00 ) :
    mapl = list()
    mapr = list()
    RV   = list()
    LV   = list()
    venL = RLval-eps
    venR = RLval+eps

    for i,p4  in enumerate(bivent) :
        p = p4[0:3]
        if p4[RLidx] <= venR :  
            add_to_ventricle( i, RV, mapr, p, scale, branch )
        if p4[RLidx] >= venR :  
            add_to_ventricle( i, LV, mapl, p, scale, branch )

    if verbose : print("Finished splitting mesh")
    return (RV,mapr), (LV,mapl)


def search_cartesian( haystack, targets, n ) :
    if verbose : print("Starting Cartesian cKDtree")
    array = np.array( haystack )
    kdt = scipy.spatial.cKDTree(array)
    if verbose : print("Finished Cartesian cKDtree")
    return kdt.query(targets,n)


def search_hcoords( lv, rv, targets, n, coord, limit, scale, nsearch=3  ) :
    if verbose : print("Building cKDTree")
    if lv[0] :
        if verbose : print("left tree has "+str(len(lv[0]))+" entries")
        larray = np.array( lv[0] )
        lkdt = scipy.spatial.cKDTree(larray,compact_nodes=False,balanced_tree=False)
    if verbose : print("Finished left cKDTree")
    if rv[0] :
        if verbose : print("right tree has "+str(len(rv[0]))+" entries")
        rarray = np.array( rv[0] )
        rkdt = scipy.spatial.cKDTree(rarray,compact_nodes=False,balanced_tree=False)
    if verbose : print("Finished right cKDTree")
    foundDst = list()
    foundIdx = list()

    for t in targets :
        p = map( lambda x,y: x*y, t[0:nsearch], scale )
        hc = [ p ]
        if t[coord]>=limit :
            tree    = lkdt
            mapping = lv[1]
        else :
            tree    = rkdt
            mapping = rv[1]
        (d,idx) = tree.query(hc,n)
        foundIdx.append( [ mapping[x] for x in idx[0]] if n>1 else mapping[idx[0]] )
        foundDst.append( d[0] )

    if verbose : print("Finished search")
    return (foundDst,foundIdx)


def scale_pts( pts, scale ) :
    for p in pts :
        p[:] = map( lambda x,y: x*y, p, scale )
    return pts

# average angles over a set of points which may be on both sides of the branch cut
#
# heart  - all points in model
# idx    - indices of points
# weight - weights of indicies
# p      - readjust entry 2 in heart coordinates
# branch - branch cut
def average_angles( heart, idx, weight, p, branch ) :
    if len(heart[0]) == 3 : return

    under = False
    over  = False
    for i in idx :
        if heart[i][2] < branch[0] : under = True
        if heart[i][2] > branch[1] : over  = True
    if under and over :
        wavg = complex(0,0)
        for w,i in zip(weight,idx) :
            wavg += w*cmath.exp(1j*heart[i][2])
        old= str(p[2])
        p[2] = math.atan2( wavg.imag, wavg.real )
    else :
        wavg = 0
        for w,i in zip(weight,idx) :
            wavg += w*heart[i][2]
        p[2] = wavg


def gauss( x, var ) :
    return math.exp( -x*x/2./var )


#determine weights based on distance
def weights(distance,var):
    if distance[0] == 0. :
        return [ 1 ]
    global idw_pow
    if var :
        rd = np.array([ gauss(x,var) for x in distance ])
    else :
        rd = np.array([ 1./(x**idw_pow) for x in distance ])
    tw = sum(rd)
    return rd/tw


#weighted average
def average( A, idx, w ) :
    avg = np.array(A[idx[0]])*w[0]
    for i in range(1,len(w)) :
        avg = avg + np.array(A[idx[i]])*w[i]
    return avg


#get the covariant or contravarient basis from the deformation gradient
def dual_base( F, pull ) :
    warnings.filterwarnings("error")
    if not pull :
        f = F
    else :
        try :
            f = np.linalg.inv(F.transpose())
        except :
            print('Singular matrix')
            f = np.identity(3)

    return f


#construct an orthonormal basis from a deformation gradient
def orthonormal_basis( F ) :
    warnings.filterwarnings("error")
    e0 = np.squeeze(np.asarray(F[0,:]))
    try :
        e0 = e0/np.linalg.norm(e0)
    except :
        print('e0 = '+str(e0))
    e1 = np.squeeze(np.asarray(F[1,:]))
    e1 = e1 - np.dot(e1,e0)*e0
    try :
        e1 = e1/np.linalg.norm(e1)
    except :
        print('e1 = '+str(e1))
    e2 = np.cross( e0, e1 )

    return [e0,e1,e2]
    


#project a vector onto a basis or reconstruct a vector from the basis
def project_ortho( F, data, pull ) :
    base = orthonormal_basis( F )
    if pull :
        return  [ np.dot(np.array(data), b) for b in base ]
    else :
         v = np.array([0.,0.,0.])
         for j in range(len(data)) :
            v = v + base[j]*data[j]
         return v

#project a vector onto a basis or reconstruct a vector from the basis
def project_dual( F, data, pull ) :
    base = np.array(dual_base( F, pull )).transpose()
    if pull :
        return base.transpose()*np.matrix(data).T
    else :
        return base*np.matrix(data).T


def output_orthonormalbases( F, out ) :
    outf = open( out+'.vec', 'w' )
    for f in F:
        fv = orthonormal_basis(f)
        for e in fv:
            for x in e:
                outf.write( str(x)+' ' )
        outf.write('\n')
    outf.close()


project=project_dual

# project vector (directional) data 
#
# ipt  - buffer in which to place results
# base - offset into buffer
# data - vectors on source model
# Fs   - deformation gradients on source model
# indx - closest source nodes to target points
# dist - distance of closest nodes from target points
# n    - number of closest nodes
# pull - push or pull operation
# var  - type of interpolation
#
def interpolate_vector_F( ipt, base, data, Fs, indx, dist, n, pull, var=None ):
    if n==1 :
        for i in range(len(indx)) :
            ipt[base+i] = (ctypes.c_float *3)(*project( Fs[indx[i]], data[indx[i]], pull))
    else :
        for i in range(len(indx)) :
            rd      = weights( dist[i], var )
            avgF    = average(Fs, indx[i], rd )
            avgdata = average( data, indx[i], rd )
            ipt[base+i] = (ctypes.c_float *3)(*project(avgF, avgdata, pull))


# project vector (directional) data 
#
# ipt  - buffer in which to place results
# base - offset into buffer
# data - vectors on source model
# indx - closest source nodes to target points
# dist - distance of closest nodes from target points
# n    - number of closest nodes
# pull - push or pull operation
# var  - type of interpolation
#
def interpolate_vector_noF( ipt, base, data, indx, dist, n, pull, var=None ):
    if n==1 :
        for i in range(len(indx)) :
            ipt[base+i] = (ctypes.c_float*3)(*data[indx[i]])
    else :
        for i in range(len(indx)) :
            rd      = weights( dist[i], var )
            avgdata = average( data, indx[i], rd )
            ipt[base+i] = (ctypes.c_float *3)(*avgdata)


# mp wrapper to project data 
#
# data - vectors on source model
# Fs   - deformation gradients on source model
# indx - closest source nodes to target points
# dist - distance of closest nodes from target points
# n    - number of closest nodes
# pull - push or pull operation
# var  - type of interpolation
#
def interpolate_vector( data, Fs, indx, dist, n, pull, var=0 ):
    ipt = mpc.Array( ctypes.c_float*3, len(idx), lock=False ) 
    if verbose : print("using "+str(num_proc)+" processors")
    rng = divvy( len(idx), num_proc )
    proc = list()
    for i in range( num_proc ) :
        if Fs :
            proc.append(Process(target=interpolate_vector_F,
                args=(ipt, rng[i][0], data, Fs, indx[rng[i][0]:rng[i][1]], 
                             dist[rng[i][0]:rng[i][1]], n, pull, var ) ))
        else:
            proc.append(Process(target=interpolate_vector_noF,
                args=(ipt, rng[i][0], data, indx[rng[i][0]:rng[i][1]], 
                             dist[rng[i][0]:rng[i][1]], n, pull, var ) ))
    for p in proc: p.start()
    for p in proc : p.join()
    return ipt


# interpolate points, cartesian or HC
#
# vec    - values for each point
# indx   - list of lists of indices of closest points
# dist   - list of lists of distances to target
# n      - max closest number of neighbors
# branch - branch cut info
# var    - type of interpolation
#
def interpolate_point( vec, indx, dist, n, branch, var=None ):
    if n==1 :
        ipt = [ vec[i] for i in indx ]
    else :
        ipt = list()
        for i in range(len(indx)) :
            if dist[i][0] == 0 :
                ipt.append( vec[indx[i][0]] )
            else :
                rd = weights( dist[i], var )
                cp = [0]*len(vec[0])
                for j in range(len(indx[i])) :
                    cp = map( lambda x,y: x+rd[j]*y, cp, vec[indx[i][j]] )
                average_angles( vec, indx[i], rd, cp, branch ) 
                ipt.append( cp )
    return ipt


def push_pull( d, F, pull=False ) :
    a = np.matrix(d).T
    if not pull :
        res = F.transpose()*a
    else :
        try :
            res = np.linalg.inv(F.transpose())*a
        except :
            res = np.matrix([0,0,0])
            print("Could not find inverse")
    return res.A1


# project gradient data 
#
# ipt  - buffer in which to place results
# base - offset into buffer
# data - vectors on source model
# Fs   - deformation gradients on source model
# indx - closest source nodes to target points
# dist - distance of closest nodes from target points
# n    - number of closest nodes
# pull - push or pull operation
# var  - type of interpolation
#
def interpolate_gradient_f( ipt, base, data, Fs, indx, dist, n, pull, var=None ):
    if n==1 :
        for i in range(len(indx)) :
            ipt[base+i] =  (ctypes.c_float *3)(*push_pull(data[i],Fs[i],pull)) 
    else :
        for i in range(len(indx)) :
            if dist[i][0] == 0 :
                ipt[base+i] = (ctypes.c_float *3)(*push_pull(data[indx[i][0]],Fs[indx[i][0]],pull))
            else :
                rd = weights( dist[i], var )
                datum = [0]*3
                for j in range(len(indx[i])) :
                    grad = push_pull( data[indx[i][j]], Fs[indx[i][j]], pull )
                    datum[:] = [ datum[k]+grad[k]*rd[k] for k in range(3) ]
                ipt[base+i]  = (ctypes.c_float *3)(*datum)


# mp wrapper to project gradient data 
#
# data - vectors on source model
# Fs   - deformation gradients on source model
# indx - closest source nodes to target points
# dist - distance of closest nodes from target points
# n    - number of closest nodes
# pull - push or pull operation
# var  - type of interpolation
#
def interpolate_gradient( data, Fs, indx, dist, n, pull, var=0 ):
    ipt = mpc.Array( ctypes.c_float*3, len(idx), lock=False ) 
    if verbose : print("using "+str(num_proc)+" processors")
    rng = divvy( len(idx), num_proc )
    proc = list()
    for i in range( num_proc ) :
        proc.append(Process(target=interpolate_gradient_f,
            args=(ipt, rng[i][0], data, Fs, indx[rng[i][0]:rng[i][1]], 
                             dist[rng[i][0]:rng[i][1]], n, pull, var ) ))
    for p in proc : p.start()
    for p in proc : p.join()
    return ipt


# angle - true if angle data : look out for branch cut
def interpolate_scalar( data, indx, dist, n, angle, var=None ):
    if n==1 :
        idata = [ data[i] for i in indx ]
    else :
        idata = list()
        for i in range(len(indx)) :
            if dist[i][0] == 0 :
                idata.append( data[indx[i][0]] )
            else :
                rd = weights( dist[i], var )
                datum = complex(0,0) if angle else 0
                for j in range(len(indx[i])) :
                    if angle :
                        datum += rd[j]*cmath.exp(1j*data[indx[i][j]])
                    else :
                        datum += rd[j]*data[indx[i][j]]
                if angle :
                    (rad,datum) = cmath.polar(datum)
                idata.append( datum )
    return idata


# ensure each list of closest points only contains vertices from one side of the heart
# We use the side of the heart that the closest point is in
# for points we find on the other side, we make their distance artificially extremely large
# so that they are ignored.
#
# hpts - UVC corrds for each point
# d, idx lists of indices and distances
# 
# use the side of the closest point
#
def make_one_sided( hpts, idx, d, sindx, sval ) :
    try:
        closest = idx[0][0]
    except:
        return

    for i in range(len(idx)) :
        side = hpts[idx[i][0]][sindx]>sval
        for j in range(len(idx[i])-1,0,-1) :
	        pside = hpts[idx[i][j]][sindx]>sval
	        if pside != side : d[i][j] = 1.e36
  

def check_inputs( opts ) :

    if opts.operation[0:4] == 'pts-' :
        if opts.Cartesian == None :
            print('Need to specify a Cartesian file for pt operations')
            sys.exit(1)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description=description,formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('operation', 
                        choices=['pts-to-H','pts-from-H','data-to-H', 'data-from-H','vec-to-H', 'vec-from-H', 'check-orthonorm'], 
                        help='conversion to apply' )
    parser.add_argument('heart_coords', help='heart coordinates for each node (z,rho,phi,v) with extension .pts' )
    parser.add_argument('coords', help='file of known coordinates (XYZ or UVC)' )
    parser.add_argument('--branch-tol', type=float, default=0.05, help='tolerance for branch cut [%(default)s]' )
    parser.add_argument('--branch-cut',  default=[-math.pi,math.pi], nargs=2, type=float, help='branch cut %(default)s' )
    parser.add_argument('--transformed', default='transformed',help='output file of transformed coordinates (- for stdout) [%(default)s]' )
    parser.add_argument('--Hscale', default=[1.,1.],nargs=2,type=float,help='heart coordinate scale factors  %(default)s' )
    parser.add_argument('--numfind', default=1,type=int,help='average over this many nearest nodes [%(default)s]' )
    parser.add_argument('--search-info',default=None,help='save search results in this file' )
    parser.add_argument('--data',default=None,help='data to transfer on to points' )
    parser.add_argument('--angle-data',action='store_true',help='true if data in range +/- pi' )
    parser.add_argument('--gauss-sd',type=float,default=None,help='use gauss interpolation with this std dev')
    parser.add_argument('--IDW-power',type=float,default=2,help='power for Shepard interpolation [%(default)%]')
    parser.add_argument('--F',default=None,help='deformation gradient: Grad z, Grad rho, Grad phi')
    parser.add_argument('--F-fields',default=[0,1,2],nargs=3,type=int,help='coords to use in F  %(default)s')
    parser.add_argument('--Cartesian',default=None,help='Cartesian coordinates for points, needed for pts-to-H')
    parser.add_argument('--RL',default=[3.,0.],type=float,nargs=2,help='coordinate defining LV-RV and max/min value for RV %(default)s')
    parser.add_argument('--RVisMax',action='store_true',help='set if RV is highest value of RL coord')
    parser.add_argument('--num-proc',default=1,type=int,help='number of processors to run on  [%(default)s]')
    parser.add_argument('--verbose',action='store_true',help='verbose output')

    opts       = parser.parse_args()
    opts.RL[0] = int(opts.RL[0])
    num_proc   = opts.num_proc
    check_inputs( opts )

    verbose = opts.verbose
    opts.Hscale.append( 1. )
    idw_pow = opts.IDW_power

    if opts.operation=='check-orthonorm' :
        output_orthonormalbases( read_data(opts.F,3,opts.F_fields), 'ortho' )
        sys.exit(0)

    hpts = read_pts( opts.heart_coords, 4 )
    branch_lims = branch_limits( opts.branch_cut, opts.branch_tol )

    if opts.operation == 'pts-to-H':
        ntarg   = 3
    else :
        ntarg   = 4
        (rv,lv) = split_mesh( hpts, branch_lims, opts.Hscale, *opts.RL )

    target_pts = read_pts( opts.coords, ntarg )

    if opts.operation in ['pts-to-H'] :    # convert given Cartesian coordinates to H coordinates
        (d, idx) = search_cartesian( read_pts(opts.Cartesian), target_pts, opts.numfind )

    elif opts.operation in ['pts-from-H','data-to-H','data-from-H','vec-from-H','vec-to-H'] :
        #scale_pts( target_pts, opts.Hscale )       # convert given H coordinates to Cartesian
        (d, idx) = search_hcoords( lv, rv, target_pts, opts.numfind, int(opts.RL[0]), opts.RL[1], opts.Hscale )
     
    var = opts.gauss_sd*opts.gauss_sd*9 if opts.gauss_sd is not None else None

    pull = 'to-H' in opts.operation

    if opts.operation in [ 'data-to-H', 'data-from-H' ] :
        if opts.F :
            transferred = interpolate_gradient( read_data(opts.data,3),
                    read_data(opts.F,3,opts.F_fields), 
                    idx, d, opts.numfind, pull, var)
        else:
            transferred = interpolate_scalar( read_data(opts.data), idx, d, opts.numfind,
                    opts.angle_data, var )
        output_list( transferred, opts.transformed, '.vec' if opts.F else '.dat' )

    elif opts.operation in ['vec-to-H','vec-from-H'] :
        if opts.F :
            transferred = interpolate_vector( read_data(opts.data,3), 
                             read_data(opts.F,3,opts.F_fields), idx, d, opts.numfind, pull, var)
        else :
            transferred = interpolate_vector( read_data(opts.data,3), None, idx, d, 
                                                        opts.numfind, pull, var)
        output_list( transferred, opts.transformed, '.vec' )
    
    else :
        pts = hpts if pull else read_pts(opts.Cartesian)
        if pull : make_one_sided( pts, idx, d, int(opts.RL[0]), opts.RL[1] )
        transferred = interpolate_point( pts, idx, d, opts.numfind, branch_lims, var )
        output_list( transferred, opts.transformed, '.pts', True )

    if opts.search_info :
        output_list( d,   opts.search_info+'_dist', '.dat' )
        output_list( idx, opts.search_info+'_indx', '.dat' )
    

#!/usr/bin/env python
"""
.. _run.meshtool:

Meshtool basic example
======================

In the example we will go through the commands in a basic meshtool workflow.
It consists of :

* extracting a submesh
* querying some mesh information
* computing surfaces

We start by generating the initial mesh

.. code-block:: bash

    $ mesher \\
    -size[0] 0.05 \\
    -size[1] 0.6 \\
    -size[2] 0.3 \\
    -bath[0] -0.02 \\
    -bath[1] -0.02 \\
    -bath[2] -0.02 \\
    -resolution[0] 100.0 \\
    -resolution[1] 100.0 \\
    -resolution[2] 100.0 \\
    -mesh block

Now we check the tags of the mesh

.. code-block:: bash

    $ meshtool query tags -msh=block
    Reading mesh: block.*
    Reading elements in text CARP format:                       [====================]
    Reading points in text CARP format:                         [====================]
    Reading fibers (1 direction) in text CARP format:           [====================]
    Done in 0.15757 sec
    All tags:
    0 1
    Extracting myocardium ..
    Done in 0.077463 sec
    Myocardium tags:
    1

We then extract the myocard submesh

.. code-block:: bash

    $ meshtool extract mesh -msh=block -submsh=block_i -tags=1


Let's query the bounding box of the myocard

.. code-block:: bash

    $ meshtool query bbox -msh=block_i
    Reading points of mesh: block_i.*
    Reading points in text CARP format:                         [====================]
    Done in 0.01595 sec
    Computing bounding box ..
    Done in 0.000406 sec
    Bbox:
    x: (      -250.00 ,       250.00 ), delta 500.00
    y: (     -3000.00 ,      3000.00 ), delta 6000.00
    z: (     -1500.00 ,      1500.00 ), delta 3000.00

We also want to now if the spatial resolution is as requested

.. code-block:: bash

    $ meshtool query edges -msh=block_i
    Reading mesh: block_i.*
    Reading elements in text CARP format:                       [====================]
    Reading points in text CARP format:                         [====================]
    Reading fibers (1 direction) in text CARP format:           [====================]
    Done in 0.092507 sec
    Computing surface 1/1
    Done in 0.086649 sec
    Writing surface block_i.surf
    Writing vertices block_i.surf.vtx
    Writing neubc block_i.neubc
    Done in 0.009526 sec
    [aneic@AVNpc cme-tutorial-wspace]$ meshtool query edges -msh=block_i
    Reading mesh: block_i.*
    Reading elements in text CARP format:                       [====================]
    Reading points in text CARP format:                         [====================]
    Done in 0.138968 sec
    Setting up n2e / n2n graphs ..
    Computing connectivity graphs:                              [====================]
    Done in 0.057902 sec

    Number of connections

    Average: 11.7254, (min: 4, max: 19)

     == Histogramm ==

             --------------------------------------------------------------------------------------------
       +0.30 |                                                                                          |
             |                 **                                                                     **|
       +0.26 |                 **                                                                     * |
             |                ***                                                                     * |
             |                * **                                                                   ** |
             |                *  *                                                                   *  |
       +0.17 |        **     **  *                                     **                            *  |
             |        **     *   **                                    **                           **  |
             |       ****    *    *                                   ****                          *   |
             |       *  *    *    *                                   *  *                          *   |
       +0.09 |      *   **  *     *                                  **  **                        **   |
             |     **    *  *      *                                 *    *                        *    |
             |     *     ** *      *                                *     **                       *    |
             |    **      **       *                                *      *                      **    |
       +0.00 |*****       **        ********************************       ************************     |
             --------------------------------------------------------------------------------------------
              4.0             6.7             9.4            12.1            14.8            17.5

    Edge lengths

    Average: 119.912, (min: 100, max: 141.421)

     == Histogramm ==

             --------------------------------------------------------------------------------------------
       +0.52 |*                                                                                         |
             |*                                                                                         |
       +0.45 |*                                                                                       **|
             |*                                                                                       * |
             | *                                                                                      * |
             | *                                                                                     *  |
       +0.30 | *                                                                                     *  |
             |  *                                                                                    *  |
             |  *                                                                                   *   |
             |  *                                                                                   *   |
       +0.15 |  **                                                                                  *   |
             |   *                                                                                 *    |
             |   *                                                                                 *    |
             |   **                                                                               **    |
       +0.00 |    *********************************************************************************     |
             --------------------------------------------------------------------------------------------
            100.0           107.4           114.9           122.3           129.8           137.2


Finally, we extract the full surface

.. code-block:: bash

    $ meshtool extract surface -msh=block_i -surf=block_i

and the surface of the upper wedge side

.. code-block:: bash

    $ meshtool extract surface -msh=block_i -surf=block_i.top -coord=0,3010,0 -edge=10



"""

EXAMPLE_DESCRIPTIVE_NAME = 'A meshtool basic example'
EXAMPLE_AUTHOR = 'Aurel Neic <aurel.neic@medunigraz.at>'

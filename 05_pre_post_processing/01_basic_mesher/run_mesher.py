#!/usr/bin/env python
"""

.. _run.mesher:

Basic Mesher Example
====================

The following example creates a myocardial slab of dimensions 0.05 x 0.6 x 0.3 cm
immersed in a bath of 0.2 cm on each side:

.. code-block:: bash

    $ mesher \\
    -size[0] 0.05 \\
    -size[1] 0.6 \\
    -size[2] 0.3 \\
    -bath[0] -0.2 \\
    -bath[1] -0.2 \\
    -bath[2] -0.2 \\
    -resolution[0] 100.0 \\
    -resolution[1] 100.0 \\
    -resolution[2] 100.0 \\
    -mesh block

By default, the geometry's center is located at (0,0,0). As specified, the bounding box
of the
intracellular domain is

.. code-block:: bash

    Bbox:
    x: (      -250.00 ,       250.00 ), delta 500.00
    y: (     -3000.00 ,      3000.00 ), delta 6000.00
    z: (     -1500.00 ,      1500.00 ), delta 3000.00

while the bounding box of the overall mesh is

.. code-block:: bash

    Bbox:
    x: (      -450.00 ,       450.00 ), delta 900.00
    y: (     -3200.00 ,      3200.00 ), delta 6400.00
    z: (     -1700.00 ,      1700.00 ), delta 3400.00

Rule-based fibers can be added with the parameters in the ``fibers`` parameter structure.
In the previous example, fiber rotation can be added with:

.. code-block:: bash

    $ mesher \\
    -size[0] 0.05 \\
    -size[1] 0.6 \\
    -size[2] 0.3 \\
    -bath[0] -0.02 \\
    -bath[1] -0.02 \\
    -bath[2] -0.02 \\
    -resolution[0] 100.0 \\
    -resolution[1] 100.0 \\
    -resolution[2] 100.0 \\
    -mesh block \\
    -fibers.rotEndo 60 \\
    -fibers.rotEpi -60 \\
    -fibers.sheetEndo 90 \\
    -fibers.sheetEpi  90

The resulting geometry is:

.. figure:: /images/Mesher_block.png
   :scale: 65 %

   A slab mesh produced by mesher.

"""

EXAMPLE_DESCRIPTIVE_NAME = 'A mesher basic example'
EXAMPLE_AUTHOR = 'Aurel Neic <aurel.neic@medunigraz.at>'

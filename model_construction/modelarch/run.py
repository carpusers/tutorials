#!/usr/bin/env python
# coding=utf-8

r"""
.. _modelarch:

*modelarchlap* is a python script for automatical incorporating model architecture required for cardiac electromechanical
simulation into a ventricular mesh. Namely, modelarchlap is capable of generating  fiber orientations, 9-region heterogeneity,
and universal ventricular coordinates (UVCs). The executable uses a series of laplacian solutions with geodasic
normalization to automatically based on .... Bayer 2018, et al, etc..
Most boundary conditions required for the laplacian solutions are automatically extracted using :ref:`meshtool <meshtool>`.

This executable is available in the bin directory of carputils. The following dependencies must be locatable in your
searchable path and are checked during execution:

* :ref:`meshtool <meshtool>`
* ekbatch
* GlElemcenter
* python2

The number of cores (n) can be specified using the ``--np`` flag. The parent directory for output (jobID) can be
specified using the ``--ID`` flag. The user to change the overwrite settings of the specific job directory if it already
exists using the ``--overwrite-behavour`` flag. Without other inputs, modelarchlap is therefore called as:

.. code-block:: bash

  ./modelarchlap [--np n] 
                 [--ID jobID ] 
                 [--overwrite-behaviour append,prompt,overwrite]


Mesh Input 
==========

Input of a carp mesh (<path_to>/meshname) into modelarchlap is specified by the ``--basemesh`` flag. The type of mesh,
namely left-ventricular (lv), bi-ventricular (biv), or four chamber heart (h4c), must also be specificed by
the ``--mode`` flag. Depending on the mode, tag definitions for the mesh must both be present and correctly
specified within a bash tagging file (tags.sh) specified using the ``--tags`` flag. Additional surfaces may
also be required and exist in the same directory as the bashmesh with the naming basemesh.*.surf.vtx.

.. code-block:: bash

  ./modelarchlap [--np n] 
                 [--ID jobID ] 
                 [--overwrite-behaviour append,prompt,overwrite]
                 [--basemesh <path_to>/meshname]
                 [--mode lv,biv,h4c]
                 [--tags tags.sh]
                 

Four Chamber Heart
------------------

Say the mesh inputted has tags 1,2,3,4,5,6 corresponding to the left atria, right atria, lv, rv, left blood pool,
and right blood pool, respectively, the tagging file must contain

.. code-block:: bash

    T_ATRIA=1,2
    T_LV=3
    T_RV=4
    T_LVBP=5
    T_RVBP=6

All surfaces are automatically extracted. No additional input is neccessary.

.. note::

  Mutlitple element tags can be assigned to a single tissue type using commas. 


Bi-ventricular heart
--------------------

.. code-block:: bash

    T_LV=3
    T_RV=4
    T_LVBP=5
    T_RVBP=6

The base surface must be exist in the same directory as the input mesh with the naming meshname.base.surf.vtx.
This surface defines basal surface of the ventricles, or the intersection of the ventricles with the atrium.


.. warning!:: 

  * The mesh must be fully connected and of sufficient quality for the code to run! Please check laplacian simulation
  output to ensure it didn't diverge.
  * Please note the atria or base surface must intersect with the blood pool in the lv to run.

Generating Architecture
=======================

modelarchlap can either be used to generate fibers, heterogeinety, and UVCs simulataneously, or each component can be
run seperately by specifying relevent flags.

9-region heterogeneity
----------------------

.. code-block:: bash

   ./modelarchlap [--hetero]
              [--rerun_hetero] #rerun the heterogeneity calculations
              [--tags_apba 100 200 300] #Base element tags for apico-basal heart regions
              [--tags_wall 25 50 75] #Element tags for wall elements
              [--epi_endo_cutoffs 25 30] #percentage cutoffs through ventricular wall for regions
              [--apex_base_cutoffs 45 75] #percentage cutoffs from apex to base for regions

Fiber Orientations in Biv Mesh
------------------------------

.. code-block:: bash

   ./modelarchlap [--fibers]
              [--rerun_fibers]
              [--fibre_rule -60 60 -65 25] #Fiber angle rules in order alpha_endo, alpha_epi, beta_endo, beta_epi.
              [--fibre_rule_alpha -60 60] #Fiber angle rules for only alpha_endo and alpha_epi. Defulat for beta becomes -65 and 25, respectively.


UVCs
----

.. code-block:: bash

   ./modelarchlap [--uvc]


Code Output and Organization
----------------------------

The resultant output structure depending on the command inputs becomes: 

.. code-block:: bash

    jobID
    ├── fibers
    │   └── fibres_-60.0_60.0_-65_25.lon
    ├── hetero
    │   ├── meshname.biv.FE_45.0_75.0.elem
    ├── model
    │   ├── base #Copied over mesh with extracted surfaces
    │   ├── biv  #Extracted biv submesh corresponding to uvc, fibers, and hetero
    │   ├── lv   #Extracted lv submesh
    │   └── rv   #Extracted rv submesh
    ├── sols
    │   ├── init
    │   └── lap_rv_phi
    └── UVC
        ├── COMBINED_COORDS_Z_RHO_PHI_V.dat
        ├── COMBINED_COORDS_Z_RHO_PHI_V.pts
        ├── COORDS_PHI.dat
        ├── COORDS_PHI.pts
        ├── COORDS_RHO.dat
        ├── COORDS_RHO.pts
        ├── COORDS_V.dat
        ├── COORDS_V.pts
        ├── COORDS_Z.dat
        └── COORDS_Z.pts

References
==========

.. [#bayer_ldrb]  Bayer JD, Blake RC, Plank G, Trayanova NA.
   **A Novel Rule-Based Algorithm for Assigning Myocardial Fiber Orientation to Computational Heart Models**,
   Ann Biomed Eng 40(10):2243-54, 2012. `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/22648575>`_

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Incorporation of model architecture using modelarchlap'
EXAMPLE_AUTHOR = 'Karli Gillette <karli.gillette@medunigraz.at>'

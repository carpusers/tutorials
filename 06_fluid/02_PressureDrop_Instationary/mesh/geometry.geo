lc      = .01250;
length  = 1;
radius  = .050;

//+
Point(1) = {0, 0, 0, lc};
//+
Point(2) = {0, radius, 0, lc};
//+
Point(3) = {0, 0, radius, lc};
//+
Point(4) = {0, -radius, 0, lc};
//+
Point(5) = {0, 0, -radius, lc};
//+
Circle(1) = {3, 1, 2};
//+
Circle(2) = {2, 1, 5};
//+
Circle(3) = {5, 1, 4};
//+
Circle(4) = {4, 1, 3};
//+
Extrude {length, 0, 0} {
  Line{2}; Line{1}; Line{4}; Line{3};
}
//+
Curve Loop(1) = {1, 2, 3, 4};
//+
Plane Surface(21) = {1};
//+
Curve Loop(2) = {9, 5, 17, 13};
//+
Plane Surface(22) = {2};
//+
Surface Loop(1) = {21, 12, 8, 20, 16, 22};
//+
Volume(1) = {1};
//+
Physical Volume(100) = {1};

#!/usr/bin/env python
# -*- coding: utf-8 -*-

r"""

.. _hagenpouseille:

.. |dp| replace:: :math:`\Delta p`
.. |n| replace:: :math:`\mu`
.. |Qn| replace:: :math:`Q_\text{numeric}`
.. |Qh| replace:: :math:`Q_\text{HP}`
.. |Qe| replace:: :math:`\lvert\frac{Q_\text{numeric}-Q_\text{HP}}{Q_\text{HP}}\rvert`
.. |Re| replace:: :math:`\mathrm{Re}`

This example demonstrates a simple application for fluid dynamics in a straight cylindrical pipe.

**Law of Hagen-Poiseuille**
***************************

The `law of Hagen-Poiseuille <https://en.wikipedia.org/wiki/Hagen–Poiseuille_equation>`_ is a physical law that gives the
pressure drop :math:`\Delta p` for an incompressible Newtonian fluid in the laminar regime flowing through a long cylindrical pipe of constant cross section. This geometric
setup is depicted in figure :numref:`fig-pouseille-setup`.

.. _fig-pouseille-setup:

.. figure:: /images/HagenPouseilleSetup.png
   :width: 2222px
   :scale: 25%
   :align: center

   Geometry for the Hagen-Poiseuille law.

The law states that

.. math::

   \Delta p = \frac{8\mu LQ}{\pi R^4}

where

* :math:`\Delta p` is the pressure drop (in Pascal)
* :math:`L` is the length of the pipe (in meter)
* :math:`\mu` is the dynamic viscosity of the fluid (in Pascal seconds)
* :math:`Q` is the volumetric flow rate (in cubic meter per seconds)
* :math:`R` is the pipe radius (in meter)

*Derivation of Hagen-Poiseuille's Law*
======================================
For the derivation we will use the **Navier-Stokes equations**

.. math::

    \rho \partial_t \vec u + \rho \nabla \vec u \vec u - \mu \Delta \vec u + \nabla p &= \vec f,\\
    \nabla \cdot \vec u &= 0.

First we assume that :math:`\vec f = \vec 0` and :math:`\rho` constant. To derive Hagen-Poiseuille's law we will rewrite the Navier-Stokes
equations in cylindrical coordinates

.. math::
    :label: continuity

     \rho \left(\frac{\partial u_r}{\partial t} + u_r \frac{\partial u_r}{\partial r} + \frac{u_{\phi}}{r}
     \frac{\partial u_r}{\partial \phi} + u_z \frac{\partial u_r}{\partial z} - \frac{u_{\phi}^2}{r}\right) &=
     -\frac{\partial p}{\partial r} + \mu \left(\frac{1}{r}\frac{\partial}{\partial r}\left(r \frac{\partial
     u_r}{\partial r}\right) + \frac{1}{r^2}\frac{\partial^2 u_r}{\partial \phi^2} + \frac{\partial^2 u_r}{\partial z^2} - \frac{u_r}{r^2} -
         \frac{2}{r^2}\frac{\partial u_\phi}{\partial \phi} \right),\\
     \rho \left(\frac{\partial u_{\phi}}{\partial t} + u_r \frac{\partial u_{\phi}}{\partial r} +
        \frac{u_{\phi}}{r} \frac{\partial u_{\phi}}{\partial \phi} + u_z \frac{\partial u_{\phi}}{\partial z} + \frac{u_r u_{\phi}}{r}\right) &= -\frac{1}{r}\frac{\partial p}{\partial \phi} + \mu \left(\frac{1}{r}\frac{\partial}{\partial r}\left(r \frac{\partial u_{\phi}}{\partial r}\right) +
        \frac{1}{r^2}\frac{\partial^2 u_{\phi}}{\partial \phi^2} + \frac{\partial^2 u_{\phi}}{\partial z^2} +
        \frac{2}{r^2}\frac{\partial u_r}{\partial \phi}-\frac{u_{\phi}}{r^2}\right),\\
     \rho \left(\frac{\partial u_z}{\partial t} + u_r \frac{\partial u_z}{\partial r} + \frac{u_{\phi}}{r} \frac{\partial u_z}{\partial \phi} +
        u_z \frac{\partial u_z}{\partial z}\right) &= -\frac{\partial p}{\partial z} + \mu \left(\frac{1}{r}\frac{\partial}{\partial r}\left(r \frac{\partial u_z}{\partial r}\right) +
        \frac{1}{r^2}\frac{\partial^2 u_z}{\partial \phi^2} + \frac{\partial^2 u_z}{\partial z^2}\right),\\
        \frac{1}{r}\frac{\partial}{\partial r}\left(r u_r\right) +
        \frac{1}{r}\frac{\partial \left(u_\phi\right)}{\partial \phi} + \frac{\partial \left(
        u_z\right)}{\partial z} &= 0.

Additionally the following is assumed

#. Steady flow, meaning :math:`\frac{\partial}{\partial t}(\cdot) = 0`
#. Radial and swirl components of the fluid velocity are zero meaning :math:`u_r = u_\phi = 0`
#. The flow is assumed to be axisymmetric, meaning :math:`\frac{\partial}{\partial \phi}(\cdot) = 0`
#. The flow is fully developed, meaning :math:`\frac{\partial}{\partial z} u_z = 0`

First with this assumptions the continuity equation (fourth line of :eq:`continuity`) is trivially fulfilled. Further it follows that

.. math::
    :label: simplified_NS

    \frac{\partial}{\partial r} p &= 0,\\
    \frac{1}{r}\frac{\partial}{\partial \phi} p &= 0,\\
    \mu\left(\frac{1}{r}\frac{\partial}{\partial r}\left(r \frac{\partial}{\partial r} u_z\right)\right) -
    \frac{\partial}{\partial z}p &= 0.

From this we can deduce that :math:`p = p(z)`. Plugging this into the last line of :eq:`simplified_NS` we can solve for
:math:`u_z` by twice integrating and get

.. math::

    u_z(r,z) = \frac{r^2}{4 \mu} \frac{dp}{dz} + c_1 \ln r + c_2

with some arbitrary integration constants :math:`c_1,c_2`. To get a closed representation we will use the following
observations

#. On the boundary of the cylinder (:math:`r=R`) we want :math:`u_z=0`
#. For :math:`r=0` we want :math:`u_z` to be finite

The first obervation yields :math:`c_2 = -\frac{R^2}{4\mu}` and the second yields that :math:`c_1=0`. Putting all
together we get a closed expression for :math:`u_z`

.. math::

    u_z(r,z) = -\frac{1}{4\mu}\left(R^2 - r^2\right) \frac{dp}{dz}.

As a last assumption, let $p$ be decreasing linearly from :math:`z=0` to :math:`z=L` yielding :math:`\frac{dp}{dz} =
\frac{\Delta p}{L}`. Now we can calculate the volumetric flux :math:`Q` through the cross section of the cylinder as

.. math::

    Q &= -2\pi \int_{0}^{R} u_z(r) r dr\\
      &= \frac{\Delta p \pi R^4}{8\mu L}

which gives the Law of Hagen-Poiseuille.

*Numeric Verification*
======================

For the experiments we choose a cylindrical pipe with :math:`R=0.05m,~L=0.5m`. We will vary the pressure drop
:math:`\Delta p` as well as the viscosity :math:`\mu`. For simplicity we assume that :math:`\rho=1.0`. We have three
different discretizations (coarse, medium, fine). The coarse discretization has a maximal edge length of
:math:`h_\text{coarse}=0.2R`, the medium has :math:`h_\text{medium}=0.5h_\text{coarse}`, and the fine has
:math:`h_\text{fine}=0.25h_\text{coarse}`.

.. _table-experiments:

+--------------+-----------+------------+------------+
|              | |dp| [Pa] | |n| [Pa s] | |Re|       |
+==============+===========+============+============+
| Experiment 1 | 1.0       | 0.01       | 0.625      |
+--------------+-----------+------------+------------+
| Experiment 2 | 5.0       | 0.01       | 3.125      |
+--------------+-----------+------------+------------+
| Experiment 3 | 10.0      | 0.01       | 6.25       |
+--------------+-----------+------------+------------+
| Experiment 4 | 15.0      | 0.01       | 9.38       |
+--------------+-----------+------------+------------+
| Experiment 5 | 1.0       | 0.001      | 62.5       |
+--------------+-----------+------------+------------+
| Experiment 6 | 5.0       | 0.001      | 312.5      |
+--------------+-----------+------------+------------+
| Experiment 7 | 10.0      | 0.001      | 625.0      |
+--------------+-----------+------------+------------+
| Experiment 8 | 15.0      | 0.001      | 937.5      |
+--------------+-----------+------------+------------+

The results for the coarse discretization are depicted in Table :numref:`table-results-coarse`, the results for the
medium discretization are depicted in Table :numref:`table-results-medium`, and the results for the fine discretization
are depcited in Table :numref:`table-results-fine`. One can clearly see the influence of the Reynolds number on the
accuracy of the numeric solution. For :math:`\mathrm{Re}\approx 1000` in a cylindrical pipe the assumptions for the law
of Hagen-Poiseuille are not longer valid.


*Usage*
*******

To run your own validation experiment just type in the command

.. code-block:: bash

    ./run.py --discretization TYPE --mu VALM --rho VALR --pressureDrop VALP --np NP

Here **TYPE** can be either *coarse*, *medium* or *fine*. This relates to the meshes used. The value **VALM** is the
value for the dynamic viscosity :math:`\mu` of the fluid in Pascal seconds. Value **VALR** is the value for the fluid
density :math:`\rho` in kilogram per cubic meters. **VALP** denotes the value for the pressure drop :math:`\Delta p`
given in Pascal. At the inflow a value of :math:`\frac{3 \Delta p}{2}` will be prescribed and at the outflow a value of
:math:`\frac{\Delta p}{2}`. Last, **NP** stands for the number of processors.

The above code uses the standard meshes provided with this run script. This cylindrical mesh has the default length
:math:`L=0.5[m]`, and default radius :math:`R=5[cm]`.  You can also generate your own mesh. This is achieved via the
flags:

.. code-block:: bash

    ./run.py --discretization TYPE --mu VALM --rho VALR --pressureDrop VALP --np NP --generate 1 --radius R --length L
    --radiusfactor HR

Here, **R** denotes the radius of your cylinder in centimeter, **L** denotes the length of your cylinder in meter and
**HR** gives the factor for calculating the average edgelength in the mesh. It should be between zero and one. The edgelength is calculated as
:math:`h_e=HR*0.01*R`. To use this functionalty you need to have **meshtool** in your search path as well as **gmsh**.
The software package **gmsh** can be downloaded from  `here <http://gmsh.info>`_. Depending on the chosen parameters the
generation of the meshes can take some time.


.. rubric:: Coarse

.. _table-results-coarse:

+--------------+------------+------------+------------+
|              |  |Qn|      |  |Qh|      | |Qe|       |
+==============+============+============+============+
| coarse (1)   | 4.8157e-04 | 4.9087e-04 | 0.0190     |
+--------------+------------+------------+------------+
| coarse (2)   | 2.4077e-03 | 2.4544e-03 | 0.0190     |
+--------------+------------+------------+------------+
| coarse (3)   | 4.8157e-03 | 4.9087e-03 | 0.0190     |
+--------------+------------+------------+------------+
| coarse (4)   | 7.2243e-03 | 7.3631e-03 | 0.0188     |
+--------------+------------+------------+------------+
| coarse (5)   | 4.5198e-03 | 4.9087e-03 | 0.0792     |
+--------------+------------+------------+------------+
| coarse (6)   | 2.0044e-02 | 2.4544e-02 | 0.1833     |
+--------------+------------+------------+------------+
| coarse (7)   | 3.6429e-02 | 4.9087e-02 | 0.2579     |
+--------------+------------+------------+------------+
| coarse (8)   | 5.0822e-02 | 7.3631e-02 | 0.3098     |
+--------------+------------+------------+------------+

.. rubric:: Medium

.. _table-results-medium:

+--------------+-----------+------------+------------+
|              |  |Qn|     |  |Qh|      | |Qe|       |
+==============+===========+============+============+
| medium (1)   | 4.8869e-04| 4.9087e-04 | 0.0044     |
+--------------+-----------+------------+------------+
| medium (2)   | 2.4435e-03| 2.4544e-03 | 0.0044     |
+--------------+-----------+------------+------------+
| medium (3)   | 4.8875e-03| 4.9087e-03 | 0.0043     |
+--------------+-----------+------------+------------+
| medium (4)   | 7.3321e-03| 7.3631e-03 | 0.0043     |
+--------------+-----------+------------+------------+
| medium (5)   | 4.8394e-03| 4.9087e-03 | 0.0141     |
+--------------+-----------+------------+------------+
| medium (6)   | 2.3317e-02| 2.4544e-02 | 0.05       |
+--------------+-----------+------------+------------+
| medium (7)   | 4.5841e-02| 4.9087e-02 | 0.0661     |
+--------------+-----------+------------+------------+
| medium (8)   | 6.7741e-02| 7.3631e-02 | 0.08       |
+--------------+-----------+------------+------------+

.. rubric:: Fine

.. _table-results-fine:

+--------------+-----------+------------+------------+
|              |  |Qn|     |  |Qh|      | |Qe|       |
+==============+===========+============+============+
| fine (1)     | 4.9044e-04| 4.9087e-04 | 0.0009     |
+--------------+-----------+------------+------------+
| fine (2)     | 2.4523e-03| 2.4544e-03 | 0.0009     |
+--------------+-----------+------------+------------+
| fine (3)     | 4.9046e-03| 4.9087e-03 | 0.0008     |
+--------------+-----------+------------+------------+
| fine (4)     | 7.3573e-03| 7.3631e-03 | 0.0008     |
+--------------+-----------+------------+------------+
| fine (5)     | 4.9081e-03| 4.9087e-03 | 0.0001     |
+--------------+-----------+------------+------------+
| fine (6)     | 2.3535e-02| 2.4544e-02 | 0.0411     |
+--------------+-----------+------------+------------+
| fine (7)     | 4.6091e-02| 4.9087e-02 | 0.0610     |
+--------------+-----------+------------+------------+
| fine (8)     | 6.9728e-02| 7.3631e-02 | 0.0530     |
+--------------+-----------+------------+------------+



"""
EXAMPLE_DESCRIPTIVE_NAME = 'Instationary Pressure Drop Simulation'
EXAMPLE_AUTHOR = ('Elias Karabelas <elias.karabelas@medunigraz.at>')

import os
import numpy as np
import sys
import shlex
import math
from datetime import date
from carputils import settings
from carputils import tools
from carputils.resources import petsc_block_options
from carputils.settings  import config
from carputils import format
from subprocess import call

EQUALS_LINE = '=' * min(format.terminal_width(120), 120)
DASHED_LINE = '-' * min(format.terminal_width(120), 120)

def write(str, fp=None):
    print(str)
    if fp is not None:
        fp.write('{}\n'.format(str))

def parser():
    parser = tools.standard_parser()

    geom   = parser.add_argument_group('geometry options')
    geom.add_argument('--generate',
                        default=0,
                        help='Generate geometry with specified parameters')
    geom.add_argument('--radius',
                        default=10,
                        help='The cylinder radius for the experiment in mm')
    geom.add_argument('--length',
                        default=200,
                        help='The cylinder length for the experiment in mm')
    geom.add_argument('--radiusfactor',
                        default=0.25,
                        help='The maximal edge length of the coarsest mesh in percentage of the radius (in [0,1])')
    geom.add_argument('--discretization',
                        default='coarse',
                        choices=['coarse','medium','fine'],
                        help='Pick refinenment level')


    fluid = parser.add_argument_group('fluid options')
    fluid.add_argument('--mu',
                        default=0.1,
                        help='Dynamic fluid viscosity in Pa * s')
    fluid.add_argument('--rho',
                        default=1.0,
                        help='Fluid density in kg / m^3')
    fluid.add_argument('--pressure-drop',
                        default=1.0,
                        help='The maximal pressure drop [mmHg]')
    fluid.add_argument('--pressure-offset',
                        default=760.0,
                        help='The pressure offset [mmHg]')
    fluid.add_argument('--implicit',
                       type=int,
                       default=0,
                       help='Use implicit time discretization')
    fluid.add_argument('--outflow-stabilization',
                       type=int,
                       default=1,
                       help='Apply outflow stabilization. 0 --> No Stabilization, 1 --> Normal velocity penalty, 2 --> Tangent velocity penalty')
    fluid.add_argument('--dt',
                       default=1.0,
                       help='Time step size in ms')
    fluid.add_argument('--with-obstacle',
                       type=int,
                       default=0,
                       help='Add obstacles')
    fluid.add_argument('--adapt-viscosity-in-obstacle',
                       type=int,
                       default=0,
                       help='Adapt the viscosities in the obstacles')
    fluid.add_argument('--no-slip-treatment',
                       default='strong',
                       choices=['strong', 'weak'],
                       help='How should the no-slip boundary conditons be enforced.')


    return parser

def jobID(args):
    """
    Generate name of top level output directory
    """
    today=date.today()
    if args.implicit == 0 :
       jobid = '{}_IMEX_{}_MU{}_RHO{}_DP{}_PGAUGE{}_{}_noslip'.format(today.isoformat(),
                args.discretization,
                args.mu,args.rho,args.pressure_drop, args.pressure_offset, args.no_slip_treatment)
    else :
       jobid = '{}_GA_{}_MU{}_RHO{}_DP{}_PGAUGE{}_{}_noslip'.format(today.isoformat(),args.discretization,
                args.mu,args.rho,args.pressure_drop,args.pressure_offset,args.no_slip_treatment)

    if args.with_obstacle == 1 :
       jobid += '_obstacle'

    jobid += '_np{}'.format(args.np)

    return jobid.replace(',','_')

def mesh_from_parser(args, meshdir, pressuredrop):
    info={}

    #generate and return base name
    meshname = os.path.join(meshdir, args.discretization, 'HagenPouseille')

    info['meshdir']     = os.path.dirname(meshname)
    info['meshname']    = meshname

    #dirichlet
    if args.no_slip_treatment == 'strong':
       dbc = ['-num_flow_dbc', 1]
       dbc += ['-flow_DBC[0].name', 'Wall',
               '-flow_DBC[0].vtx_file', '{}.wall.surf'.format(meshname),
               '-flow_DBC[0].surf_file', '{}.wall'.format(meshname),
               '-flow_DBC[0].bctype', 0]
    else :
       dbc = ['-num_flow_fsbc', 1]
       dbc += ['-flow_FSBC[0].name', 'Wall',
               '-flow_FSBC[0].surf_file', '{}.wall'.format(meshname),
               '-flow_FSBC[0].logQuantities', 1,
               '-flow_FSBC[0].logQuantitiesFileU', 'wall_flux',
               '-flow_FSBC[0].logQuantitiesFileP', 'wall_avgp'
              ]
    #neumann
    #figure out pressure values
    p0 = (1.5 * float(pressuredrop))
    p1 = (0.5 * float(pressuredrop))

    #make the periodic signals
    num_pts = 81
    time_eval = np.linspace(0.0, 1000.0, num=num_pts) # in ms
    ft0 = p0 * np.cos((2.0/1000.0)*np.math.pi*time_eval)
    ft1 = p1 * np.cos((2.0/1000.0)*np.math.pi*time_eval)
    working_dir = os.path.dirname(os.path.realpath(__file__))
    name_p0 = os.path.join(working_dir, 'inflow_pressure_signal')
    name_p1 = os.path.join(working_dir, 'outflow_pressure_signal')
    np.savetxt('{}.dat'.format(name_p0), np.column_stack((time_eval,ft0)), fmt='%1.6f', header='{}'.format(num_pts), comments='')
    np.savetxt('{}.dat'.format(name_p1), np.column_stack((time_eval,ft1)), fmt='%1.6f', header='{}'.format(num_pts), comments='')

    nbc = ['-num_flow_nbc', 2]
    nbc += ['-flow_NBC[0].name', 'Inflow',
            '-flow_NBC[0].surf_file', '{}.inflow'.format(meshname),
            '-flow_NBC[0].bctype', 2,
            '-flow_NBC[0].pressure_trace', '{}'.format(name_p0),
            '-flow_NBC[0].logQuantities', 1,
            '-flow_NBC[0].logQuantitiesFileU', 'inflow_flux',
            '-flow_NBC[0].logQuantitiesFileP', 'inflow_avgp',
            '-flow_NBC[0].outflow_stabilization' , args.outflow_stabilization]
    nbc += ['-flow_NBC[1].name', 'Outflow',
            '-flow_NBC[1].surf_file', '{}.outflow'.format(meshname),
            '-flow_NBC[1].bctype', 2,
            '-flow_NBC[1].pressure_trace', '{}'.format(name_p1),
            '-flow_NBC[1].logQuantities', 1,
            '-flow_NBC[1].logQuantitiesFileU', 'outflow_flux',
            '-flow_NBC[1].logQuantitiesFileP', 'outflow_avgp',
            '-flow_NBC[1].outflow_stabilization' , args.outflow_stabilization]
    #store info
    info['dbc'] = dbc
    info['nbc'] = nbc



    return info

def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None


def flux_hagen_pouseille(pressuredrop, radius, length, rho, mu):
    flux = (float(pressuredrop) * math.pi * float(radius) ** 4) / (8.0 * float(mu) * float(length))
    return flux

def reynolds_number(flux, radius, area, mu, rho):
    reynolds = 2. * float(flux) * float(radius) / ((float(mu) / float(rho)) * float(area))
    return reynolds

@tools.carpexample(parser, jobID, clean_pattern = r'^(\d{4}-)')
def run(args, job):

    #check if we need to generate a mesh
    defaultlength       = 200
    defaultradius       = 10
    defaultradiusfactor = 0.25

    length          = args.length
    radius          = args.radius
    radiusfactor    = args.radiusfactor

    meshdir         = None
    working_dir = os.path.dirname(os.path.realpath(__file__))

    if args.generate != 0:
        #check if the programs exist
        havegmsh = None
        havegmsh = which('gmsh')
        if havegmsh is None:
            print('ERROR: It seems you do not have gmsh in your system path. Download it from http://gmsh.info/#Download')
            sys.exit(-1)
        havemeshtool = None
        havemeshtool = which('meshtool')
        if havemeshtool is None:
            print('ERROR: It seems you do not have meshtool in your system path.')
            sys.exit(-1)
        ir = float(args.radius)
        il = float(args.length)
        ih = float(args.radiusfactor) * ir
        print(('GENERATING cylindrical mesh with gmsh/meshtool with r={:2.4f} [m] l={:2.4f} [m] hmax={:2.4f} [m] ...').format(ir, il, ih))
        f = open(working_dir + "/meshing.log", "w")
        mesh_working_dir = os.path.join(working_dir, 'mesh')
        os.chdir(mesh_working_dir)
        call(shlex.split("bash generate_mesh.sh" + " " + str(args.length) + " " + str(args.radius) + " " +
                         str(args.radiusfactor)),cwd=mesh_working_dir,stdout=f)
        f.close()
        os.chdir(working_dir)
        meshdir = os.path.join(working_dir, 'mesh', 'custom','HagenPouseille')
        print(' done!')
    else:
        meshdir = os.path.join(working_dir, 'mesh', 'standard', 'HagenPouseille')

    meshinfo    = mesh_from_parser(args, meshdir, args.pressure_drop)

    cmd         = [settings.execs.FLUIDSOLVE]

    # general stuff
    cmd         += ['-fluid_meshname',         meshinfo['meshname'],
                    '-fluid_meshformat',       2,
                    '-fluid_meshscaling',      3,
                    '-fluid_output_dir',       job.ID,
                    '-fluid_use_pt',           0,
                    '-fluid_options_file',     petsc_block_options.path('fgmres_fieldsplit_navier_stokes_boomeramg'),
                    '-flowDT',                 float(args.dt),
                    '-flowOutputDT',           float(args.dt),
                    '-fluid_generalized_alpha_rho_inf', 0.2,
                    '-fluid_gauge_pressure', args.pressure_offset,
                    '-Tend',                   1000]

    # experiment definition
    if args.implicit == 1 :
       cmd += ['-experiment', 6,
               '-newton_adaptive_tol_fluid', 1,
               '-newton_precalculate_stokes_solution', 0,
               '-newton_freeze_jacobian_assembly', 999,
               '-newton_line_search_fluid', 4]
    else :
       cmd += ['-experiment', 4]

    cmd += ['-fluid_krylov_rtol', 1e-10,
            '-fluid_krylov_atol', 0.0]
    # material properties
    cmd += ['-flow_viscosity',         args.mu,
            '-flow_density',           args.rho]

    #add boundary info
    cmd += meshinfo['dbc']
    cmd += meshinfo['nbc']

    #obstacle
    if args.with_obstacle == 1 :
       cmd += ['-fluid_obstacle_adapt_viscosity', 0,
              '-num_obstacles', 2,
              '-flow_Obstacles[0].name', r'"Sphere1"',
              '-flow_Obstacles[0].input_element_vector', os.path.join(meshinfo['meshdir'], 'obstacle_sphere_1.fracs'),
              '-flow_Obstacles[0].kappa', 1e-8,
              '-flow_Obstacles[0].t0', 10.0,
              '-flow_Obstacles[0].t2', 14.0,
              '-flow_Obstacles[0].t3', 45.0,
              '-flow_Obstacles[0].t1', 50.0,
              '-flow_Obstacles[0].period', 200.0,
              '-flow_Obstacles[1].name', r'"Sphere2"',
              '-flow_Obstacles[1].input_element_vector', os.path.join(meshinfo['meshdir'], 'obstacle_sphere_2.fracs'),
              '-flow_Obstacles[1].kappa', 1e-8,
              '-flow_Obstacles[1].t0', 20.0,
              '-flow_Obstacles[1].t2', 24.0,
              '-flow_Obstacles[1].t3', 65.0,
              '-flow_Obstacles[1].t1', 70.0,
              '-flow_Obstacles[1].switch_invert', 1,
              '-flow_Obstacles[1].period', 200.0]

    # test checkpointing
    job.mpi(cmd)

    #postprocessing
    cmd += ['-experiment',  8,
            '-flow_output', 2,
            '-flow_vtk_output_mode', 3,
            '-vorticity_value', 1,
            '-turbulence_criterion_value', 4+16,
            '-flow_surface_postproc',  1
            ]
    job.mpi(cmd)

    #postprocessing
    #if not args.dry:
    #    outfp = open(os.path.join(job.ID, 'pouseille.out'), 'w')
    #    write(EQUALS_LINE, outfp)
    #    write(' EXPERIMENT SUMMARY',outfp)
    #    write('Radius            (r)   : {:2.4f} [cm]'.format(float(args.radius)),outfp)
    #    write('Length            (l)   : {:2.4f} [m]'.format(float(args.length)),outfp)
    #    write('Discretization          : ' + args.discretization ,outfp)
    #    write('Density           (rho) : {:1.4e} [kg/m3]'.format(float(args.rho)),outfp)
    #    write('Viscosity         (mu)  : {:1.4e} [Pa s]'.format(float(args.mu)),outfp)
    #    write('Pressure Inflow   (P0)  : {:2.4e} [Pa]'.format(1.5*float(args.pressureDrop)),outfp)
    #    write('Pressure Outflow  (P1)  : {:2.4e} [Pa]'.format(0.5*float(args.pressureDrop)),outfp)
    #    write(EQUALS_LINE,outfp)
    #    write(' LAW OF HAGEN-POUSEILLE',outfp)
    #    write(DASHED_LINE, outfp)
    #    write('        dp * Pi * Diam^4', outfp)
    #    write('Q = -------------------------',outfp)
    #    write('       128 * mu * Length', outfp)
    #    write(DASHED_LINE, outfp)
    #    #read the report file
    #    filePInflow = os.path.join(job.ID,'LOGNBCs','inflow_avgp.dat')
    #    fileQInflow = os.path.join(job.ID,'LOGNBCs','inflow_flux.dat')
    #    filePOutflow = os.path.join(job.ID,'LOGNBCs','outflow_avgp.dat')
    #    fileQOutflow = os.path.join(job.ID,'LOGNBCs','outflow_flux.dat')
    #    f1 = open(filePInflow, 'r')
    #    f2 = open(fileQInflow, 'r')
    #    f3 = open(filePOutflow, 'r')
    #    f4 = open(fileQOutflow, 'r')

    #    Qnumeric_inflow  = 0
    #    Qnumeric_outflow = 0
    #    Pnumeric_inflow  = 0
    #    Pnumeric_outflow = 0

    #    for line in f1:
    #       line = line.strip()
    #       columns = line.split()
    #       Pnumeric_inflow = float(columns[1])
    #    f1.close()

    #    for line in f2:
    #       line = line.strip()
    #       columns = line.split()
    #       Qnumeric_inflow = float(columns[1])
    #    f2.close()

    #    for line in f3:
    #       line = line.strip()
    #       columns = line.split()
    #       Pnumeric_outflow = float(columns[1])
    #    f3.close()

    #    for line in f4:
    #       line = line.strip()
    #       columns = line.split()
    #       Qnumeric_outflow = float(columns[1])
    #    f4.close()
    #    radiusinmeter    = float(args.radius) * 1e-2
    #    area             = radiusinmeter ** 2 * math.pi
    #    QHP              = flux_hagen_pouseille(float(args.pressureDrop), float(radiusinmeter), float(args.length),
    #                                            float(args.rho), float(args.mu))
    #    relerr           = 100*math.fabs(QHP - Qnumeric_outflow) / (0.5*(math.fabs(QHP)+math.fabs(Qnumeric_outflow)))
    #    R                = reynolds_number(QHP, radiusinmeter, area, float(args.mu), float(args.rho))
    #    write('Flux from simulation                 (QN)  : {:2.4e} [m3/s]'.format(Qnumeric_outflow), outfp)
    #    write('Flux from Hagen-Poiseuille           (QH)  : {:2.4e} [m3/s]'.format(QHP), outfp)
    #    write('Relative error                             : {:2.4f} [%]'.format(relerr), outfp)
    #    write('Pressure at inflow from simulation   (P0N) : {:2.4e} [Pa]'.format(Pnumeric_inflow), outfp)
    #    write('Pressure at outflow from simulation  (P0N) : {:2.4e} [Pa]'.format(Pnumeric_outflow), outfp)
    #    write('Pressure drop                        (dP)  : {:2.4e} [Pa]'.format(float(args.pressureDrop)),outfp)
    #    write('Reynolds Number                      (Re)  : {:3.2f}'.format(R), outfp)
    #    outfp.close()


if __name__ == '__main__':
    run()








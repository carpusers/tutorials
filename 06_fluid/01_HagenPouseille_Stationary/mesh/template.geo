lc      = TEMPRESOLUTION;
length  = TEMPLENGTH;
radius  = TEMPRADIUS;


//+
Point(1) = {0, 0, 0, lc};
//+
Point(2) = {0, radius, 0, lc};
//+
Point(3) = {0, 0, radius, lc};
//+
Point(4) = {0, -radius, 0, lc};
//+
Point(5) = {0, 0, -radius, lc};
//+
Circle(1) = {3, 1, 2};
//+
Circle(2) = {2, 1, 5};
//+
Circle(3) = {5, 1, 4};
//+
Circle(4) = {4, 1, 3};
//+
Extrude {length, 0, 0} {
  Line{2}; Line{1}; Line{4}; Line{3};
}
//+
Line Loop(1) = {2, 3, 4, 1};
//+
Plane Surface(21) = {1};
//+
Line Loop(2) = {9, 5, 17, 13};
//+
Plane Surface(22) = {2};
//+
Line Loop(3) = {6, -9, -10, 1};
//+
Surface(23) = {3};
//+
Line Loop(4) = {10, -13, -14, 4};
//+
Surface(24) = {4};
//+
Line Loop(5) = {14, -17, -7, 3};
//+
Surface(25) = {5};
//+
Line Loop(6) = {7, -5, -6, 2};
//+
Surface(26) = {6};
//+
Surface Loop(1) = {12, 8, 20, 16, 21, 22};
//+
Volume(1) = {1};
Physical Volume(100) = {1};

#!/bin/bash

if [ $# -ne 3 ]
then
  echo "Wrong usage. Use: $0 <LENGTH in meter> <RADIUS in centimeter> <RESOLUTION percentage>"
  exit 1
fi

LENGTHIN=$1
RADIUSIN=$(echo $2*0.01 | bc -l)
RESOLUTIONIN=$(echo $RADIUSIN*$3 | bc -l)

sed -e "s/TEMPRESOLUTION/$RESOLUTIONIN/g" template.geo | sed -e "s/TEMPLENGTH/$LENGTHIN/g" | sed -e "s/TEMPRADIUS/$RADIUSIN/g" > geometry.geo

mkdir -p custom/HagenPouseille

mkdir -p custom/HagenPouseille/{coarse,medium,fine}

gmsh -3 -match -clscale 1.0  -f mesh -o custom/HagenPouseille/coarse/HagenPouseille.mesh geometry.geo
gmsh -3 -match -clscale 0.5  -f mesh -o custom/HagenPouseille/medium/HagenPouseille.mesh geometry.geo
gmsh -3 -match -clscale 0.25 -f mesh -o custom/HagenPouseille/fine/HagenPouseille.mesh  geometry.geo

meshtool convert -imsh=custom/HagenPouseille/coarse/HagenPouseille -ifmt=mmg -ofmt=carp_bin -omsh=custom/HagenPouseille/coarse/HagenPouseille
rm custom/HagenPouseille/coarse/HagenPouseille.mesh
meshtool convert -imsh=custom/HagenPouseille/medium/HagenPouseille -ifmt=mmg -ofmt=carp_bin -omsh=custom/HagenPouseille/medium/HagenPouseille
rm custom/HagenPouseille/medium/HagenPouseille.mesh
meshtool convert -imsh=custom/HagenPouseille/fine/HagenPouseille -ifmt=mmg -ofmt=carp_bin -omsh=custom/HagenPouseille/fine/HagenPouseille
rm custom/HagenPouseille/fine/HagenPouseille.mesh


#query the idx
TEMP=$(echo $LENGTHIN+0.1 | bc -l)
TEMP2=$(echo $LENGTHIN*0.5 | bc -l)
TEMP3=$(echo $RADIUSIN+0.1 | bc -l)

#extract the surfaces
meshtool extract surface -msh=custom/HagenPouseille/coarse/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/coarse/HagenPouseille.inflow -ofmt=vtk_bin -edge=30.0 -coord=-0.1,0,0
meshtool extract surface -msh=custom/HagenPouseille/coarse/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/coarse/HagenPouseille.outflow -ofmt=vtk_bin -edge=30.0 -coord=$TEMP,0,0
meshtool extract surface -msh=custom/HagenPouseille/coarse/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/coarse/HagenPouseille.wall -ofmt=vtk_bin -edge=30.0 -coord=$TEMP2,$TEMP3,0

#extract the surfaces
meshtool extract surface -msh=custom/HagenPouseille/medium/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/medium/HagenPouseille.inflow -ofmt=vtk_bin -edge=30.0 -coord=-0.1,0,0
meshtool extract surface -msh=custom/HagenPouseille/medium/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/medium/HagenPouseille.outflow -ofmt=vtk_bin -edge=30.0 -coord=$TEMP,0,0
meshtool extract surface -msh=custom/HagenPouseille/medium/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/medium/HagenPouseille.wall -ofmt=vtk_bin -edge=30.0 -coord=$TEMP2,$TEMP3,0

#extract the surfaces
meshtool extract surface -msh=custom/HagenPouseille/fine/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/fine/HagenPouseille.inflow -ofmt=vtk_bin -edge=30.0 -coord=-0.1,0,0
meshtool extract surface -msh=custom/HagenPouseille/fine/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/fine/HagenPouseille.outflow -ofmt=vtk_bin -edge=30.0 -coord=$TEMP,0,0
meshtool extract surface -msh=custom/HagenPouseille/fine/HagenPouseille -ifmt=carp_bin -surf=custom/HagenPouseille/fine/HagenPouseille.wall -ofmt=vtk_bin -edge=30.0  -coord=$TEMP2,$TEMP3,0




